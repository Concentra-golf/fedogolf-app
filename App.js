import * as React from 'react';
import { SafeAreaView } from 'react-native';

// Modules

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { connect } from 'react-redux';
import OneSignal from 'react-native-onesignal';
import { Root } from "native-base";

// Stacks

import { DrawerStack } from './app/views/Stacks/DrawerStack';

// Views

import UserAccess from './app/views/UserAccess/UserAccess';
import { setNavigator } from './app/utilities/NavigationService';
import OptionsSelector from './app/views/Settings/Membership/Renew/OptionsSelector';
import FederationSignUp from './app/views/Settings/Membership/FederationSignUp';

const MainStack = createStackNavigator();


class App extends React.Component {
  constructor(properties) {
    super(properties);
    //Remove this method to stop OneSignal Debugging 
    OneSignal.setLogLevel(6, 0);

    // Replace 'YOUR_ONESIGNAL_APP_ID' with your OneSignal App ID.
    OneSignal.init("14a772d6-94e2-49d9-9eb6-b0921fe616bb", { kOSSettingsKeyAutoPrompt: false, kOSSettingsKeyInAppLaunchURL: false, kOSSettingsKeyInFocusDisplayOption: 2 });
    OneSignal.inFocusDisplaying(2); // Controls what should happen if a notification is received while the app is open. 2 means that the notification will go directly to the device's notification center.

    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  onReceived(notification) {
    console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

  onIds(device) {
    console.log('Device info: ', device);
  }

  componentDidMount() {
    setNavigator(this.navigator)
  }

  render() {
    const { session } = this.props;
    return (
      <Root>
        <NavigationContainer ref={nav => { this.navigator = nav; }}>
          <MainStack.Navigator initialRouteName={session.user.isLoggedIn ? "Tabs" : "UserAccess"}>
            {/* <MainStack.Navigator headerMode="none" initialRouteName={"UserAccess"}> */}
            <MainStack.Screen name="UserAccess" component={UserAccess} options={{ headerShown: false }} />
            <MainStack.Screen name="Tabs" component={DrawerStack} options={{ headerShown: false }} />
            <MainStack.Screen name="FederationSignUp" component={FederationSignUp} options={{ headerShown: true, headerTitle: '', headerTintColor: 'black', }} />
            <MainStack.Screen name="OptionsSelector" component={OptionsSelector} options={{ headerShown: true, headerTitle: '', headerTintColor: 'black' }} />

          </MainStack.Navigator>
          <SafeAreaView style={{ backgroundColor: 'black' }} />
        </NavigationContainer>
      </Root>
    );
  }
}

function mapStateToProps(state) {
  // state.session.user.isLoggedIn = true
  return {
    session: state.session,
  };
}

console.disableYellowBox = true;

export default connect(mapStateToProps)(App);
