import 'react-native-gesture-handler';
import * as React from 'react';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

// Redux

import {PersistGate} from 'redux-persist/integration/react';
import {Provider} from 'react-redux';

// Store

import {store, persistor} from './app/store/store';

const AppWithStore = () => (
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <App />
        </PersistGate>
    </Provider>
)

AppRegistry.registerComponent(appName, () => AppWithStore);
