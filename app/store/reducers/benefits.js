/* eslint-disable prettier/prettier */
import * as ActionTypes from '../actions/types';

// Setup the initial state

const initialState = {
  benefits: {},
  benefit: {},
  allbenefits: {}
};

function benefits(state = initialState, action) {
  switch (action.type) {

    // Get all the benefits 

    case ActionTypes.GET_BENEFITS_INFORMATION_REQUEST:
      return {
        ...state,
        allbenefits: {
          isLoading: true,
          error: null,
          success: false
        },
      };

    case ActionTypes.GET_BENEFITS_INFORMATION_SUCCESS:
      return {
        ...state,
        allbenefits: {
          data: action.payload,
          isLoading: false,
          error: null,
          success: true
        },
      };

    case ActionTypes.GET_BENEFITS_INFORMATION_FAILURE:
      return {
        ...state,
        allbenefits: {
          isLoading: false,
          error: action.payload,
          success: false
        },
      };


    // Get all the benefits by federation

    case ActionTypes.FEDERATION_BENEFITS_REQUEST:
      return {
        ...state,
        benefits: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.FEDERATION_BENEFITS_SUCCESS:
      return {
        ...state,
        benefits: {
          data: action.payload,
          isLoading: false,
          error: null,
        },
      };

    case ActionTypes.FEDERATION_BENEFITS_FAILURE:
      return {
        ...state,
        benefits: {
          isLoading: false,
          error: action.payload,
        },
      };

    default:
      return state;
  }
}

export default benefits;
