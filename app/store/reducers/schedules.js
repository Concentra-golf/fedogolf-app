/* eslint-disable prettier/prettier */
import * as ActionTypes from '../actions/types';

// Setup the initial state

const initialState = {
  schedules: {},
};

function schedules(state = initialState, action) {
  switch (action.type) {

    // Get Schedules

    case ActionTypes.GET_SCHEDULES_REQUEST:
      return {
        ...state,
        schedules: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.GET_SCHEDULES_REQUEST:
      return {
        ...state,
        schedules: {
          data: action.payload,
          isLoading: false,
          error: null,
        },
      };

    case ActionTypes.GET_SCHEDULES_REQUEST:
      return {
        ...state,
        schedules: {
          isLoading: false,
          error: action.payload,
        },
      };

    default:
      return state;
  }
}

export default schedules;
