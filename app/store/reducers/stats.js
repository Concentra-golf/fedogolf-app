/* eslint-disable prettier/prettier */
import * as ActionTypes from '../actions/types';

// Setup the initial state

const initialState = {
  stats: {},
};

function stats(state = initialState, action) {
  switch (action.type) {

    // Get Stats

    case ActionTypes.GET_STATS_REQUEST:
      return {
        ...state,
        stats: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.GET_STATS_SUCCESS:
      return {
        ...state,
        stats: {
          data: action.payload,
          isLoading: false,
          error: null,
        },
      };

    case ActionTypes.GET_STATS_FAILURE:
      return {
        ...state,
        stats: {
          isLoading: false,
          error: action.payload,
        },
      };

    default:
      return state;
  }
}

export default stats;
