/* eslint-disable prettier/prettier */
import * as ActionTypes from '../actions/types';

// Setup the initial state

const initialState = {
  courses: {},
  course: {},
  allCourses: {}
};

function courses(state = initialState, action) {
  switch (action.type) {

    // Get courses by Location

    case ActionTypes.GET_COURSES_REQUEST:
      return {
        ...state,
        courses: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.GET_COURSES_SUCCESS:
      return {
        ...state,
        courses: {
          data: action.payload.data,
          isLoading: false,
          error: null,
          success: true
        },
      };

    case ActionTypes.GET_COURSES_FAILURE:
      return {
        ...state,
        courses: {
          isLoading: false,
          error: action.payload,
          success: false
        },
      };

    // Get courses by Location

    case ActionTypes.GET_ALL_COURSES_REQUEST:
      return {
        ...state,
        allCourses: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.GET_ALL_COURSES_SUCCESS:
      return {
        ...state,
        allCourses: {
          data: action.payload.data,
          isLoading: false,
          error: null,
          success: true
        },
      };

    case ActionTypes.GET_ALL_COURSES_FAILURE:
      return {
        ...state,
        allCourses: {
          isLoading: false,
          error: action.payload,
          success: false
        },
      };

    // GET COURSES Details
    case ActionTypes.GET_COURSE_DETAIL_REQUEST:
      return {
        ...state,
        coursesDetails: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.GET_COURSE_DETAIL_SUCCESS:
      return {
        ...state,
        coursesDetails: {
          data: action.payload.data,
          isLoading: false,
          error: null,
          success: true
        },
      };

    case ActionTypes.GET_COURSE_DETAIL_FAILURE:
      return {
        ...state,
        coursesDetails: {
          isLoading: false,
          error: action.payload,
        },
      };

    // GET COURSES RATING
    case ActionTypes.COURSES_RATING_REQUEST:
      return {
        ...state,
        coursesRating: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.COURSES_RATING_SUCCESS:
      return {
        ...state,
        coursesRating: {
          data: action.payload.data,
          isLoading: false,
          error: null,
          success: true
        },
      };

    case ActionTypes.COURSES_RATING_FAILURE:
      return {
        ...state,
        coursesRating: {
          isLoading: false,
          error: action.payload,
          success: false
        },
      };
    // GET COUNTRY
    case ActionTypes.COUNTRY_REQUEST:
      return {
        ...state,
        country: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.COUNTRY_SUCCESS:
      return {
        ...state,
        country: {
          data: action.payload.data,
          isLoading: false,
          error: null,
          success: true
        },
      };

    case ActionTypes.COUNTRY_FAILURE:
      return {
        ...state,
        country: {
          isLoading: false,
          error: action.payload,
          success: false
        },
      };
    // filters by country
    case ActionTypes.FILTER_BY_COUNTRY_REQUEST:
      return {
        ...state,
        filterCountry: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.FILTER_BY_COUNTRY_SUCCESS:
      return {
        ...state,
        filterCountry: {
          data: action.payload.data,
          isLoading: false,
          error: null,
          success: true
        },
      };

    case ActionTypes.FILTER_BY_COUNTRY_FAILURE:
      return {
        ...state,
        filterCountry: {
          isLoading: false,
          error: action.payload,
          success: false
        },
      };
    // filters 
    case ActionTypes.FILTER_REQUEST:
      return {
        ...state,
        filter: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.FILTER_SUCCESS:
      return {
        ...state,
        filter: {
          data: action.payload.data,
          isLoading: false,
          error: null,
          success: true
        },
      };

    case ActionTypes.FILTER_FAILURE:
      return {
        ...state,
        filter: {
          isLoading: false,
          error: action.payload,
          success: false
        },
      };
    default:
      return state;
  }
}

export default courses;
