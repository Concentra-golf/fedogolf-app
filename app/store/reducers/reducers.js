import {combineReducers} from 'redux';

import session from './session';
import courses from './courses';
import schedules from './schedules';
import settings from './settings';
import stats from './stats';
import benefits from './benefits';
import membership from './membership';
import payment from './payment';
import playgolf from './playgolf';

const rootReducer = combineReducers({
  session,
  courses,
  schedules,
  settings,
  stats,
  membership,
  benefits,
  payment,
  playgolf
});

export default rootReducer;
