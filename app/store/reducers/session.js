/* eslint-disable prettier/prettier */
import * as ActionTypes from '../actions/types';

// Setup the initial state

const initialState = {
  user: {},
  passwordRecovery: {},
  fedogolfUser: {},
  isValidated: false
};

function session(state = initialState, action) {
  switch (action.type) {

    // Sign up a new user

    case ActionTypes.USER_SIGN_UP_REQUEST:
      return {
        ...state,
        user: {
          isLoading: true,
          error: null,
        },
        accessToken: ''
      };

    case ActionTypes.USER_SIGN_UP_SUCCESS:
      return {
        ...state,
        user: {
          accessToken: action.payload.access_token,
          data: action.payload.user,
          isLoading: false,
          isCreated: true,
          isLoggedIn: true,
          error: null,
        },
        accessToken: action.payload.access_token
      };

    case ActionTypes.USER_SIGN_UP_FAILURE:
      return {
        ...state,
        user: {
          isLoading: false,
          error: action.payload,
        },
        accessToken: null
      };

    // Fedogolf sign up

    case ActionTypes.FEDOGOLF_SIGN_UP_REQUEST:
      return {
        ...state,
        fedogolfUser: {
          isLoading: true,
          error: null,
          success: false
        }
      };

    case ActionTypes.FEDOGOLF_SIGN_UP_SUCCESS:
      return {
        ...state,
        fedogolfUser: {
          data: action.payload,
          isLoading: false,
          isCreated: true,
          isLoggedIn: true,
          error: null,
          success: true
        }
      };

    case ActionTypes.FEDOGOLF_SIGN_UP_FAILURE:
      return {
        ...state,
        fedogolfUser: {
          isLoading: false,
          error: action.payload,
          success: false
        }
      };

    // SIGN UP USER GOLFER INFORMATION

    case ActionTypes.USER_SIGN_UP_GOLFER_INFORMATION_REQUEST:
      return {
        ...state,
        user: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.USER_SIGN_UP_GOLFER_INFORMATION_SUCCESS:
      return {
        ...state,
        user: {
          data: action.payload,
          isLoading: false,
          isCreated: true,
          error: null,
        },
      };

    case ActionTypes.USER_SIGN_UP_GOLFER_INFORMATION_FAILURE:
      return {
        ...state,
        user: {
          isLoading: false,
          error: action.payload,
        },
        accessToken: null
      };

    //  USER INFORMATION

    case ActionTypes.PUT_PERSONAL_INFORMATION_REQUEST:
      return {
        ...state,
        user: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.PUT_PERSONAL_INFORMATION_SUCCESS:
      return {
        ...state,
        user: {
          data: action.payload,
          isLoading: false,
          isCreated: true,
          error: null,
        },
      };

    case ActionTypes.PUT_PERSONAL_INFORMATION_FAILURE:
      return {
        ...state,
        user: {
          isLoading: false,
          error: action.payload,
        },
      };

    // Sign in the user

    case ActionTypes.USER_SIGN_IN_REQUEST:
      return {
        ...state,
        user: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.USER_SIGN_IN_SUCCESS:
      return {
        ...state,
        user: {
          new_user: action.payload.new_user,
          accessToken: action.payload.access_token,
          data: action.payload.user,
          isLoading: false,
          isLoggedIn: true,
          error: null,
          success: true
        },
        accessToken: action.payload.access_token,
      };

    case ActionTypes.USER_SIGN_IN_FAILURE:
      return {
        ...state,
        user: {
          isLoading: false,
          error: action.payload,
          success: false
        },
      };

    // Reset the user password

    case ActionTypes.FORGOT_PASSWORD_REQUEST:
      return {
        ...state,
        passwordRecovery: {
          isLoading: true,
          error: null
        }
      };

    case ActionTypes.FORGOT_PASSWORD_SUCCESS:
      return {
        ...state,
        passwordRecovery: {
          data: action.payload,
          isLoading: false,
          error: null,
        },
      };

    case ActionTypes.FORGOT_PASSWORD_FAILURE:
      return {
        ...state,
        passwordRecovery: {
          isLoading: false,
          error: action.payload,
        },
      };


    // Clean the session store

    case ActionTypes.CLEAN_SESSION_STORE:
      return {
        ...state,
        user: {},
        accessToken: null
      };

    default:
      return state;


    // FedoGolf in the user

    case ActionTypes.POST_FEDOGOLF_REQUEST:
      return {
        ...state,
        fedoGolf: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.POST_FEDOGOLF_SUCCESS:
      return {
        ...state,
        fedoGolf: {
          data: action.payload,
          isLoading: false,
          error: null,
        },
        accessToken: action.payload.access_token,
      };

    case ActionTypes.POST_FEDOGOLF_FAILURE:
      return {
        ...state,
        fedogolf_user: {
          isLoading: false,
          error: action.payload,
        },
      };


    // FedoGolf Validate

    case ActionTypes.FEDOGOLF_USER_VERIFY_REQUEST:
      return {
        ...state,
        fedogolfUser: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.FEDOGOLF_USER_VERIFY_SUCCESS:
      return {
        ...state,
        fedogolfUser: {
          isLoading: false,
          success: true,
          isValidated: action.payload
        },
        isValidated: action.payload,
      };

    case ActionTypes.FEDOGOLF_USER_VERIFY_FAILURE:
      return {
        ...state,
        fedogolfUser: {
          isLoading: false,
          success: false,
          error: action.payload,
        },
      };

    // Refresh Token

    case ActionTypes.USER_REFRESH_TOKEN_REQUEST:
      return {
        ...state,
        user: {
          isLoading: true,
          error: null,
          success: false
        },
      };

    case ActionTypes.USER_REFRESH_TOKEN_SUCCESS:
      return {
        ...state,
        user: {
          accessToken: action.payload.access_token,
          data: action.payload.user,
          isLoading: false,
          isCreated: true,
          isLoggedIn: true,
          error: null,
          success: true,
          tokenRefreshed: true
        },
        accessToken: action.payload.access_token
      };

    case ActionTypes.USER_REFRESH_TOKEN_FAILURE:
      return {
        ...state,
        user: {
          isLoading: false,
          error: action.payload,
          success: false,
          tokenRefreshed: false
        },
        accessToken: ''
      };

    // FEDERATION REQUEST

    case ActionTypes.FEDERATION_REQUEST_REQUEST:
      return {
        ...state,
        membershipsList: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.FEDERATION_REQUEST_SUCCESS:
      return {
        ...state,
        membershipsList: {
          data: action.payload.data,
          error: null,
          isLoading: false,
          success: true
        },
      };

    case ActionTypes.FEDERATION_REQUEST_FAILURE:
      return {
        ...state,
        membershipsList: {
          isLoading: false,
          error: action.payload,
        },
      };

    // FEDERATION REQUESTS BY ID

    case ActionTypes.FEDERATION_REQUEST_BY_ID_REQUEST:
      return {
        ...state,
        membershipsItem: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.FEDERATION_REQUEST_BY_ID_SUCCESS:
      return {
        ...state,
        membershipsItem: {
          data: action.payload,
          error: null,
          isLoading: false,
          success: true
        },
      };

    case ActionTypes.FEDERATION_REQUEST_BY_ID_FAILURE:
      return {
        ...state,
        membershipsItem: {
          isLoading: false,
          error: action.payload,
        },
      };

    // FEDERATION REQUEST UPLOAD DOCUMENT

    case ActionTypes.FEDERATION_DOCUMENTS_REQUEST:
      return {
        ...state,
        docUpload: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.FEDERATION_DOCUMENTS_SUCCESS:
      return {
        ...state,
        docUpload: {
          data: action.payload,
          error: null,
          isLoading: false,
          success: true
        },
      };

    case ActionTypes.FEDERATION_DOCUMENTS_FAILURE:
      return {
        ...state,
        docUpload: {
          isLoading: false,
          error: action.payload,
        },
      };

    // MEMBERSHIPS TYPES REQUEST

    case ActionTypes.MEMBERSHIPS_TYPES_REQUEST:
      return {
        ...state,
        membershipTypes: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.MEMBERSHIPS_TYPES_SUCCESS:
      return {
        ...state,
        membershipTypes: {
          data: action.payload,
          error: null,
          isLoading: false,
          success: true
        },
      };

    case ActionTypes.MEMBERSHIPS_TYPES_FAILURE:
      return {
        ...state,
        membershipTypes: {
          isLoading: false,
          error: action.payload,
        },
      };
  }
}

export default session;
