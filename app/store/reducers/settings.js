/* eslint-disable prettier/prettier */
import * as ActionTypes from '../actions/types';

// Setup the initial state

const initialState = {
  settings: {},
};

function settings(state = initialState, action) {
  switch (action.type) {

    // Get Settings

    case ActionTypes.GET_SETTINGS_REQUEST:
      return {
        ...state,
        settings: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.GET_SETTINGS_SUCCESS:
      return {
        ...state,
        settings: {
          data: action.payload,
          isLoading: false,
          error: null,
        },
      };

    case ActionTypes.GET_SETTINGS_FAILURE:
      return {
        ...state,
        settings: {
          isLoading: false,
          error: action.payload,
        },
      };

    default:
      return state;
  }
}

export default settings;
