/* eslint-disable prettier/prettier */
import * as ActionTypes from '../actions/types';

// Setup the initial state

const initialState = {
    receiptPDF: {},
    renewPaymentResponse: {},
    simulatePaymentResponse: {},
    isPaymentSuccess: {
        isLoading: false,
        success: false
    },
    userPaymentCards: {},
    recurrentMembership: {},
    newRecurrentMembership: {},
    updateRecurrentMembership: {}
};

function payment(state = initialState, action) {
    switch (action.type) {

        // RENEW PAYMENT REQUEST

        case ActionTypes.RENEW_PAYMENT_REQUEST:
            return {
                ...state,
                renewPaymentResponse: {
                    isLoading: true,
                    error: null,
                },
            };

        case ActionTypes.RENEW_PAYMENT_SUCCESS:
            return {
                ...state,
                renewPaymentResponse: {
                    data: action.payload.data,
                    isLoading: false,
                    success: true,
                    error: null,
                },
            };

        case ActionTypes.RENEW_PAYMENT_FAILURE:
            return {
                ...state,
                renewPaymentResponse: {
                    isLoading: false,
                    error: action,
                    success: false,
                },
            };

        //SIMULATE PAYMENT 
        case ActionTypes.SIMULATE_PAYMENT_REQUEST:
            return {
                ...state,
                simulatePaymentResponse: {
                    isLoading: true,
                    error: null,
                },
            };

        case ActionTypes.SIMULATE_PAYMENT_SUCCESS:
            return {
                ...state,
                simulatePaymentResponse: {
                    data: action.payload,
                    isLoading: false,
                    success: true,
                    error: null,
                },
            };

        case ActionTypes.SIMULATE_PAYMENT_FAILURE:
            return {
                ...state,
                simulatePaymentResponse: {
                    isLoading: false,
                    error: action,
                    success: false,
                },
            };
        // IDENTYFY PAYMENT SUCCESS
        case ActionTypes.PAYMENT_REQUEST:
            return {
                ...state,
                isPaymentSuccess: {
                    isLoading: true,
                    success: false,
                    error: false
                },
            };
        case ActionTypes.PAYMENT_SUCCESS:
            return {
                ...state,
                isPaymentSuccess: {
                    success: action.payload,
                    isLoading: false,
                    error: false
                },
            };

        case ActionTypes.PAYMENT_FAILURE:
            return {
                ...state,
                isPaymentSuccess: {
                    success: action.payload,
                    isLoading: false,
                    error: true
                },
            };
        // RECEIPT LIST
        case ActionTypes.RECEIPT_LIST_REQUEST:
            return {
                ...state,
                receiptList: {
                    isLoading: true,
                    success: false,
                },
            };
        case ActionTypes.RECEIPT_LIST_SUCCESS:
            return {
                ...state,
                receiptList: {
                    data: action.payload.data,
                    success: true,
                    isLoading: false,
                },
            };

        case ActionTypes.RECEIPT_LIST_FAILURE:
            return {
                ...state,
                receiptList: {
                    success: false,
                    isLoading: false,
                    error: action
                },
            };
        // RECEIPT LIST
        case ActionTypes.RECEIPT_PDF_REQUEST:
            return {
                ...state,
                receiptPDF: {
                    isLoading: true,
                    success: false,
                },
            };
        case ActionTypes.RECEIPT_PDF_SUCCESS:
            return {
                ...state,
                receiptPDF: {
                    data: action.payload,
                    success: true,
                    isLoading: false,
                },
            };

        case ActionTypes.RECEIPT_PDF_FAILURE:
            return {
                ...state,
                receiptPDF: {
                    success: false,
                    isLoading: false,
                    error: action
                },
            };
        // GET USER CREDIT CARDS
        case ActionTypes.USER_PAYMENT_REQUEST:
            return {
                ...state,
                userPaymentCards: {
                    isLoading: true,
                    success: false,
                },
            };
        case ActionTypes.USER_PAYMENT_SUCCESS:
            return {
                ...state,
                userPaymentCards: {
                    data: action.payload,
                    success: true,
                    isLoading: false,
                },
            };

        case ActionTypes.USER_PAYMENT_FAILURE:
            return {
                ...state,
                userPaymentCards: {
                    success: false,
                    isLoading: false,
                    error: action
                },
            };

        // SAVE USER CREDIT CARDS
        case ActionTypes.SAVE_PAYMENT_REQUEST:
            return {
                ...state,
                saveUserCards: {
                    isLoading: true,
                    success: false,
                },
            };
        case ActionTypes.SAVE_PAYMENT_SUCCESS:
            return {
                ...state,
                saveUserCards: {
                    data: action.payload,
                    success: true,
                    isLoading: false,
                },
            };

        case ActionTypes.SAVE_PAYMENT_FAILURE:
            return {
                ...state,
                saveUserCards: {
                    success: false,
                    isLoading: false,
                    error: action
                },
            };

        // DELETE USER CREDIT CARDS
        case ActionTypes.DELETE_CARD_REQUEST:
            return {
                ...state,
                deletedUserPayment: {
                    isLoading: true,
                    success: false,
                },
            };
        case ActionTypes.DELETE_CARD_SUCCESS:
            return {
                ...state,
                deletedUserPayment: {
                    data: action.payload,
                    success: true,
                    isLoading: false,
                },
            };

        case ActionTypes.DELETE_CARD_FAILURE:
            return {
                ...state,
                deletedUserPayment: {
                    success: false,
                    isLoading: false,
                    error: action
                },
            };
        // RECURRENT PAYMENT By Membership 
        case ActionTypes.RECURRENT_MEMBERSHIP_REQUEST:
            return {
                ...state,
                recurrentMembership: {
                    isLoading: true,
                    success: false,
                },
            };
        case ActionTypes.RECURRENT_MEMBERSHIP_SUCCESS:
            return {
                ...state,
                recurrentMembership: {
                    data: action.payload.data,
                    success: true,
                    isLoading: false,
                },
            };

        case ActionTypes.RECURRENT_MEMBERSHIP_FAILURE:
            return {
                ...state,
                recurrentMembership: {
                    success: false,
                    isLoading: false,
                    error: action
                },
            };
        //  Set non RECURRENT PAYMENT MEMBERSHIPS RECURRENT
        case ActionTypes.NEW_RECURRENT_MEMBERSHIP_REQUEST:
            return {
                ...state,
                newRecurrentMembership: {
                    isLoading: true,
                    success: false,
                },
            };
        case ActionTypes.NEW_RECURRENT_MEMBERSHIP_SUCCESS:
            return {
                ...state,
                newRecurrentMembership: {
                    data: action.payload.data,
                    success: true,
                    isLoading: false,
                },
            };

        case ActionTypes.NEW_RECURRENT_MEMBERSHIP_FAILURE:
            return {
                ...state,
                newRecurrentMembership: {
                    success: false,
                    isLoading: false,
                    error: action
                },
            };
        //  UPDATE RECURRENT PAYMENT MEMBERSHIPS 
        case ActionTypes.UPDATE_RECURRENT_MEMBERSHIP_REQUEST:
            return {
                ...state,
                updateRecurrentMembership: {
                    isLoading: true,
                    success: false,
                },
            };
        case ActionTypes.UPDATE_RECURRENT_MEMBERSHIP_SUCCESS:
            return {
                ...state,
                updateRecurrentMembership: {
                    data: action.payload.data,
                    success: true,
                    isLoading: false,
                },
            };

        case ActionTypes.UPDATE_RECURRENT_MEMBERSHIP_FAILURE:
            return {
                ...state,
                updateRecurrentMembership: {
                    success: false,
                    isLoading: false,
                    error: action
                },
            };
        //  DELETE RECURRENT PAYMENT MEMBERSHIPS 
        case ActionTypes.DELETE_RECURRENT_MEMBERSHIP_REQUEST:
            return {
                ...state,
                deleteRecurrentMembership: {
                    isLoading: true,
                    success: false,
                },
            };
        case ActionTypes.DELETE_RECURRENT_MEMBERSHIP_SUCCESS:
            return {
                ...state,
                deleteRecurrentMembership: {
                    data: action.payload.data,
                    success: true,
                    isLoading: false,
                },
            };

        case ActionTypes.DELETE_RECURRENT_MEMBERSHIP_FAILURE:
            return {
                ...state,
                deleteRecurrentMembership: {
                    success: false,
                    isLoading: false,
                    error: action
                },
            };
        default:
            return state;
    }
}

export default payment;
