/* eslint-disable prettier/prettier */
import * as ActionTypes from '../actions/types';

// Setup the initial state

const initialState = {
  membership: {},
  membershipfees: {}
};

function membership(state = initialState, action) {
  switch (action.type) {

    // Get all the membership

    case ActionTypes.GET_ALL_FEDERATIONS_REQUEST:
      return {
        ...state,
        membership: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.GET_ALL_FEDERATIONS_SUCCESS:
      return {
        ...state,
        membership: {
          federations: action.payload,
          isLoading: false,
          error: null,
        },
      };

    case ActionTypes.GET_ALL_FEDERATIONS_FAILURE:
      return {
        ...state,
        membership: {
          isLoading: false,
          error: action.payload,
        },
      };
    //MEMEBERSHIPS FEES
    case ActionTypes.MEMBERSHIPS_FEES_REQUEST:
      return {
        ...state,
        membershipfees: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.MEMBERSHIPS_FEES_SUCCESS:
      return {
        ...state,
        membershipfees: {
          membershipDetails: action.payload,
          isLoading: false,
          error: null,
          success: true
        },
      };

    case ActionTypes.MEMBERSHIPS_FEES_FAILURE:
      return {
        ...state,
        membershipfees: {
          isLoading: false,
          error: action.payload,
        },
      };
    // MEMBERSHIPS FEES BY ID
    case ActionTypes.MEMBERSHIPS_FEES_BYID_REQUEST:
      return {
        ...state,
        membershipfeesById: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.MEMBERSHIPS_FEES_BYID_SUCCESS:
      return {
        ...state,
        membershipfeesById: {
          membershipDetails: action.payload,
          isLoading: false,
          error: null,
          success: true
        },
      };

    case ActionTypes.MEMBERSHIPS_FEES_BYID_FAILURE:
      return {
        ...state,
        membershipfeesById: {
          isLoading: false,
          error: action.payload,
        },
      };
    default:
      return state;
  }
}

export default membership;
