/* eslint-disable prettier/prettier */
import * as ActionTypes from '../actions/types';

// Setup the initial state

const initialState = {
  searchPlayByEmail: {},
  postGame: {},
  continueGame: {}
};

function membership(state = initialState, action) {
  switch (action.type) {

    // Get all the membership
    case ActionTypes.SEARCH_PLAYER_BYMAIL_REQUEST:
      return {
        ...state,
        searchPlayByEmail: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.SEARCH_PLAYER_BYMAIL_SUCCESS:
      return {
        ...state,
        searchPlayByEmail: {
          data: action.payload.data,
          isLoading: false,
          error: null,
          success: true
        },
      };

    case ActionTypes.SEARCH_PLAYER_BYMAIL_FAILURE:
      return {
        ...state,
        searchPlayByEmail: {
          isLoading: false,
          error: action.payload,
          success: false
        },
      };
    // Create Golf Game
    case ActionTypes.CREATE_GOLFGAME_REQUEST:
      return {
        ...state,
        golfGame: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.CREATE_GOLFGAME_SUCCESS:
      return {
        ...state,
        golfGame: {
          data: action.payload.data,
          isLoading: false,
          error: null,
          success: true
        },
      };

    case ActionTypes.CREATE_GOLFGAME_FAILURE:
      return {
        ...state,
        golfGame: {
          isLoading: false,
          error: action.payload,
          success: false
        },
      };
    // GET FREQUENLY PLAYERS 
    case ActionTypes.FREQUENLY_PLAYERS_REQUEST:
      return {
        ...state,
        frequenlyPlayers: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.FREQUENLY_PLAYERS_SUCCESS:
      return {
        ...state,
        frequenlyPlayers: {
          data: action.payload.data,
          isLoading: false,
          error: null,
          success: true
        },
      };

    case ActionTypes.FREQUENLY_PLAYERS_FAILURE:
      return {
        ...state,
        frequenlyPlayers: {
          isLoading: false,
          error: action.payload,
          success: false
        },
      };
    // GET OPEN PLAYS
    case ActionTypes.OPEN_PLAYS_REQUEST:
      return {
        ...state,
        getOpenPlays: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.OPEN_PLAYS_SUCCESS:
      return {
        ...state,
        getOpenPlays: {
          data: action.payload.data,
          isLoading: false,
          error: null,
          success: true
        },
      };

    case ActionTypes.OPEN_PLAYS_FAILURE:
      return {
        ...state,
        getOpenPlays: {
          isLoading: false,
          error: action.payload,
          success: false
        },
      };
    // GET OPEN PLAYS BY ID
    case ActionTypes.OPEN_PLAYS_BY_ID_REQUEST:
      return {
        ...state,
        getOpenPlaysById: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.OPEN_PLAYS_BY_ID_SUCCESS:
      return {
        ...state,
        getOpenPlaysById: {
          data: action.payload.data,
          isLoading: false,
          error: null,
          success: true
        },
      };

    case ActionTypes.OPEN_PLAYS_BY_ID_FAILURE:
      return {
        ...state,
        getOpenPlaysById: {
          isLoading: false,
          error: action.payload,
          success: false
        },
      };
    // POST GAME
    case ActionTypes.POST_GAME_REQUEST:
      return {
        ...state,
        postGame: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.POST_GAME_SUCCESS:
      return {
        ...state,
        postGame: {
          data: action.payload.data,
          isLoading: false,
          error: null,
          success: true
        },
      };

    case ActionTypes.POST_GAME_FAILURE:
      return {
        ...state,
        postGame: {
          isLoading: false,
          error: action.payload,
          success: false
        },
      };
    // END GAME
    case ActionTypes.END_GAME_REQUEST:
      return {
        ...state,
        endGame: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.END_GAME_SUCCESS:
      return {
        ...state,
        endGame: {
          data: action.payload.data,
          isLoading: false,
          error: null,
          success: true
        },
      };

    case ActionTypes.END_GAME_FAILURE:
      return {
        ...state,
        endGame: {
          isLoading: false,
          error: action.payload,
          success: false
        },
      };
    //CONTINUE GAME
    case ActionTypes.CONTINUE_GAME_REQUEST:
      return {
        ...state,
        continueGame: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.CONTINUE_GAME_SUCCESS:
      return {
        ...state,
        continueGame: {
          data: action.payload.data,
          isLoading: false,
          error: null,
          success: true
        },
      };

    case ActionTypes.CONTINUE_GAME_FAILURE:
      return {
        ...state,
        continueGame: {
          isLoading: false,
          error: action.payload,
          success: false
        },
      };
    //DELETE GAME
    case ActionTypes.DELETE_OPENGAME_REQUEST:
      return {
        ...state,
        deleteGame: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.DELETE_OPENGAME_SUCCESS:
      return {
        ...state,
        deleteGame: {
          data: action.payload.data,
          isLoading: false,
          error: null,
          success: true
        },
      };

    case ActionTypes.DELETE_OPENGAME_FAILURE:
      return {
        ...state,
        deleteGame: {
          isLoading: false,
          error: action.payload,
          success: false
        },
      };
    //RATING GAME
    case ActionTypes.RATING_GAME_REQUEST:
      return {
        ...state,
        ratingGame: {
          isLoading: true,
          error: null,
        },
      };

    case ActionTypes.RATING_GAME_SUCCESS:
      return {
        ...state,
        ratingGame: {
          data: action.payload.data,
          isLoading: false,
          error: null,
          success: true
        },
      };

    case ActionTypes.RATING_GAME_FAILURE:
      return {
        ...state,
        ratingGame: {
          isLoading: false,
          error: action.payload,
          success: false
        },
      };
    default:
      return state;
  }
}

export default membership;
