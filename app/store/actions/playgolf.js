/* eslint-disable prettier/prettier */
import * as ActionTypes from '../actions/types';

// API

import SessionService from '../../config/services/sessionService'
import FedogolfService from '../../config/services/FedogolfService'

export const createGolfGame = (course_id, data, token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.CREATE_GOLFGAME_REQUEST });
  const { session } = getState();

  try {
    let request = await SessionService.createGolfGame(course_id, data, token)
    dispatch({
      type: ActionTypes.CREATE_GOLFGAME_SUCCESS,
      payload: request,
    })
  } catch (error) {
    dispatch({
      type: ActionTypes.CREATE_GOLFGAME_FAILURE,
      payload: error
    });
  }
};

export const postGame = (data, game_id, competitor_id, token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.POST_GAME_REQUEST });
  const { session } = getState();

  try {
    let request = await SessionService.postGame(data, game_id, competitor_id, token)
    dispatch({
      type: ActionTypes.POST_GAME_SUCCESS,
      payload: request,
    })
  } catch (error) {
    dispatch({
      type: ActionTypes.POST_GAME_FAILURE,
      payload: error
    });
  }
};

export const endGame = (id_game, token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.END_GAME_REQUEST });
  const { session } = getState();

  try {
    let request = await SessionService.endGame(id_game, token)
    dispatch({
      type: ActionTypes.END_GAME_SUCCESS,
      payload: request,
    })
  } catch (error) {
    dispatch({
      type: ActionTypes.END_GAME_FAILURE,
      payload: error
    });
  }
};

export const continueGame = (game_id, token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.CONTINUE_GAME_REQUEST });
  const { session } = getState();

  try {
    let request = await SessionService.continueGame(game_id, token)
    dispatch({
      type: ActionTypes.CONTINUE_GAME_SUCCESS,
      payload: request,
    })
  } catch (error) {
    dispatch({
      type: ActionTypes.CONTINUE_GAME_FAILURE,
      payload: error
    });
  }
};

export const deleteOpenGame = (game_id, token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.DELETE_OPENGAME_REQUEST });
  const { session } = getState();

  try {
    let request = await SessionService.deleteOpenGame(game_id, token)
    dispatch({
      type: ActionTypes.DELETE_OPENGAME_SUCCESS,
      payload: request,
    })
  } catch (error) {
    dispatch({
      type: ActionTypes.DELETE_OPENGAME_FAILURE,
      payload: error
    });
  }
};

export const ratingGame = (data, game_id, token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.RATING_GAME_REQUEST });
  const { session } = getState();

  try {
    let request = await SessionService.ratingGame(data, game_id, token)
    dispatch({
      type: ActionTypes.RATING_GAME_SUCCESS,
      payload: request,
    })
  } catch (error) {
    dispatch({
      type: ActionTypes.RATING_GAME_FAILURE,
      payload: error
    });
  }
};


export const searchPlayerByEmail = (playerMail, token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.SEARCH_PLAYER_BYMAIL_REQUEST });
  const { session } = getState();

  try {
    let request = await SessionService.searchPlayerByEmail(playerMail, token)
    dispatch({
      type: ActionTypes.SEARCH_PLAYER_BYMAIL_SUCCESS,
      payload: request,
    })
  } catch (error) {
    dispatch({
      type: ActionTypes.RENEW_PAYMENT_FAILURE,
      payload: error
    });
  }
};

export const getFrequenlyPlayer = (user_id, token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.FREQUENLY_PLAYERS_REQUEST });
  const { session } = getState();

  try {
    let request = await SessionService.getFrequenlyPlayer(user_id, token)
    dispatch({
      type: ActionTypes.FREQUENLY_PLAYERS_SUCCESS,
      payload: request,
    })
  } catch (error) {
    dispatch({
      type: ActionTypes.FREQUENLY_PLAYERS_FAILURE,
      payload: error
    });
  }
};

export const getOpenPlays = (token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.OPEN_PLAYS_REQUEST });
  const { session } = getState();

  try {
    let request = await SessionService.getOpenPlays(token)
    dispatch({
      type: ActionTypes.OPEN_PLAYS_SUCCESS,
      payload: request,
    })
  } catch (error) {
    dispatch({
      type: ActionTypes.OPEN_PLAYS_FAILURE,
      payload: error
    });
  }
};

export const getOpenPlaysById = (id_game, token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.OPEN_PLAYS_BY_ID_REQUEST });
  const { session } = getState();

  try {
    let request = await SessionService.getOpenPlaysById(id_game, token)
    dispatch({
      type: ActionTypes.OPEN_PLAYS_BY_ID_SUCCESS,
      payload: request,
    })
  } catch (error) {
    dispatch({
      type: ActionTypes.OPEN_PLAYS_BY_ID_FAILURE,
      payload: error
    });
  }
};

