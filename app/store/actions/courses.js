/* eslint-disable prettier/prettier */
import * as ActionTypes from '../actions/types';

// API

import API, { setApiHeaders, requestMethod } from '../../config/api';
import SessionService from '../../config/services/sessionService';

/* Get all the courses =================================================================================================== */

/**
  * @description Gets the courses list
  * @returns A list of the courses
*/

export const getCourses = token => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.GET_ALL_COURSES_REQUEST });
  const { session } = getState();

  try {
    let requestData = await SessionService.getAllCourses(token)

    dispatch({
      type: ActionTypes.GET_ALL_COURSES_SUCCESS,
      payload: requestData,
    });

  } catch (error) {
    dispatch({
      type: ActionTypes.GET_ALL_COURSES_FAILURE,
      payload: 'Error while loading courses, please try again.',
    });
  }
};

/* Get the courses by location =================================================================================================== */

/**
  * @description Gets the courses list
  * @returns A list of the courses
*/

export const getCoursesByLocation = (location, token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.GET_COURSES_REQUEST });
  const { session } = getState();

  try {
    let requestData = await SessionService.getCoursesByLocation(location, token)

    dispatch({
      type: ActionTypes.GET_COURSES_SUCCESS,
      payload: requestData,
    });

  } catch (error) {
    dispatch({
      type: ActionTypes.GET_COURSES_FAILURE,
      payload: error,
      // payload: 'Error while loading courses, please try again.',
    });
  }
};


/* Get the courses Details  =================================================================================================== */

export const getCourseById = (course_id, token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.GET_COURSE_DETAIL_REQUEST });
  const { session } = getState();

  try {
    let requestData = await SessionService.getCourseById(course_id, token)

    dispatch({
      type: ActionTypes.GET_COURSE_DETAIL_SUCCESS,
      payload: requestData,
    });

  } catch (error) {
    dispatch({
      type: ActionTypes.GET_COURSE_DETAIL_FAILURE,
      payload: error
    });
  }
};

/* Get the courses ratings  =================================================================================================== */

export const getCoursesRatings = (course_id, token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.COURSES_RATING_REQUEST });
  const { session } = getState();

  try {
    let requestData = await SessionService.getCourseRatings(course_id, token)

    dispatch({
      type: ActionTypes.COURSES_RATING_SUCCESS,
      payload: requestData,
    });

  } catch (error) {
    dispatch({
      type: ActionTypes.COURSES_RATING_FAILURE,
      payload: 'Error while loading courses, please try again.',
    });
  }
};

/* Get the country filters  =================================================================================================== */

export const getCountry = (token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.COUNTRY_REQUEST });
  const { session } = getState();

  try {
    let requestData = await SessionService.getCountry(token)

    dispatch({
      type: ActionTypes.COUNTRY_SUCCESS,
      payload: requestData,
    });

  } catch (error) {
    dispatch({
      type: ActionTypes.COUNTRY_FAILURE,
      payload: error
    });
  }
};

/* Get the filters by country  =================================================================================================== */

export const getFilterbyCity = (countryID, token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.FILTER_BY_COUNTRY_REQUEST });
  const { session } = getState();

  try {
    let requestData = await SessionService.getFilterbyCity(countryID, token)

    dispatch({
      type: ActionTypes.FILTER_BY_COUNTRY_SUCCESS,
      payload: requestData,
    });

  } catch (error) {
    dispatch({
      type: ActionTypes.FILTER_BY_COUNTRY_FAILURE,
      payload: error
    });
  }
};

/* Get the filters =================================================================================================== */

export const getFilter = (data, token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.FILTER_REQUEST });
  const { session } = getState();

  try {
    let requestData = await SessionService.getFilter(data, token)

    dispatch({
      type: ActionTypes.FILTER_SUCCESS,
      payload: requestData,
    });

  } catch (error) {
    dispatch({
      type: ActionTypes.FILTER_FAILURE,
      payload: error
    });
  }
};