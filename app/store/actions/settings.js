/* eslint-disable prettier/prettier */
import * as ActionTypes from '../actions/types';

// API

import API, {setApiHeaders, requestMethod} from '../../config/api';

/* Get all the settings =================================================================================================== */

/**
  * @description Gets the settings list
  * @returns A list of the settings
*/

export const getSettings = data => async (dispatch, getState) => {

  dispatch({type: ActionTypes.GET_SETTINGS_REQUEST});

  const { session } = getState();

  try {

    let request = await fetch(API.settings.getSettings, { ...setApiHeaders(requestMethod.get, session.token.data.access_token) });

    let requestData = await request.json();

    if (request.status === 200 || request.status === 201) {

      dispatch({
        type: ActionTypes.GET_SETTINGS_SUCCESS,
        payload: requestData,
      });

    } else {

      dispatch({
        type: ActionTypes.GET_SETTINGS_FAILURE,
        payload: requestData.message,
      });

    }

  } catch (error) {

    dispatch({
      type: ActionTypes.GET_SETTINGS_FAILURE,
      payload: 'Error while loading settings, please try again.',
    });

  }

};
