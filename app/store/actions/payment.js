/* eslint-disable prettier/prettier */
import * as ActionTypes from '../actions/types';

// API

import SessionService from '../../config/services/sessionService'
import FedogolfService from '../../config/services/FedogolfService'


export const renewPaymentRequest = (userID, membershipId, flow_type, request_payment_id, token) => async (dispatch, getState) => {

  dispatch({ type: ActionTypes.RENEW_PAYMENT_REQUEST });
  const { session } = getState();

  try {
    let request = await SessionService.renewPaymentRequest(userID, membershipId, flow_type, request_payment_id, token)
    dispatch({
      type: ActionTypes.RENEW_PAYMENT_SUCCESS,
      payload: request,
    })
  } catch (error) {
    if (error.response.status === 422) {
      dispatch({
        type: ActionTypes.RENEW_PAYMENT_FAILURE,
        payload: 'Email is already registered'
      })
    } else {
      dispatch({
        type: ActionTypes.RENEW_PAYMENT_FAILURE,
        payload: error
      });
    }
  }
};

export const simulatePayment = (requestPayment_id, application_id, paymentInfo, token) => async (dispatch, getState) => {

  dispatch({ type: ActionTypes.SIMULATE_PAYMENT_REQUEST });
  const { session } = getState();

  try {
    let request = await SessionService.simulatePayment(requestPayment_id, application_id, paymentInfo, token)
    dispatch({
      type: ActionTypes.SIMULATE_PAYMENT_SUCCESS,
      payload: request,
    })
  } catch (error) {
    if (error.response.status === 422) {
      dispatch({
        type: ActionTypes.SIMULATE_PAYMENT_FAILURE,
        payload: 'Email is already registered'
      })
    } else {
      dispatch({
        type: ActionTypes.SIMULATE_PAYMENT_FAILURE,
        payload: error
      });
    }
  }
};

// SET SUCCESS MODAL WHEN PAYMENT

export const paymentSuccess = (isSuccess) => async (dispatch, getState) => {

  dispatch({ type: ActionTypes.PAYMENT_REQUEST });

  if (isSuccess) {
    dispatch({
      type: ActionTypes.PAYMENT_SUCCESS,
      payload: true,
    })
  } else {
    dispatch({
      type: ActionTypes.PAYMENT_FAILURE,
      payload: false,
    })
  }
};

export const getAllReceipt = (user_id, token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.RECEIPT_LIST_REQUEST });
  const { session } = getState();

  try {
    let request = await SessionService.getAllReceipt(user_id, token)
    dispatch({
      type: ActionTypes.RECEIPT_LIST_SUCCESS,
      payload: request,
    })
  } catch (error) {
    dispatch({
      type: ActionTypes.RECEIPT_LIST_FAILURE,
      payload: error
    });
  }
};

export const getPdfReceipt = (transaction_id, token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.RECEIPT_PDF_REQUEST });
  const { session } = getState();

  try {
    let request = await SessionService.getPdfReceipt(transaction_id, token)
    dispatch({
      type: ActionTypes.RECEIPT_PDF_SUCCESS,
      payload: request,
    })
  } catch (error) {
    dispatch({
      type: ActionTypes.RECEIPT_PDF_FAILURE,
      payload: error
    });
  }
};

// GET USER CREDIT CARDS

export const getUserPayments = (user_id, token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.USER_PAYMENT_REQUEST });
  const { session } = getState();

  try {
    let request = await SessionService.getUserPayments(user_id, token)
    dispatch({
      type: ActionTypes.USER_PAYMENT_SUCCESS,
      payload: request,
    })
  } catch (error) {
    dispatch({
      type: ActionTypes.USER_PAYMENT_FAILURE,
      payload: error
    });
  }
};

// SAVE USER CREDIT CARDS

export const saveUserCard = (data, token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.SAVE_PAYMENT_REQUEST });
  const { session } = getState();

  try {
    let request = await SessionService.saveUserCard(data, token)
    dispatch({
      type: ActionTypes.SAVE_PAYMENT_SUCCESS,
      payload: request,
    })
  } catch (error) {
    dispatch({
      type: ActionTypes.SAVE_PAYMENT_FAILURE,
      payload: error
    });
  }
};

// DELETE USER CREDIT CARDS

export const deleteUserCard = (card_id, token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.DELETE_CARD_REQUEST });
  const { session } = getState();

  try {
    let request = await SessionService.deleteUserCard(card_id, token)
    dispatch({
      type: ActionTypes.DELETE_CARD_SUCCESS,
      payload: request,
    })
  } catch (error) {
    dispatch({
      type: ActionTypes.DELETE_CARD_FAILURE,
      payload: error
    });
  }
};

/**
  * @description ENDPOINT TO CONFIGURE RECURRENT PAYMENT By Membership 
  * @returns membership billing info.
*/

export const recurrentbyMembership = (membership_id, token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.RECURRENT_MEMBERSHIP_REQUEST });
  const { session } = getState();

  try {
    let request = await SessionService.recurrentbyMembership(membership_id, token)
    dispatch({
      type: ActionTypes.RECURRENT_MEMBERSHIP_SUCCESS,
      payload: request,
    })
  } catch (error) {
    dispatch({
      type: ActionTypes.RECURRENT_MEMBERSHIP_FAILURE,
      payload: error
    });
  }
};

/* ENDPOINT TO CONFIGURE a non recurrent membership recurrent =================================================================================================== */

export const setRecurrentMembership = (data, token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.RECURRENT_MEMBERSHIP_REQUEST });
  const { session } = getState();

  try {
    let request = await SessionService.setRecurrentMembership(data, token)
    dispatch({
      type: ActionTypes.RECURRENT_MEMBERSHIP_SUCCESS,
      payload: request,
    })
  } catch (error) {
    dispatch({
      type: ActionTypes.RECURRENT_MEMBERSHIP_FAILURE,
      payload: error
    });
  }
};

/* ENDPOINT TO UPDATE a recurrent membership =================================================================================================== */

export const updateRecurrentMembership = (id_recurrencia, data, token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.UPDATE_RECURRENT_MEMBERSHIP_REQUEST });
  const { session } = getState();

  try {
    let request = await SessionService.updateRecurrentMembership(id_recurrencia, data, token)
    dispatch({
      type: ActionTypes.UPDATE_RECURRENT_MEMBERSHIP_SUCCESS,
      payload: request,
    })
  } catch (error) {
    dispatch({
      type: ActionTypes.UPDATE_RECURRENT_MEMBERSHIP_FAILURE,
      payload: error
    });
  }
};

/* ENDPOINT TO DELETE a recurrent membership =================================================================================================== */

export const deleteRecurrentMembership = (id_recurrencia, token) => async (dispatch, getState) => {
  dispatch({ type: ActionTypes.DELETE_RECURRENT_MEMBERSHIP_REQUEST });
  const { session } = getState();

  try {
    let request = await SessionService.deleteRecurrentMembership(id_recurrencia, token)
    dispatch({
      type: ActionTypes.DELETE_RECURRENT_MEMBERSHIP_SUCCESS,
      payload: request,
    })
  } catch (error) {
    dispatch({
      type: ActionTypes.DELETE_RECURRENT_MEMBERSHIP_FAILURE,
      payload: error
    });
  }
};



