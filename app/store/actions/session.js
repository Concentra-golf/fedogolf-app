/* eslint-disable prettier/prettier */
import * as ActionTypes from '../actions/types';

// API

import API, { setApiHeaders, requestMethod } from '../../config/api';
import SessionService from '../../config/services/sessionService'
import FedogolfService from '../../config/services/FedogolfService'

/* SignUp a new user =================================================================================================== */

/**
  * @description Sends the participant's confirmation to the server
  * @param data {
        username: the id of the user's QR Code,
        password: the id of the event.
        email: the participation status of the user (always set to 1 to confirm participation),
      }
    }
  * @returns true or false depending on if the request was successful
*/

export const signUp = data => async (dispatch, getState) => {

  dispatch({ type: ActionTypes.USER_SIGN_UP_REQUEST });

  const { session } = getState();

  try {

    // let request = await fetch(API.session.signUp, { ...setApiHeaders(requestMethod.post, session.token.data.access_token), body: JSON.stringify(data) });
    let request = await SessionService.signup(data)

    dispatch({
      type: ActionTypes.USER_SIGN_UP_SUCCESS,
      payload: request.data,
    })

  } catch (error) {
    console.log(error.response)

    if (error.response.status === 422) {
      dispatch({
        type: ActionTypes.USER_SIGN_UP_FAILURE,
        payload: 'Email is already registered'
      })
    } else {

      dispatch({
        type: ActionTypes.USER_SIGN_UP_FAILURE,
        payload: error
      });
    }

  }

};


/* SignUp Golfer  Information ============================================================================================= */

/**
  * @description Sends the User Golfer Information to the server
  * @param data {
        username: the id of the user's QR Code,
        password: the id of the event.
        email: the participation status of the user (always set to 1 to confirm participation),
      }
    }
  * @returns true or false depending on if the request was successful
*/

export const signUpGolferInformation = (id, token, data) => async (dispatch, getState) => {

  dispatch({ type: ActionTypes.USER_SIGN_UP_GOLFER_INFORMATION_REQUEST });

  const { session } = getState();

  try {

    // let request = await fetch(API.session.signUp, { ...setApiHeaders(requestMethod.post, session.token.data.access_token), body: JSON.stringify(data) });
    let request = await SessionService.signUpGolferInformation(id, token, data)

    dispatch({
      type: ActionTypes.USER_SIGN_UP_GOLFER_INFORMATION_SUCCESS,
      payload: request.data.user,
    })

  } catch (error) {
    console.log(error.response)

    if (error.response.status === 422) {
      dispatch({
        type: ActionTypes.USER_SIGN_UP_GOLFER_INFORMATION_FAILURE,
        payload: 'Email is already registered'
      })
    } else {

      dispatch({
        type: ActionTypes.USER_SIGN_UP_GOLFER_INFORMATION_FAILURE,
        payload: 'Error while registering user please try again',
      });
    }

  }

};


/* Update User Information ============================================================================================= */

/**
  * @description Sends the User Golfer Information to the server
  * @param data {
        username: the id of the user's QR Code,
        password: the id of the event.
        email: the participation status of the user (always set to 1 to confirm participation),
      }
    }
  * @returns true or false depending on if the request was successful
*/

export const updateProfile = (id, accessToken, data) => async (dispatch, getState) => {

  console.log('works');

  dispatch({ type: ActionTypes.PUT_PERSONAL_INFORMATION_REQUEST });

  console.log('data from updateProfile', data)

  try {

    // let request = await fetch(API.login, { ...setApiHeaders(requestMethod.post), body: JSON.stringify(data) });

    let request = await SessionService.updateProfile(id, accessToken, data)

    console.log(request)

    if (request.status === 200 || request.status === 201) {

      dispatch({
        type: ActionTypes.PUT_PERSONAL_INFORMATION_SUCCESS,
        payload: request.data,
      });

    } else {

      dispatch({
        type: ActionTypes.PUT_PERSONAL_INFORMATION_FAILURE,
        payload: 'Something went wrong',
      });

    }

  } catch (error) {

    if (error.response.status === 401) {
      dispatch({
        type: ActionTypes.PUT_PERSONAL_INFORMATION_FAILURE,
        payload: 'Invalid email or password',
      });
    } else {

      dispatch({
        type: ActionTypes.PUT_PERSONAL_INFORMATION_FAILURE,
        payload: 'Error while signing in, please try again',
      });
    }
  }

};

// Sign In the user ====================================================================================================

/**
  * @description Log In the user
  * @param data {
      identification: the user name,
      password: the password,
    }
  * @returns The user's sessión token
*/

export const signIn = data => async (dispatch, getState) => {

  console.log('works');

  dispatch({ type: ActionTypes.USER_SIGN_IN_REQUEST });

  console.log('data')

  try {

    // let request = await fetch(API.login, { ...setApiHeaders(requestMethod.post), body: JSON.stringify(data) });

    let request = await SessionService.signIn(data)


    if (request.status === 200 || request.status === 201) {

      dispatch({
        type: ActionTypes.USER_SIGN_IN_SUCCESS,
        payload: request.data,
      });

    } else {

      dispatch({
        type: ActionTypes.USER_SIGN_IN_FAILURE,
        payload: requestData.message,
      });

    }

  } catch (error) {

    if (error.response.status === 401) {
      dispatch({
        type: ActionTypes.USER_SIGN_IN_FAILURE,
        payload: 'Invalid email or password',
      });
    } else {

      dispatch({
        type: ActionTypes.USER_SIGN_IN_FAILURE,
        payload: 'Error while signing in, please try again',
      });
    }
  }

};

// Sign In the user with facebook =====================================================================================

/**
  * @description Log In the user with facebook
  * @param data {
      identification: the user name,
      password: the password,
    }
  * @returns The user's session token
*/

export const facebookSignIn = data => async (dispatch, getState) => {


  dispatch({ type: ActionTypes.USER_SIGN_IN_REQUEST });


  try {
    // let request = await fetch(API.login, { ...setApiHeaders(requestMethod.post), body: JSON.stringify(data) });

    console.log('arrived at the facebookSignIn action')
    let request = await SessionService.facebookSignIn(data)

    console.log('request redux', request)

    if (request.status === 200 || request.status === 201) {

      dispatch({
        type: ActionTypes.USER_SIGN_IN_SUCCESS,
        payload: request.data,
      });

    } else {

      dispatch({
        type: ActionTypes.USER_SIGN_IN_FAILURE,
        payload: requestData.message,
      });

    }

  } catch (error) {

    if (error.response.status === 401) {
      dispatch({
        type: ActionTypes.USER_SIGN_IN_FAILURE,
        payload: 'Invalid email or password',
      });
    } else {

      dispatch({
        type: ActionTypes.USER_SIGN_IN_FAILURE,
        payload: 'Error while signing in, please try again',
        // payload: 'Error while signing in, please try again',
      });
    }
  }

};




// Log Out the user ====================================================================================================

/**
    * @description Log out the user
*/

export const signOut = data => async dispatch => {
  dispatch(cleanSessionStore());
};


// Clean Session Store ====================================================================================================

/**
 * @description Cleans up the Session store
 */

export const cleanSessionStore = () => (dispatch, getState) => {

  dispatch({ type: ActionTypes.CLEAN_SESSION_STORE });

};


// Forgot the user password ====================================================================================================

export const forgotPassword = email => async (dispatch) => {
  dispatch({ type: ActionTypes.FORGOT_PASSWORD_REQUEST });


  try {
    let request = await SessionService.resetPassword(email)

    if (request.status === 200 || request.status === 201) {

      dispatch({
        type: ActionTypes.FORGOT_PASSWORD_SUCCESS,
        payload: request.data,
      });

    } else {

      dispatch({
        type: ActionTypes.FORGOT_PASSWORD_FAILURE,
        payload: requestData.message,
      });

    }

  } catch (error) {

    if (error.response.status === 401) {
      dispatch({
        type: ActionTypes.FORGOT_PASSWORD_FAILURE,
        payload: 'Invalid email or password',
      });
    } else {

      dispatch({
        type: ActionTypes.FORGOT_PASSWORD_FAILURE,
        payload: 'Error while signing in, please try again',
      });
    }
  }
}



/* FedoGolf Register a new user =================================================================================================== */

/**
  * @description Sends the participant's confirmation to the server
  * @param data {
        username: the id of the user's QR Code,
        password: the id of the event.
        email: the participation status of the user (always set to 1 to confirm participation),
      }
    }
  * @returns true or false depending on if the request was successful
*/

export const fedogolfSignUp = (token, data) => async (dispatch, getState) => {

  dispatch({ type: ActionTypes.FEDOGOLF_SIGN_UP_REQUEST });

  const { session } = getState();

  try {

    // let request = await fetch(API.session.signUp, { ...setApiHeaders(requestMethod.post, session.token.data.access_token), body: JSON.stringify(data) });
    let request = await FedogolfService.fedogolfSignUp(token, data)

    dispatch({
      type: ActionTypes.FEDOGOLF_SIGN_UP_SUCCESS,
      payload: request.data,
    })

  } catch (error) {
    console.log(error.response)

    if (error.response.status === 422) {
      dispatch({
        type: ActionTypes.FEDOGOLF_SIGN_UP_FAILURE,
        payload: error
      })
    } else {

      dispatch({
        type: ActionTypes.FEDOGOLF_SIGN_UP_FAILURE,
        payload: error
      });
    }

  }

};

/* FedoGolf Register edit request form =================================================================================================== */

/**
  * @description Sends the participant's confirmation to the server
  * @param data {
        username: the id of the user's QR Code,
        password: the id of the event.
        email: the participation status of the user (always set to 1 to confirm participation),
      }
    }
  * @returns true or false depending on if the request was successful
*/

export const editfedogolfSignUp = (token, id, data) => async (dispatch, getState) => {

  dispatch({ type: ActionTypes.FEDOGOLF_SIGN_UP_REQUEST });

  const { session } = getState();

  try {

    // let request = await fetch(API.session.signUp, { ...setApiHeaders(requestMethod.post, session.token.data.access_token), body: JSON.stringify(data) });
    let request = await FedogolfService.editFedogolfSignUp(token, id, data)

    dispatch({
      type: ActionTypes.FEDOGOLF_SIGN_UP_SUCCESS,
      payload: request.data,
    })

  } catch (error) {
    console.log(error.response)

    if (error.response.status === 422) {
      dispatch({
        type: ActionTypes.FEDOGOLF_SIGN_UP_FAILURE,
        payload: 'Email is already registered'
      })
    } else {

      dispatch({
        type: ActionTypes.FEDOGOLF_SIGN_UP_FAILURE,
        payload: error
      });
    }

  }

};


/* Get all the settings =================================================================================================== */

/**
  * @description Gets the settings list
  * @returns A list of the settings
*/

export const getFedoGolf = data => async (dispatch, getState) => {

  dispatch({ type: ActionTypes.GET_FEDOGOLF_REQUEST });

  const { session } = getState();

  try {
    let request = await fetch(API.session.fedogolf, { ...setApiHeaders(requestMethod.get, session.token.data.access_token) });

    let requestData = await request.json();

    if (request.status === 200 || request.status === 201) {
      dispatch({
        type: ActionTypes.GET_FEDOGOLF_SUCCESS,
        payload: requestData,
      });

    } else {
      dispatch({
        type: ActionTypes.GET_FEDOGOLF_FAILURE,
        payload: requestData.message,
      });

    }

  } catch (error) {

    dispatch({
      type: ActionTypes.GET_FEDOGOLF_FAILURE,
      payload: 'Error while loading fedogolf information, please try again.',
    });

  }

};


/* Fedogolf Validation =================================================================================================== */
/**
  * @description Log In the user
  * @param data {
      identification: the user name,
      password: the password,
    }
  * @returns The user's sessión token
*/
export const fedogolfValidation = (data, token) => async (dispatch, getState) => {
  console.log('works');

  dispatch({ type: ActionTypes.FEDOGOLF_USER_VERIFY_REQUEST });

  console.log('data', data)

  try {

    // let request = await fetch(API.login, { ...setApiHeaders(requestMethod.post), body: JSON.stringify(data) });

    let request = await SessionService.fedogolfValidation(data, token)
    console.log('request', request.data.validated)


    if (request.status === 200 || request.status === 201) {

      dispatch({
        type: ActionTypes.FEDOGOLF_USER_VERIFY_SUCCESS,
        payload: 'An confimation mail has been sent, please check your email',
      });
      return request.data;

    } else {

      dispatch({
        type: ActionTypes.FEDOGOLF_USER_VERIFY_FAILURE,
        payload: 'An error has ocurred validating account',
      });

    }

  } catch (error) {
    if (error.response.status === 401) {
      dispatch({
        type: ActionTypes.FEDOGOLF_USER_VERIFY_FAILURE,
        payload: error,
        // payload: 'Invalid Fedogolf Account',
      });
    } else {

      dispatch({
        type: ActionTypes.FEDOGOLF_USER_VERIFY_FAILURE,
        payload: error,
        // payload: 'An error has ocurred validating account',
      });
    }
  }

};

export const refreshToken = token => async (dispatch, getState) => {

  dispatch({ type: ActionTypes.USER_REFRESH_TOKEN_REQUEST });
  const { session } = getState();


  try {
    let request = await SessionService.refreshToken(token);
    console.log(request);

    dispatch({
      type: ActionTypes.USER_REFRESH_TOKEN_SUCCESS,
      payload: request.data,
    })

  } catch (error) {
    if (error.response.status === 401) {
      dispatch(cleanSessionStore());
    } else {
      dispatch({
        type: ActionTypes.USER_REFRESH_TOKEN_FAILURE,
        payload: error,
      });
    }

    return error;

  }
};

export const requestMemberships = token => async (dispatch, getState) => {

  dispatch({ type: ActionTypes.FEDERATION_REQUEST_REQUEST });
  const { session } = getState();


  try {
    let request = await SessionService.getRequests(token);

    dispatch({
      type: ActionTypes.FEDERATION_REQUEST_SUCCESS,
      payload: request.data,
    })

  } catch (error) {
    dispatch({
      type: ActionTypes.FEDERATION_REQUEST_FAILURE,
      payload: error,
    });

    return error;
  }
};

export const requestMembershipsById = (application_id, token) => async (dispatch, getState) => {

  dispatch({ type: ActionTypes.FEDERATION_REQUEST_BY_ID_REQUEST });
  const { session } = getState();


  try {
    let request = await SessionService.getRequestsById(application_id, token);

    dispatch({
      type: ActionTypes.FEDERATION_REQUEST_BY_ID_SUCCESS,
      payload: request.data,
    })

  } catch (error) {
    dispatch({
      type: ActionTypes.FEDERATION_REQUEST_BY_ID_FAILURE,
      payload: error,
    });

    return error;
  }
};

export const uploadDomcumentToRequest = (golf_application_id, file, token) => async (dispatch, getState) => {

  dispatch({ type: ActionTypes.FEDERATION_DOCUMENTS_REQUEST });
  const { session } = getState();

  try {
    let request = await SessionService.attachDocumentToRequest(golf_application_id, file, token);
    console.log(request);

    dispatch({
      type: ActionTypes.FEDERATION_DOCUMENTS_SUCCESS,
      payload: request.data,
    })

  } catch (error) {
    dispatch({
      type: ActionTypes.FEDERATION_DOCUMENTS_FAILURE,
      payload: error,
    });

    return error;
  }
};


export const getMembershipsTypes = (federation_id, token) => async (dispatch, getState) => {

  dispatch({ type: ActionTypes.MEMBERSHIPS_TYPES_REQUEST });
  const { session } = getState();

  try {
    let request = await SessionService.getMembershipsTypes(federation_id, token);
    console.log(request);

    dispatch({
      type: ActionTypes.MEMBERSHIPS_TYPES_SUCCESS,
      payload: request.data,
    })

  } catch (error) {
    dispatch({
      type: ActionTypes.MEMBERSHIPS_TYPES_FAILURE,
      payload: error,
    });

    return error;
  }
};