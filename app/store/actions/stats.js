/* eslint-disable prettier/prettier */
import * as ActionTypes from '../actions/types';

// API

import API, {setApiHeaders, requestMethod} from '../../config/api';

/* Get all the stats =================================================================================================== */

/**
  * @description Gets the stats list
  * @returns A list of the stats
*/

export const getStats = data => async (dispatch, getState) => {

  dispatch({type: ActionTypes.GET_STATS_REQUEST});

  const { session } = getState();

  try {

    let request = await fetch(API.stats.getStats, { ...setApiHeaders(requestMethod.get, session.token.data.access_token) });

    let requestData = await request.json();

    if (request.status === 200 || request.status === 201) {

      dispatch({
        type: ActionTypes.GET_STATS_SUCCESS,
        payload: requestData,
      });

    } else {

      dispatch({
        type: ActionTypes.GET_STATS_FAILURE,
        payload: requestData.message,
      });

    }

  } catch (error) {

    dispatch({
      type: ActionTypes.GET_STATS_FAILURE,
      payload: 'Error while loading stats, please try again.',
    });

  }

};
