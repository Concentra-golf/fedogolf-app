/* eslint-disable prettier/prettier */
import * as ActionTypes from '../actions/types';

// API

import API, { setApiHeaders, requestMethod } from '../../config/api';
import SessionService from '../../config/services/sessionService'




/* get All Federations =================================================================================================== */

/**
  * @description Sends the participant's confirmation to the server
  * @param data {
        username: the id of the user's QR Code,
        password: the id of the event.
        email: the participation status of the user (always set to 1 to confirm participation),
      }
    }
  * @returns true or false depending on if the request was successful
*/

export const getAllFederations = (token) => async (dispatch, getState) => {
  
  dispatch({ type: ActionTypes.GET_ALL_FEDERATIONS_REQUEST });
  
  const { session } = getState();

  try {
    // let request = await fetch(API.session.signUp, { ...setApiHeaders(requestMethod.post, session.token.data.access_token), body: JSON.stringify(data) });
    let request = await SessionService.getAllFederations(session.accessToken)
    console.log('klkkkkk', request);

    dispatch({
      type: ActionTypes.GET_ALL_FEDERATIONS_SUCCESS,
      payload: request.data,
    })

  } catch (error) {
    console.log(error.response)

    dispatch({
      type: ActionTypes.GET_ALL_FEDERATIONS_FAILURE,
      payload: error
    });

  }

};

export const getAllMembershipFees = (membership_id, token) => async (dispatch, getState) => {
  
  dispatch({ type: ActionTypes.MEMBERSHIPS_FEES_REQUEST });
  
  const { session } = getState();

  try {
    // let request = await fetch(API.session.signUp, { ...setApiHeaders(requestMethod.post, session.token.data.access_token), body: JSON.stringify(data) });
    let request = await SessionService.getMembershipsFees(membership_id, session.accessToken)

    dispatch({
      type: ActionTypes.MEMBERSHIPS_FEES_SUCCESS,
      payload: request.data,
    })

  } catch (error) {
    console.log(error.response)

    dispatch({
      type: ActionTypes.MEMBERSHIPS_FEES_FAILURE,
      payload: error
    });

  }

};

export const getMembershipsTypesById = (membership_id, token) => async (dispatch, getState) => {
  
  dispatch({ type: ActionTypes.MEMBERSHIPS_FEES_BYID_REQUEST });
  
  const { session } = getState();

  try {
    // let request = await fetch(API.session.signUp, { ...setApiHeaders(requestMethod.post, session.token.data.access_token), body: JSON.stringify(data) });
    let request = await SessionService.getMembershipsTypesById(membership_id, token)

    dispatch({
      type: ActionTypes.MEMBERSHIPS_FEES_BYID_SUCCESS,
      payload: request.data,
    })

  } catch (error) {
    console.log(error.response)

    dispatch({
      type: ActionTypes.MEMBERSHIPS_FEES_BYID_FAILURE,
      payload: error
    });

  }

};
