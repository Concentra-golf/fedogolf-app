/* eslint-disable prettier/prettier */
import * as ActionTypes from '../actions/types';

// API

import API, {setApiHeaders, requestMethod} from '../../config/api';

/* Get all the schedules =================================================================================================== */

/**
  * @description Gets the schedules list
  * @returns A list of the schedules
*/

export const getCourses = data => async (dispatch, getState) => {

  dispatch({type: ActionTypes.GET_SCHEDULES_REQUEST});

  const { session } = getState();

  try {

    let request = await fetch(API.schedules.getSchedules, { ...setApiHeaders(requestMethod.get, session.token.data.access_token) });

    let requestData = await request.json();

    if (request.status === 200 || request.status === 201) {

      dispatch({
        type: ActionTypes.GET_SCHEDULES_SUCCESS,
        payload: requestData,
      });

    } else {

      dispatch({
        type: ActionTypes.GET_SCHEDULES_FAILURE,
        payload: requestData.message,
      });

    }

  } catch (error) {

    dispatch({
      type: ActionTypes.GET_COURSES_FAILURE,
      payload: 'Error while loading schedules, please try again.',
    });

  }

};
