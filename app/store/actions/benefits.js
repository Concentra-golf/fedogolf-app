/* eslint-disable prettier/prettier */
import * as ActionTypes from '../actions/types';

// API

import API, { setApiHeaders, requestMethod } from '../../config/api';
import SessionService from '../../config/services/sessionService'

/* USER BENEFITS =================================================================================================== */

/**
  * @description Get benefits by user
  * @param userID 
  * @returns array of benefits available
*/

export const getAllBenefits = (userID, token) => async (dispatch, getState) => {

  dispatch({ type: ActionTypes.GET_BENEFITS_INFORMATION_REQUEST });

  const { benefits } = getState();

  try {
    let request = await SessionService.getAllBenefits(userID, token)
    console.log('Benefits Request', request);

    dispatch({
      type: ActionTypes.GET_BENEFITS_INFORMATION_SUCCESS,
      payload: request.data,
    })

    // return request.data;

  } catch (error) {
    console.log(error.response)

    if (error.response.status === 422) {
      dispatch({
        type: ActionTypes.GET_BENEFITS_INFORMATION_FAILURE,
        payload: 'No Benefits Available'
      })
    } else {
      dispatch({
        type: ActionTypes.GET_BENEFITS_INFORMATION_FAILURE,
        payload: 'No Benefits Available'
      });
    }

  }

};


export const getfederationsBenefits = (fedID, token) => async (dispatch, getState) => {

  dispatch({ type: ActionTypes.FEDERATION_BENEFITS_REQUEST });

  const { benefits } = getState();

  try {
    let request = await SessionService.getBenefitsByFederation(fedID, token)
    console.log('Benefits by Federation', request);

    dispatch({
      type: ActionTypes.FEDERATION_BENEFITS_SUCCESS,
      payload: request.data,
    })

    return request.data;

  } catch (error) {
    console.log(error.response)
      dispatch({
        type: ActionTypes.FEDERATION_BENEFITS_FAILURE,
        payload: 'No Benefits Available'
      });
  }
};
