import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, SafeAreaView } from 'react-native';
import { verticalScale, moderateScale, scale } from '../../../utilities/ScalingScreen';
import Octicons from 'react-native-vector-icons/Octicons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { TouchableOpacity } from 'react-native-gesture-handler';

class MembershipDetailHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }


    render() {
        const { data, navigation } = this.props;
        return (
            <SafeAreaView style={styles.container}>

                <View style={styles.imageContainer}>
                    <Image
                        source={{ uri: 'https://fedogolf.org.do/wp-content/uploads/2018/03/logo-fedogolf.png' }}
                        style={styles.federationLogo}
                    />
                </View>

                <View style={styles.iconsContainer}>
                    {/* <TouchableOpacity
                    >
                        <Octicons
                            name='settings'
                            size={29}
                            style={{ paddingRight: 15 }}
                            color='black'
                        />
                    </TouchableOpacity> */}

                    {/* <TouchableOpacity
                        onPress={() => navigation.navigate('Search', { data: this.props.data, type: 'benefits' })}
                    >
                        <Ionicons
                            name='ios-search'
                            size={29}
                            color='black'
                        />
                    </TouchableOpacity> */}

                </View>

            </SafeAreaView>
        );
    }
}

export default MembershipDetailHeader;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: verticalScale(90),
        padding: scale(10),
        flexDirection: 'row',
        alignContent: "center",
        backgroundColor: 'white'
    },
    imageContainer: {
        position: 'absolute',
        alignSelf: 'center',
        width: '100%',
        alignItems: 'center'
    },
    federationLogo: {
        width: moderateScale(120),
        height: verticalScale(20),
        marginTop: verticalScale(15)

    },
    iconsContainer: {
        flexDirection: 'row',
        position: 'absolute',
        alignSelf: 'center',
        right: moderateScale(15),
        bottom: verticalScale(23),
        alignItems: 'center'
    }
})