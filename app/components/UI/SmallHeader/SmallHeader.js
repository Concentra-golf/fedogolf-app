import React from 'react';
import { View, Dimensions, StatusBar, NativeModules, ImageBackground } from 'react-native';

import MenuButton from '../HeaderButtons/MenuButton/MenuButton';
import BackButton from '../HeaderButtons/BackButton/BackButton';
import ViewHeader from '../ViewHeader/ViewHeader';

import StatusBarHeight from '../../../utilities/StatusBarHeight';

// Images

const backgrounds = {
    default: require('../../../assets/images/big-header.png'),
}

export default function SmallHeader(props) {

    console.log('statusBarHeight: ', StatusBarHeight);

    var options = {
        menu: () => <MenuButton />,
        back: () => <BackButton />
    }

    // var leftButton = 
    return (
        <View style={{ position: "absolute", width: "100%", top: 0, height: StatusBarHeight + 55 }}>
            <ImageBackground
                style={{
                    bottom: 0,
                    width: "100%",
                    height: StatusBarHeight + 55
                }}
                source={require('../../../assets/images/small-header.png')}
            >

            </ImageBackground>
        </View>
    )
}
