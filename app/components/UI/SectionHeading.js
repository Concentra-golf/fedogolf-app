import React from 'react';
import {View} from 'react-native';

// Components

import TextLabel from './TextLabel';

export default class SectionHeading extends React.Component {

    render() {
        const { color, size, align, transform, additionalStyles } = this.props;

        return (
            <View style={{ marginLeft: 15, marginRight: 15 }}>
                <TextLabel
                    font={"primarybold"}
                    transform={transform}
                    size={size ? size : 16}
                    heading={true}
                    additionalStyles={additionalStyles}
                    align={align ? align : null}
                    color={color ? color : "darkgrey"}
                >
                    {this.props.children}
                </TextLabel>
            </View>
        );
    }
    
}