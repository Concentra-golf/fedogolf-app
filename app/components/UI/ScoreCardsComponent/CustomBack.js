import React, { Component } from 'react';
import { View, Text, StyleSheet, Alert } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import { COLORS } from '../../../config/constants';
import { verticalScale, moderateScale, scale } from '../../../utilities/ScalingScreen';
import { TouchableOpacity } from 'react-native-gesture-handler';

import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';
import * as RNLocalize from 'react-native-localize';

class CustomBack extends Component {
    constructor(props) {
        super(props);
        setI18nConfig();
        this.state = {
        };
    }

    componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);
    }


    goBack() {
        const hey = this.props;
        Alert.alert(
            translate('alertTitle'),
            translate('alertMsg'),
            [
                { text: translate('yes'), onPress: (e) => this.props.action() },
                { text: translate('cancel'), onPress: () => console.log('hey') },
            ])
    }


    render() {
        return (
            <TouchableOpacity
                style={styles.container}
                onPress={() => this.goBack()}
            >
                <Ionicons
                    name='ios-arrow-back'
                    color={COLORS.white}
                    size={scale(24)}
                />
            </TouchableOpacity>
        );
    }
}

export default CustomBack;

const styles = StyleSheet.create({
    container: {
        width: 45,
        height: 45,
        marginTop: verticalScale(15),
        marginLeft: moderateScale(15)
    },
})
