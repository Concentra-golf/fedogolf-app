import React, { Component } from 'react';
import { StyleSheet, StatusBar, Platform } from 'react-native'
import { Header, Left, Body, Right, Button, Title, Text, } from 'native-base';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { COLORS } from '../../../config/constants';
import { faArrowLeft, faSearch } from '@fortawesome/free-solid-svg-icons';

class NativeBaseHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <Header
        style={{ backgroundColor: this.props.theme === 'dark-blue' ? COLORS.darkblue : 'white' }}
      >
        <StatusBar 
          barStyle={this.props.theme === 'dark-blue' ? 'light-content' : 'dark-content'}
        />
        <Left>
          <Button
            hasText
            transparent
            onPress={this.props.leftButton}
          >
            {this.props.leftIcon !== null ? (
              <FontAwesomeIcon
                icon={this.props.leftIcon ? faArrowLeft : this.props.leftIcon}
                size={Platform.OS === 'android' ? 18 : 22}
                color={this.props.theme === 'dark-blue' ? COLORS.white : COLORS.black}
              />
            ) : (
                <Text>{this.props.title}</Text>
              )
            }
          </Button>
        </Left>

        <Body>
          <Title style={{ color: this.props.theme === 'dark-blue' ? COLORS.white : COLORS.black }}>{this.props.title}</Title>
        </Body>
        <Right>
          <Button
            hasText
            transparent>
            {this.props.rightIcon ? (
              <FontAwesomeIcon
                icon={this.props.rightIcon ? faSearch : this.props.rightIcon}
                size={32}
                color={COLORS.black}
              />
            ) : (
                <Text>{this.props.rightTitle}</Text>
              )
            }
          </Button>
        </Right>
      </Header>
    );
  }
}

export default NativeBaseHeader;
