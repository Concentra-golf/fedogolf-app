import React, { Component } from 'react';
import { View, TextInput, Switch, Image, TouchableOpacity, Text } from 'react-native';

// Modules

import RNPickerSelect from 'react-native-picker-select';
import DatePicker from 'react-native-datepicker';

// Components

import TextLabel from '../TextLabel';

// Styles

import { styles, stylesLight } from './Styles';
import { COLORS } from '../../../config/constants';

// Images

const caretDown = require('./Images/icn-caret-down.png');
const icnDatePicker = require('./Images/icn-date-picker.png');

class FormInput extends Component {
  render() {
    const {
      maxLength,
      keyboardType,
      onChangeText,
      onBlur,
      placeholder,
      type,
      value,
      data,
      editable,
      theme,
      error,
      actionLabel,
      action,
      onValueChange,
      withLabel,
      label,
      isSignUpForm,
      name,
      onFocus
    } = this.props;

    return (
      <React.Fragment>
        {withLabel && <TextLabel
          weight={"bold"}
          size={16}
          color={"black"}
          additionalStyles={styles.textInputLabel}
        >
          {label ? label : placeholder}
        </TextLabel>}
        {type === 'picker' ? (
          <RNPickerSelect
            key={this.props.placeholder}
            placeholder={{
              label: placeholder,
              value: null,
            }}
            placeholderTextColor={theme === 'light' ? '#8E8E93' : theme ? theme : '#fff'}
            items={data}
            onValueChange={onChangeText}
            useNativeAndroidPickerStyle={false}
            doneText="Confirm"
            style={theme === 'light' ? { ...stylesLight, ...this.props.additionalStyles } : { ...styles, ...this.props.additionalStyles }}
            value={value}
            Icon={() => {
              return (
                <Image
                  style={styles.pickerIcon}
                  resizeMode="contain"
                  source={caretDown}
                />
              );
            }}
          />
        ) : type === 'switch' ? (
          <View style={styles.switchContainer}>


            <Switch
              onValueChange={onValueChange}
              value={value}
              trackColor={{ true: COLORS.lightGreen }}
            />

            <View>
              <TextLabel color={theme === 'light' ? 'default' : 'white'} additionalStyles={{ marginLeft: 20, marginTop: 5 }}>
                {placeholder}
              </TextLabel>

              {actionLabel && action && (
                <TouchableOpacity activeOpacity={0.8} onPress={() => action()}>
                  <TextLabel color={'orange'}>{actionLabel}</TextLabel>
                </TouchableOpacity>
              )}
            </View>
          </View>
        ) : type === 'date' ? (
          <View style={styles.datePickerContainer}>
            <DatePicker
              style={
                [theme === 'light' ? stylesLight.datepicker : styles.datepicker, this.props.additionalStyles]
              }
              date={value}
              mode="date"
              placeholder={placeholder}
              iconSource={icnDatePicker}
              format="DD-MM-YYYY"
              minDate="01-01-1910"
              // maxDate="31-12-2030"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              customStyles={
                theme === 'light'
                  ? {
                    dateIcon: stylesLight.dateIcon,
                    dateInput: stylesLight.dateInput,
                    placeholderText: stylesLight.datePlaceholderText,
                    dateText: stylesLight.dateText,
                    dateTouchBody: stylesLight.dateTouchBody,
                    btnTextConfirm: stylesLight.dateBtnTextConfirm,
                    datePickerCon: stylesLight.datePickerCon,
                    datePicker: stylesLight.datePickerSelect,
                  }
                  : {
                    dateIcon: styles.dateIcon,
                    dateInput: styles.dateInput,
                    placeholderText: styles.datePlaceholderText,
                    dateText: styles.dateText,
                    dateTouchBody: styles.dateTouchBody,
                    btnTextConfirm: styles.btnTextConfirm,
                    datePickerCon: styles.datePickerCon,
                    datePicker: styles.datePickerSelect,
                  }
              }
              onDateChange={onChangeText}
            />
          </View>
        ) : (
                <TextInput
                  multiline={this.props.multiline}
                  // scrollEnabled={false}
                  style={[theme === 'light' ? stylesLight.textInput : styles.textInput, this.props.additionalStyles, isSignUpForm ? styles.signUpTextInput : null]}
                  onChangeText={onChangeText}
                  onBlur={onBlur}
                  onFocus={onFocus}
                  value={value}
                  maxLength={maxLength}
                  placeholder={placeholder}
                  placeholderTextColor={theme === 'light' ? '#8E8E93' : theme ? theme : '#fff'}
                  textContentType={type ? type : 'none'}
                  secureTextEntry={type === 'password' ? true : false}
                  autoCapitalize="none"
                  name={name}
                  autoCorrect={false}
                  // keyboardType={"number-pad"}
                  keyboardType={keyboardType ? keyboardType : 'default'}
                  editable={editable === undefined ? true : editable}
                />
              )}

        {/* Input validation error message */}

        {error && (
          <TextLabel
            size={13}
            color={'danger'}
            additionalStyles={{ marginBottom: 10 }}>
            {error}
          </TextLabel>
        )}
      </React.Fragment>
    );
  }
}

export default FormInput;
