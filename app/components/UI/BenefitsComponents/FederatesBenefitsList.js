import React, { Component } from 'react';
import { View, Text, FlatList, Modal, StyleSheet, TouchableWithoutFeedback, SafeAreaView } from 'react-native';

import FederatedBenefitsCards from './FederatedBenefitsCards';

import * as benefits from '../../../store/actions/benefits';
import { verticalScale, moderateScale, scale } from '../../../utilities/ScalingScreen';
import BenefitsModal from '../Modals/BenefitsModal';
import Loader from '../Loader/Loader';
import MembershipsCards from '../../cards/MembershipsCards/Memberships/MembershipsCards'

class FederatesBenefitsList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            benefitsList: [],
            loading: true,
            showModal: false,
            modalDetail: {},
            refreshing: false
        };
        this.showDetailsModal = this.showDetailsModal.bind(this);
    }

    showDetailsModal(modal, data) {
        this.setState({
            showModal: modal,
            modalDetail: data
        })
        this.props.reRender();
    }


    render() {
        const props = this.props.membershipList.memberships
        console.log(props)
        return (
            <View style={styles.container}>

                {!this.props.loading && this.props.membershipList.memberships.length > 0 &&
                    <FlatList
                        data={this.props.membershipList.memberships}
                        keyExtractor={(item, index) => index.toString()}
                        refreshing={this.props.refreshing}
                        onRefresh={this.props.handleRefresh}
                        renderItem={({ item, index }) =>
                            <MembershipsCards
                                allInfo={item}
                                name={item.name}
                                expirationDate={item.pivot.validity}
                                navigation={this.props.navigation}
                                route={'benefits'}
                                onClick={() => this.props.navigation.navigate('MembershipsBenefitsDetails', {
                                    allInfo: item
                                })}
                            />
                        }
                    />
                }

                {this.props.loading &&
                    <Loader
                        loadingMessage={'Loading Benefits'}
                    />
                }

                {!this.props.loading && this.props.membershipList.memberships.length === 0 &&
                    <Loader
                        nothing={true}
                        nothingMessage={'No benefits Available'}
                    />
                }

                <Modal
                    transparent={true}
                    visible={this.state.showModal}
                    animationType={'fade'}
                    statusBarTranslucent
                    presentationStyle={'overFullScreen'}
                >
                    <TouchableWithoutFeedback
                        onPress={(e) => this.showDetailsModal(false)}
                    >
                        <View style={styles.modalContainer} >
                        </View>
                    </TouchableWithoutFeedback>

                    <View style={{ justifyContent: 'center', alignItems: 'center', height: '100%' }}>
                        <View style={styles.containerBox}>
                            <BenefitsModal
                                close={(e) => this.showDetailsModal(false)}
                                data={this.state.modalDetail}
                            />
                        </View>
                    </View>

                </Modal>

            </View>
        );
    }
}

export default FederatesBenefitsList;

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        alignItems: 'center',
        height: '100%'
    },
    modalContainer: {
        position: 'absolute',
        backgroundColor: 'rgba(1, 1, 1, 0.5)',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%'
    },
    containerBox: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        width: moderateScale(330),
        borderRadius: scale(20)
    },
})