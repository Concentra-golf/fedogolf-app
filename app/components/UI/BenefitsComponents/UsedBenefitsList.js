import React, { Component } from 'react';
import { View, Text, FlatList, Modal, StyleSheet, TouchableWithoutFeedback, SafeAreaView } from 'react-native';

import BenefitsCards from './BenefitsCards';

import * as benefits from '../../../store/actions/benefits';
import { verticalScale, moderateScale, scale } from '../../../utilities/ScalingScreen';
import BenefitsModal from '../Modals/BenefitsModal';
import Loader from '../Loader/Loader';
// import MembershipHeader from '../../../components/UI/MembershipHeader/MembershipHeader';


class UsedBenefitsList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            benefitsList: [],
            loading: true,
            showModal: false,
            modalDetail: {},
            refreshing: false
        };
        this.showDetailsModal = this.showDetailsModal.bind(this);
    }

    showDetailsModal(modal, data) {
        this.setState({
            showModal: modal,
            modalDetail: data
        })
        this.props.reRender();
        this.benefitsArray = []
    }

    componentDidMount() {
        const benefitsList = this.props.benefitsList;

        if (benefitsList.length > 0) {
            const benefits = benefitsList.filter((item, key) => {
                const array = [];
                if (item.pivot.active !== 1) {
                    array.push(item);
                    return array;
                }
            })
            console.log('benefits', benefits);
            this.setState({
                benefitsList: benefits,
                loading: false
            })
        } else {
            this.setState({
                benefitsList: [],
                loading: false
            })
        }
    }

    render() {
        return (
            <View style={styles.container}>

                {!this.state.loading && this.state.benefitsList.length > 0 &&
                    <FlatList
                        data={this.state.benefitsList}
                        keyExtractor={(index) => index.toString()}
                        renderItem={({ item, index }) =>
                            <BenefitsCards
                                key={index}
                                title={item.name}
                                description={item.description}
                                counter={item.pivot.qty}
                                OnClick={this.props.route === 'federation' ? () => console.log('nada') : (e) => this.showDetailsModal(true, item)}
                            />
                        }
                    />
                }

                {this.state.loading &&
                    <Loader
                        loadingMessage={'Loading Benefits'}
                    />
                }

                {!this.state.loading && this.state.benefitsList.length === 0 &&
                    <Loader
                        nothing={true}
                        nothingMessage={'No benefits Available'}
                    />
                }

                <Modal
                    transparent={true}
                    visible={this.state.showModal}
                    animationType={'fade'}
                    statusBarTranslucent
                    presentationStyle={'overFullScreen'}
                >
                    <TouchableWithoutFeedback
                        onPress={(e) => this.showDetailsModal(false)}
                    >
                        <View style={styles.modalContainer} >
                        </View>
                    </TouchableWithoutFeedback>

                    <View style={{ justifyContent: 'center', alignItems: 'center', height: '100%' }}>
                        <View style={styles.containerBox}>
                            <BenefitsModal
                                close={(e) => this.showDetailsModal(false)}
                                data={this.state.modalDetail}
                                userData={this.props.userData}
                            />
                        </View>
                    </View>

                </Modal>

            </View>
        );
    }
}

export default UsedBenefitsList;

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
        alignItems: 'center'
    },
    modalContainer: {
        position: 'absolute',
        backgroundColor: 'rgba(1, 1, 1, 0.5)',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%'
    },
    containerBox: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        width: moderateScale(330),
        borderRadius: scale(20)
    },
})