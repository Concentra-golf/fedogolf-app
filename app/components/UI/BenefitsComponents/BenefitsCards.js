import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { scale, moderateScale, verticalScale } from '../../../utilities/ScalingScreen';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { COLORS } from '../../../config/constants';

class BenefitsCards extends Component {
    constructor(props) {
        super(props);
        this.state = {
            numberOfLines: 1
        };
    }

    componentDidMount() {
        if (this.props.federated) {
            this.setState({
                numberOfLines: 0
            })
        }
    }

    render() {
        const data = this.props
        return (
            <TouchableOpacity
                style={styles.container}
                onPress={(e) => this.props.OnClick(e)}
            >
                <View style={styles.textContainer}>
                    <Text style={styles.title}>{this.props.title}</Text>
                </View>

                {this.props.federated !== true &&
                    <View style={styles.containerRight}>
                        <View style={styles.verticalLine} />
                        <View style={styles.counterContainer}>
                            <Text style={styles.counterNumber}>{this.props.counter}</Text>
                            <FontAwesome
                                name='ticket'
                                size={30}
                                color='#666666'
                            />
                        </View>
                    </View>
                }

                <View style={styles.lineContainer}>
                    <View style={styles.horizontalLine} />
                </View>

            </TouchableOpacity >
        );
    }
}

export default BenefitsCards;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        width: '100%',
        padding: moderateScale(15),
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    textContainer: {
        flexDirection: 'column',
        marginRight: moderateScale(30),
    },
    title: {
        fontWeight: 'bold',
        fontSize: moderateScale(17),
        color: COLORS.softBlack,
        width: moderateScale(190),
    },
    subTitle: {
        marginTop: verticalScale(5),
        fontSize: moderateScale(12),
        fontWeight: '500',
        width: moderateScale(190),
        color: '#1B1B1B'
    },
    containerRight: {
        flexDirection: 'row',
        width: moderateScale(120),
        justifyContent: "space-evenly",
        alignItems: 'center',
    },
    verticalLine: {
        height: verticalScale(40),
        width: moderateScale(1),
        backgroundColor: '#666666',
        marginRight: moderateScale(10),
    },
    counterContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        width: moderateScale(100),
        marginRight: moderateScale(40)
    },
    counterNumber: {
        fontSize: moderateScale(35),
        marginRight: moderateScale(10),
        fontWeight: 'bold',
        color: '#666666'
    },
    lineContainer: {
        bottom: 0,
        right: moderateScale(20),
        position: 'absolute',
        alignContent: 'center',
        width: '100%',
        alignSelf: 'center',
    },
    horizontalLine: {
        borderWidth: 0.2,
        width: '100%',
        backgroundColor: '#707070'
    },
})