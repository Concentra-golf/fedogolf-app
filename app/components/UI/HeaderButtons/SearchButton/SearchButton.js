import React, { Component } from 'react';
import {
    View,
    TouchableOpacity,
    Image,
} from 'react-native';

// Styles

import styles from './Styles';

const icon = require('./Images/icn-search-white.png');

class SearchButton extends Component {
    render() {
        return (
            <TouchableOpacity activeOpacity={0.8} onPress={() => this.props.action()}>

                <View style={styles.button}>

                    <Image style={styles.buttonIcon} source={icon} />

                </View>

            </TouchableOpacity>
        );
    }
}

export default SearchButton;
