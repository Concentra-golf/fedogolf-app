import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    button: {
        width: 20,
        height: 20,
        marginRight: 20
    },
    buttonIcon: {
        width: 20,
        height: 20
    }
});