import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import { COLORS } from '../../../../config/constants';

// Modules
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as session from '../../../../store/actions/session';

class FederationQr extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fedogolf: {},
            hasFedogolf: false
        };
    }

    componentDidMount() {
        const fedogolf = this.props.session.user.data.federations.find(x => x.id === 1)
        console.log(fedogolf)
        if (fedogolf !== {} && fedogolf !== undefined) {
            this.setState({
                fedogolf: fedogolf,
                hasFedogolf: true
            })
        } else {
            this.setState({
                fedogolf: {},
                hasFedogolf: false
            })
        }
    }

    render() {
        return (
            <View>
                {this.state.hasFedogolf &&
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('MembershipQR', {
                            membership: this.state.fedogolf
                        })}
                    >
                        <FontAwesome
                            name='qrcode'
                            size={(35)}
                            color={COLORS.white}
                            style={{ marginRight: (20) }}
                        />
                    </TouchableOpacity>
                }
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        ...state.membership,
        session: state.session,
        user: state.session.user.data,
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators({ session }, dispatch);
}


export default connect(
    mapStateToProps,
    mapDispatchProps
)(FederationQr);