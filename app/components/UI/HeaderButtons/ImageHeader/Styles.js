import { StyleSheet, Dimensions, Platform } from "react-native";

import { verticalScale, moderateScale, scale, widthPercentageToDP } from "../../../../utilities/ScalingScreen";

const MIN_HEIGHT = verticalScale(100);
const MAX_HEIGHT = moderateScale(220);

export default StyleSheet.create({
  headerContainer: {
    height: MAX_HEIGHT,
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: moderateScale(20),
    paddingVertical: verticalScale(10),
    transform: [{ 'translate': [0, 0, 1] }],
  },
  imageTitle: {
    color: 'white',
    fontSize: moderateScale(24),
    fontWeight: 'bold',
    position: 'absolute',
    bottom: verticalScale(25),
    left: moderateScale(25),
    width: widthPercentageToDP('90%')
  },

  headerBackButton: {
    height: 40,
    width: 40,
    backgroundColor: 'white',
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: Platform.OS === 'ios' ? verticalScale(35) : verticalScale(10)
  }
});
