import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar, TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'

import styles from './Styles';

export default function renderForeground({ title }) {
    return (
        <Text style={styles.imageTitle}>{title}</Text>
    )
}