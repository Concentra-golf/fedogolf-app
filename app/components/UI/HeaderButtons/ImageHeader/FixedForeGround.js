import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar, TouchableOpacity } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'

import styles from './Styles';
import { scale } from '../../../../utilities/ScalingScreen';

export default function renderFixedForeground({navigation, title}) {
    return (
        <View style={styles.headerContainer} >
            <TouchableOpacity
                style={styles.headerBackButton}
                onPress={() => navigation.goBack()}
            >
                <FontAwesome5
                    name='arrow-left'
                    size={scale(20)}
                />
            </TouchableOpacity>

            <Text style={styles.imageTitle}>{title}</Text>
        </View>
    )
}