import { StyleSheet } from "react-native";
import { verticalScale, moderateScale } from "../../../../utilities/ScalingScreen";

export default StyleSheet.create({
    container: {
        width: 45,
        height: 45,
        marginTop: verticalScale(15),
        marginLeft: moderateScale(15)
    },
});
