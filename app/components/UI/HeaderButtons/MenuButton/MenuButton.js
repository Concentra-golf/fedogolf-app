import React from 'react';
import {View, TouchableOpacity} from 'react-native';

// Modules

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';

// Styles

import styles from "./Styles";

// Constants

import {COLORS} from '../../../../config/constants';
import { scale } from '../../../../utilities/ScalingScreen';

export default function MenuButton({action, dark}) {
    return (
        <TouchableOpacity onPress={() => action()} style={styles.container}>
            <FontAwesomeIcon icon={faBars} size={scale(24)} color={dark ? COLORS.darkgrey : COLORS.white} />
        </TouchableOpacity>
    )
}
