import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';

// Modules

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';

// Constants

import { COLORS } from '../../../../config/constants';

export default function BackButton({ action, dark }) {
    return (
        <TouchableOpacity onPress={() => action()} style={styles.container}>
            <FontAwesomeIcon icon={faChevronLeft} size={24} color={dark ? COLORS.darkgrey : COLORS.white} />
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        width: 45,
        height: 45,
        marginTop: 18,
        marginLeft: 15
    },
});
