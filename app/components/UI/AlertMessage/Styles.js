import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    marginBottom: 15,
    padding: 5,
    paddingLeft: 10,
    paddingRight: 10,
    borderWidth: 1,
    borderRadius: 5,
    width: 280,
    height: 48
  }
});
