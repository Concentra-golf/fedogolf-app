import React, { Component } from 'react';
import {
    View,
} from 'react-native';

// Constants

import { COLORS } from '../../../config/constants';

// Components

import TextLabel from '../TextLabel';

// Styles

import styles from './Styles';

class AlertMessage extends Component {
    render() {

        // Alert Message Theme

        const theme = {
            success: {
                border: COLORS.success,
                background: COLORS.white,
                label: 'green',
            },
            danger: {
                border: COLORS.danger,
                background: '#FEF6F7',
                label: 'red',
            },
        };

        return (
            <View style={[styles.container, { backgroundColor: theme[this.props.type].background, borderColor: theme[this.props.type].border }]}>

                <TextLabel
                    size={14}
                    color={theme[this.props.type].label}
                >

                    {this.props.message}

                </TextLabel>

            </View>
        );
    }
}

export default AlertMessage;