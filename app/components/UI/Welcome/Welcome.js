import * as React from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    TouchableOpacity,
    KeyboardAvoidingView,
    Keyboard,
    Platform,
    ImageBackground,
    Dimensions,
    SafeAreaView,
    StyleSheet,
    StatusBar,
} from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import * as RNLocalize from 'react-native-localize';

import Button from '../Button'
import BrandLogo from '../BrandLogo/BrandLogo';
// Styles
import bgHeaderLogin from '../../forms/SignUp/Images/bg-header-login.png';
import { moderateScale, verticalScale, scale } from '../../../utilities/ScalingScreen';
import WelcomeHeaderLine from './WelcomeHeaderLine';

import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';

// import styles from './Styles';

class Welcome extends React.Component {
    constructor(props) {
        super(props);
        setI18nConfig();
        this.state = { activeSlide: 0 };
    }

    componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);
    }

    componentWillUnmount() {
        RNLocalize.removeEventListener('change', handleLocalizationChange);
    }

    render() {
        const switchForms = this.props.switchForms;
        return (
            <ImageBackground
                source={require('./images/bk2.jpeg')}
                style={styles.container}
                imageStyle={styles.imageStyle}
                resizeMode="cover"
            >
                <StatusBar barStyle="dark-content" />

                <WelcomeHeaderLine

                />
                {/* <ImageBackground
                    source={bgHeaderLogin}
                    style={styles.lineHeader}
                    imageStyle={styles.imageLineHeader}
                >
                    <View style={styles.logoIconContainer}>
                        <BrandLogo
                            size={scale(100)}
                        />
                    </View>
                </ImageBackground> */}

                <View style={styles.buttonsContainer}>
                    <Button
                        theme='darkblue'
                        // label='Login'
                        label={translate('login')}
                        action={() => switchForms("login")}
                        additionalStyles={{ width: moderateScale(230), marginBottom: verticalScale(10), }}
                    />
                    <Button
                        theme='white'
                        // label='Register'
                        label={translate('register')}
                        action={() => switchForms("signup")}
                        additionalStyles={{ width: moderateScale(230) }}
                    />
                </View>

            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: Dimensions.get('window').height
    },
    imageStyle: {
        height: '100%',
        width: '100%'
    },
    lineHeader: {
        width: '100%',
        height: 155
    },
    imageLineHeader: {
        height: '100%',
        width: '100%'
    },
    logoIconContainer: {
        marginLeft: moderateScale(-15),
        marginTop: verticalScale(-10)
    },
    buttonsContainer: {
        height: '24%',
        width: '80%',
        position: 'absolute',
        bottom: Platform.OS === 'ios' ? verticalScale(40) : verticalScale(10),
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    }
});

export default Welcome
