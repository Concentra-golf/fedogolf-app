import React, { Component } from 'react';
import { View, Text, Dimensions, StyleSheet, Platform } from 'react-native';

import HeaderLine from '../../../assets/images/SVG/HeaderLine'
import { verticalScale, moderateScale, scale } from '../../../utilities/ScalingScreen';
import BrandLogo from '../BrandLogo/BrandLogo';

class WelcomeHeaderLine extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const width = Dimensions.get('window').width
    return (
      <View>
        <View style={{ width: '100%', marginTop: verticalScale(-25) }}>
          <HeaderLine
            width={width}
            height={verticalScale(160)}
          />

          <View style={styles.logoIconContainer}>
            <BrandLogo
              size={Platform.OS === 'OS' ? scale(100) : scale(90)}
            />
          </View>
        </View>
      </View>
    );
  }
}

export default WelcomeHeaderLine;

const styles = StyleSheet.create({
  logoIconContainer: {
    position: 'absolute',
    top: verticalScale(30)
  }
})