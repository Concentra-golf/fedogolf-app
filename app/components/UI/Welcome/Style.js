import { StyleSheet } from "react-native";

import { COLORS } from "../../config/constants";

export default StyleSheet.create({
  loginContainer: {
    flex: 1,
    backgroundColor: COLORS.darkgreen
  },

  signUpContainer: {
    flex: 1,
    backgroundColor: COLORS.white
  },

  scrollViewWrapper: {
    // paddingTop:
  },

  scrollViewWrapperTablet: {
    width: "70%",
    alignSelf: "center"
  },

  brandShade: {
    width: 200,
    height: 200,
    position: "absolute",
    top: 0,
    left: 0
  },

  termsWrapper: {
    marginTop: 40,
    marginBottom: 40,
    marginLeft: 20,
    marginRight: 20
  }
});
