import React, { Component } from "react";
import { TouchableOpacity, ActivityIndicator, StyleSheet } from "react-native";

// Components

import TextLabel from "./TextLabel";

// Constants

import { COLORS } from "../../config/constants";

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import {
  faUserAlt,
  faArrowRight
} from '@fortawesome/free-solid-svg-icons';

class Button extends Component {
  render() {

    const icons = {
      altUser: faUserAlt,
      arrowRight: faArrowRight
    }

    // Button Themes

    const theme = {
      solid: {
        background: COLORS.orange,
        borderColor: COLORS.orange,
        loader: COLORS.white,
        label: "white"
      },
      alternate: {
        background: COLORS.grey,
        borderColor: COLORS.grey,
        loader: COLORS.white,
        label: "white"
      },
      success: {
        background: COLORS.success,
        borderColor: COLORS.success,
        loader: COLORS.white,
        label: "white"
      },
      outline: {
        background: "transparent",
        borderColor: COLORS.lightorange,
        loader: COLORS.lightorange,
        label: "lightorange"
      },
      outlinedark: {
        background: "transparent",
        borderColor: COLORS.orange,
        loader: COLORS.orange,
        label: "orange"
      },
      outlinegrey: {
        background: "transparent",
        borderColor: COLORS.grey,
        loader: COLORS.grey,
        label: "grey"
      },
      transparent: {
        background: "transparent",
        borderColor: COLORS.white,
        loader: COLORS.white,
        label: "white"
      },
      white: {
        background: "white",
        borderColor: COLORS.white,
        loader: COLORS.green,
        label: "green"
      },
      danger: {
        background: "transparent",
        borderColor: COLORS.danger,
        loader: COLORS.danger,
        label: COLORS.black
      },
      darkblue: {
        background: COLORS.darkblue,
        borderColor: COLORS.darkblue,
        loader: COLORS.white,
        label: "white"
      },
      lightblue: {
        background: COLORS.lightblue,
        borderColor: COLORS.lightblue,
        loader: COLORS.white,
        label: "white"
      },
      lightblueOutline: {
        background: "white",
        borderColor: COLORS.lightblue,
        loader: COLORS.white,
        label: COLORS.lightblue
      },
      darkGreen: {
        background: COLORS.darkGreen,
        borderColor: COLORS.white,
        loader: COLORS.white,
        label: "white"
      },
      green: {
        background: COLORS.midgreen,
        borderColor: COLORS.white,
        loader: COLORS.white,
        label: "white"
      },
      lightGreen: {
        background: COLORS.lightGreen,
        borderColor: "white",
        loader: COLORS.white,
        label: "white"
      },
      facebook: {
        background: COLORS.facebookBlue,
        borderColor: COLORS.facebookBlue,
        loader: COLORS.white,
        label: "white"
      },
      settingsButton: {
        background: COLORS.white,
        borderColor: COLORS.white,
        label: COLORS.black,
        loader: COLORS.black
      },
      redTheme: {
        background: COLORS.red,
        borderColor: COLORS.red,
        label: 'white',
        loader: COLORS.white
      }
    };



    // Button Styles

    const styles = StyleSheet.create({
      button: {
        // width: '100%',
        height: 50,
        paddingLeft: 25,
        paddingRight: 25,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: this.props.theme
          ? theme[this.props.theme].background
          : theme.solid.borderColor,
        borderWidth: this.props.theme === "transparent" ? 2 : 1,
        borderColor: this.props.theme
          ? theme[this.props.theme].borderColor
          : theme.solid.borderColor,
        borderRadius: 25,
        opacity: this.props.disabled ? 0.5 : 1
      },
      shadow: this.props.shadow
        ? {
          shadowOffset: { width: 0, height: 3 },
          shadowColor: COLORS.darkblue,
          shadowOpacity: 1.0,
          shadowRadius: 10,
          elevation: 1
        }
        : {},
      additionalStyles: this.props.additionalStyles
        ? this.props.additionalStyles
        : {},
      buttonText: {
        textAlign: this.props.align
          ? this.props.align
          : "center",
      }
    });

    return (
      <TouchableOpacity
        activeOpacity={0.8}
        disabled={this.props.disabled}
        style={[styles.button, styles.shadow, styles.additionalStyles]}
        onPress={this.props.isLoading ? null : this.props.action}
      >
        {this.props.isLoading ? (
          <ActivityIndicator
            size="small"
            color={
              this.props.theme
                ? theme[this.props.theme].loader
                : theme.solid.loader
            }
          />
        ) : (
            <TextLabel
              weight={"bold"}
              size={16}
              color={
                this.props.theme
                  ? theme[this.props.theme].label
                  : theme.solid.label
              }
              additionalStyles={styles.buttonText}
            >
              {this.props.icon ? <FontAwesomeIcon icon={icons[this.props.icon]} style={{ marginRight: 15 }} /> : null} {this.props.label}
              {this.props.secondIcon ? <FontAwesomeIcon icon={icons[this.props.secondIcon]} style={{ paddingright: 15 }} /> : null} </TextLabel>
          )}
      </TouchableOpacity>
    );
  }
}

export default Button;
