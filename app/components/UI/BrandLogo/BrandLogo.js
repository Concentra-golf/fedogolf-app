import React from 'react';
import { View, Image } from "react-native";

// Styles

import styles from "./Styles";

// Images

const logo = require('./Images/img-brand.png');

export default function BrandLogo(props) {
    return (
        <Image source={logo} style={[styles.logo, props.size && { height: props.size, width: props.size }]} />
    )
}
