import { StyleSheet } from "react-native";

export default StyleSheet.create({
    logo: {
        width: 105,
        height: 117,
        marginLeft: 25,
        marginTop: 5,
    },
});