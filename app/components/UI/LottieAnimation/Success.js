import React, { Component } from 'react';
import { View, Animated, Dimensions, StyleSheet } from 'react-native';
import LottieView from 'lottie-react-native';
import { verticalScale, moderateScale } from '../../../utilities/ScalingScreen.js';

let screenHeight = Dimensions.get('window').height;

class Success extends Component {
    constructor(props) {
        super(props);
        this.state = {
            top: new Animated.Value(0),
            opacity: new Animated.Value(0)
        };
    }

    componentDidMount() {
        // this.animation.play();
    }

    componentDidUpdate() {
        if (this.props.isActive) {
            Animated.timing(this.state.top, { toValue: 0, duration: 0 }).start();
            Animated.timing(this.state.opacity, { toValue: 1 }).start();

            this.animation.play()
        } else {
            Animated.timing(this.state.top, { toValue: screenHeight, duration: 0 }).start();
            Animated.timing(this.state.opacity, { toValue: 0 }).start();

            this.animation.loop = false;
        }
    }

    render() {
        return (
            <LottieView
                source={require('../../../assets/lottieAnimations/16680-success.json')}
                autoPlay={true}
                loop={false}
                ref={animation => { this.animation = animation }}
                style={{ width: moderateScale(250), height: verticalScale(100)}}

            />
        );
    }
}

export default Success;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(255,255,255,0.9)',
        position: 'absolute',
        top: 0,
        bottom: 0,
        backgroundColor: 'red',
    }
})
