import { StyleSheet, Platform } from 'react-native';
import { verticalScale } from '../../../utilities/ScalingScreen';

export default StyleSheet.create({
    container: {
        marginBottom: verticalScale(20)
    },
    headerInner: {
        padding: 15,
        paddingTop: Platform.OS === "ios" ? verticalScale(90) : verticalScale(55),
        paddingBottom: verticalScale(30)
    },
});
