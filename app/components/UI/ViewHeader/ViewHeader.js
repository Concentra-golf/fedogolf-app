import React from 'react';
import { View, ImageBackground } from 'react-native';

// Styles

import styles from './Styles';

// Images

const backgrounds = {
  default: require('../../../assets/images/big-header.png'),
  bgUserCard: require('./Images/bg-drawer-user-card.jpg')

}

export default function ViewHeader(props) {
  return (
    <View style={[styles.container, props.additionalStyles ? props.additionalStyles : {}]}>

      {/* <ImageBackground source={props.background ? backgrounds[props.background] : backgrounds.default} resizeMode={props.resizeMode ? props.resizeMode : "cover"} style={{ width: '100%' }}> */}
      <ImageBackground
        // resizeMethod={'auto'}
        source={props.background ? backgrounds[props.background] : backgrounds.default}

        imageStyle={{
          resizeMode: "cover",
          alignSelf: "flex-end"
        }}

        style={{ width: '100%' }}
      >
        <View style={styles.headerInner}>

          {props.children}

        </View>

      </ImageBackground>

    </View>
  )
}
