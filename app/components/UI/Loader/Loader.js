import React, { Component } from 'react';
import { View, Image, ActivityIndicator } from 'react-native';

// UI

import TextLabel from '../TextLabel';

// Styles

import styles from './Styles';

class Loader extends Component {
  render() {
    return (
      <View style={styles.wrapper}>
        <View style={styles.container}>
          {this.props.errorState === true ? (
            <View style={styles.containerInner}>
              <Image
                source={require('./Images/icn-error.png')}
                style={this.props.small ? styles.iconSmall : styles.icon}
              />

              <TextLabel
                color={this.props.lightTheme ? 'white' : 'grey'}
                weight={'bold'}
                additionalStyles={styles.errorMessage}>
                {this.props.errorMessage}
              </TextLabel>

              {/* Load aditional components */}
              {this.props.children}
            </View>
          ) : this.props.nothing === true ? (
            <View style={styles.containerInner}>
              <Image
                source={require('./Images/interface.png')}
                style={this.props.small ? styles.iconSmall : styles.icon}
              />

              <TextLabel
                color={this.props.lightTheme ? 'white' : 'grey'}
                weight={'bold'}
                additionalStyles={styles.errorMessage}>
                {this.props.nothingMessage}
              </TextLabel>

              {/* Load aditional components */}
              {this.props.children}
            </View>
          ) : (
                <View>
                  <ActivityIndicator
                    size="large"
                    color={this.props.lightTheme ? 'white' : '#CCCCCC'}
                  />
                  <TextLabel
                    size={13}
                    color={this.props.lightTheme ? 'white' : 'grey'}
                    transform={'uppercase'}
                    additionalStyles={styles.loadingMessage}>
                    {this.props.loadingMessage}
                  </TextLabel>
                </View>
              )}
        </View>
      </View>
    );
  }
}

export default Loader;
