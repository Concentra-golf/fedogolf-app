import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  wrapper: {
    width: '100%',
    flex: 1,
    alignItems: 'center',
  },

  container: {
    width: '80%',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 'auto',
    marginRight: 'auto',
  },

  containerInner: {
    width: '100%',
    alignItems: 'center',
  },

  icon: {
    width: 80,
    height: 80,
    marginBottom: 25,
  },

  iconSmall: {
    width: 60,
    height: 60,
    marginBottom: 15,
  },

  errorMessage: {
    marginBottom: 15,
    textAlign: 'center',
    lineHeight: 31,
  },

  loadingMessage: {
    marginTop: 15,
  },
});
