import React, { Component } from "react";
import { Text, StyleSheet } from "react-native";

// Constants

import { COLORS } from "../../config/constants";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faUserAlt } from "@fortawesome/free-solid-svg-icons";
import { moderateScale } from "../../utilities/ScalingScreen";

// Components

export default class TextLabel extends Component {
  render() {
    // Font Colors

    const colors = {
      grey: COLORS.grey,
      lightgrey: COLORS.lightgrey,
      white: COLORS.white,
      default: COLORS.darkgrey,
      green: COLORS.green,
      lightblue: COLORS.lightblue,
      darkblue: COLORS.darkblue,
      lightgreen: COLORS.lightGreen,
      midgreen: COLORS.midgreen,
      danger: COLORS.red,
      softBlack: COLORS.softBlack
    };

    // Font & Weight

    const font = {
      primary: "Lato-Regular",
      primarylight: "Lato-Light",
      primarybold: "Lato-Bold",
      secondary: "Roboto-Regular",
      secondarybold: "Roboto-Bold"
    };

    // Text Transform

    const transform = {
      lowercase: "lowercase",
      uppercase: "uppercase",
      capitalize: "capitalize",
      default: "none"
    };

    // Text align

    const align = {
      default: "left",
      right: "right",
      center: "center",
    }

    // Styles

    const styles = StyleSheet.create({
      textStyles: {
        fontFamily: this.props.font
          ? font[this.props.font]
          : font.primary,
        fontSize: this.props.size ? this.props.size : moderateScale(16),
        color: this.props.color ? colors[this.props.color] : colors.default,
        textTransform: this.props.transform
          ? transform[this.props.transform]
          : transform.default,
        textAlign: this.props.align ? align[this.props.align] : align.default
        , fontWeight: this.props.weight || 'normal',
      },
      sectionHeading: this.props.heading ? { marginBottom: 15 } : {},
      additionalStyles: this.props.additionalStyles
        ? this.props.additionalStyles
        : {}
    });

    return (
      <Text
        style={[
          styles.textStyles,
          styles.sectionHeading,
          styles.additionalStyles
        ]}
        numberOfLines={this.props.numberOfLines ? this.props.numberOfLines : 0}
        ellipsizeMode={this.props.ellipsizeMode ? this.props.ellipsizeMode : 'tail'}
      >
        {this.props.children}
      </Text>
    );
  }
}
