import React, { Component } from "react";
import { Text, View, ImageBackground, Image, StyleSheet } from "react-native";

// Components

import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import {
  faUsers,
  faUserAlt,
  faChartBar,
  faCreditCard,
  faBell,
  faCogs,
  faGolfBall,
  faQrcode,
  faHandHoldingUsd,
  faReceipt,
  faInfo,
  faFile,
  faBook,
  faQuestion
} from "@fortawesome/free-solid-svg-icons";
import { faSignOutAlt } from "@fortawesome/free-solid-svg-icons";

// Images


import { TouchableOpacity } from 'react-native-gesture-handler';
import TextLabel from './TextLabel';
import { verticalScale, moderateScale, scale, heightPercentageToDP } from '../../utilities/ScalingScreen';
import { COLORS } from '../../config/constants';

export default class SettingsButton extends Component {
  render() {
    const icons = {
      users: (
        <FontAwesomeIcon
          size={moderateScale(20)}
          icon={faUsers}
          style={styles.iconStyles}
        />
      ),
      altUser: (
        <FontAwesomeIcon
          size={moderateScale(20)}
          icon={faUserAlt}
          style={styles.iconStyles}
        />
      ),
      stats: (
        <FontAwesomeIcon
          size={moderateScale(20)}
          icon={faChartBar}
          style={styles.iconStyles}
        />
      ),
      notifications: (
        <FontAwesomeIcon
          size={moderateScale(20)}
          icon={faBell}
          style={styles.iconStyles}
        />
      ),
      membership: (
        <FontAwesomeIcon
          size={moderateScale(20)}
          icon={faUsers}
          style={styles.iconStyles}
        />
      ),
      payments: (
        <FontAwesomeIcon
          size={moderateScale(20)}
          icon={faCreditCard}
          style={styles.iconStyles}
        />
      ),
      configuration: (
        <FontAwesomeIcon
          size={moderateScale(20)}
          icon={faCogs}
          style={styles.iconStyles}
        />
      ),
      golf: (
        <FontAwesomeIcon
          size={moderateScale(20)}
          icon={faGolfBall}
          style={styles.iconStyles}
        />
      ),
      qrCode: (
        <FontAwesomeIcon
          size={moderateScale(20)}
          icon={faQrcode}
          style={styles.iconStyles}
        />
      ),
      benefits: (
        <FontAwesomeIcon
          size={moderateScale(20)}
          icon={faHandHoldingUsd}
          style={styles.iconStyles}
        />
      ),
      receipt: (
        <FontAwesomeIcon
          size={moderateScale(20)}
          icon={faReceipt}
          style={styles.iconStyles}
        />
      ),
      signOut: (
        <FontAwesomeIcon
          size={moderateScale(20)}
          icon={faSignOutAlt}
          style={styles.iconStyles}
        />
      ),
      info: (
        <FontAwesomeIcon
          size={moderateScale(20)}
          icon={faInfo}
          style={styles.iconStyles}
        />
      ),
      termsOfUse: (
        <FontAwesomeIcon
          size={moderateScale(20)}
          icon={faBook}
          style={styles.iconStyles}
        />
      ),
      privacyPolicies: (
        <FontAwesomeIcon
          size={moderateScale(20)}
          icon={faFile}
          style={styles.iconStyles}
        />
      ),
      aboutGolfertek: (
        <FontAwesomeIcon
          size={moderateScale(20)}
          icon={faQuestion}
          style={styles.iconStyles}
        />
      )
    };
    return (
      <TouchableOpacity
        style={styles.buttons}
        onPress={this.props.isLoading ? null : () => this.props.action()}
      >
        <TextLabel
          weight={"bold"}
          size={moderateScale(16)}
          font={"secondary"}
          colors={"softBlack"}
          additionalStyles={
            this.props.additionalStyles ? this.props.additionalStyles : {}
          }
        >
          {this.props.icon ? icons[this.props.icon] : ""}{" "}
          {"  " + this.props.label}
        </TextLabel>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
    buttons: {
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: "#D6D6D6",
        borderRadius: 0,
        backgroundColor: "#FFFFFF",
        padding: scale(10),
        width: '100%',
        height: heightPercentageToDP('9%')
    },
    iconStyles: {
        color: COLORS.softBlack,
        marginLeft: moderateScale(14),
        textAlign: 'center'
    }
})
