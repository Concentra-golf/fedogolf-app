import React, { Component } from 'react';
import {
    Image,
    StyleSheet,
    Text,
    View,
    Platform,
    Dimensions,
    TouchableOpacity,
    FlatList,
    TouchableHighlight
} from 'react-native';

import { moderateScale, scale, heightPercentageToDP, widthPercentageToDP, verticalScale } from '../../../utilities/ScalingScreen';
import { COLORS } from '../../../config/constants';

export default class FilterButtons extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showText: this.props.showText,
            showLeftButton: this.props.showLeftButton,
            getFilters: [],
        };
    }

    render() {
        const selected = this.props.activeIndex
        const data = this.props.data
        return (
            <View style={styles.container}>
                <FlatList
                    horizontal
                    data={data}
                    extraData={this.props.activeIndex}
                    keyExtractor={(item) => item.id}
                    contentContainerStyle={styles.listContainer}
                    renderItem={({ item }) =>
                        <TouchableOpacity
                            key={item.id}
                            onPress={() => this.props.renderList(item.key)}
                            style={[styles.buttonNotSelected,  this.props.activeIndex  === item.key  ? styles.button : {}]}
                        >
                            <Text style={[styles.buttonTextNotSelected, this.props.activeIndex === item.key? styles.buttonText : {}]}>{item.name}</Text>
                        </TouchableOpacity>
                    }
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        height: heightPercentageToDP('8%'),
        paddingHorizontal: moderateScale(10),
        marginTop: verticalScale(-15)
    },
    listContainer: {
        width: '100%',
        justifyContent: 'space-evenly',
        alignItems: 'center',
    },
    button: {
        borderRadius: 25,
        justifyContent: 'center',
        alignItems: 'center',
        width: widthPercentageToDP('28%'),
        height: heightPercentageToDP('5%'),
        backgroundColor: COLORS.green
    },
    buttonText: {
        fontSize: scale(14),
        fontWeight: 'bold',
        color: 'white',
        textAlign: 'center',
    },
    buttonNotSelected: {
        justifyContent: 'center',
        alignItems: 'center',
        width: widthPercentageToDP('28%'),
        height: heightPercentageToDP('5%')
    },
    buttonTextNotSelected: {
        fontSize: scale(14),
        fontWeight: 'bold',
        color: COLORS.softBlack,
        textAlign: 'center',
    }
});
