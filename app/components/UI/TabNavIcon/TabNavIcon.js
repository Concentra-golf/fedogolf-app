import React from 'react';
import { View, Image } from 'react-native';

// Styles

import styles from './Styles';

const icons = {
  home: require('./Images/icn-home.png'),
  play: require('./Images/icn-play.png'),
  courses: require('./Images/icn-courses.png'),
  schedule: require('./Images/icn-schedule.png'),
  settings: require('./Images/icn-settings.png'),
};

export default class TabNavIcon extends React.Component {
  render() {
    //We use this to hide the courses icon when you open courses details.
    let style = styles.iconLarge;
    if (this.props.route.name === 'Courses' || this.props.route.name === 'Campos') {
      if (this.props.route.state && this.props.route.state.index > 0) {
        style = {
          width: 0,
          height: 0,
        }
      } else {
        style = styles.iconLarge
      }
    } else if (this.props.route.name === 'Play' || this.props.route.name === 'Jugar') {
      if (this.props.route.state && this.props.route.state.index > 0) {
        style = {
          width: 0,
          height: 0,
        }
      } else {
        style = styles.iconLarge
      }
    }
    return (
      <View style={this.props.large ? styles.wrapper : {}}>
        <Image
          style={this.props.large ? style : styles.icon}
          source={icons[this.props.name]}
        />
      </View>
    );
  }
}
