import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  wrapper: {
    marginBottom: 35,
    width: 60,
    height: 60,
    alignItems: 'center',
  },
  icon: {
    width: 26,
    height: 26,
  },
  iconLarge: {
    width: 60,
    height: 60,
  },
});
