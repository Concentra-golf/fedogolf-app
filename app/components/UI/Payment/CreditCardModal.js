import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  Image,
  FlatList,
  ActivityIndicator,
  SafeAreaView,
  StatusBar
} from 'react-native';
import { CreditCardInput } from "react-native-credit-card-input";

import { Icon } from 'native-base';
import { moderateScale, verticalScale, scale } from '../../../utilities/ScalingScreen';
import { COLORS } from '../../../config/constants';
import Ionicons from 'react-native-vector-icons/dist/Ionicons'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

export default class CreditCardModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      payService: false,
      step: 1,
      buttonClicked: false,
      creditCard: {}
    }
    this.getCreditCard = this.getCreditCard.bind(this);
    this.selectedItem = this.selectedItem.bind(this);
  }


  getCreditCard(e) {
    const creditCard = {
      number: e.values.number,
      cvc: e.values.cvc,
      expiry: e.values.expiry,
      name: e.values.name,
      type: e.values.type
    }

    this.setState({
      creditCard: creditCard
    })

  }

  addCreditCard() {
    this.props.getCreditCardValues(this.state.creditCard, this.state.buttonClicked);
  }

  selectedItem(e) {
    this.setState({
      buttonClicked: e
    })
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="dark-content" />
        <KeyboardAwareScrollView >

          <View style={styles.header}>
            <TouchableOpacity
              onPress={() => this.props.close(false)}
            >
              <Ionicons
                name='ios-close'
                size={40}
                color={COLORS.airBnbLightGrey}
              />
            </TouchableOpacity>

            <Text style={styles.mainTitle}>Add Credit Card</Text>
          </View>

          <View style={styles.cardInfoContainer}>

            <CreditCardInput
              onChange={this.getCreditCard}
              requiresName
              labelStyle={{
                color: COLORS.airBnbGrey,
              }}
              inputStyle={{
                borderWidth: 1,
                borderRadius: 10,
                paddingHorizontal: moderateScale(10),
                marginTop: moderateScale(10),
                height: verticalScale(40)
              }}
              inputContainerStyle={{

              }}
            />
          </View>

          {this.props.route !== 'addCard' &&
            <TouchableOpacity
              onPress={(e) => this.selectedItem(!this.state.buttonClicked)}
              style={styles.saveContainer}
            >
              <View style={styles.wrapper}>
                <View style={styles.radioButon}>
                  {
                    this.state.buttonClicked &&
                    <View style={{
                      width: 20,
                      height: 20,
                      borderRadius: 20 / 2,
                      backgroundColor: COLORS.green,
                    }}
                    />
                  }
                </View>
                <Text style={styles.radioButonText}>Save this Card</Text>
              </View>
            </TouchableOpacity>
          }

          <View style={styles.buttonContainer}>
            <TouchableOpacity
              onPress={() => this.addCreditCard()}
              style={styles.payButton}
            >
              {this.props.addingCard ? (
                <ActivityIndicator color='white' />
              ) : (
                  <Text style={styles.payText}>{this.props.route !== 'addCard' ? 'Proceed to confirm' : 'Save Card'}</Text>
                )
              }
            </TouchableOpacity>
          </View>

        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  header: {
    width: '100%',
    height: verticalScale(70),
    padding: scale(20),
    marginBottom: verticalScale(30)
  },
  mainTitle: {
    fontSize: moderateScale(25),
    fontWeight: Platform.OS === 'ios' ? '600' : 'bold',
    color: COLORS.lightBlack,
  },
  cardInfoContainer: {
    width: '100%',
  },
  saveContainer: {
    width: '100%',
    marginTop: scale(20),
    paddingHorizontal: scale(20)
  },
  wrapper: {
    marginLeft: moderateScale(20),
    flexDirection: 'row',
    alignItems: 'center',
  },
  radioButon: {
    width: 25,
    height: 25,
    borderRadius: 25 / 2,
    borderWidth: 1,
    marginRight: moderateScale(20),
    justifyContent: 'center',
    alignItems: 'center'
  },
  radioButonText: {
    fontSize: moderateScale(14),
    color: COLORS.lightBlack,
    fontWeight: '600'
  },
  buttonContainer: {
    width: '100%',
    height: Platform.OS === 'ios' ? verticalScale(60) : verticalScale(70),
    marginTop: verticalScale(20),
    marginBottom: Platform.OS === 'ios' ? verticalScale(20) : verticalScale(30),
    justifyContent: 'center',
    alignItems: 'center'
  },
  payButton: {
    width: moderateScale(320),
    height: verticalScale(45),
    backgroundColor: COLORS.green,
    justifyContent: 'center',
    alignItems: 'center'
  },
  payText: {
    fontSize: moderateScale(14),
    color: '#fff',
    fontWeight: 'bold'
  },
  button: {
    width: moderateScale(250),
    height: verticalScale(40),
    backgroundColor: COLORS.lightblue,
    borderRadius: scale(50),
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: moderateScale(18),
    margin: scale(20)
  }
});
