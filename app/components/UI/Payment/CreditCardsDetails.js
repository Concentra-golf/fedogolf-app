import React, { Component } from 'react';
import { View, Text, Alert, StyleSheet, TouchableOpacity, SafeAreaView, StatusBar, Platform } from 'react-native';
import Ionicons from 'react-native-vector-icons/dist/Ionicons'
import { COLORS } from '../../../config/constants';
import { moderateScale, verticalScale, scale } from '../../../utilities/ScalingScreen';

class CreditCardsDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cardInfo: {},
            loading: true
        };
    }

    componentDidMount() {
        String.prototype.splice = function (idx, rem, str) {
            return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
        };
        if (this.props.cardData !== null) {
            const cardNumber = this.props.cardData.card_number.substring(9);
            const expiration = this.props.cardData.expiration.splice(4, 0, " / ");

            this.setState({
                cardBrand: this.props.cardData.card_brand,
                cardNumber: cardNumber,
                expiration: expiration,
                loading: false
            })
        }

    }


    deleteCard(data) {
        Alert.alert(
            'Are you sure yo want to delete this payment method?',
            '',
            [
                { text: 'Yes', onPress: (e) => this.props.deletePayment(data) },
                { text: 'Cancel', onPress: () => console.log('hey') },
            ])
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar barStyle="dark-content" />

                <View style={styles.header}>
                    <View style={styles.wrapper}>
                        <TouchableOpacity
                            onPress={() => this.props.close(false)}
                        >
                            <Ionicons
                                name='ios-close'
                                size={40}
                                color={COLORS.airBnbLightGrey}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this.deleteCard(this.props.cardData)}
                        >
                            <Ionicons
                                name='ios-remove-circle'
                                size={35}
                                color={COLORS.airBnbLightGrey}
                            />
                        </TouchableOpacity>
                    </View>

                    {!this.state.loading && <Text style={styles.mainTitle}>{this.state.cardBrand}</Text>}
                </View>

                {!this.state.loading &&
                    <View style={styles.contentContainer}>
                        <View style={styles.textContainer}>
                            <Text style={styles.title}>Card Number</Text>
                            <Text style={styles.subtitle}>•••• •••• •••• {this.state.cardNumber}</Text>
                        </View>

                        <View style={styles.textContainer}>
                            <Text style={styles.title}>Expiry Date</Text>
                            <Text style={styles.subtitle}>{this.state.expiration}</Text>
                        </View>
                    </View>
                }

            </SafeAreaView>
        );
    }
}

export default CreditCardsDetails;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    header: {
        width: '100%',
        // height: verticalScale(70),
        padding: scale(20),
        marginBottom: verticalScale(30)
    },
    wrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    mainTitle: {
        marginTop: verticalScale(10),
        fontSize: moderateScale(25),
        fontWeight: Platform.OS === 'ios' ? '600' : 'bold',
        color: COLORS.lightBlack,
    },
    contentContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    textContainer: {
        marginTop: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#D8D8D8',
        paddingVertical: 10,
        width: '90%',
        flexDirection: "row",
        justifyContent: "space-between"
    },
    title: {
        fontSize: moderateScale(14),
        color: COLORS.airBnbGrey,
        fontWeight: '500'
    },
    subtitle: {
        fontSize: moderateScale(16),
        color: COLORS.softBlack,
        fontWeight: "bold"
    }
})