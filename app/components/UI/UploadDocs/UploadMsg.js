import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/dist/FontAwesome5';
import { COLORS } from '../../../config/constants';
import { verticalScale, moderateScale } from '../../../utilities/ScalingScreen';

class UploadMsg extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={styles.container}>
                <FontAwesome5
                    name={this.props.icon}
                    size={100}
                    color={this.props.color}
                    style={styles.icon}
                />
                <Text style={styles.title}>{this.props.msg}</Text>
            </View>
        );
    }
}

export default UploadMsg;

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%'
    },
    icon: {
        marginBottom: verticalScale(15)
    },
    title: {
        fontSize: moderateScale(18),
        color: COLORS.grey,
        fontWeight: 'bold'
    }
})