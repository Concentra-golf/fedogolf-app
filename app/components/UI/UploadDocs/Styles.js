import { StyleSheet, Platform } from "react-native";
import { COLORS } from "../../../config/constants";
import { verticalScale, moderateScale, scale } from "../../../utilities/ScalingScreen";

export default StyleSheet.create({
    uploadVoucherContainer: {
        width: '100%',
        justifyContent: 'center',
        padding: scale(10),
        marginTop: verticalScale(20),
    },
    uploadTitle: {
        fontSize: moderateScale(16),
        marginBottom: moderateScale(25),
        fontWeight: '600',
    },
    fileContainer: {
        borderWidth: 1,
        width: moderateScale(280),
        height: verticalScale(40),
        padding: scale(10),
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: moderateScale(15),
        borderRadius: scale(10)
    },
    fileIcon: {
        marginRight: moderateScale(15),
        fontSize: moderateScale(20)
    },
    fileTitle: {
        fontSize: moderateScale(16)
    },
    uploadButton: {
        backgroundColor: COLORS.lightblue,
        height: verticalScale(40),
        width: '80%',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: verticalScale(20)
    },
    selectDoc: {
        backgroundColor: COLORS.lightblue,
        height: verticalScale(40),
        width: moderateScale(130),
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    uploadButtonText: {
        color: COLORS.white,
        fontSize: moderateScale(14),
        fontWeight: '600'
    },
    buttonsContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        paddingBottom: verticalScale(50)
    },
    buttonFileContainers: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    clearIconContainer: {
        borderRadius: scale(5),
        marginLeft: moderateScale(10),
        marginTop: verticalScale(5),
        backgroundColor: 'red',
        width: 40, 
        height: 40,
        borderRadius: (40 / 2),
        justifyContent: 'center',
        alignItems: 'center'
    },
    clearIcon: {
        color: '#FFF',
        fontSize: moderateScale(25),
        fontWeight: 'bold'
    },
    errorMsg: {
        color: 'red',
        fontSize: moderateScale(16),
        textAlign: 'center',
        marginTop: verticalScale(15)
    },
    rowContainer: {
        marginTop: verticalScale(20),
        borderBottomWidth: 1,
        borderBottomColor: '#D8D8D8',
        paddingVertical: 10,
        width: '100%',
        flexDirection: "row",
        justifyContent: "space-between",
        paddingBottom: verticalScale(10)
    },
    paymentTitle: {
        fontSize: moderateScale(18),
        color: COLORS.airBnbGrey, 
        fontWeight: 'bold'
    },
    paymentContainer: {
        paddingBottom: Platform.OS === 'ios' ? verticalScale(25) : verticalScale(50)
    },
    title: {
        fontSize: moderateScale(17),
        color: COLORS.airBnbGrey,
        fontWeight: 'normal'
    },
    subtitle: {
        fontSize: moderateScale(14),
        fontWeight: "bold"
    },
    paymentButtonContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    submitButton: {
        width: moderateScale(280)
    }
});
