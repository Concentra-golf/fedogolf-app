import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';

import MaterialCommunityIcons from "react-native-vector-icons/dist/MaterialCommunityIcons";

import styles from './Styles';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import Loader from '../../../components/UI/Loader/Loader';

import * as session from '../../../store/actions/session';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import UploadMsg from './UploadMsg';
import { COLORS } from '../../../config/constants';

import { ListItem, CheckBox, Body } from 'native-base';
import { verticalScale } from '../../../utilities/ScalingScreen';
import Button from '../Button';

class UploadDocs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            file: false,
            doc: false,
            select: false,
            docName: '',
            docFile: {},
            hasFile: false,
            fileUploaded: false,
            fileAlready: false,
            error: false,
            typeError: false,
            options: true,
            titleMsg: 'Did you make the payment?'
        };
    }

    componentDidMount() {
        const dataInfo = this.props.dataInfo;
        if (dataInfo && dataInfo.payment_link !== null) {
            this.setState({ fileAlready: true })
        } else {
            this.setState({ fileAlready: false })
        }

    }

    async selectDocument() {
        this.setState({ loading: true })

        const doc = this.state.docFile;
        const id = this.props.dataInfo.id;
        const token = this.props.session.accessToken;

        let response = await this.props.uploadDomcumentToRequest(id, doc, token)

        if (this.props.session.docUpload.success) {
            this.setState({
                loading: false,
                success: true,
                msg: 'Upload success!',
                fileUploaded: true
            })
            setTimeout(() => {
                this.setState({
                    success: false,
                    fileUploaded: false,
                    hasFile: false,
                    select: true,
                    fileSelected: false
                })
            }, 2000);
            this.props.reRender()
            this.props.reRender02();
        } else {
            this.setState({
                loading: false,
                error: true,
                msg: 'Upload error!',
                fileUploaded: false
            })
        }

    }

    async uploadDocument() {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            })
            const name = res.name;
            const ext = name.substring(name.length - 3);
            const permittedFiles = ['jpg', 'jpeg', 'png', 'pdf'];
            const load = permittedFiles.find((item) => item === ext);

            if (load) {
                console.log('doc')
                this.setState({
                    hasFile: true,
                    docName: res.name,
                    docFile: res,
                    select: false,
                    fileSelected: true
                })
            } else {
                this.setState({
                    typeError: true,
                    msg: 'Please upload file with this extentions jpg, jpeg, png, pdf'
                })
            }


        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }

    uploadImage = async () => {
        const options = {
            title: 'Select Avatar',
            // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
            maxWidth: 73,
            maxHeight: 73
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                console.log('response', response)

                this.setState({
                    hasFile: true,
                    docName: response.fileName,
                    docFile: response,
                    select: false
                })
            }
        });
    }

    deleteFile() {
        console.log('deleting file')
        this.setState({
            hasFile: false,
            docName: '',
            docFile: {},
            select: true,
            fileUploaded: false,
            fileSelected: false
        })
    }

    optionSelected() {
        this.setState({
            select: true,
            options: false,
            titleMsg: 'Upload payment voucher'
        })
    }

    render() {
        return (
            <View style={styles.uploadVoucherContainer}>

                {!this.state.fileUploaded && !this.state.loading && !this.state.fileAlready && !this.state.error &&
                    <View>
                        <View style={styles.buttonFileContainers}>
                            {/* <Text style={styles.uploadTitle}>{this.state.titleMsg}</Text> */}

                            {this.state.hasFile &&
                                <View style={styles.fileContainer}>
                                    <MaterialCommunityIcons name='file-document' style={styles.fileIcon} />
                                    <Text style={styles.fileTilte}>{this.state.docName}</Text>
                                </View>
                            }

                            {this.state.hasFile &&
                                <TouchableOpacity
                                    onPress={() => this.deleteFile()}
                                    style={styles.clearIconContainer}>
                                    <Ionicons name='md-close' style={styles.clearIcon} />
                                </TouchableOpacity>
                            }
                        </View>

                            <View style={styles.paymentContainer}>
                                <Text style={styles.paymentTitle}>Select a payment method</Text>

                                <TouchableOpacity
                                    style={styles.rowContainer}
                                    onPress={() => this.uploadDocument()}
                                >
                                    <Text style={styles.title}>Upload a voucher</Text>
                                    <MaterialCommunityIcons
                                        name='receipt'
                                        size={33}
                                        style={{ color: 'black' }}
                                    />
                                </TouchableOpacity>

                                <TouchableOpacity
                                    style={styles.rowContainer}
                                    onPress={() => this.props.navigation.navigate('OptionsSelector', {
                                        data: this.props.dataInfo,
                                        renewPaymentInfo: null
                                    })}
                                >
                                    <Text style={styles.title}>Credit or debit card</Text>
                                    <MaterialCommunityIcons
                                        name='credit-card'
                                        size={33}
                                        style={{ color: 'black' }}
                                    />
                                </TouchableOpacity>

                            </View>
                        

                        {this.state.fileSelected &&
                            <View style={styles.buttonsContainer}>
                                <TouchableOpacity
                                    style={styles.uploadButton}
                                    onPress={() => this.selectDocument()}
                                >
                                    <Text style={styles.uploadButtonText}>Upload File</Text>
                                </TouchableOpacity>
                            </View>
                        }

                    </View>
                }

                {this.state.typeError && <Text style={styles.errorMsg}>{this.state.msg}</Text>}

                {this.state.success &&
                    <UploadMsg
                        icon={'check-circle'}
                        msg={this.state.msg}
                        color={COLORS.midgreen}
                    />
                }

                {this.state.error &&
                    <Loader
                        errorState={true}
                        errorMessage={this.state.msg}
                    />
                }

                {this.state.loading &&
                    <Loader
                        loadingMessage={'Uploading Documents'}
                    />
                }

            </View >
        );
    }
}

function mapStateToProps(state) {
    return {
        session: state.session,
        user: state.session.user.data,
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators(session, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchProps
)(UploadDocs);

const paymentOptions = [
    {
        key: '1',
        option: 'Yes',
        value: true
    },
    {
        key: '2',
        option: 'No',
        value: false
    },
]