import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { scale, moderateScale, verticalScale } from '../../../utilities/ScalingScreen';
import Ionicons from "react-native-vector-icons/dist/Ionicons";
import QRCode from 'react-native-qrcode-svg';
import { COLORS } from '../../../config/constants';

class BenefitsModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showQr: false,
            showDetails: true
        };
        this.showQr = this.showQr.bind(this);
    }

    showQr(e) {
        this.setState({
            showQr: e,
            showDetails: false
        })
    }

    render() {
        const data = this.props.data;
        String.prototype.splice = function (idx, rem, str) {
            return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
        };
        const dataString = data.pivot.code.splice(3, 0, '-');
        const code = dataString.splice(7, 0, '-');
        return (
            <View style={styles.container}>

                <View style={styles.container}>
                    <TouchableOpacity
                        onPress={(e) => this.props.close(e)}
                        style={{
                            position: 'absolute',
                            top: verticalScale(0),
                            right: moderateScale(15),
                            zIndex: 1
                        }}
                    >
                        <Ionicons
                            name='ios-close'
                            size={moderateScale(32)}
                        />
                    </TouchableOpacity>

                    <Text style={styles.title}>{data.name}</Text>

                    {/* <View style={styles.qrContainer}>

                        <QRCode
                            value={data.pivot.code}
                            size={170}
                        />

                        <Text style={styles.qrCode}>{code}</Text>
                    </View> */}

                    <View style={styles.line} />
                    <View style={styles.descriptionContainer}>
                        {/* <Text style={styles.descriptionTitle}>{data.pivot.qty} Cupon</Text> */}
                        <Text style={styles.descriptionTitle}>Coupon Description: {data.description || 'N/A'}</Text>

                        {data.golf_courses[0].price !== null &&
                            <Text style={[styles.descriptionTitle, { color: 'red' }]}>
                                Price: {data.golf_courses[0].price || '0'} {data.currency}
                            </Text>
                        }

                        {data.golf_courses[0].pivot.price !== null && data.golf_courses[0].pivot.price === '0.00' ? (
                            <Text style={[styles.descriptionTitle, { color: 'green' }]}>Your Price: Free </Text>
                        ) : (
                                <Text style={[styles.descriptionTitle, { color: 'green' }]}>
                                    Your Price: {data.golf_courses[0].pivot.price || 'FREE'} {data.currency}
                                </Text>
                            )
                        }

                        {data.golf_cart_price === '0.00' ? (
                            <Text style={styles.descriptionTitle}>Golf Car price: Free per person</Text>
                        ) : (
                                <Text style={styles.descriptionTitle}>Golf Car price: {data.currency}$ {data.golf_cart_price} per person</Text>
                            )}

                    </View>
                </View>


            </View>
        );
    }
}

export default BenefitsModal;

const styles = StyleSheet.create({
    container: {
        // backgroundColor: 'red',
        width: '100%',
        padding: scale(10),
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: moderateScale(25),
        textAlign: 'center',
        fontWeight: 'bold',
        marginTop: verticalScale(20),
        color: COLORS.softBlack
    },
    qrContainer: {
        marginTop: verticalScale(20),
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    qrCode: {
        fontSize: moderateScale(18),
        textAlign: 'center',
        marginTop: verticalScale(20),
        marginBottom: verticalScale(15)
    },
    line: {
        borderWidth: 1,
        marginTop: verticalScale(10),
        marginBottom: verticalScale(10),
        width: '90%',
        borderColor: '#0000002B'
    },
    descriptionContainer: {
        width: moderateScale(280),

    },
    descriptionTitle: {
        fontSize: moderateScale(16),
        fontWeight: '500',
        margin: verticalScale(3),
        color: '#1B1B1B'
    },
    button: {
        backgroundColor: 'black',
        width: moderateScale(200),
        height: verticalScale(35),
        marginTop: verticalScale(20),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: scale(10)
    },
    buttonText: {
        color: 'white',
        fontSize: moderateScale(14),
        fontWeight: 'bold'
    }
})