import React, { Component } from 'react';
import { View, Text, Modal, StyleSheet, SafeAreaView, TouchableOpacity } from 'react-native';
import { verticalScale, moderateScale, scale } from '../../../utilities/ScalingScreen';
import Ionicons from 'react-native-vector-icons/Ionicons'
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import { COLORS } from '../../../config/constants';

import * as payment from '../../../store/actions/payment';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Moment from 'moment';
import { extendMoment } from 'moment-range';
import MembershipsInfo from '../../../views/Settings/Membership/MembershipsInfo';
import CreditCardPayment from '../../../views/Settings/Payment/CreditCardPayment';
const moment = extendMoment(Moment);

class RenewMemberships extends Component {
    constructor(props) {
        super(props);
        this.state = {
            renewPaymentInfo: false,
            payment: false
        };
    }

    requestPayment() {
        const { info, session, user } = this.props;

        const request = this.props.renewPaymentRequest(user.id, info.id, this.props.session.accessToken);

        if (this.props.renewPayment.renewPaymentResponse.success) {
            this.setState({
                renewPaymentInfo: this.props.renewPayment.renewPaymentResponse.data.requestPayment,
                payment: true
            })
        } else {
            console.log('error')
        }
    }


    render() {
        const { info, session, user } = this.props;
        console.log(info);
        return (
            <Modal
                visible={this.props.visible}
                animationType="slide"
            >
                {this.state.payment ? (
                    <MembershipsInfo
                        info={info}
                        session={session}
                        user={user}
                        showInfoModal={(e) => this.props.showInfoModal(e)}
                    />
                ) : (
                    <CreditCardPayment 
                        isModal={true}
                        showInfoModal={(e) => this.props.showInfoModal(e)}
                    />
                )
            }


            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        session: state.session,
        user: state.session.user.data,
        renewPayment: state.payment
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators(payment, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchProps
)(RenewMemberships);


const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})