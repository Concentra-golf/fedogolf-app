import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { scale, moderateScale, verticalScale } from '../../../utilities/ScalingScreen';
import Ionicons from "react-native-vector-icons/dist/Ionicons";
import QRCode from 'react-native-qrcode-svg';

class UserQR extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userData: {}
        };
    }

    // componentDidMount() {
    //     const user = this.props.user;
    //     console.log(user);

    //     const qrData = {
    //         name: user.firstname,
    //         lastName: user.lastname,
    //         email: user.email,
    //         phone: user.phone,
    //     }

    //     this.setState({
    //         userData: qrData
    //     })

    // }


    render() {
        const { code } = this.props;
        return (
            <View style={styles.container}>

                <TouchableOpacity
                    onPress={(e) => this.props.showModalQr(false)}
                    style={{
                        position: 'absolute',
                        top: verticalScale(10),
                        right: moderateScale(25),
                        zIndex: 1
                    }}
                >
                    <Ionicons
                        name='ios-close'
                        size={moderateScale(32)}
                    />
                </TouchableOpacity>

                <View style={styles.qrContainer}>
                    <QRCode
                        value={JSON.stringify(code)}
                        size={170}
                    />
                </View>
            </View>
        );
    }
}

export default UserQR;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        padding: scale(10),
        justifyContent: 'center',
        alignItems: 'center'
    },
    qrContainer: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    qrCode: {
        fontSize: moderateScale(18),
        textAlign: 'center',
        marginTop: verticalScale(20),
        marginBottom: verticalScale(15)
    },
})