import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';

import NativeBaseHeader from '../NativeBase/NativeBaseHeader';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { heightPercentageToDP, moderateScale, verticalScale } from '../../../utilities/ScalingScreen';
import { COLORS } from '../../../config/constants';
import AddFriend from '../../forms/AddPlayer/AddFriend';
import SearchPlayer from '../../forms/AddPlayer/SearchPlayer';
import * as RNLocalize from 'react-native-localize';

import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';

class AddPlayer extends Component {
    constructor(props) {
        super(props);
        setI18nConfig();
        this.state = {
            active: 1
        };
    }

    componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);
    }

    componentWillUnmount() {
        RNLocalize.removeEventListener('change', handleLocalizationChange);
    }

    changePage(e) {
        this.setState({ active: e })
    }

    playerSelected(e, route) {
        console.log(e)


        const hole = parseInt(e.holeStart.slice(-2));
        console.log(hole);

        const playerSelected = {
            firstname: e.firstname,
            lastname: e.lastname,
            gender: e.gender,
            email: e.email,
            id: e.id,
            tees: e.tees,
            start: hole
        }
        this.props.getPlayers(playerSelected)

    }

    render() {
        const { active } = this.state;
        return (
            <View style={styles.container}>
                <NativeBaseHeader
                    title={translate('addPlayer')}
                    leftIcon={faArrowLeft}
                    leftButton={() => this.props.close(false)}
                    theme='dark-blue'
                />

                <View style={styles.buttonContainer}>

                    <TouchableOpacity
                        style={styles.buttonSelector}
                        onPress={() => this.changePage(1)}
                    >
                        <Text style={[styles.buttonText, active === 1 ? { color: COLORS.darkblue } : styles.buttonText]}>{translate('frecuents')}</Text>
                    </TouchableOpacity>

                    <View style={styles.line} />

                    <TouchableOpacity
                        style={styles.buttonSelector}
                        onPress={() => this.changePage(2)}
                    >
                        <Text style={[styles.buttonText, active === 2 ? { color: COLORS.darkblue } : styles.buttonText]}>{translate('searchPlayers')}</Text>
                    </TouchableOpacity>
                </View>

                {/* RENDER PAGE */}

                <View style={{ flexGrow: 1 }}>
                    {active === 1 ? (
                        <AddFriend
                            playerSelected={(e) => this.playerSelected(e)}
                            tees={this.props.tees}
                            holes={this.props.holes}

                        />

                    ) : (
                            <SearchPlayer
                                playerSelected={(e) => this.playerSelected(e)}
                                tees={this.props.tees}
                                holes={this.props.holes}
                            />
                        )
                    }
                </View>



            </View>
        );
    }
}

export default AddPlayer;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    buttonContainer: {
        width: '100%',
        height: heightPercentageToDP('8%'),
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        // borderWidth: 1
    },
    buttonSelector: {
        // backgroundColor: 'red',
        width: '40%',
        height: heightPercentageToDP('5%'),
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText: {
        fontSize: moderateScale(14),
        fontWeight: '600',
    },
    line: {
        width: 1,
        height: verticalScale(30),
        backgroundColor: 'black',
        transform: [{ rotate: '10' }]
    },

})


