import React, { Component } from 'react';
import { View, Text, StyleSheet, ActivityIndicator, TouchableOpacity } from 'react-native';
import StarRating from 'react-native-star-rating';
import { TextInput } from 'react-native-gesture-handler';
import * as RNLocalize from 'react-native-localize';

import { translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';
import { moderateScale, verticalScale, scale } from '../../../utilities/ScalingScreen';
import { COLORS } from '../../../config/constants';

import * as playgolf from '../../../store/actions/playgolf';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Success from '../../../components/UI/LottieAnimation/Success';

class GameRating extends Component {
    constructor(props) {
        super(props);
        setI18nConfig();
        this.state = {
            stars: 0,
            comment: "",
            sendingInfo: false,
            success: false
        };
    }

    componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);
    }

    componentWillUnmount() {
        RNLocalize.removeEventListener('change', handleLocalizationChange);
    }

    async sentScore() {
        this.setState({ sendingInfo: true })
        const { golfCourse, gameDetails } = this.props;

        if (this.state.stars !== 0 && this.state.comment !== "") {
            const rating = { rating: this.state.stars, comment: this.state.comment };
            await this.props.ratingGame(rating, golfCourse.id, this.props.session.accessToken);
            if (this.props.playgolf.ratingGame.success) {
                this.endGame();
            } else {
                console.log('Error Sending the Ratting');
                this.setState({ sendingInfo: false })
            }
        } else {
            this.endGame();
        }
    }

    async endGame() {
        const { golfCourse, gameDetails } = this.props;

        let gameDate = {}
        if (this.props.route === 'PlayTab') {
            gameDate = gameDetails;
        } else {
            gameDate = gameDetails.gameDetails;
        }


        await this.props.endGame(gameDate.id, this.props.session.accessToken);
        if (this.props.playgolf.endGame.success) {
            this.setState({ sendingInfo: false, success: true });

            console.log('Success')
        } else {
            this.setState({ sendingInfo: false })
            console.log('Error')
        }
    }


    onSuccess() {
        this.props.close(false);
        this.props.navigation.popToTop();
    }

    render() {
        return (
            <View style={styles.container}>
                {!this.state.success ? (
                    <View>
                        <Text style={styles.title}>{translate('gameRating')}</Text>

                        <StarRating
                            disabled={false}
                            maxStars={5}
                            rating={this.state.stars}
                            fullStarColor={COLORS.green}
                            starSize={35}
                            starStyle={styles.starStyle}
                            containerStyle={styles.starContainer}
                            selectedStar={(e) => this.setState({ stars: e })}
                        />

                        <TextInput
                            style={styles.textInput}
                            multiline
                            placeholder={translate('commentPlaceholder')}
                            onChangeText={e => this.setState({ comment: e })}
                        />

                        {!this.state.sendingInfo ? (
                            <View style={styles.wrapper}>
                                <TouchableOpacity
                                    style={[styles.nextButton, { backgroundColor: COLORS.red }]}
                                    onPress={() => this.props.close(false)}
                                >
                                    <Text style={styles.nextButtonText}>{translate('cancel')}</Text>
                                </TouchableOpacity>

                                <TouchableOpacity
                                    style={styles.nextButton}
                                    onPress={() => this.sentScore()}
                                >
                                    <Text style={styles.nextButtonText}>{translate('finish')}</Text>
                                </TouchableOpacity>
                            </View>
                        ) : (
                                <View style={styles.loadingContainer}>
                                    <ActivityIndicator size='large' color={COLORS.green} />
                                    <Text style={styles.loadingText}>Sending Game</Text>
                                </View>
                            )
                        }
                    </View>
                ) : (
                        <View style={styles.Successcontainer}>
                            <Success />

                            <Text style={styles.title}>{translate('success')}</Text>
                            <Text style={styles.subTitle}>{translate('successMsgRating')}</Text>

                            <TouchableOpacity
                                onPress={() => this.onSuccess()}
                                style={styles.doneButton}
                            >
                                <Text style={styles.doneButtonText}>{translate('done')}</Text>
                            </TouchableOpacity>
                        </View>
                    )

                }

            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        session: state.session,
        user: state.session.user.data,
        playgolf: state.playgolf
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators(playgolf, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchProps
)(GameRating);

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        width: '100%',
        padding: scale(10),
        borderRadius: 10,
        justifyContent: 'center'
        // height: heightPercentageToDP('25%'),
    },
    starContainer: {
        width: 200,
        marginTop: verticalScale(10),
        marginVertical: verticalScale(5),
    },
    textInput: {
        width: '100%',
        borderWidth: 1,
        borderColor: COLORS.airBnbLightGrey,
        borderRadius: scale(5),
        height: verticalScale(60),
        marginTop: verticalScale(10),
        padding: scale(5),
        fontSize: moderateScale(14)
    },
    wrapper: {
        flexDirection: 'row',
        marginTop: verticalScale(10),
    },
    nextButton: {
        marginHorizontal: moderateScale(10),
        height: verticalScale(45),
        width: moderateScale(140),
        backgroundColor: COLORS.green,
        justifyContent: 'center',
        alignItems: 'center'
    },
    nextButtonText: {
        fontSize: moderateScale(16),
        fontWeight: 'bold',
        color: 'white'
    },
    Successcontainer: {
        backgroundColor: COLORS.white,
        width: moderateScale(320),
        borderRadius: scale(20),
        alignItems: 'center',
        padding: scale(20)
    },
    title: {
        fontSize: moderateScale(20),
        marginVertical: verticalScale(10),
        fontWeight: 'bold',
        color: COLORS.green
    },
    subTitle: {
        fontSize: moderateScale(14),
        marginVertical: verticalScale(5),
        textAlign: 'center',
        color: COLORS.airBnbLightGrey
    },
    doneButton: {
        backgroundColor: COLORS.green,
        width: moderateScale(250),
        height: verticalScale(40),
        marginVertical: verticalScale(20),
        justifyContent: 'center',
        alignItems: 'center'
    },
    doneButtonText: {
        color: COLORS.white,
        fontWeight: 'bold',
        fontSize: moderateScale(16)
    },
    loadingContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: scale(10),
    },
    loadingText: {
        marginTop: verticalScale(10),
        fontSize: moderateScale(15),
        color: COLORS.darkblue
    }
})