import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import FormInput from "../../../components/UI/FormInput/FormInput";

// Modules
import { Formik } from "formik";
import * as yup from "yup";
import { COLORS } from '../../../config/constants';
import { verticalScale, moderateScale, scale } from '../../../utilities/ScalingScreen';

import FederationService from '../../../config/services/FederationService'
import { getValueAndLabelFromArray } from '../../../utilities/helper-functions';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';

import * as RNLocalize from 'react-native-localize';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';

class ConfirmAssociationModal extends Component {
    constructor(props) {
        super(props);
        setI18nConfig(); 
        this.state = {
            associations: [],
        };
    }

    async componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);
        const { session } = this.props
        const response = await FederationService.getAllFederations(session.accessToken)

        response.data.asociations.unshift({ id: 0, name: "Ninguna" });

        this.setState({
            associations: getValueAndLabelFromArray(response.data.asociations, 'name'),
        })
        console.log(this.state.associations)
    }

    render() {
        const { session } = this.props
        return (
            <View style={styles.modal}>

                <TouchableOpacity
                    style={styles.closeButton}
                    onPress={() => this.props.close()}
                >
                    <Ionicons
                        name='ios-close'
                        size={35}
                    />
                </TouchableOpacity>

                <Text style={styles.title}>{translate('confirmationTitle')}</Text>

                <Formik
                    initialValues={this.props.session.user.data}
                    validationSchema={validationSchema}
                    onSubmit={(values, actions) => this.props.validateAsosociation(values, actions)}
                >
                    {props => {
                        return (
                            <React.Fragment>
                                <View style={styles.fedogolfInputs}>
                                <FormInput
                                    onChangeText={value => props.setFieldValue('association_id', value)}
                                    value={props.values.association_id}
                                    type={'picker'}
                                    theme={'light'}
                                    placeholder={translate('association')}
                                    additionalStyles={{}}
                                    data={this.state.associations}
                                    editable={false}
                                    error={props.errors.association_id}
                                />
                                </View>

                                <View style={styles.buttonContainer}>
                                    <TouchableOpacity
                                        style={styles.sentRequestCotainer}
                                        onPress={() => props.handleSubmit()}
                                    >
                                        <Text style={styles.sentRequestText}>Confirm</Text>
                                    </TouchableOpacity>
                                </View>
                            </React.Fragment>
                        )
                    }}
                </Formik>

            </View>
        );
    }
}

export default ConfirmAssociationModal;

const styles = StyleSheet.create({
    modal: {
        backgroundColor: COLORS.white,
        padding: scale(10),
        borderRadius: scale(10),
        width: moderateScale(350),
        height: verticalScale(250),
        // justifyContent: 'space-between',
    },
    closeButton: {
        position: 'absolute',
        right: moderateScale(25),
        top: verticalScale(10)
    },
    modalContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        borderRadius: 6,
        padding: scale(10),
    },
    title: {
        fontSize: moderateScale(18),
        marginTop: verticalScale(35),
        fontWeight: 'bold',
        color: COLORS.lightBlack
    },
    fedogolfInputs: {
        marginTop: verticalScale(35),
        // backgroundColor: 'transparent',
        // borderColor: '#8E8E93',
        // borderWidth: 1,
        // color: 'black',
        // fontSize: moderateScale(14),
        // textAlign: 'left',
        // paddingHorizontal: moderateScale(20),
        // height: 52,
        // borderRadius: 24,
    },
    buttonContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: verticalScale(25),
    },
    sentRequestCotainer: {
        height: verticalScale(45),
        width: moderateScale(200),
        borderRadius: scale(10),
        backgroundColor: COLORS.green,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: verticalScale(30)
    },
    sentRequestText: {
        fontSize: moderateScale(16),
        fontWeight: 'bold',
        color: 'white'
    }
})

const validationSchema = yup.object().shape({
    association_id: yup.string().required('This field is required'),
});
