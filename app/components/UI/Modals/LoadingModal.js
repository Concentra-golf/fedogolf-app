import React from 'react';
import {
    View,
    ActivityIndicator,
    Text,
    StyleSheet
} from 'react-native';
import Modal from 'react-native-modal';
import { moderateScale, scale, verticalScale } from '../../../utilities/ScalingScreen';


const loadinModal = (props) => (
    <Modal
        style={styles.modalContainer}
        isVisible={props.visible}
    >
        <View style={styles.loadingModal}>
            <View>
                <ActivityIndicator size='large' color='#0E38B1' />
                <Text style={styles.textStyle}>{props.message}...</Text>
            </View>
        </View>
    </Modal>
);


const styles = StyleSheet.create({
    modalContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        borderRadius: 6
    },
    loadingModal: {
        marginTop: verticalScale(22),
        backgroundColor: '#fff',
        padding: scale(15),
        width: moderateScale(250),
        margin: scale(25),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 6
    },
    textStyle: {
        fontSize: moderateScale(14),
        margin: scale(10)
    }
});

export default loadinModal; 
