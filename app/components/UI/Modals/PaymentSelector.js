import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, SafeAreaView, Platform, ScrollView, Modal, ActivityIndicator } from 'react-native';
import CardPayment from '../../lists/CardPayment/CardPayment';

import * as payment from '../../../store/actions/payment';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { scale, verticalScale, moderateScale, heightPercentageToDP } from '../../../utilities/ScalingScreen';

import MaterialCommunityIcons from "react-native-vector-icons/dist/MaterialCommunityIcons";
import { COLORS } from '../../../config/constants';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import CreditCardModal from '../../UI/Payment/CreditCardModal';

import * as RNLocalize from 'react-native-localize';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';

class PaymentSelector extends Component {
    constructor(props) {
        super(props);
        setI18nConfig();
        this.state = {
            cardList: [],
            loading: true,
            activeIndex: null,
            creditCard: false,
            paymentSelected: null,
            settingUp: false
        };
        this.getCreditCardValues = this.getCreditCardValues.bind(this);
        this.cardSelected = this.cardSelected.bind(this);
    }

    componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);
        this.getMyCard();
    }

    async getMyCard() {
        const userid = this.props.session.user.data.id;
        await this.props.getUserPayments(userid, this.props.session.accessToken);

        if (this.props.payment.userPaymentCards.success) {
            console.log(this.props.payment.userPaymentCards.data.data);
            this.setState({
                cardList: this.props.payment.userPaymentCards.data.data,
                loading: false
            })
        } else {
            this.setState({
                cardList: [],
                loading: false,
                error: true,
            })
        }
    }

    cardSelected(e) {
        this.setState({
            activeIndex: e.id,
            paymentSelected: e,
            buttonActive: true
        });
    }

    addCreditCardModal(visible) {
        this.setState({ creditCard: visible })
    }

    //We receive the new credit card values
    async getCreditCardValues(e) {
        if (e !== {}) {
            this.setState({ addingCard: true })
            let cardList = this.state.cardList;

            //We set the card values to AZUL Structure
            let creditCard = e.number.replace(/ /g, ''); //we eliminate the spaces of the cards

            let expirationMonth = e.expiry.substring(0, 2)
            let expirationYear = e.expiry.substring(3)
            let setExpDate = '20' + expirationYear + expirationMonth; // we create the AZUL format for the dates (202012)

            const newCreditCard = {
                cardNumber: creditCard,
                cvc: e.cvc,
                expiration: setExpDate
            }

            const request = await this.props.saveUserCard(newCreditCard, this.props.session.accessToken);
            if (this.props.payment.saveUserCards.success) {
                this.setState({ addingCard: false })
                this.getMyCard();
                this.addCreditCardModal(false)
            } else {
                console.log('Error adding the card')
            }
        }
    }

    async changeMembership() {
        this.setState({ settingUp: true })

        const creditCard = this.state.paymentSelected;
        const data = this.props.data;
        const requestData = {
            membership: data.id,
            card: creditCard.id
        }

        const request = await this.props.setRecurrentMembership(requestData, this.props.session.accessToken);

        if (this.props.payment.recurrentMembership.success) {
            this.setState({ settingUp: false })
            console.log('success');
            this.props.reloadPage();
            this.props.close(false);
        } else {
            console.log('error')
        }

    }

    async updatePayment() {
        this.setState({ settingUp: true })

        const creditCard = this.state.paymentSelected;
        const data = this.props.data;
        const recurrenceId = this.props.membershipInfo.id;

        const requestData = {
            membership: data.id,
            card: creditCard.id
        }

        const request = await this.props.updateRecurrentMembership(recurrenceId, requestData, this.props.session.accessToken);

        if (this.props.payment.updateRecurrentMembership.success) {
            this.setState({ settingUp: false })
            console.log('success');
            this.props.reloadPage();
            this.props.close(false);
        } else {
            console.log('error')
        }

    }

    render() {
        return (
            <SafeAreaView style={styles.container}>

                <View style={styles.header}>
                    <TouchableOpacity
                        onPress={() => this.props.close(false)}
                    >
                        <Ionicons
                            name='ios-close'
                            size={scale(35)}
                            style={{ marginLeft: verticalScale(5), color: COLORS.softBlack }}
                        />
                    </TouchableOpacity>
                    <Text style={styles.headerTitle}>{translate('paymentMethodTitle')}</Text>
                </View>

                <ScrollView style={styles.cardsContainer}>
                    {!this.state.loading && this.state.cardList.map((item, i) =>
                        <CardPayment
                            key={i}
                            data={item}
                            type={item.card_brand}
                            cardNumber={item.card_number}
                            expiration={item.expiration}
                            route={'formSelect'} // We set The route to use the component
                            activeIndex={this.state.activeIndex}
                            onPress={() => this.cardSelected(item)}
                        />
                    )}

                    <TouchableOpacity
                        style={styles.addNewCreditCard}
                        onPress={() => this.addCreditCardModal(true)}
                    >
                        <MaterialCommunityIcons
                            name='credit-card'
                            size={20}
                            style={{ color: COLORS.red }}
                        />
                        <Text style={styles.addNewCreditCardText}>{translate('addNewCC')}</Text>
                    </TouchableOpacity>

                </ScrollView>


                <Modal
                    visible={this.state.creditCard}
                    animationType='slide'
                >
                    <CreditCardModal
                        close={(e) => this.addCreditCardModal(e)}
                        getCreditCardValues={(e, save) => this.getCreditCardValues(e, save)}
                        addingCard={this.state.addingCard}
                        route={'addCard'}
                    />
                </Modal>


                <TouchableOpacity
                    style={styles.aceptButton}
                    onPress={this.props.route === 'updatePayment' ? () => this.updatePayment() : () => this.changeMembership()}
                >
                    {this.state.settingUp ? (
                        <ActivityIndicator size='large' color='white' />
                    ) : (
                            <Text style={styles.aceptButtonText}>{this.props.route === 'updatePayment' ? translate('updatePay')  : translate('changeMembershipType')}</Text>
                        )
                    }
                </TouchableOpacity>

            </SafeAreaView>
        );
    }
}

function mapStateToProps(state) {
    return {
        session: state.session,
        user: state.session.user.data,
        payment: state.payment
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators(payment, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchProps
)(PaymentSelector);

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        width: '100%',
        height: Platform.OS === 'ios' ? heightPercentageToDP('15%') : heightPercentageToDP('17%'),
        padding: scale(10),
        alignContent: "center",
    },
    headerTitle: {
        fontSize: Platform.OS === 'ios' ? moderateScale(25) : moderateScale(25),
        marginTop: Platform.OS === 'ios' ? verticalScale(10) : verticalScale(10),
        marginLeft: verticalScale(5),
        fontWeight: 'bold',
        color: COLORS.softBlack
    },
    cardsContainer: {
        padding: scale(10),
        marginTop: verticalScale(10)
    },
    addNewCreditCard: {
        flexDirection: 'row',
        width: '100%',
        height: verticalScale(30),
        alignItems: 'center'
    },
    addNewCreditCardText: {
        fontSize: moderateScale(14),
        color: COLORS.red,
        marginLeft: moderateScale(5)
    },
    aceptButton: {
        width: '100%',
        height: verticalScale(50),
        backgroundColor: COLORS.darkblue,
        justifyContent: 'center',
        alignItems: 'center'
    },
    aceptButtonText: {
        fontSize: moderateScale(16),
        fontWeight: 'bold',
        color: COLORS.white
    }
})
