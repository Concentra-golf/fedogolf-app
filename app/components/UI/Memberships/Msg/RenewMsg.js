import React, { Component } from 'react';
import { View, Text, Modal } from 'react-native';
import { moderateScale, verticalScale } from '../../../../utilities/ScalingScreen';

class RenewMsg extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <Modal
                visible={this.props.modelVisible}
                transparent={true}
            >
                <View style={{ 
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: '100%'
                 }}>
                    <View style={{
                        width: moderateScale(350),
                        height: verticalScale(220),
                        backgroundColor: 'red',
                        borderRadius: 10
                    }}>
                        <Text> RenewMsg </Text>
                    </View>
                </View>
            </Modal>
        );
    }
}

export default RenewMsg;
