import React, { Component } from 'react';
import { View, Text } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import { moderateScale, widthPercentageToDP, heightPercentageToDP, verticalScale, scale } from '../../../utilities/ScalingScreen';
import { COLORS } from '../../../config/constants';

class PickerSelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const { data, value, placeholder, action } = this.props;
        return (
            <RNPickerSelect
                placeholder={{
                    label: placeholder,
                    value: null,
                }}
                placeholderTextColor={COLORS.darkgrey}
                items={data}
                onValueChange={action}
                useNativeAndroidPickerStyle={false}
                doneText="Confirm"
                style={{
                    inputAndroid: {
                        fontSize: moderateScale(14),
                        color: COLORS.airBnbLightGrey,
                        width: widthPercentageToDP('90%'),
                    },
                    inputIOSContainer: {
                        fontSize: moderateScale(16),
                        color: COLORS.airBnbLightGrey,
                        width: widthPercentageToDP('85%'),
                        height: heightPercentageToDP('5%'),
                        justifyContent: 'center',
                        // backgroundColor: 'blue',
                    },
                    placeholder: {
                        color: COLORS.airBnbLightGrey,
                        fontSize: moderateScale(14),
                    },
                }}
                value={value}
            />
        );
    }
}

export default PickerSelect;
