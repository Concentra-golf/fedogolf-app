import { StyleSheet } from "react-native";
import { COLORS } from "../../../config/constants";
import { verticalScale, moderateScale, scale } from "../../../utilities/ScalingScreen";

export default StyleSheet.create({
  wrapper: {
    width: "100%",
    marginTop: 10,
    paddingTop: 20
  },

  inputContainer: {
    marginLeft: 30,
    marginRight: 30
  },

  backgroundImage: {
    width: 414,
    height: 403,
    flex: 1,
  },
  header: {
    position: 'relative',
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 20,
    width: "100%"
  },
  iconButton: {
    position: 'absolute',
    left: moderateScale(20),
    top: verticalScale(20)
  },
  headerTitle: {
    fontSize: moderateScale(20),
    fontWeight: "bold",
    textAlign: 'center',
    top: verticalScale(16),
  },
  institutionLogo: {
    alignItems: 'center',
    paddingVertical: 20,
    marginBottom: 40,
    borderBottomWidth: 1,
    borderBottomColor: '#5553'
  },
  createAccountButton: {
    // paddingTop: 35
    paddingBottom: 35
    // borderTopColor: "#505050",
    // borderTopWidth: 1,
  },

  emailInputContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },

  emailInput: {
    flex: 1
  },

  submitButton: {
    marginBottom: verticalScale(30)
  },

  biometryButtonIcon: {
    width: 35,
    height: 35
  },

  forgetPasswordButton: {
    marginBottom: 35,
    borderRadius: 0,
    borderWidth: 0,
    marginTop: 0
  },

  fedogolfInputs: {
    backgroundColor: 'transparent',
    borderColor: '#8E8E93',
    borderWidth: 1,
    color: 'black',
    fontSize: 14,
    textAlign: 'left',
    paddingHorizontal: 20,
    height: 52,
    borderRadius: 24,
  },


  genderBtn: {
    width: '45%',
    marginBottom: 20,
    backgroundColor: "transparent",
    borderWidth: 0,
    borderRadius: 10,
    backgroundColor: COLORS.grey,

  },
  genderActiveBtn: {
    backgroundColor: COLORS.lightblue,
  },

  buttonText: {
    alignSelf: 'center'
  },
  uploadVoucherContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
    // height: verticalScale(70),
  },
  uploadTitle: {
    fontSize: moderateScale(16),
    marginBottom: moderateScale(15)
  },
  fileContainer: {
    borderWidth: 1,
    width: '100%',
    height: verticalScale(40),
    padding: scale(10),
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: moderateScale(15),
    borderRadius: scale(10)
  },
  fileIcon: {
    marginRight: moderateScale(15),
    fontSize: moderateScale(20)
  },
  fileTitle: {
    fontSize: moderateScale(14)
  },
  uploadButton: {
    backgroundColor: COLORS.lightblue,
    height: verticalScale(40),
    width: moderateScale(130),
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  uploadButtonText: {
    color: COLORS.white,
    fontSize: moderateScale(14),
    fontWeight: '600'
  },
  successContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  }
});
