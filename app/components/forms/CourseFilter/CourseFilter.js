import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Modal, SafeAreaView, ScrollView, Platform } from 'react-native';

import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';

import { COLORS } from '../../../config/constants';
import { widthPercentageToDP, heightPercentageToDP, verticalScale, scale, moderateScale } from '../../../utilities/ScalingScreen';
import TextLabel from '../../UI/TextLabel';

import PickerSelect from '../../../components/UI/Picker/PickerSelect';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import NativeBaseHeader from '../../UI/NativeBase/NativeBaseHeader';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';

import { getValueAndLabelFromArray } from '../../../utilities/helper-functions';
import StarRating from 'react-native-star-rating';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as courses from '../../../store/actions/courses';

class CourseFilter extends Component {
    constructor(props) {
        super(props);
        setI18nConfig(); 
        this.state = {
            countryList: [],
            cityList: [],
            ratingList: [],
            rating: 1
        };
    }

    async componentDidMount() {
        const data = this.props.data;
        RNLocalize.addEventListener('change', handleLocalizationChange);

        await this.props.getCountry(this.props.session.accessToken);

        if (this.props.courses.country.success) {
            const countryValues = getValueAndLabelFromArray(this.props.courses.country.data, 'name');
            console.log(countryValues);
            this.setState({
                countryList: countryValues,
            })
        }
    }


    filterSearch() {
        this.props.filterOptions(this.state.countrySelected, this.state.citySelected, this.state.rating);
        this.props.showFilterModal(false);
    }

    async setValues(e, type) {
        console.log(e, type);
        switch (type) {
            case 'country':
                this.setState({ countrySelected: e });
                await this.props.getFilterbyCity(e, this.props.session.accessToken)
                if (this.props.courses.filterCountry.success) {
                    const cityValues = getValueAndLabelFromArray(this.props.courses.filterCountry.data, 'name');
                    this.setState({
                        cityList: cityValues,
                    })
                }
                break;
            case 'city':
                this.setState({ citySelected: e });
                break;
        }

    }

    render() {
        return (
            <Modal
                style={styles.modalContainer}
                visible={this.props.visible}
                animationType='slide'
            >
                <SafeAreaView style={styles.container}>

                    <NativeBaseHeader
                        title={translate('filterTitle')}
                        leftIcon={faArrowLeft}
                        leftButton={() => this.props.showFilterModal(false)}
                    />

                    <ScrollView contentContainerStyle={styles.scrollContainer} style={styles.content}>

                        <View style={styles.sectionContainer}>
                            <Text style={styles.title}>{translate('country')}</Text>
                            <View style={styles.pickerContainer}>
                                <PickerSelect
                                    data={this.state.countryList}
                                    value={this.state.countrySelected}
                                    placeholder={translate('countryPlaceholder')}
                                    action={(value) => this.setValues(value, 'country')}
                                />
                                {Platform.OS === 'ios' &&
                                    <FontAwesome
                                        name='caret-down'
                                        size={scale(15)}
                                        color={COLORS.softBlack}
                                    />
                                }
                            </View>
                        </View>

                        <View style={styles.divider} />

                        <View style={styles.sectionContainer}>
                            <Text style={styles.title}>{translate('city')}</Text>
                            <View style={styles.pickerContainer}>
                                <PickerSelect
                                    data={this.state.cityList}
                                    value={this.state.citySelected}
                                    placeholder={translate('cityPlaceholder')}
                                    action={(value) => this.setValues(value, 'city')}
                                />
                                {Platform.OS === 'ios' &&
                                    <FontAwesome
                                        name='caret-down'
                                        size={scale(15)}
                                        color={COLORS.softBlack}
                                    />
                                }
                            </View>
                        </View>

                        <View style={styles.divider} />

                        <View style={styles.sectionContainer}>
                            <Text style={styles.title}>Rating</Text>

                            <StarRating
                                disabled={false}
                                maxStars={5}
                                rating={this.state.rating}
                                fullStarColor={COLORS.green}
                                starSize={45}
                                starStyle={styles.starStyle}
                                containerStyle={styles.starContainer}
                                selectedStar={(e) => this.setState({ rating: e })}
                            />

                        </View>

                        <View style={styles.divider} />

                        <TouchableOpacity
                            style={styles.searchButton}
                            onPress={() => this.filterSearch()}
                        >
                            <Text style={styles.searchText}>{translate('search')}</Text>
                        </TouchableOpacity>

                    </ScrollView>
                </SafeAreaView>
            </Modal>
        );
    }
}


function mapStateToProps(state) {
    return {
        session: state.session,
        user: state.session.user.data,
        courses: state.courses
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators(courses, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchProps,
)(CourseFilter);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    content: {
        width: '100%',
        padding: scale(10),
    },
    scrollContainer: {
        alignItems: 'center'
    },
    divider: {
        height: 1,
        width: widthPercentageToDP('90%'),
        backgroundColor: COLORS.airBnbLightGrey,
        marginVertical: verticalScale(20)
    },
    sectionContainer: {
        // backgroundColor: 'red',
        width: '100%',
    },
    title: {
        fontSize: moderateScale(18),
        fontWeight: '600',
        color: COLORS.softBlack
    },
    pickerContainer: {
        flexDirection: 'row',
        borderWidth: 1,
        width: '100%',
        borderColor: COLORS.airBnbGrey,
        height: heightPercentageToDP('7%'),
        marginTop: verticalScale(10),
        padding: scale(5),
        alignItems: 'center',
        borderRadius: scale(5),
        // backgroundColor: 'red',
    },
    starContainer: {
        width: moderateScale(290),
        marginTop: verticalScale(15),
        marginLeft: moderateScale(10)
    },
    starStyle: {

    },
    searchButton: {
        height: verticalScale(50),
        width: moderateScale(300),
        backgroundColor: COLORS.white,
        borderColor: COLORS.green,
        borderWidth: 1,
        borderRadius: scale(10),
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.3,
        shadowRadius: 6,
    },
    searchText: {
        fontSize: moderateScale(18),
        color: COLORS.green,
        fontWeight: 'bold',
    }
})