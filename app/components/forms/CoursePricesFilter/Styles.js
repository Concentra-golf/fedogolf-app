import { StyleSheet } from "react-native";
import { COLORS } from "../../../config/constants";

export default StyleSheet.create({
    container: {
        padding: 15,
        paddingTop: 20,
        paddingRight: 20,
        flexDirection: 'row',
        backgroundColor: '#F9F9F9'
    },
    filterOption: {
        flexDirection: 'row',
        marginRight: 25
    },
    filterOptionIcon: {
        marginRight: 5
    },
    inputIOS: {
        minWidth: 40,
        fontSize: 16,
        textTransform: 'uppercase',
        paddingHorizontal: 0,
    },
    inputAndroid: {
        minWidth: 40,
        fontSize: 16,
        textTransform: 'uppercase',
        paddingVertical: -10,
        paddingHorizontal: 0,
    },
    placeholder: {
        textTransform: 'uppercase'
    },
    datepicker: {
        minWidth: 140,
        marginTop: -10,
        height: 45,
    },
    dateTouchBody: {
        height: 45,
    },
    datePickerSelect: {
        backgroundColor: '#d0d4db',
    },
    datePickerCon: {
        backgroundColor: '#EFF1F2',
    },
    dateInput: {
        margin: 0,
        borderWidth: 0,
        alignItems: 'flex-start',
    },
    datePlaceholderText: {
        fontSize: 16,
        color: COLORS.darkgrey,
    },
    dateText: {
        fontSize: 16,
        color: COLORS.darkgrey,
    },
    dateBtnTextConfirm: {
        color: COLORS.darkgrey,
    },
});
