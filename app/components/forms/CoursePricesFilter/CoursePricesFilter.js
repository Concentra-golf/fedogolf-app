import React, { Component } from 'react';
import {View, TouchableOpacity} from 'react-native';

// Modules

import DatePicker from 'react-native-datepicker';
import RNPickerSelect from 'react-native-picker-select';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faGolfBall, faUsers } from '@fortawesome/free-solid-svg-icons';
import { faCalendar } from '@fortawesome/free-regular-svg-icons';
import { Formik } from "formik";
import Moment from 'moment';

// Styles

import styles from "./Styles";

// Constants

import { COLORS } from '../../../config/constants';

// Sample Data

const personsData = [
    { label: "1", value: "1" },
    { label: "2", value: "1" },
    { label: "3", value: "1" },
    { label: "4", value: "1" },
]

const holesData = [
    { label: "2", value: "2" },
    { label: "4", value: "4" },
    { label: "8", value: "8" },
    { label: "16", value: "16" },
]

export default class CoursePricesFilter extends Component {

    // Filter course prices

    filterCoursePrices = async (values, actions) => {

        console.log(values)
    };

    // shouldComponentUpdate(nextProps, nextState) {
    //     console.log('updated', this.props);
    // }

    render() {
        const today = Moment().format('dddd, MMM Do');

        return (
            <View style={styles.container}>

                <Formik
                    initialValues={{ date: today, persons: "", holes: "" }}
                    onSubmit={(values, actions) => this.filterCoursePrices(values, actions)}
                >
                {props => (
                    <React.Fragment>

                        {/* Date Input */}

                        <TouchableOpacity style={styles.filterOption} onPress={null}>

                            <FontAwesomeIcon icon={faCalendar} size={21} color={COLORS.darkgrey} style={styles.filterOptionIcon} />

                            <DatePickerSelect
                                value={props.values.date}
                                placeholder={"Today, Feb 7"}
                                action={props.handleChange('date')}
                            />

                        </TouchableOpacity>


                        {/* Persons Input */}

                        <View style={styles.filterOption} onPress={null}>

                            <FontAwesomeIcon icon={faUsers} size={21} color={COLORS.darkgrey} style={styles.filterOptionIcon} />

                            <PickerSelect
                                data={personsData}
                                value={props.values.persons}
                                placeholder={"Any"}
                                action={props.handleChange('persons')}
                            />

                        </View>


                        {/* Holes Input */}

                        <TouchableOpacity style={styles.filterOption} onPress={null}>

                            <FontAwesomeIcon icon={faGolfBall} size={21} color={COLORS.darkgrey} style={styles.filterOptionIcon} />

                            <PickerSelect
                                data={holesData}
                                value={props.values.holes}
                                placeholder={"Any"}
                                action={props.handleChange('holes')}
                            />

                        </TouchableOpacity>

                    </React.Fragment>
                )}
                </Formik>

            </View>
        )
    }
}


// Date Picker

const DatePickerSelect = ({ value, placeholder, action }) => (
    <DatePicker
        style={styles.datepicker}
        date={value}
        mode="date"
        placeholder={placeholder}
        format="dddd, MMM Do"
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        showIcon={false}
        customStyles={{
            dateInput: styles.dateInput,
            placeholderText: styles.datePlaceholderText,
            dateText: styles.dateText,
            dateTouchBody: styles.dateTouchBody,
            btnTextConfirm: styles.btnTextConfirm,
            datePicker: styles.datePickerSelect,
        }}
        onDateChange={action}
    />
);


// Picker Select

const PickerSelect = ({ data, value, placeholder, action }) => (
    <RNPickerSelect
        placeholder={{
            label: placeholder,
            value: null,
        }}
        placeholderTextColor={COLORS.darkgrey}
        items={data}
        onValueChange={action}
        useNativeAndroidPickerStyle={false}
        doneText="Confirm"
        style={{ ...styles }}
        value={value}
    />
);