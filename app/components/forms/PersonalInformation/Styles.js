import { StyleSheet } from "react-native";
import { COLORS } from "../../../config/constants";

export default StyleSheet.create({
  wrapper: {
    width: "100%",
    marginTop: 10,
    paddingTop: 20
  },

  inputContainer: {
    marginLeft: 30,
    marginRight: 30
  },

  backgroundImage: {
    width: 414,
    height: 403,
    flex: 1,
  },

  createAccountButton: {
    // paddingTop: 35
    paddingBottom: 35
    // borderTopColor: "#505050",
    // borderTopWidth: 1,
  },

  emailInputContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },

  emailInput: {
    flex: 1
  },

  submitButton: {
    marginTop: 20
  },

  biometryButtonIcon: {
    width: 35,
    height: 35
  },

  forgetPasswordButton: {
    marginBottom: 35,
    borderRadius: 0,
    borderWidth: 0,
    marginTop: 0
  },

  personalInformationInput: {
    backgroundColor: 'transparent',
    borderColor: 'black',
    borderWidth: 1,
    color: 'black',
    fontSize: 14,
    textAlign: 'left',
    paddingHorizontal: 20
  },

  buttonText: {
    alignSelf: 'center'
  }
});
