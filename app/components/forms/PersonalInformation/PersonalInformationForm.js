import React from "react";
import { View, Image, Alert, Text, ImageBackground, Dimensions } from "react-native";
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons'

// Modules

import { Formik } from "formik";
import * as yup from "yup";
import * as session from '../../../store/actions/session';

// Components

import AlertMessage from "../../UI/AlertMessage/AlertMessage";
import Button from "../../UI/Button";
import FormInput from "../../UI/FormInput/FormInput";
import BrandLogo from "../../UI/BrandLogo/BrandLogo";
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';
import * as RNLocalize from 'react-native-localize';

// Styles

import styles from "./Styles";
import SessionService from "../../../config/services/sessionService";

// Images


const ratio = Math.min(
  Dimensions.get('window').width / 414,
  Dimensions.get('window').height / 420,
)

const height = 420 * ratio;

export default class PersonalInformationForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    RNLocalize.removeEventListener('change', handleLocalizationChange);
  }

  // Sign in the user

  updateProfile = async (values, actions) => {
    const { updateProfile, user } = this.props

    const data = {
      firstname: values.firstname,
      lastname: values.lastname,
      phone: values.phone,
      // email: values.email,
      dob: values.dob,
    };

    // SessionService.updateProfile(user.id, user.accessToken, data)


    await this.props.updateProfile(user.id, this.props.session.accessToken, data);

    // Close Form if the user is logged in

    if (this.props.session.user.data) {
      Alert.alert('Successfuly edited user', '', [{ text: 'Ok' }])
    }
  };

  render() {
    const { isLoading, user, switchForms, error } = this.props;
    return (

      <View style={styles.wrapper}>

        <View style={styles.inputContainer}>

          {/* Form Error Message */}

          {error && <AlertMessage type={"danger"} message={error} />}

          {/* Form */}

          <Formik
            initialValues={user}
            validationSchema={validationSchema}
            onSubmit={(values, actions) => this.updateProfile(values, actions)}
          >
            {props => {

              return (
                <React.Fragment>
                  {/* Form Inputs */}


                  <FormInput
                    multiline={true}
                    onChangeText={props.handleChange("firstname")}
                    onBlur={props.handleBlur("firstname")}
                    value={props.values.firstname}
                    placeholder={translate('firstName')}
                    withLabel={true}
                    additionalStyles={styles.personalInformationInput}
                    theme={'black'}
                    error={props.touched.firstname && props.errors.firstname}
                    theme={"#00000094"}
                  />

                  <FormInput
                    multiline={true}
                    onChangeText={props.handleChange("lastname")}
                    onBlur={props.handleBlur("lastname")}
                    value={props.values.lastname}
                    withLabel={true}
                    placeholder={translate('lastName')}
                    additionalStyles={styles.personalInformationInput}
                    theme={'black'}
                    error={props.touched.lastname && props.errors.lastname}
                    theme={"#00000094"}
                  />

                  <FormInput
                    multiline={true}
                    onChangeText={props.handleChange("phone")}
                    onBlur={props.handleBlur("phone")}
                    value={props.values.phone}
                    placeholder={translate('phone')}
                    withLabel={true}
                    type="telephoneNumber"
                    keyboardType="number-pad"
                    additionalStyles={styles.personalInformationInput}
                    theme={'black'}
                    error={props.touched.phone && props.errors.phone}
                    theme={"#00000094"}
                  />

                  <FormInput
                    onChangeText={props.handleChange("email")}
                    onBlur={props.handleBlur("email")}
                    value={props.values.email}
                    placeholder={translate('email')}
                    withLabel={true}
                    type="emailAddress"
                    additionalStyles={styles.personalInformationInput}
                    editable={false}
                    error={props.touched.email && props.errors.email}
                    theme={"#00000094"}
                  />

                  <FormInput
                    onChangeText={value => props.setFieldValue('dob', value)}
                    value={props.values.dob}
                    theme={'light'}
                    withLabel={true}
                    type={'date'}
                    placeholder={translate('birthday')}
                    additionalStyles={styles.personalInformationInput}
                    data={this.state.countries}
                    editable={false}
                    error={props.touched.dob && props.errors.dob}
                  />

                  {/* Submit Buttons */}

                  <Button
                    label={translate('save')}
                    isLoading={isLoading}
                    theme={"darkblue"}
                    additionalStyles={styles.submitButton}
                    action={() => props.handleSubmit()}
                  />


                </React.Fragment>
              )
            }}
          </Formik>




          <View style={styles.createAccountButton}>
            {/* Create Account Button */}

          </View>
        </View>
      </View>
    );
  }
}

// Form initial values and validaiton schemas

// const initialValues = { email: 'willypumarol@gmail.com', phone: "809531344", cellphone: "8093302133", address: "Calle Federico Geraldino #40", city: "Santo Domingo", country: "Dominican Republic", postalCode: "49426" };

const validationSchema = yup.object().shape({
  firstname: yup.string().required("This field is required"),
  lastname: yup.string().required("This field is required"),
  phone: yup.string().matches(/^\d+$/, 'This field is numeric only').required('This field is required'),
  // phone: yup.string().matches(/^\d+$/, 'This field is numeric only').required('This field is required'),
});
