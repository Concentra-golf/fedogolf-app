import React, { Component } from 'react';
import {
    View,
    TextInput,
    Text,
    StyleSheet,
    Platform,
    Dimensions,
    TouchableOpacity
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { moderateScale, scale, verticalScale } from '../../../utilities/ScalingScreen';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faSearch, faTimes, faArrowLeft, faFilter } from '@fortawesome/free-solid-svg-icons';

import Button from '../../../components/UI/Button';

import styles from './Styles';
import { COLORS } from '../../../config/constants';

export default class SearchBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchQuery: this.props.query ? this.props.query : null
        }

    }

    render() {
        const { navigation, action, lightTheme, data } = this.props;
        return (
            <View>
                {!this.props.isButton ? (
                    <View style={styles.searchBar}>

                        <TouchableOpacity
                            style={[styles.icnSearch, lightTheme ? styles.icnSearchLight : {}]}
                            onPress={() => this.props.navigation.goBack()}
                        >
                            <FontAwesomeIcon
                                icon={faArrowLeft}
                                color={lightTheme ? COLORS.darkblue : COLORS.midgreen}
                                size={21}
                            />
                        </TouchableOpacity>

                        <View style={[styles.searchInput, lightTheme ? styles.searchInputLight : {}]} >
                            <TextInput
                                autoFocus
                                style={styles.textInput}
                                value={this.props.value}
                                placeholder={this.props.placeholder}
                                placeholderTextColor={lightTheme ? COLORS.darkblue : COLORS.midgreen}
                                autoCapitalize="none"
                                onChangeText={(e) => this.props.itemSearch(e)}
                            />
                        </View>

                        <View style={styles.filterButton}>
                            <TouchableOpacity
                                onPress={() => this.props.showFilterModal(true)}>
                                <FontAwesomeIcon
                                    icon={faFilter}
                                    color={lightTheme ? COLORS.darkblue : COLORS.midgreen}
                                    size={21}
                                />
                            </TouchableOpacity>
                        </View>

                        <View style={styles.deleteOption}>
                            {this.props.delete &&
                                <TouchableOpacity
                                    onPress={() => this.props.clearText()}>
                                    <Ionicons
                                        name='ios-close-circle'
                                        style={styles.icon}
                                        color={COLORS.darkblue}
                                        size={32}
                                    />
                                </TouchableOpacity>
                            }
                        </View>

                    </View>
                ) : (
                        <TouchableOpacity
                            style={styles.searchBar}
                            onPress={() => navigation.navigate('Search', {
                                searchQuery: this.state.searchQuery,
                                data: this.props.data
                            })}
                        >
                            <View style={[styles.icnSearch, lightTheme ? styles.icnSearchLight : {}]}>
                                <FontAwesomeIcon icon={faSearch} color={lightTheme ? COLORS.darkblue : COLORS.midgreen} size={21} />
                            </View>

                            <View style={[styles.searchInput, lightTheme ? styles.searchInputLight : {}]}>
                                <Text
                                    style={[styles.buttonPlaceHolder, { color: lightTheme ? COLORS.darkblue : COLORS.midgreen }]}
                                >
                                    {this.props.placeholder}
                                </Text>
                            </View>

                            <View style={styles.deleteOption}>
                                {this.props.delete &&
                                    <TouchableOpacity
                                        onPress={() => this.props.clearText()}>
                                        <Ionicons name='close-circle' style={styles.icon} />
                                    </TouchableOpacity>
                                }
                            </View>

                        </TouchableOpacity>
                    )
                }
            </View>
        )
    }
}
