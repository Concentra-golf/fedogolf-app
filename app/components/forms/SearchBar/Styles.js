import { StyleSheet } from 'react-native';

import { COLORS } from '../../../config/constants';
import { moderateScale, verticalScale, widthPercentageToDP, heightPercentageToDP } from '../../../utilities/ScalingScreen';

export default StyleSheet.create({
    searchBar: {
        width: "100%",
        marginBottom: 20,
        position: 'relative',
        justifyContent: 'center',
    },
    icnSearch: {
        position: 'absolute',
        top: 14,
        left: 15,
        zIndex: 1
    },
    deleteOption: {
        position: 'absolute',
        top: verticalScale(7),
        right: moderateScale(55),
        // zIndex: 1
    },
    filterButton: {
        position: 'absolute',
        top: verticalScale(12),
        right: moderateScale(30),
        // zIndex: 1
    },
    searchInput: {
        padding: 20,
        paddingLeft: 45,
        paddingRight: 45,
        paddingTop: 10,
        paddingBottom: 10,
        height: 48,
        fontSize: 18,
        color: COLORS.darkgreen,
        borderRadius: 50,
        backgroundColor: COLORS.white,
        justifyContent: 'center',
    },
    icnSearchLight: {
        left: 30,
    },
    searchInputLight: {
        marginLeft: 15,
        marginRight: 15,
        color: COLORS.darkblue,
        backgroundColor: '#F7F7F7',
        justifyContent: 'center',
    },
    textInput: {
        width: widthPercentageToDP('60%'),
        height: heightPercentageToDP('4%'),
        fontSize: 18,
    },
    icnSearchClear: {
        width: 25,
        height: 25,
        position: 'absolute',
        top: 14,
        right: 25,
        zIndex: 1
    },
    buttonPlaceHolder: {
        fontSize: moderateScale(16),
        marginTop: verticalScale(2)
    }
});
