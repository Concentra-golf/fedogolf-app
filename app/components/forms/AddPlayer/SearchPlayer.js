import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, TextInput, FlatList, SafeAreaView } from 'react-native';
import { scale, verticalScale, moderateScale, widthPercentageToDP } from '../../../utilities/ScalingScreen';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as playgolf from '../../../store/actions/playgolf';
import { COLORS } from '../../../config/constants';

import PlayerCard from '../../cards/PlayGolf/PlayerCard';
import PlayerSelected from './PlayerSelected';
import Loader from '../../UI/Loader/Loader';

class SearchPlayer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      list: [],
      loading: false,
      configurePlayer: false,
      playerInfo: {},
      notFound: false
    };
  }


  async searchPlayer() {
    const search = this.state.search;
    this.setState({ loading: true })

    await this.props.searchPlayerByEmail(search, this.props.session.accessToken);

    if (this.props.playGolf.searchPlayByEmail.success) {
      if (this.props.playGolf.searchPlayByEmail.data.length !== 0) {
        this.setState({
          list: this.props.playGolf.searchPlayByEmail.data,
          loading: false
        })
      } else {
        this.setState({
          notFound: true,
          loading: false
        })
      }
    } else {
      this.setState({
        list: [],
        loading: false
      })
    }
  }

  playerSelected(e) {
    const playerInfo = e
    this.setState({
      configurePlayer: true,
      playerSelected: playerInfo
    })
  }

  cancelPlayerSelection(e) {
    this.setState({
      configurePlayer: e
    })
  }

  addingPlayer(e) {
    const playerInfo = e
    this.props.playerSelected(playerInfo);
  }

  render() {
    return (
      <View style={styles.container}>

        {!this.state.configurePlayer ? (
          <View style={styles.content}>
            <View style={styles.wrapper}>
              <View style={styles.searchContainer}>
                <Ionicons
                  name='ios-search'
                  color={COLORS.airBnbLightGrey}
                  size={32}
                />

                <TextInput
                  style={styles.search}
                  placeholderTextColor='black'
                  placeholder='Search by mail or citizen ID'
                  onChangeText={search => this.setState({ search })}
                />
              </View>

              <TouchableOpacity
                style={styles.button}
                onPress={() => this.searchPlayer()}
              >
                <Text style={styles.textButton}>Search</Text>
              </TouchableOpacity>
            </View>


            {!this.state.loading && this.state.list.length > 0 &&
              <FlatList
                data={this.state.list}
                extraData={this.state.list}
                keyExtractor={(item, index) => index.toString()}
                contentContainerStyle={styles.contentList}
                renderItem={({ item, index }) =>
                  <PlayerCard
                    firstname={item.firstname}
                    lastname={item.lastname}
                    email={item.email}
                    onPress={() => this.playerSelected(item)}
                    route={'SearchPlayer'}
                  />
                }
              />
            }

            {!this.state.loading && this.state.notFound &&
              <Loader
                nothing={true}
                nothingMessage={'No player found'}
              />
            }

          </View>
        ) : (
            <View>
              <PlayerSelected
                playerInfo={this.state.playerSelected}
                tees={this.props.tees}
                holes={this.props.holes}
                cancel={(e) => this.cancelPlayerSelection(e)}
                addingPlayer={(e) => this.addingPlayer(e)}
              />
            </View>
          )
        }

      </View >
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
    user: state.session.user.data,
    playGolf: state.playgolf
  };
}

function mapDispatchProps(dispatch) {
  return bindActionCreators(playgolf, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchProps,
)(SearchPlayer);

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1
  },
  content: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: moderateScale(25),
    marginTop: verticalScale(10)
  },
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    height: verticalScale(40),
    paddingHorizontal: moderateScale(10),
    borderWidth: 1,
    borderRadius: scale(5)
  },
  search: {
    height: verticalScale(40),
    width: '100%',
    paddingHorizontal: moderateScale(10),
  },
  button: {
    height: verticalScale(40),
    width: moderateScale(200),
    borderWidth: 1,
    borderColor: COLORS.darkblue,
    justifyContent: 'center',
    alignItems: 'center',
    margin: scale(20),
    borderRadius: 5
  },
  textButton: {
    fontWeight: 'bold',
    fontSize: moderateScale(14),
    color: COLORS.darkblue
  },
  contentList: {
    paddingBottom: verticalScale(30),
    width: widthPercentageToDP('90%')
  }
})