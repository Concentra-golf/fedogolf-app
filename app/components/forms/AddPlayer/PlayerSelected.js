import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { widthPercentageToDP, verticalScale, moderateScale, heightPercentageToDP, scale } from '../../../utilities/ScalingScreen';
import { COLORS } from '../../../config/constants';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';

import PickerSelect from '../../../components/UI/Picker/PickerSelect';
import TextLabel from '../../UI/TextLabel';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import NativeBaseHeader from '../../UI/NativeBase/NativeBaseHeader';

class PlayerSelected extends Component {
    constructor(props) {
        super(props);
        this.state = {
            teesValue: null,
            holeValue: null,
            msg: this.props.msg,
            playerInfo: {}
        };
    }

    componentDidMount() {
        if (this.props.playerInfo.tees) {
            this.setState({
                teesValue: this.props.playerInfo.tees,
            })
        }

        if (this.props.route === 'frecuent') {
            this.setState({
                playerInfo: this.props.playerInfo.user
            })
        } else {
            this.setState({
                playerInfo: this.props.playerInfo
            })
        }

    }

    addPlayer() {
        //We create the object for the selected player
        const { playerInfo } = this.props;
        if (this.state.teesValue !== null) { // We validate if the tee is selected
            const playerInformation = {
                firstname: `${this.state.playerInfo.firstname}`,
                lastname: `${this.state.playerInfo.lastname}`,
                id: this.state.playerInfo.id,
                gender: this.state.playerInfo.gender,
                email: this.state.playerInfo.email,
                tees: this.state.teesValue,
                holeStart: this.state.holeValue
            }
            //The we sent the created object to the funtion addingPlayer locate on SearchPlayer Component
            this.props.addingPlayer(playerInformation)
        } else {
            this.setState({
                msg: 'Please Select a Tees'
            })
        }
    }

    render() {
        const { playerInfo } = this.props;
        return (
            //We validate the style with the Route if the route is equal to MainSetting this is comming from PlayGolf Component
            <View
                style={[
                    styles.container, this.props.route === 'MainSetting' ? { flex: 1, width: '100%' } : {}
                ]}>

                {this.props.route === 'MainSetting' &&
                    <NativeBaseHeader
                        title={'Player Info'}
                        leftIcon={faArrowLeft}
                        leftButton={() => this.props.close(false)}
                        theme='dark-blue'
                    />
                }

                <View
                    style={[
                        styles.playerInformation, this.props.route === 'MainSetting' ? { paddingHorizontal: moderateScale(10), width: '100%' } : {}
                    ]}>

                    <View style={styles.wrapper}>
                        <Text style={styles.title}>Name</Text>
                        <Text style={styles.subtitle}>{this.state.playerInfo.firstname} {this.state.playerInfo.lastname}</Text>
                    </View>

                    <View style={styles.wrapper}>
                        <Text style={styles.title}>Email</Text>
                        <Text style={styles.subtitle}>{this.state.playerInfo.email} </Text>
                    </View>

                    <View style={styles.wrapper}>
                        <Text style={styles.title}>Gender</Text>
                        <Text style={styles.subtitle}>{this.state.playerInfo.gender}</Text>
                    </View>

                    <View style={styles.teesContainer}>
                        <TextLabel additionalStyles={styles.pickerTitle}>Tees</TextLabel>
                        <View style={styles.pickerContainer}>
                            <PickerSelect
                                data={this.props.tees}
                                value={this.state.teesValue}
                                placeholder={'Please select your tees'}
                                action={(value) => this.setState({ teesValue: value })}
                            />
                            <FontAwesome
                                name='caret-down'
                                size={scale(15)}
                                color={COLORS.softBlack}
                            />
                        </View>
                    </View>

                    <View style={styles.teesContainer}>
                        <TextLabel additionalStyles={styles.pickerTitle}>Starting Hole</TextLabel>
                        <View style={styles.pickerContainer}>
                            <PickerSelect
                                data={this.props.holes}
                                value={this.state.startingHole}
                                placeholder={'Please select your starting hole'}
                                action={(value) => this.setState({ holeValue: value })}
                            />
                            <FontAwesome
                                name='caret-down'
                                size={scale(15)}
                                color={COLORS.softBlack}
                            />
                        </View>
                    </View>

                    <Text style={styles.errorMsg}>{this.state.msg}</Text>

                    {this.props.route !== 'MainSetting' ? (
                        <View style={styles.buttonContainer}>
                            <TouchableOpacity
                                style={styles.cancelButton}
                                onPress={() => this.props.cancel(false)}
                            >
                                <Text style={styles.buttonText}>Cancel</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => this.addPlayer()}
                                style={styles.addButton}
                            >
                                <Text style={styles.buttonText}>Add Player</Text>
                            </TouchableOpacity>
                        </View>
                    ) : (
                            <TouchableOpacity
                                style={[styles.cancelButton, { width: widthPercentageToDP('90%') }]}
                                onPress={() => this.props.deletePlayer(playerInfo)}
                            >
                                <Text style={styles.buttonText}>Delete Player</Text>
                            </TouchableOpacity>
                        )
                    }

                </View>
            </View>
        );
    }
}

export default PlayerSelected;

const styles = StyleSheet.create({
    container: {
        width: widthPercentageToDP('90%'),
        // backgroundColor: 'red'
    },
    wrapper: {
        marginTop: verticalScale(20),
        borderBottomWidth: 1,
        borderBottomColor: '#D8D8D8',
        paddingVertical: verticalScale(10),
        flexDirection: "row",
        justifyContent: "space-between"
    },
    title: {
        fontSize: moderateScale(16),
        fontWeight: 'bold',
        color: COLORS.softBlack
    },
    subtitle: {
        fontSize: moderateScale(14),
        color: COLORS.black
    },
    teesContainer: {
        width: '100%',
        marginTop: verticalScale(20)
    },
    pickerContainer: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        width: '100%',
        // backgroundColor: 'red',
        borderColor: COLORS.airBnbGrey,
        height: heightPercentageToDP('7%'),
        marginTop: verticalScale(10),
        padding: scale(5),
        alignItems: 'center',
    },
    pickerTitle: {
        fontSize: moderateScale(16),
        color: COLORS.airBnbGrey,
        fontWeight: '600'
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    cancelButton: {
        marginTop: verticalScale(20),
        marginHorizontal: moderateScale(10),
        height: verticalScale(45),
        width: moderateScale(155),
        backgroundColor: COLORS.red,
        justifyContent: 'center',
        alignItems: 'center'
    },
    addButton: {
        marginTop: verticalScale(20),
        marginHorizontal: moderateScale(10),
        height: verticalScale(45),
        width: moderateScale(155),
        backgroundColor: COLORS.green,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText: {
        fontSize: moderateScale(16),
        fontWeight: 'bold',
        color: 'white'
    },
    errorMsg: {
        fontSize: moderateScale(14),
        color: COLORS.red,
        marginTop: verticalScale(5)
    }
})