import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, FlatList } from 'react-native';
import { scale, moderateScale, verticalScale, widthPercentageToDP } from '../../../utilities/ScalingScreen';

import { COLORS } from '../../../config/constants';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as playgolf from '../../../store/actions/playgolf';
import Loader from '../../UI/Loader/Loader';
import PlayerCard from '../../cards/PlayGolf/PlayerCard';
import PlayerSelected from './PlayerSelected';

class AddFriend extends Component {
  constructor(props) {
    super(props);
    this.state = {
      playersList: [],
      loading: true,
      configurePlayer: false,
      playerInfo: {}
    };
  }

  //We Get the Frencuent player list
  async componentDidMount() {
    await this.props.getFrequenlyPlayer(this.props.user.id, this.props.session.accessToken);

    if (this.props.playGolf.frequenlyPlayers.success) {
      this.setState({
        playersList: this.props.playGolf.frequenlyPlayers.data,
        loading: false
      })
    } else {
      this.setState({
        playersList: [],
        loading: false
      })
    }
  }

  playerSelected(e) {
    const playerInfo = e
    this.setState({
      configurePlayer: true,
      playerSelected: playerInfo
    })
  }

  cancelPlayerSelection(e) {
    this.setState({
      configurePlayer: e
    })
  }

  addingPlayer(e) {
    const playerInfo = e
    this.props.playerSelected(playerInfo, 'frecuent');
  }


  render() {
    return (
      <View style={styles.container}>

        {!this.state.configurePlayer ? (
          <View style={styles.content}>
            {!this.state.loading && this.state.playersList.length > 0 &&
              <FlatList
                data={this.state.playersList}
                extraData={this.state.playersList}
                keyExtractor={(item, index) => index.toString()}
                contentContainerStyle={styles.contentList}
                renderItem={({ item, index }) =>
                  <PlayerCard
                    firstname={item.user.firstname}
                    lastname={item.user.lastname}
                    email={item.user.email}
                    onPress={() => this.playerSelected(item)}
                    route={'AddFriend'}
                  />
                }
              />
            }

            {!this.state.loading && this.state.playersList.length === 0 &&
              <Loader
                nothing={true}
                nothingMessage={'No frecuent users available'}
              />
            }
          </View>
        ) : (
            <PlayerSelected
              playerInfo={this.state.playerSelected}
              route='frecuent'
              tees={this.props.tees}
              holes={this.props.holes}
              cancel={(e) => this.cancelPlayerSelection(e)}
              addingPlayer={(e) => this.addingPlayer(e)}
            />
          )
        }
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
    user: state.session.user.data,
    playGolf: state.playgolf
  };
}

function mapDispatchProps(dispatch) {
  return bindActionCreators(playgolf, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchProps,
)(AddFriend);

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
  },
  content: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%'
  },
  contentList: {
    paddingBottom: verticalScale(30),
    width: widthPercentageToDP('90%'),
  },
})
