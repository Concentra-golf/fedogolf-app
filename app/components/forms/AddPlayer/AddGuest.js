import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, TextInput, } from 'react-native';

import NativeBaseHeader from '../../UI/NativeBase/NativeBaseHeader';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { scale, moderateScale, verticalScale, heightPercentageToDP } from '../../../utilities/ScalingScreen';
import { COLORS } from '../../../config/constants';

import { Formik } from "formik";
import * as yup from "yup";
import FormInput from '../../UI/FormInput/FormInput';
import TextLabel from '../../UI/TextLabel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import PickerSelect from '../../UI/Picker/PickerSelect';
import FontAwesome from 'react-native-vector-icons/FontAwesome';


class AddGuest extends Component {
    constructor(props) {
        super(props);
        this.state = {
            teesValue: null,
            holeValue: null
        };
    }

    guestValues(values, actions) {
        console.log(values, actions)

        const hole = parseInt(this.state.holeValue.slice(-2));
        console.log(hole);

        const guestPlayer = {
            firstname: `${values.firstname}`,
            lastname: `${values.lastname}`,
            email: values.email,
            tees: this.state.teesValue,
            start: hole,
            "guest": true,
        }
        this.props.getPlayers(guestPlayer);
        this.props.close(false);
    }

    render() {
        return (
            <View style={styles.container}>

                <NativeBaseHeader
                    title={'Add Guest'}
                    leftIcon={faArrowLeft}
                    leftButton={() => this.props.close(false)}
                    theme='dark-blue'
                />

                <KeyboardAwareScrollView
                    contentContainerStyle={styles.content}
                >
                    <Text style={styles.title}>Please enter the guest information</Text>


                    <Formik
                        validationSchema={validationSchema}
                        initialValues={this.props}
                        onSubmit={(values, actions) => this.guestValues(values, actions)}
                    >
                        {props => {
                            return (
                                <View style={styles.formContainer}>

                                    <View style={styles.inputsContainer}>
                                        <TextLabel additionalStyles={styles.pickerTitle}>First Name</TextLabel>
                                        <FormInput
                                            onChangeText={props.handleChange("firstname")}
                                            // onBlur={props.handleBlur("firstname")}
                                            // value={props.values.firstname}
                                            placeholder="First Name"
                                            additionalStyles={styles.inputs}
                                            theme={'black'}
                                            error={props.errors.firstname}
                                            theme={"#00000094"}
                                        />
                                    </View>


                                    <View style={styles.inputsContainer}>
                                        <TextLabel additionalStyles={styles.pickerTitle}>Last Name</TextLabel>
                                        <FormInput
                                            onChangeText={props.handleChange("lastname")}
                                            // onBlur={props.handleBlur("lastname")}
                                            // value={props.values.lastname}
                                            placeholder="Last Name"
                                            additionalStyles={styles.inputs}
                                            theme={'black'}
                                            error={props.errors.lastname}
                                            theme={"#00000094"}
                                        />
                                    </View>


                                    <View style={styles.inputsContainer}>
                                        <TextLabel additionalStyles={styles.pickerTitle}>Email</TextLabel>
                                        <FormInput
                                            onChangeText={props.handleChange("email")}
                                            // onBlur={props.handleBlur("email")}
                                            // value={props.values.email}
                                            placeholder="Email"
                                            type="emailAddress"
                                            additionalStyles={styles.inputs}
                                            error={props.errors.email}
                                            theme={"#00000094"}
                                        />

                                    </View>

                                    {/* <View style={styles.inputsContainer}>
                                        <TextLabel additionalStyles={styles.pickerTitle}>Alias</TextLabel>
                                        <FormInput
                                            onChangeText={props.handleChange("alias")}
                                            // onBlur={props.handleBlur("lastname")}
                                            // value={props.values.lastname}
                                            placeholder="Guest alias"
                                            additionalStyles={styles.inputs}
                                            theme={'black'}
                                            error={props.errors.lastname}
                                            theme={"#00000094"}
                                        />
                                    </View> */}

                                    <View style={styles.inputsContainer}>
                                        <TextLabel additionalStyles={styles.pickerTitle}>Tees</TextLabel>
                                        <View style={styles.pickerContainer}>
                                            <PickerSelect
                                                data={this.props.tees}
                                                value={this.state.teesValue}
                                                placeholder={'Please select your tees'}
                                                action={(value) => this.setState({ teesValue: value })}
                                            />
                                            <FontAwesome
                                                name='caret-down'
                                                size={scale(15)}
                                                color={COLORS.softBlack}
                                            />
                                        </View>
                                    </View>

                                    <View style={styles.inputsContainer}>
                                        <TextLabel additionalStyles={styles.pickerTitle}>Starting Hole</TextLabel>
                                        <View style={styles.pickerContainer}>
                                            <PickerSelect
                                                data={this.props.holes}
                                                value={this.state.startingHole}
                                                placeholder={'Please select your starting hole'}
                                                action={(value) => this.setState({ holeValue: value })}
                                            />
                                            <FontAwesome
                                                name='caret-down'
                                                size={scale(15)}
                                                color={COLORS.softBlack}
                                            />
                                        </View>
                                    </View>

                                    <TouchableOpacity
                                        style={styles.addButton}
                                        onPress={() => props.handleSubmit()}
                                    >
                                        <Text style={styles.buttonText}>Add Player</Text>
                                    </TouchableOpacity>

                                </View>
                            )
                        }}
                    </Formik>

                </KeyboardAwareScrollView>

            </View>
        );
    }
}

export default AddGuest;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    content: {
        padding: scale(10),
        justifyContent: 'center'
    },
    scrollContent: {

    },
    title: {
        fontSize: moderateScale(17),
        color: COLORS.airBnbGrey,
        fontWeight: '600'
    },
    formContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: verticalScale(20)
    },
    inputs: {
        backgroundColor: 'transparent',
        borderColor: '#8E8E93',
        borderWidth: 1,
        color: 'black',
        fontSize: moderateScale(14),
        textAlign: 'left',
        paddingHorizontal: moderateScale(20),
        height: 52,
        borderRadius: 24,
    },
    addButton: {
        marginTop: verticalScale(20),
        marginHorizontal: moderateScale(10),
        height: verticalScale(45),
        width: '100%',
        backgroundColor: COLORS.green,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: verticalScale(30)
    },
    buttonText: {
        fontSize: moderateScale(16),
        fontWeight: 'bold',
        color: 'white'
    },
    inputsContainer: {
        width: '100%',
        marginTop: verticalScale(5),
    },
    pickerTitle: {
        fontSize: moderateScale(16),
        color: COLORS.airBnbGrey,
        fontWeight: '600',
        marginBottom: verticalScale(5)
    },
    pickerContainer: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        width: '100%',
        // backgroundColor: 'red',
        borderColor: COLORS.airBnbGrey,
        height: heightPercentageToDP('7%'),
        marginTop: verticalScale(10),
        padding: scale(5),
        alignItems: 'center',
    },
})

const validationSchema = yup.object().shape({
    firstname: yup.string().required("This field is required"),
    lastname: yup.string().required("This field is required"),
    email: yup.string().required("This field is required"),
    tees: yup.string().required("This field is required"),
});
