import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, StatusBar, Platform, TouchableOpacity } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons'
import { COLORS } from '../../../config/constants';
import { verticalScale, scale, moderateScale } from '../../../utilities/ScalingScreen';

class PrivacyTerms extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar barStyle="dark-content" />

                <View style={styles.headerContainer}>
                    <TouchableOpacity
                        onPress={() => this.props.close(false)}
                    >
                        <Ionicons
                            name='ios-close'
                            size={35}
                            color={COLORS.lightBlack}
                        />
                    </TouchableOpacity>
                    <Text style={styles.mainTitle}>Privacy Policy</Text>
                </View>

                <View style={styles.content}>
                    <Text style={styles.SubText}>
                        Este GOLFERTEK garantiza que la información personal que usted envía cuenta con la seguridad necesaria. Los datos ingresados por usuario o en el caso de requerir una validación de los pedidos no serán entregados a terceros, salvo que deba ser revelada en cumplimiento a una orden judicial o requerimientos legales.
                La suscripción a boletines de correos electrónicos publicitarios es voluntaria y podría ser seleccionada al momento de crear su cuenta.
        </Text>

                    <Text style={styles.SubText}>
                        Al asociarse a una federación, asociación o liga sus datos pueden estar siendo compartidas con las mismas.
        </Text>

                    <Text style={styles.SubText}>
                        Innovix reserva los derechos de cambiar o de modificar estos términos sin previo aviso.
                </Text>
                </View>

            </SafeAreaView>
        );
    }
}

export default PrivacyTerms;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    headerContainer: {
        width: '100%',
        padding: scale(20),
    },
    mainTitle: {
        marginTop: verticalScale(10),
        fontSize: moderateScale(25),
        fontWeight: Platform.OS === 'ios' ? '600' : 'bold',
        color: COLORS.lightBlack,
    },
    content: {
        paddingHorizontal: scale(20)
    },
    SubText: {
        marginVertical: verticalScale(10),
        fontSize: moderateScale(14),
        lineHeight: scale(20),
        color: COLORS.lightBlack
    }
})