import React, { Component } from 'react'
import { Platform, StyleSheet, Text, View, Dimensions, TouchableOpacity } from 'react-native';
import AntDesing from 'react-native-vector-icons/AntDesign';
import { verticalScale, moderateScale, scale } from '../../../../utilities/ScalingScreen';

class Success extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>

        <AntDesing
          name='checkcircleo'
          size={100}
          style={{ color: 'green' }}
        />

        <Text style={styles.title}>Membership Linked!</Text>
        <Text style={styles.subtitle}>A confimation mail has been sent, please check your email</Text>

        <View style={styles.buttonContainer}>

          <TouchableOpacity
            style={styles.successButton}
            onPress={() => this.props.onClick()}
          >
            <Text style={styles.successText}>{this.props.msg}</Text>
          </TouchableOpacity>

        </View>
      </View>
    );
  }
}

export default Success;

const styles = StyleSheet.create({
  container: {
      width: moderateScale(350),
      backgroundColor: '#FFF',
      borderRadius: scale(20),
      justifyContent: 'center',
      alignItems: 'center',
      padding: scale(10)
  },
  title: {
      marginTop: verticalScale(15),
      fontSize: moderateScale(23),
      color: 'green',
      fontWeight: 'bold',
      textAlign: 'center'
  },
  subtitle: {
      marginTop: verticalScale(15),
      fontSize: moderateScale(16),
      color: '#7A869A',
      fontWeight: '500',
      textAlign: 'center',
      width: Dimensions.get('window').width / 1.2
  },
  buttonContainer: {
      marginTop: verticalScale(20)
  },
  successButton: {
      width: moderateScale(250),
      height: verticalScale(40),
      borderRadius: scale(15),
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#0083A9'
  },
  successText: {
      fontSize: moderateScale(14),
      color: '#FFF',
      fontWeight: 'bold'
  }
});