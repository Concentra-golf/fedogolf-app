import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import * as RNLocalize from 'react-native-localize';

import Button from '../../UI/Button';
import { verticalScale } from '../../../utilities/ScalingScreen';

import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';

class Step01 extends Component {
    constructor(props) {
        super(props);
        setI18nConfig(); 
        this.state = {
        };
    }

    componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);
      }
    
      componentWillUnmount() {
        RNLocalize.removeEventListener('change', handleLocalizationChange);
      }

    render() {
        return (
            <View style={styles.buttonsContainer}>
                {!this.props.showFedogolfVerification &&
                    <View style={styles.buttonContent}>
                        <Button
                            // label={"Link Account"}
                            label={translate('linkAccount')}
                            theme={"lightblue"}
                            additionalStyles={styles.linkAccount}
                            action={(e) => this.props.firstAction(e)}
                        />
                        <Button
                            // label={"Request a new membership"}
                            label={translate('newMembership')}
                            theme={"green"}
                            additionalStyles={styles.requestMembership}
                            action={(e) => this.props.secondAction(e)}
                        />
                    </View>
                }
            </View>
        );
    }
}

export default Step01;

const styles = StyleSheet.create({
    buttonsContainer: {
        alignItems: 'center',
        width: '100%',
      },
      buttonContent: {
        width: '80%',
        paddingVertical: verticalScale(30)
      },
      linkAccount: {
        width: '100%',
        paddingVertical: verticalScale(12),
        height: 'auto'
      },
      requestMembership: {
        width: '100%',
        paddingVertical: verticalScale(12),
        marginTop: verticalScale(10),
        height: 'auto'
      },
})