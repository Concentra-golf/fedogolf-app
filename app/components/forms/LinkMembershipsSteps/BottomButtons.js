import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { COLORS } from '../../../config/constants';
import { scale, verticalScale } from '../../../utilities/ScalingScreen';

class BottomButtons extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={styles.container}>
                {!this.props.showFedogolfVerification &&
                    <View style={styles.buttonsContainer}>
                        <TouchableOpacity onPress={(e) => this.props.closeModal(e)}>
                            <Text style={{ color: '#555' }}>Go Back</Text>
                        </TouchableOpacity>

                        {/* <TouchableOpacity onPress={(e) => this.props.actionButton(e)}>
                            <Text style={{ color: COLORS.green }}>See Benefits</Text>
                        </TouchableOpacity> */}

                    </View>
                }
            </View>

        );
    }
}

export default BottomButtons;

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        width: '100%',
    },
    buttonsContainer: {
        flexDirection: "row",
        paddingBottom: verticalScale(10),
        justifyContent: "space-between",
        width: "75%"
    }
})