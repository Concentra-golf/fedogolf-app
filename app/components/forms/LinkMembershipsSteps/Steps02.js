import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ActivityIndicator } from 'react-native';
import { Formik } from 'formik';
import * as RNLocalize from 'react-native-localize';

import Button from '../../UI/Button';
import FormInput from '../../UI/FormInput/FormInput';
import { moderateScale, verticalScale } from '../../../utilities/ScalingScreen';
import { COLORS } from '../../../config/constants';

import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';

class Steps02 extends Component {
    constructor(props) {
        super(props);
        setI18nConfig(); 
        this.state = {
        };
    }

    componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);
      }
    
      componentWillUnmount() {
        RNLocalize.removeEventListener('change', handleLocalizationChange);
      }

    render() {
        return (
            <View style={styles.container}>
                {this.props.showFedogolfVerification &&
                    <Formik
                        initialValues={{
                            id_number: '',
                            membership_number: '',
                        }}
                        // validationSchema={validationSchemaFedoGolf}
                        onSubmit={(values, actions) => this.props.fedogolfValidation(values, actions)}
                    >
                        {props => {
                            return (
                                <React.Fragment>

                                    {this.props.loading &&
                                        <ActivityIndicator
                                            size='large'
                                            color={COLORS.lightblue}
                                        />
                                    }

                                    {!this.props.loading &&
                                        <View style={{ paddingVertical: 30, alignItems: 'center', width: '80%', }}>
                                            <FormInput
                                                onChangeText={props.handleChange("id_number")}
                                                onBlur={props.handleBlur("id_number")}
                                                value={props.values.id_number}
                                                // placeholder="Identification / Passport"
                                                placeholder={translate('idPassport')}
                                                keyboardType={'number-pad'}
                                                error={props.touched.id_number && props.errors.id_number}
                                                theme={'light'}
                                                // keyboardType={'number-pad'}
                                                additionalStyles={[styles.fedogolfInput, { borderRadius: 30, height: 45, marginBottom: 0 }]}
                                            />

                                            <FormInput
                                                onChangeText={props.handleChange("membership_number")}
                                                onBlur={props.handleBlur("membership_number")}
                                                value={props.values.email}
                                                // placeholder="Email"
                                                placeholder={translate('email')}
                                                error={props.touched.email && props.errors.email}
                                                theme={'light'}
                                                additionalStyles={[styles.fedogolfInput, { borderRadius: 30, height: 45, marginBottom: 0, marginTop: 10, }]}
                                            />

                                            {this.props.fedogolf_user && this.props.fedogolf_user.error &&
                                                <View style={{ marginVertical: 10 }}>
                                                    <Text style={{ color: 'red' }}>{this.props.fedogolf_user.error}</Text>
                                                </View>
                                            }
                                        </View>
                                    }


                                    <Text style={[styles.msgText, this.props.success ? { color: 'green' } : { color: 'red' }]}>{this.props.msg}</Text>


                                    {!this.props.loading &&
                                        <View style={styles.bottomBtnContainer}>
                                            <TouchableOpacity onPress={(e) => this.props.goBack(e)}>
                                                <Text style={{ color: '#555', marginLeft: 10 }}>{translate('goBack')}</Text>
                                            </TouchableOpacity>
                                            <Button
                                                label={translate('continue')}
                                                theme={"lightblue"}
                                                // isLoading={this.props.session.fedogolfUser && this.props.session.fedogolfUser.isLoading || false}
                                                additionalStyles={{ paddingVertical: 8, paddingHorizontal: 10, height: 'auto', margin: 0 }}
                                                action={props.handleSubmit}
                                            />
                                        </View>
                                    }

                                </React.Fragment>

                            )
                        }}
                    </Formik>
                }
            </View>

        );
    }
}

export default Steps02;

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        width: '100%',
    },
    bottomBtnContainer: {
        width: '80%',
        alignItems: 'center',
        paddingBottom: 10,
        paddingTop: 5,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    msgText: {
        fontSize: moderateScale(14),
        marginBottom: verticalScale(10)
    }
})