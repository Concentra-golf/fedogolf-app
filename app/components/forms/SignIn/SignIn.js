import React from "react";
import { View, Image, Alert, Text, ImageBackground, Dimensions, TouchableOpacity, Platform, StatusBar } from "react-native";
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons'
import { LoginManager, LoginButton, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';

// Constants

import { FACEBOOK_LOGIN_PERMISSIONS, FACEBOOK_LOGIN_PERMISSIONS_REQUEST } from '../../../config/constants';

// Modules

import { Formik } from "formik";
import * as yup from "yup";

// Components

import AlertMessage from "../../UI/AlertMessage/AlertMessage";
import Button from "../../UI/Button";
import FormInput from "../../UI/FormInput/FormInput";
import BrandLogo from "../../UI/BrandLogo/BrandLogo";

// Styles

import styles from "./Styles";

// Images

import bgHeaderLogin from './Images/bg-header-login.png';
import bgLogin from './Images/bg-login.jpg';
import { text } from "@fortawesome/fontawesome-svg-core";
import * as RNLocalize from 'react-native-localize';

const ratio = Math.min(
  Dimensions.get('window').width / 414,
  Dimensions.get('window').height / 420,
)

const height = 420 * ratio;

import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';

export default class SignIn extends React.Component {
  constructor(props) {
    super(props);
    setI18nConfig(); // set initial config
    this.state = { user: {}, hasData: false, loadingTokenData: false, isLoading: false };
  }

  componentDidMount() {
    RNLocalize.addEventListener('change', handleLocalizationChange);
  }
  // Sign in the user

  userSignIn = async (values, actions) => {
    const data = {
      email: values.email,
      password: values.password
    };

    await this.props.signIn(data);


    // Close Form if the user is logged in

    if (this.props.session.data) {
      this.props.navigation.navigate('Tabs');
    }
  };


  userSignInWithFacebook = async (result) => {


    const data = {
      firstname: result.first_name,
      lastname: result.last_name,
      email: result.email,
      facebook_id: result.id,
    }

    const response = await this.props.facebookSignIn(data)

    // Close Form if the user is logged in

    if (this.props.session.new_user) {
      this.props.switchForms('signUpGolferInformationForm')
    } else if (this.props.session.isLoggedIn) {
      this.props.navigation.navigate('Tabs');
    } else {
      Alert.alert('Algo fallo', '', [{ text: 'Ok' }])
    }

    console.log('newUser', this.props.session.new_user)
    console.log('LoggedIN', this.props.session.isLoggedIn)

    console.log('response', response)

  }

  faceBookLogin = () => {


    const self = this;
    LoginManager.setLoginBehavior(Platform.OS === 'ios' ? 'browser' : 'web_only')
    LoginManager.logInWithPermissions(FACEBOOK_LOGIN_PERMISSIONS).then(
      function (result) {
        if (!result.isCancelled) {
          self.accessFacebookUserTokenData();
        }
      },
      function (error) {
        console.log('Login fail with error: ' + error);
      }
    );
  }

  // Access Facebook User Token Data

  accessFacebookUserTokenData = () => {

    console.log('2')

    this.setState({ loadingTokenData: true });

    const self = this;

    AccessToken.getCurrentAccessToken().then(data => {
      console.log('the token', data.accessToken.toString());

      const processRequest = new GraphRequest(FACEBOOK_LOGIN_PERMISSIONS_REQUEST, null, self.getFacebookUserData);
      // Start the graph request.
      new GraphRequestManager().addRequest(processRequest).start(); ``

    }).catch(error => {
      // console.log('the token error', error);
      self.facebookLogout();
    });

  }

  // Get the user data from Facebook
  getFacebookUserData = async (error, result) => {


    if (error) {

      console.log('error fetching data', error);

      this.setState({ loadingTokenData: false });

    } else {

      this.setState({ user: result, hasData: true, loadingTokenData: false });
      await this.userSignInWithFacebook(result)
      console.log('3')


    }

  }

  // Log out the app from Faceboook

  facebookLogout = () => {

    LoginManager.logOut();

    this.setState({ userData: {}, hasData: false, loadingTokenData: false, userStore: null });

  }

  // 4Ml7kIsM85wm+5VVF72abLNxYP8=

  componentWillUnmount() {
    RNLocalize.removeEventListener('change', handleLocalizationChange);
  }


  render() {
    const { isLoading, switchForms, error, facebookLogin } = this.props;

    return (
      <ImageBackground source={bgLogin} style={{ width: '100%' }} imageStyle={{ height: height, width: Dimensions.get('window').width }} resizeMode="contain">
        <StatusBar barStyle="dark-content" />
        <View style={styles.wrapper}>

          <ImageBackground source={bgHeaderLogin} style={{ width: '100%', height: 155 }}>
            <View style={{ position: 'relative', flexDirection: 'row', justifyContent: 'center', marginBottom: 20, width: "100%" }}>
              <TouchableOpacity onPress={() => switchForms('welcome')} style={{ position: 'absolute', left: 20, top: 15 }}>
                <FontAwesomeIcon icon={faChevronLeft} size={25} />
              </TouchableOpacity>
              <Text style={{ fontSize: 20, fontWeight: "bold", textAlign: 'center', top: 16, }}>
                {translate('login')}
              </Text>
            </View>

            <View>
              <BrandLogo size={90} />
            </View>

          </ImageBackground>
          <View style={styles.inputContainer}>

            {/* Form Error Message */}

            {error && <AlertMessage type={"danger"} message={error} />}

            {/* Form */}

            <Formik
              initialValues={initialValues}
              validationSchema={validationSchema}
              onSubmit={(values, actions) => this.userSignIn(values, actions)}
            >
              {props => (
                <React.Fragment>
                  {/* Form Inputs */}

                  <View style={styles.emailInputContainer}>
                    <View style={styles.emailInput}>
                      <FormInput
                        onChangeText={props.handleChange("email")}
                        onBlur={props.handleBlur("email")}
                        value={props.values.email}
                        placeholder={translate('email')}
                        type="emailAddress"
                        additionalStyles={styles.input}
                        error={props.touched.email && props.errors.email}
                      />
                    </View>
                  </View>

                  <FormInput
                    onChangeText={props.handleChange("password")}
                    value={props.values.password}
                    placeholder={translate('password')}
                    type="password"
                    additionalStyles={styles.input}
                    error={props.touched.password && props.errors.password}
                  />

                  {/* Password Reset Button */}

                  <Button
                    label={translate('forgotPass')}
                    // label={"Forgot Password?"}
                    // theme={"success"}
                    additionalStyles={styles.forgetPasswordButton}
                    action={() => switchForms("password")}
                  />

                  {/* Log In Button */}

                  <Button
                    // label={"Login"}
                    label={translate('login')}
                    isLoading={isLoading}
                    theme={"white"}
                    additionalStyles={styles.logginButton}
                    action={props.handleSubmit}
                  />
                </React.Fragment>
              )}
            </Formik>



            {/* Register Buttons */}

            <View style={styles.createAccountButton}>
              {/* Create Account Button */}

              <Button
                label={translate('loginFacebook')}
                // label={"Continue with Facebook"}
                theme={"facebook"}
                action={facebookLogin}
                additionalStyles={styles.facebookSignInButton}
              />


              {/* <LoginButton
                onLoginFinished={(error, result) => {
                  if (error) {
                    console.log('login has error: ' + result.error);
                  } else if (result.isCancelled) {
                    console.log('login is cancelled.');
                  } else {
                    AccessToken.getCurrentAccessToken().then(data => {
                      const accessToken = data.accessToken.toString();
                      this.getInfoFromToken(accessToken);
                    });
                  }
                }}
                permissions={["email", "public_profile"]}
                style={styles.facebookSignInButton}
                onLogoutFinished={() => this.setState({ userInfo: {} })}
              /> */}

              <Button
                label={"Register"}
                label={translate('register')}
                theme={"lightblue"}
                action={() => switchForms("signup")}
                additionalStyles={styles.registerButton}
              />



            </View>
          </View>
        </View>
      </ImageBackground>
    );
  }
}

// Form initial values and validaiton schemas

const initialValues = { email: "", password: "" };

const validationSchema = yup.object().shape({
  email: yup.string().required("Este campo es requerido"),
  password: yup.string().required("Este campo es requerido")
});
