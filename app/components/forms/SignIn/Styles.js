import { StyleSheet } from "react-native";
import { COLORS } from "../../../config/constants";

export default StyleSheet.create({
  wrapper: {
    width: "100%",
    flex: 1
  },

  inputContainer: {
    paddingTop: "32%",
    marginLeft: 30,
    marginRight: 30
  },
  input: {
    fontSize: 16,
    height: 52,
    marginBottom: 14,
  },
  backgroundImage: {
    width: 414,
    height: 403,
    flex: 1,
  },

  createAccountButton: {
    // paddingTop: 35
    paddingBottom: 35
    // borderTopColor: "#505050",
    // borderTopWidth: 1,
  },

  emailInputContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },

  emailInput: {
    flex: 1
  },

  biometryButton: {
    width: 35,
    height: 35,
    marginLeft: 10
  },

  biometryButtonIcon: {
    width: 35,
    height: 35
  },

  forgetPasswordButton: {
    marginTop: 35,
    borderRadius: 0,
    borderWidth: 0,
    marginTop: 0
  },

  logginButton: {
    marginBottom: 20,
    backgroundColor: "#fff",
    color: "#2EB673"
  },

  facebookSignInButton: {
    marginBottom: 20,
    backgroundColor: COLORS.facebookBlue,
    height: 50,
    borderRadius: 25,
    color: "#fff",
    // 
  },

  registerButton: {
    marginBottom: 10,
    backgroundColor: COLORS.lightblue,
    color: "#fff"
  },
  buttonText: {
    alignSelf: 'center'
  }
});
