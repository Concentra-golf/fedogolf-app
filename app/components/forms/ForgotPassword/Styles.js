import { StyleSheet } from "react-native";
import { COLORS } from "../../../config/constants";

export default StyleSheet.create({
  wrapper: {
    width: "100%",
    flex: 1,
    paddingTop: '2%'
  },

  buttonText: {
    alignSelf: 'center',
    marginTop: 16,
    marginBottom: 10
  },

  inputContainer: {
    paddingTop: "10%",
    marginLeft: 20,
    marginRight: 20,
  },

  signUpFormInput: {
    textAlign: 'left',
    fontSize: 16,
  },

  bottomContainer: {
    paddingTop: "25%"
  },

  backgroundImage: {
    width: 414,
    height: 403,
    flex: 1,
  },

  createAccountButton: {
    // paddingTop: 35
    paddingBottom: 35
    // borderTopColor: "#505050",
    // borderTopWidth: 1,
  },

  emailInputContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },

  emailInput: {
    flex: 1
  },

  biometryButton: {
    width: 35,
    height: 35,
    marginLeft: 10
  },

  biometryButtonIcon: {
    width: 35,
    height: 35
  },

  forgetPasswordButton: {
    marginBottom: 35,
    borderRadius: 0,
    borderWidth: 0,
    marginTop: 0
  },

  recoverPasswordButton: {
    marginBottom: 20,
    backgroundColor: "transparent",
    borderWidth: 2,
    borderColor: COLORS.lightGreen,
    color: "#2EB673"
  },

  facebookSignInButton: {
    marginBottom: 20,
    backgroundColor: COLORS.facebookBlue,
    color: "#fff"
  },

  registerButton: {
    marginBottom: 10,
    backgroundColor: COLORS.lightblue,
    color: "#fff"
  },

  switchStyles: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // marginTop: 500,
  },

  icon: {
    textAlign: 'center',
  }
});
