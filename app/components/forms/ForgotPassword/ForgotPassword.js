import React from "react";
import { View, Image, Alert, Text, ImageBackground, Dimensions, TouchableOpacity } from "react-native";
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons'
import Icon from 'react-native-vector-icons/Ionicons'

// Modules

import { Formik } from "formik";
import * as yup from "yup";

// Components

import AlertMessage from "../../UI/AlertMessage/AlertMessage";
import Button from "../../UI/Button";
import FormInput from "../../UI/FormInput/FormInput";
import BrandLogo from "../../UI/BrandLogo/BrandLogo";

// Styles

import styles from "./Styles";
import TextLabel from "../../UI/TextLabel";

// Images

import bgLogin from './Images/bg-login.jpg'
import SessionService from "../../../config/services/sessionService";


const ratio = Math.min(
  Dimensions.get('window').width / 414,
  Dimensions.get('window').height / 420,
)

const height = 420 * ratio;

export default class ForgotPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstStep: false,
      secondStep: false,
      thirdStep: false,
      email: '',
      code: '',
    };
  }

  //Component did mount

  componentDidMount() {
    this.setState({ firstStep: true })
  }

  //Switch forms

  switchForms = (firstStep, firstValue, secondStep, secondValue) => {
    this.setState({
      [firstStep]: firstValue,
      [secondStep]: secondValue
    })
  }

  // Forgot password

  userForgotPassword = async (values, actions) => {

    const data = {
      email: values.email,
      code: values.code,
      password: values.password,
      password_confirmation: values.repassword
    };

    if (this.state.firstStep) {
      await this.props.forgotPassword(data);
      Alert.alert('Request send', 'Check your email', [
        { text: 'Ok', }
      ])

      this.setState({
        firstStep: false,
        secondStep: true,
        email: values.email
      })
    } else if (this.state.secondStep) {

      try {
        const result = await SessionService.sendResetPasswordWithCode(data)
        this.setState({
          firstStep: false,
          secondStep: false,
          thirdStep: true,
          code: values.code
        })
        Alert.alert('Valid code', 'Proceed to enter your new password', [
          { text: 'Ok', }
        ])

      } catch (error) {
        if (error.response.status === 401) {
          Alert.alert('Invalid code', '', [
            { text: 'Ok', }
          ])
        }
        console.log(error)
      }

    } else if (this.state.thirdStep) {

      try {
        const result = await SessionService.sendResetPasswordWithNewPassword(data)

        Alert.alert('Password successfully updated', 'Proceed to login with your new credentials', [
          {
            text: 'Ok', onPress: () => {
              this.props.switchForms('login')
            }
          }
        ])



      } catch (error) {
        if (error.response.status === 401) {
          Alert.alert('Invalid passwords', '', [
            { text: 'Ok', }
          ])
        }
        console.log(error)
      }
    }
  };

  render() {
    const { firstStep, secondStep, thirdStep } = this.state
    const { isLoading, switchForms, error, values } = this.props;

    return (
      <React.Fragment>
        <ImageBackground source={require('./Images/iPhone.png')} style={{ width: '100%' }} imageStyle={{ height: height, width: Dimensions.get('window').width }} resizeMode="stretch">
        </ImageBackground >

        <View style={styles.wrapper}>

          <View style={styles.inputContainer}>

            <Icon name="ios-lock" size={80} style={styles.icon} color="#555555" />

            <TextLabel
              weight={"bold"}
              size={20}
              color={"black"}
              additionalStyles={styles.buttonText}
            >
              Forgot password ?
        </TextLabel>

            <TextLabel
              weight={"bold"}
              size={14}
              color={"grey"}
              additionalStyles={styles.buttonText}
            >
              Enter your registered email below to receive password reset instructions
        </TextLabel>

            {/* Form Error Message */}

            {error && <AlertMessage type={"danger"} message={error} />}

            {/* Form */}

            <Formik
              initialValues={initialValues}
              validationSchema={firstStep ? firstStepValidationSchema : secondStep ? seconsStepValidationSchema : thirdStep ? thirdStepValidationValidationSchema : {}}
              onSubmit={(values, actions) => this.userForgotPassword(values, actions)}
            >
              {props => (
                <React.Fragment>
                  {/* Form Inputs */}

                  {firstStep && <FormInput
                    onChangeText={props.handleChange("email")}
                    onBlur={props.handleBlur("email")}
                    value={props.values.email}
                    placeholder="Email"
                    type="emailAddress"
                    error={props.touched.email && props.errors.email}
                    isSignUpForm="true"
                    additionalStyles={styles.signUpFormInput}
                    theme="black"
                  />}

                  {secondStep && <FormInput
                    onChangeText={props.handleChange("code")}
                    onBlur={props.handleBlur("code")}
                    value={props.values.code}
                    placeholder="Verification Code"
                    type="name"
                    error={props.touched.code && props.errors.code}
                    isSignUpForm="true"
                    additionalStyles={styles.signUpFormInput}
                    theme="black"
                  />}

                  {thirdStep &&
                    (
                      <React.Fragment>
                        <FormInput
                          onChangeText={props.handleChange("password")}
                          onBlur={props.handleBlur("password")}
                          value={props.values.password}
                          placeholder="Password"
                          type="password"
                          error={props.touched.password && props.errors.password}
                          isSignUpForm="true"
                          additionalStyles={styles.signUpFormInput}
                          theme="black"
                        />

                        <FormInput
                          onChangeText={props.handleChange("repassword")}
                          onBlur={props.handleBlur("repassword")}
                          value={props.values.repassword}
                          placeholder="Retype password"
                          type="password"
                          error={props.touched.repassword && props.errors.repassword}
                          isSignUpForm="true"
                          additionalStyles={styles.signUpFormInput}
                          theme="black"
                        />
                      </React.Fragment>
                    )}







                  <View style={styles.bottomContainer}>

                    {/* Recover Password Button */}

                    <Button
                      label={"Continue"}
                      isLoading={isLoading}
                      theme={"white"}
                      additionalStyles={styles.recoverPasswordButton}
                      action={props.handleSubmit}

                    />

                    <TouchableOpacity onPress={() => this.props.switchForms('login')}>
                      <TextLabel
                        weight={"bold"}
                        size={16}
                        color={"black"}
                        additionalStyles={styles.buttonText}

                      >
                        Already have an account ? Sign in.
                    </TextLabel>
                    </TouchableOpacity>

                  </View>
                </React.Fragment>
              )}
            </Formik>



            {/* Register Buttons */}

            <View style={styles.createAccountButton}>
              {/* Create Account Button */}
              {/* 
            <Button
              label={"Register"}
              theme={"lightblue"}
              action={() => switchForms("signup")}
              additionalStyles={styles.registerButton}
            /> */}



            </View>
          </View>
        </View>
      </React.Fragment>
    );
  }
}

// Form initial values and validaiton schemas

const initialValues = { email: "", code: "", password: "", repassword: "" };

const firstStepValidationSchema = yup.object().shape({

  email: yup.string().email('Enter a valid email address').required("This field is required"),
});

const seconsStepValidationSchema = yup.object().shape({
  code: yup.string().required("Este campo es requerido"),
})

const thirdStepValidationValidationSchema = yup.object().shape({
  password: yup.string().required("This field is required").min(8, 'You need at least 8 characters').matches(
    /^(?=.*\d)(?=.*[A-Z])(?!.*[^a-zA-Z0-9@#$^+=])(.{8,15})$/,
    'You need to have at least one capital letter'
  ),
  repassword: yup.string().required("This field is required").oneOf([yup.ref('password')], `Passwords don't match`).min(8, 'You need at least 8 characters'),
})


