import React from "react";
import { View, Image, Alert, Text, ImageBackground, Dimensions } from "react-native";
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons'

// Modules

import { Formik } from "formik";
import * as yup from "yup";

// Components

import AlertMessage from "../../UI/AlertMessage/AlertMessage";
import Button from "../../UI/Button";
import FormInput from "../../UI/FormInput/FormInput";
import BrandLogo from "../../UI/BrandLogo/BrandLogo";

// Styles

import styles from "./Styles";

// Images


const ratio = Math.min(
  Dimensions.get('window').width / 414,
  Dimensions.get('window').height / 420,
)

const height = 420 * ratio;

export default class GolferInformationForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    console.log('props from golfer information form', this.props)
  }

  // Sign in the user

  updateProfile = async (values, actions) => {
    const { updateProfile, user } = this.props

    const data = {
      homeCourse: values.homeCourse,
      handicap: values.handicap,
      ghin_number: values.ghin_number,
      // email: values.email,
      // dob: values.dob,
    };

    // SessionService.updateProfile(user.id, user.accessToken, data)

    await this.props.updateProfile(user.id, this.props.session.accessToken, data);

    // Close Form if the user is logged in
    if (this.props.session.user.data) {
      Alert.alert('Successfuly edited user', '', [{ text: 'Ok' }])
    }
  };

  render() {
    const { isLoading, switchForms, error } = this.props;

    console.log('from the golfer information form render', this.props.user)

    return (

      <View style={styles.wrapper}>
        <View style={styles.inputContainer}>
          {/* Form Error Message */}
          {error && <AlertMessage type={"danger"} message={error} />}
          {/* Form */}
          <Formik
            initialValues={this.props.user}
            validationSchema={validationSchema}
            onSubmit={(values, actions) => this.updateProfile(values, actions)}
          >
            {props => (
              <React.Fragment>
                {/* Form Inputs */}

                <FormInput
                  onChangeText={props.handleChange("handicap")}
                  onBlur={props.handleBlur("handicap")}
                  value={props.values.handicap}
                  keyboardType="decimal-pad"
                  placeholder="Handicap"
                  type="number"
                  withLabel={true}
                  additionalStyles={styles.personalInformationInput}
                  theme={'black'}
                  error={props.touched.handicap && props.errors.handicap}
                  theme={"#00000094"}
                />
                
                {this.props.user.is_ghin_number_valid === 0 &&
                  <FormInput
                    onChangeText={props.handleChange("ghin_number")}
                    onBlur={props.handleBlur("ghin_number")}
                    value={props.values.ghin_number}
                    placeholder="Ghin number"
                    keyboardType="number-pad"
                    type="number"
                    maxLength={7}
                    withLabel={true}
                    additionalStyles={styles.personalInformationInput}
                    theme={'black'}
                    error={props.errors.ghin_number}
                    theme={"#00000094"}
                  />
                }
                
                <Button
                  label={"Guardar"}
                  isLoading={isLoading}
                  theme={"darkblue"}
                  additionalStyles={styles.submitButton}
                  action={props.handleSubmit}
                />

              </React.Fragment>
            )}
          </Formik>
        </View>
      </View>
    );
  }
}

// Form initial values and validaiton schemas

const initialValues = { homeCourse: "", handicap: "", pgaNumber: "" };

const validationSchema = yup.object().shape({
  handicap: yup.string().matches(/^(\d+\.?\d{0,2}|\.\d{1,9})$/, 'This field is numeric only and a maximun of two digits after the decimal point'),
  ghin_number: yup.string().matches(/^\d+$/, 'This field is numeric only').min(7).matches(/^\d+$/, 'the number of characters must be equal to 7'),
  // country: yup.string()
});
