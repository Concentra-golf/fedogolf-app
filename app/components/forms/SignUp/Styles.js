import { StyleSheet } from "react-native";
import { COLORS } from "../../../config/constants";

export default StyleSheet.create({
  wrapper: {
    width: "100%",
    flex: 1,
    paddingTop: '2%'
  },
  header: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: "center"
  },
  backButton: {
    position: "absolute",
    top: 5,
    left: 10
  },
  buttonText: {
    alignSelf: 'center',
    paddingTop: 0,
  },
  inputContainer: {
    paddingTop: "20%",
    marginLeft: 20,
    marginRight: 20,
  },

  signUpFormInput: {
    textAlign: 'left',
    fontSize: 14,
    borderWidth: 0,
    height: 52,
    marginBottom: 14,
    borderRadius: 24,
  },

  picker: {
    borderRadius: 30,
    backgroundColor: 'red',
  },

  bottomContainer: {
    paddingTop: "15%"
  },

  backgroundImage: {
    width: 414,
    height: 403,
    flex: 1,
  },

  createAccountButton: {
    // paddingTop: 35
    paddingBottom: 35
    // borderTopColor: "#505050",
    // borderTopWidth: 1,
  },

  emailInputContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },

  emailInput: {
    flex: 1
  },

  biometryButton: {
    width: 35,
    height: 35,
    marginLeft: 10
  },

  biometryButtonIcon: {
    width: 35,
    height: 35
  },

  forgetPasswordButton: {
    marginBottom: 35,
    borderRadius: 0,
    borderWidth: 0,
    marginTop: 0
  },

  signUpButton: {
    marginBottom: 20,
    backgroundColor: "transparent",
    borderWidth: 2,
    backgroundColor: COLORS.lightblue,
    color: "#2EB673"
  },

  facebookSignInButton: {
    marginBottom: 20,
    backgroundColor: COLORS.facebookBlue,
    color: "#fff"
  },
  registerButton: {
    marginBottom: 10,
    backgroundColor: COLORS.lightblue,
    color: "#fff"
  },
  switchContainer: {
    flexDirection: 'row',
  },
  switchStyles: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // marginTop: 500,
  },
  switchText: {
    fontSize: 18,
    color: '#fff'
  },
  switchAction: {
    fontSize: 18,
    marginLeft: 5,
    color: '#57D9FF'
  },
});
