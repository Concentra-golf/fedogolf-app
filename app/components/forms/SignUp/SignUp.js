import React from "react";
import { View, ScrollView, TextInput, Image, Alert, Text, ImageBackground, Dimensions, TouchableOpacity, KeyboardAvoidingView, Modal } from "react-native";
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons'
import SweetAlert from 'react-native-sweet-alert';

// Modules

import { Formik, withFormik } from "formik";
import * as yup from "yup";
import { GENDERS, COLORS, privacyPolicy } from '../../../config/constants'

// Components

import AlertMessage from "../../UI/AlertMessage/AlertMessage";
import Button from "../../UI/Button";
import FormInput from "../../UI/FormInput/FormInput";
import BrandLogo from "../../UI/BrandLogo/BrandLogo";
import * as RNLocalize from 'react-native-localize';

// Styles

import styles from "./Styles";
import TextLabel from "../../UI/TextLabel";

// Images

import bgHeaderLogin from './Images/bg-header-login.png';
import bgLogin from './Images/hole-bg.jpg';
import PrivacyTerms from "../Terms/PrivacyTerms";
import TermsConditions from "../Terms/TermsConditions";


const ratio = Math.min(
  Dimensions.get('window').width / 414,
  Dimensions.get('window').height / 420,
)

const height = 420 * ratio;

import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';

class SignUp extends React.Component {
  constructor(props) {
    super(props);
    setI18nConfig();
    this.state = {
      privacyModal: false,
      conditionsModal: false
    };
  }

  componentDidMount() {
    RNLocalize.addEventListener('change', handleLocalizationChange);
  }

  componentWillUnmount() {
    RNLocalize.removeEventListener('change', handleLocalizationChange);
  }

  // Sign up the user

  userSignUp = async (values, actions) => {
    const data = {
      firstname: values.firstname,
      lastname: values.lastname,
      email: values.email,
      phone: values.phone,
      password: values.password,
      password_confirmation: values.repassword,
      privacyPolicy: values.privacyPolicy,
      termsAndConditions: values.termsAndConditions,
    };
    // Sign up the user
    await this.props.signUp(data);
    // Send user to the next form and close Form if the user is logged in
    if (this.props.session.isLoggedIn) {
      this.props.switchForms('signUpGolferInformationForm')
    }
  }

  showPrivacyTerms(e) {
    this.setState({ privacyModal: e })
  }

  showConditionsTerms(e) {
    this.setState({ conditionsModal: e })
  }

  render() {
    const { switchForms, values, error, isLoading, errors, facebookLogin } = this.props
    return (
      <View style={{ backgroundColor: '#405C12' }}>
        <ImageBackground
          source={bgLogin}
          style={{ width: '100%' }}
          imageStyle={{ height: Dimensions.get('window').height + 130, width: Dimensions.get('window').width }} resizeMode="cover">

          <ImageBackground
            source={bgHeaderLogin}
            style={{ width: '100%', height: 155 }}
          >
            <View style={{ position: 'relative', flexDirection: 'row', justifyContent: 'center', marginBottom: 20, width: "100%" }}>

              <TouchableOpacity
                onPress={() => switchForms('welcome')}
                style={{ position: 'absolute', left: 20, top: 15 }}
              >
                <FontAwesomeIcon icon={faChevronLeft} size={25} />
              </TouchableOpacity>

              <Text style={{ fontSize: 20, fontWeight: "bold", textAlign: 'center', top: 16, }}>{translate('register')}</Text>
            </View>

            <View>
              <BrandLogo size={90} />
            </View>

          </ImageBackground>

          <View style={styles.wrapper}>

            <View style={styles.inputContainer}>

              {/* Form Error Message */}

              {error && <AlertMessage type={"danger"} message={error} />}

              {/* Form */}

              <Formik
                initialValues={initialValues}
                validationSchema={validationSchema}
                onSubmit={(values, actions) => this.userSignUp(values, actions)}
              >
                {props => (
                  <React.Fragment>

                    {/* Form Inputs */}

                    <FormInput
                      onChangeText={props.handleChange("firstname")}
                      onBlur={props.handleBlur("firstname")}
                      value={props.values.firstname}
                      // placeholder="First Name"
                      placeholder={translate('firstName')}
                      type="name"
                      error={props.touched.firstname && props.errors.firstname}
                      isSignUpForm="true"
                      additionalStyles={styles.signUpFormInput}
                      theme="light"
                    />


                    <FormInput
                      onChangeText={props.handleChange("lastname")}
                      onBlur={props.handleBlur("lastname")}
                      value={props.values.lastname}
                      // placeholder="Last Name"
                      placeholder={translate('lastName')}
                      type="name"
                      error={props.touched.lastname && props.errors.lastname}
                      isSignUpForm="true"
                      additionalStyles={styles.signUpFormInput}
                      theme="light"
                    />

                    <FormInput
                      onChangeText={props.handleChange("email")}
                      onBlur={props.handleBlur("email")}
                      value={props.values.email}
                      // placeholder="Email"
                      placeholder={translate('email')}
                      type="emailAddress"
                      error={props.touched.email && props.errors.email}
                      isSignUpForm="true"
                      additionalStyles={styles.signUpFormInput}
                      theme="light"
                    />

                    <FormInput
                      onChangeText={props.handleChange('phone')}
                      onBlur={props.handleBlur("phone")}
                      value={props.values.phone}
                      theme={'light'}
                      // placeholder="Phone"
                      placeholder={translate('phone')}
                      keyboardType="number-pad"
                      error={props.touched.phone && props.errors.phone}
                      additionalStyles={styles.signUpFormInput}
                    />

                    <FormInput
                      onChangeText={props.handleChange("password")}
                      value={props.values.password}
                      onBlur={props.handleBlur("password")}
                      // placeholder="Password"
                      placeholder={translate('password')}
                      type="password"
                      error={props.touched.password && props.errors.password}
                      isSignUpForm="true"
                      additionalStyles={styles.signUpFormInput}
                      theme="light"
                    />

                    <FormInput
                      onChangeText={props.handleChange("repassword")}
                      value={props.values.repassword}
                      onBlur={props.handleBlur("repassword")}
                      placeholder={translate('confirmPass')}
                      // placeholder="Retype Password"
                      type="password"
                      error={props.touched.repassword && props.errors.repassword}
                      isSignUpForm="true"
                      additionalStyles={styles.signUpFormInput}
                      theme="light"
                    />

                    <View style={styles.bottomContainer}>
                      <View style={styles.switchContainer}>

                        <FormInput
                          onValueChange={value => props.setFieldValue('privacyPolicy', value)}
                          value={props.values.privacyPolicy}
                          onBlur={props.handleBlur("privacyPolicy")}
                          type="switch"
                          error={props.touched.privacyPolicy && props.errors.privacyPolicy}
                          theme="light"
                        />

                        <Text style={styles.switchText}>{translate('accept')}</Text>

                        <TouchableOpacity
                          onPress={() => this.showPrivacyTerms(true)}
                        >
                          <Text style={styles.switchAction}>
                            {/* Privacy Policy */}
                            {translate('privacy')}
                          </Text>

                        </TouchableOpacity>
                      </View>
                      <View style={styles.switchContainer}>
                        <FormInput
                          // onChangeText={handleChange("termsAndConditions")}
                          onValueChange={value => props.setFieldValue('termsAndConditions', value)}
                          onBlur={props.handleBlur("termsAndConditions")}
                          value={props.values.termsAndConditions}
                          placeholder=""
                          type="switch"
                          error={props.touched.termsAndConditions && props.errors.termsAndConditions}
                          theme="light"
                        />
                        <Text style={styles.switchText}>{translate('accept')}</Text>
                        <TouchableOpacity
                          onPress={() => this.showConditionsTerms(true)}
                        >
                          <Text style={styles.switchAction}>
                          {translate('terms')}
                          </Text>

                        </TouchableOpacity>
                      </View>



                      {/* Sign up Button */}

                      <Button
                        // label={"Sign Up"}
                        label={translate('register')}
                        isLoading={isLoading}
                        theme={"lightblue"}
                        additionalStyles={styles.signUpButton}
                        action={props.handleSubmit}
                      />


                      <Button
                        // label={"Continue with Facebook"}
                        label={translate('loginFacebook')}
                        theme={"facebook"}
                        action={facebookLogin}
                        additionalStyles={styles.facebookSignInButton}
                      />

                      <TouchableOpacity onPress={() => this.props.switchForms('login')}>
                        <TextLabel
                          weight={"bold"}
                          size={16}
                          color={"white"}
                          additionalStyles={styles.buttonText}
                        >
                          {/* Already have an account ? Sign in. */}
                          {translate('accountAlready')}
                        </TextLabel>
                      </TouchableOpacity>

                    </View>
                  </React.Fragment>
                )}

              </Formik>


              <Modal
                visible={this.state.privacyModal}
                animationType='slide'
              >
                <PrivacyTerms
                  close={(e) => this.showPrivacyTerms(e)}
                />
              </Modal>

              <Modal
                visible={this.state.conditionsModal}
                animationType='slide'
              >
                <TermsConditions
                  close={(e) => this.showConditionsTerms(e)}
                />
              </Modal>

              {/* Register Buttons */}

              <View style={styles.createAccountButton}>
              </View>
            </View>
          </View>
        </ImageBackground>
      </View>

    );
  }
}

// Form initial values and validaiton schemas

const initialValues = { firstname: '', lastname: "", email: "", password: "", repassword: "", privacyPolicy: '', termsAndConditions: '', gender: '', phone: '' };

const validationSchema = yup.object().shape({
  firstname: yup.string().required("This field is required"),
  lastname: yup.string().required("This field is required"),
  email: yup.string().email('Enter a valid email address').required("This field is required"),
  phone: yup.string().matches(/^\d+$/, 'This field is numeric only').required('This field is required'),
  password: yup.string().required("This field is required").min(8, 'You need at least 8 characters').matches(
    /^(?=.*\d)(?=.*[A-Z])(?!.*[^a-zA-Z0-9@#$^+=])(.{8,15})$/,
    'You need to have at least one capital letter'
  ),
  repassword: yup.string().required("This field is required").oneOf([yup.ref('password')], `Passwords don't match`).min(8, 'You need at least 8 characters'),
  privacyPolicy: yup.boolean().required("This field is required"),
  termsAndConditions: yup.boolean().required("This field is required"),
});


export default SignUp

