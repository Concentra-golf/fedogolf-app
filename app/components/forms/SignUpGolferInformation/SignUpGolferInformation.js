import React from "react";
import { View, ScrollView, CheckBox, Image, Modal, Alert, Text, ImageBackground, Dimensions, TouchableOpacity, Platform } from "react-native";
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import CountryPicker from 'react-native-country-picker-modal'
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons'
import * as Sentry from '@sentry/react-native';

Sentry.init({ dsn: 'https://8cff54e161f5426ebf1ab4a57f383915@o339246.ingest.sentry.io/5194232', });

// Modules

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getValueAndLabelFromArray } from '../../../utilities/helper-functions'
import * as session from '../../../store/actions/session';

import { Formik } from "formik";
import * as yup from "yup";
import FederationService from '../../../config/services/FederationService'
import axios from 'axios'
import DatePicker from 'react-native-date-picker'

// Components

import Pdf from 'react-native-pdf';
import AlertMessage from "../../UI/AlertMessage/AlertMessage";
import Button from "../../UI/Button";
import FormInput from "../../UI/FormInput/FormInput";
import BrandLogo from "../../UI/BrandLogo/BrandLogo";

import FedoGolfSignUpForm from '../../forms/FedoGolfSignUpForm/FedoGolfSignUpForm';
import Step02 from '../../forms/LinkMembershipsSteps/Steps02'

// Styles
import styles from "./Styles";
import TextLabel from "../../UI/TextLabel";

// Images
import bgHeaderLogin from './../../../assets/images/bg-header-login.png';
import bgLogin from './../../../assets/images/hole-bg.jpg';
import FedogolfLogo from '../../../assets/images/logo-fedogolf.png';

import { COLORS } from "../../../config/constants";
import Success from "../LinkMembershipsSteps/Msg/SuccessLink";

class SignUpGolferInformation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      values: { gender: 1 },
      countryPickerVisible: false,
      showFedogolfModal: false,
      showFedogolfVerification: false,
      showFedoGolfRegister: false,
      showFedoGolfBenefits: false,
      datePickerVisible: false,
      country: { code: '', name: '' },
      federations: [],
      associations: [],
      leagues: [],
      golf_clubs: [],
      selectedFederations: [],
      selectedAssociations: [],
      selectedLeagues: [],
      federationsToPost: [],
      associationsToPost: [],
      leaguesToPost: [],
      countries: [],
      loading: false,
      msg: '',
      success: false
    };
    this.fedogolfValidation = this.fedogolfValidation.bind(this);
  }

  //Component did mount

  async componentDidMount() {
    const { session } = this.props
    const response = await FederationService.getAllFederations(session.accessToken)

    response.data.golf_clubs.unshift({ id: 0, club_name: "Ninguna" });
    response.data.leagues.unshift({ id: 0, name: "Ninguna" });
    response.data.asociations.unshift({ id: 0, name: "Ninguna" });

    this.setState({
      federations: response.data.federations,
      associations: response.data.asociations,
      leagues: response.data.leagues,
      golf_clubs: response.data.golf_clubs,
    })
  }
  // Sign up the user
  userSignUp = async (values, actions) => {
    const { switchForms, signUpGolferInformation, session, updateProfile } = this.props
    const data = {
      handicap: values.handicap,
      ghin_number: values.ghin,
      country: values.country,
      homeClub: values.homeClub,
      dob: values.dob,
      promoCode: values.promoCode,
    };
    console.log('session', session)

    // Sign up the golfer information of the user
    await updateProfile(session.user.data.id, session.accessToken, data);

    // Send user to the next form
    // Close Form if the user is logged in
    if (this.props.session.user.data) {
      this.props.navigation.navigate('Tabs');
    }
  };

  // Multi select function
  onSelectedFederationChange = selectedFederations => {
    this.setState({ selectedFederations });
  };

  onSelectedAssociationChange = selectedAssociations => {
    this.setState({ selectedAssociations });
  };

  onSelectedLeagueChange = selectedLeagues => {
    this.setState({ selectedLeagues });
  };

  toggleCountryPickerModal = () => {
    this.setState({ countryPickerVisible: true })
  }


  customHandleBlur = (typeOfSelect, id, value) => {
    const { federationsToPost, associationsToPost, leaguesToPost } = this.state

    if (typeOfSelect === 'federation') {
      let data = { id: id, membership_number: value }
      if (federationsToPost.indexOf(data) === -1) {
        let joined = federationsToPost.concat(data)
        this.setState({ federationsToPost: joined })
      }
      if (federationsToPost.indexOf(data) !== -1) {
        federationsToPost.splice(data, 1);
      }
    } else if (typeOfSelect === 'association') {
      let data = {
        id: id,
        membership_number: value
      }

      if (associationsToPost.indexOf(data) === -1) {
        let joined = associationsToPost.concat(data)
        this.setState({ associationsToPost: joined })
      }

      if (associationsToPost.indexOf(data) !== -1) {
        associationsToPost.splice(data, 1);
      }
    } else if (typeOfSelect === 'league') {

      let data = {
        id: id,
        membership_number: value
      }

      if (leaguesToPost.indexOf(data) === -1) {
        let joined = leaguesToPost.concat(data)
        this.setState({ leaguesToPost: joined })
      }

      if (leaguesToPost.indexOf(data) !== -1) {
        leaguesToPost.splice(data, 1);
      }

    }



  }

  switchGender = () => {
    this.state.values.gender === 1
      ? this.setState({ values: { gender: 2 } })
      : this.setState({ values: { gender: 1 } })
  }

  fedogolfValidation = async (values, actions) => {
    this.setState({ loading: true, msg: '' })
    console.log(values);

    const data = {
      number: values.id_number,
      email: values.membership_number,
      type: 'id_number',
      membership: 1, // Cambiar a dinamico
      // membership: this.props.federationSelected.id
    }

    try {
      let request = await this.props.fedogolfValidation(data, this.props.session.accessToken);
      if (this.props.session.fedogolfUser.success) {
        this.setState({
          success: true,
          loading: false,
          msg: 'An confimation mail has been sent, please check your email'
        })
      } else {
        this.setState({
          error: true,
          loading: false,
          msg: 'Error validating account'
        })
      }
    } catch (error) {
      this.setState({ loading: false, msg: 'Error validating account' })
    }
  }

  onRequestMembership() {
    this.setState({ showFedogolfModal: false })
    this.props.navigation.navigate('FederationSignUp', {
      user: this.props.session.user.data,
      navigation: this.props.navigation,
      associations: getValueAndLabelFromArray(this.state.associations, 'name'),
      leagues: getValueAndLabelFromArray(this.state.leagues, 'name'),
      golf_clubs: getValueAndLabelFromArray(this.state.golf_clubs, 'club_name'),
      accessToken: this.props.session.accessToken,
      session: this.props.session,
      fedogolfSignUp: this.props.fedogolfSignUp,
      fedogolfUser: this.props.session.fedogolfUser,
      route: 'SignUpGolferInformation'
    })
  }

  modalFedoGolf = () => {
    return (
      <Modal
        presentationStyle='overFullScreen'
        animationType="fade"
        transparent={true}
        visible={this.state.showFedogolfModal}
        onRequestClose={() => { this.setState({ showFedogolfModal: false }) }}
        onTouchOutside={() => { this.setState({ showFedogolfModal: false }) }}
      >
        <View style={styles.centeredView}>
          {!this.state.success &&
            <View style={styles.modalView}>

              <View style={styles.iconContainer}>
                <Image source={FedogolfLogo} style={styles.imageStyles} resizeMode={'contain'} />
              </View>

              <View style={styles.content}>
                {!this.state.showFedogolfVerification &&
                  <View style={styles.buttonsContainer}>
                    <View style={styles.buttonWrapper}>
                      <Button
                        label={"Link Account"}
                        theme={"lightblue"}
                        additionalStyles={styles.buttonStyles}
                        action={() => this.setState({ showFedogolfVerification: true })} />
                      <Button
                        label={"Request a new membership"}
                        theme={"green"}
                        additionalStyles={styles.buttonStyles}
                        action={() => this.onRequestMembership()}
                      />
                    </View>
                  </View>
                }

                <View style={{ alignItems: 'center', width: '100%', }}>
                  <Step02
                    showFedogolfVerification={this.state.showFedogolfVerification}
                    fedogolfValidation={(values, actions) => this.fedogolfValidation(values, actions)}
                    loading={this.state.loading}
                    props={this.props}
                    fedogolf_user={this.props.session.fedogolf_user}
                    msg={this.state.msg}
                    success={this.state.success}
                    goBack={() => this.setState({ showFedogolfVerification: false })}
                  />

                  {!this.state.showFedogolfVerification &&
                    <View style={styles.modalButtonsContainer}
                    >
                      <TouchableOpacity
                        onPress={() => this.setState({ showFedogolfModal: false })}
                      >
                        <Text style={{ color: '#555' }}>Go Back</Text>
                      </TouchableOpacity>

                      <TouchableOpacity
                        onPress={() => this.setState({ showFedoGolfBenefits: true })}
                      >
                        <Text style={{ color: COLORS.green }}>See Benefits</Text>
                      </TouchableOpacity>
                    </View>
                  }
                </View>
              </View>
            </View>
          }
          {this.state.success &&
            <Success
              msg={'Close Modal'}
              onClick={() => this.setState({ showFedogolfModal: false })}
            />
          }
        </View>

      </Modal>
    )
  }

  fedoGolfBenefits() {
    return (
      <Modal
        presentationStyle='fullScreen'
        animationType="slide"
        visible={this.state.showFedoGolfBenefits}
        onRequestClose={() => this.setState({ showFedoGolfBenefits: false })}
        onTouchOutside={() => this.setState({ showFedoGolfBenefits: false })}
      >
        <View style={styles.wrapper}>
          <View style={{ position: 'relative', justifyContent: 'center', width: "100%" }}>
            <TouchableOpacity onPress={() => this.setState({ showFedoGolfBenefits: false })}
              style={{ left: 20, paddingVertical: 10, paddingBottom: 15 }}
            >
              <FontAwesomeIcon icon={faChevronLeft} size={18} />
            </TouchableOpacity>
          </View>
          <Pdf
            source={{ uri: "https://fedogolf.org.do/wp-content/uploads/2020/01/MembresC3ADas2020.pdf" }}
            style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').height - 75 }} />
        </View>

      </Modal >
    );
  }

  render() {
    const { isLoading, switchForms, errors, error, values, user, session, fedogolfSignUp, fedogolfUser } = this.props;
    const golf_clubs = []
    this.state.golf_clubs.map(club => { golf_clubs.push({ value: club.id, label: club.name }) })
    return (
      <View style={{ backgroundColor: '#405C12' }}>
        <ImageBackground
          source={bgLogin}
          style={{ width: '100%' }}
          imageStyle={{ height: Dimensions.get('window').height + 130, width: Dimensions.get('window').width }}
          resizeMode="cover">

          <ImageBackground source={bgHeaderLogin} style={{ width: '100%', height: 155 }}>

            <View style={{ position: 'relative', flexDirection: 'row', justifyContent: 'center', marginBottom: 20, width: "100%" }}>
              <Text style={{ fontSize: 20, fontWeight: "bold", textAlign: 'center', top: 16, }}>Sign Up</Text>
            </View>

            <View>
              <BrandLogo size={90} />
            </View>

          </ImageBackground>
          <View style={styles.wrapper}>
            <View style={styles.inputContainer}>

              {/* Form Error Message */}

              {error && <AlertMessage type={"danger"} message={error} />}

              {/* Form */}

              <Formik
                initialValues={initialValues}
                validationSchema={validationSchema}
                onSubmit={(values, actions) => this.userSignUp(values, actions)}
              >
                {props => {
                  return (
                    <React.Fragment>
                      {/* Form Inputs */}
                      <FormInput
                        onChangeText={props.handleChange("handicap")}
                        onBlur={props.handleBlur("handicap")}
                        value={props.values.handicap}
                        placeholder="Handicap"
                        keyboardType={Platform.OS === 'ios' ? 'number-pad' : 'number-pad'}
                        error={props.touched.handicap && props.errors.handicap}
                        isSignUpForm="true"
                        additionalStyles={styles.signUpFormInput}
                        theme="light"
                      />

                      <FormInput
                        onChangeText={props.handleChange("ghin")}
                        onBlur={props.handleBlur("ghin")}
                        value={props.values.ghin}
                        placeholder="Ghin Number"
                        maxLength={7}
                        keyboardType="number-pad"
                        error={props.errors.ghin}
                        isSignUpForm="true"
                        additionalStyles={styles.signUpFormInput}
                        theme="light"
                      />

                      <TouchableOpacity
                        style={[styles.signUpFormInput, styles.customSignUpForm]}
                        onPress={() => { this.toggleCountryPickerModal() }}
                      >
                        <CountryPicker
                          withFilter
                          withFlagButton
                          withFlag
                          countryCode={this.state.country.code}
                          visible={this.state.countryPickerVisible}
                          placeholder=""
                          onClose={() => this.setState({ countryPickerVisible: false })}
                          onSelect={res => {
                            this.setState({ 'country': { name: res.name, code: res.cca2 } })
                            props.setFieldValue('country', res.name)
                            res.cca2 === 'DO' && this.setState({ showFedogolfModal: true })
                            this.setState({ countryPickerVisible: false })
                          }}
                        />
                        <Text style={{ color: this.state.country.name ? '#555' : '#8E8E93' }}>{this.state.country.name || 'Country'}</Text>
                      </TouchableOpacity>

                      <FormInput
                        key={'homeClub'}
                        onChangeText={value => props.setFieldValue('homeClub', value)}
                        value={props.values.homeClub}
                        type={'picker'}
                        theme={'light'}
                        placeholder="Home Club"
                        additionalStyles={styles.signUpFormInput}
                        data={getValueAndLabelFromArray(this.state.golf_clubs, 'club_name')}
                        editable={false}
                        error={props.touched.homeClub && props.errors.homeClub}
                      />

                      <FormInput
                        onChangeText={value => props.setFieldValue('dob', value)}
                        value={props.values.dob}
                        theme={'light'}
                        type={'date'}
                        placeholder="Date of birth"
                        additionalStyles={styles.signUpFormInput}
                        data={this.state.countries}
                        editable={false}
                        error={props.touched.dob && props.errors.dob}
                      />

                      <FormInput
                        onChangeText={props.handleChange("promoCode")}
                        onBlur={props.handleBlur("promoCode")}
                        value={props.values.promoCode}
                        placeholder="Promotional Code"
                        type="name"
                        error={props.touched.promoCode && props.errors.promoCode}
                        isSignUpForm="true"
                        additionalStyles={styles.signUpFormInput}
                        theme="light"
                      />

                      <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 20, paddingVertical: 15 }}>
                        <Button
                          label={"MALE"}
                          theme={"lightblue"}
                          additionalStyles={{ ...styles.genderBtn, ...(this.state.values.gender === 1 ? styles.genderActiveBtn : {}) }}
                          action={() => this.switchGender()}
                        />
                        <Button
                          label={"FEMALE"}
                          theme={"lightblue"}
                          additionalStyles={{ ...styles.genderBtn, ...(this.state.values.gender === 2 ? styles.genderActiveBtn : {}) }}
                          action={() => this.switchGender()}
                        />
                      </View>


                      <View style={styles.bottomContainer}>


                        {/* Sign up Button */}

                        <Button
                          label={"Continue"}
                          isLoading={isLoading}
                          theme={"lightblue"}
                          additionalStyles={styles.signUpButton}
                          action={props.handleSubmit}
                        />

                        <Button
                          label={"Skip"}
                          isLoading={isLoading}
                          theme={"white"}
                          additionalStyles={styles.skipButton}
                          action={() => this.props.navigation.navigate('Tabs')}
                        />

                      </View>
                    </React.Fragment>
                  )
                }}
              </Formik>

              {/* Register Buttons */}
              <View style={styles.createAccountButton}>

              </View>
            </View>
          </View>
        </ImageBackground>

        <View style={{
          backgroundColor: '#fff',
          alignItems: "center",
          justifyContent: 'center',
          position: this.state.datePickerVisible ? "absolute" : null,
          width: '100%',
          height: Dimensions.get('window').height,
          display: this.state.datePickerVisible ? 'flex' : 'none'
        }}
        >
          <View style={{ position: 'absolute', top: 0, flexDirection: 'row', justifyContent: 'center', marginBottom: 20, width: "100%" }}>
            <TouchableOpacity onPress={() =>
              this.state.datePickerVisible ?
                this.setState({ datePickerVisible: false }) :
                this.setState({ datePickerVisible: true })}
              style={{ position: 'absolute', left: 20, top: 20 }}>
              <FontAwesomeIcon icon={faChevronLeft} size={18} />
            </TouchableOpacity>
          </View>
        </View>


        {this.modalFedoGolf()}

        {/* </TouchableOpacity> */}
        <FedoGolfSignUpForm
          user={user}
          visible={this.state.showFedoGolfRegister || false}
          closeModal={() => this.setState({ showFedoGolfRegister: false })}
          associations={getValueAndLabelFromArray(this.state.associations, 'name')}
          leagues={getValueAndLabelFromArray(this.state.leagues, 'name')}
          golf_clubs={getValueAndLabelFromArray(this.state.golf_clubs, 'club_name')}
          accessToken={session.accessToken}
          session={session}
          fedogolfSignUp={fedogolfSignUp}
          fedogolfUser={fedogolfUser}
          route={'register'}
        />

        {this.fedoGolfBenefits()}


      </View >

    );
  }
}

// Form initial values and validaiton schemas

const initialValues = { federation: "", association: "", league: "" };

const validationSchema = yup.object().shape({
  handicap: yup.string().matches(/^(\d+\.?\d{0,2}|\.\d{1,9})$/, 'This field is numeric only and a maximun of two digits after the decimal point'),
  ghin: yup.string().matches(/^\d+$/, 'This field is numeric only').min(7).matches(/^\d+$/, 'the number of characters must be equal to 7'),
  country: yup.string()
});
const validationSchemaFedoGolf = yup.object().shape({
  membership_number: yup.string().matches(/^\d+$/, 'This field is numeric only'),
  ghin: yup.string().matches('', "Can't be empty"),
});

function mapStateToProps(state) {
  return {
    session: state.session,
    user: state.session.user.data,
    fedogolfUser: state.session.fedogolfUser
  };
}

function mapDispatchProps(dispatch) {
  return bindActionCreators(session, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchProps,
)(SignUpGolferInformation);
