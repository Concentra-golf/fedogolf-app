import { StyleSheet } from "react-native";
import { COLORS } from "../../../config/constants";
import { moderateScale, scale, verticalScale } from "../../../utilities/ScalingScreen";

export default StyleSheet.create({
  wrapper: {
    width: "100%",
    flex: 1,
    paddingTop: '2%'
  },

  buttonText: {
    alignSelf: 'center',
    paddingTop: 0,
  },

  inputContainer: {
    paddingTop: "20%",
    marginLeft: 20,
    marginRight: 20,
  },
  signUpFormInput: {
    textAlign: 'left',
    fontSize: 14,
    borderWidth: 0,
    height: 52,
    marginBottom: 14,
    borderRadius: 24,
  },

  multiSelectContainer: {
    marginBottom: 20
  },

  backgroundImage: {
    width: 414,
    height: 403,
    flex: 1,
  },

  createAccountButton: {
    paddingBottom: 35
  },

  emailInputContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },

  emailInput: {
    flex: 1
  },

  biometryButton: {
    width: 35,
    height: 35,
    marginLeft: 10
  },

  biometryButtonIcon: {
    width: 35,
    height: 35
  },

  signUpButton: {
    marginBottom: 20,
    backgroundColor: "transparent",
    borderWidth: 2,
    backgroundColor: COLORS.lightblue,
  },
  genderBtn: {
    width: '45%',
    marginBottom: 20,
    backgroundColor: "transparent",
    borderWidth: 0,
    borderRadius: 10,
    backgroundColor: COLORS.grey,

  },
  genderActiveBtn: {
    backgroundColor: COLORS.lightblue,
  },

  skipButton: {
    marginBottom: 20,
    backgroundColor: "transparent",
    // borderWidth: 2,
    // borderColor: COLORS.lightGreen,
    color: "#2EB673"
  },

  facebookSignInButton: {
    marginBottom: 20,
    backgroundColor: COLORS.facebookBlue,
    color: "#fff"
  },

  registerButton: {
    marginBottom: 10,
    backgroundColor: COLORS.lightblue,
    color: "#fff"
  },

  switchStyles: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  ///////// Modal
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: '#0007',
  },
  modalView: {
    width: '90%',
    backgroundColor: "white",
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  //Modal Styles
  iconContainer: {
    alignItems: 'center',
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#5553'
  },
  imageStyles: {
    width: 220
  },
  content: {
    alignItems: "center",
    width: '100%'
  },
  buttonsContainer: {
    alignItems: 'center',
    width: '100%',
  },
  buttonWrapper: {
    paddingVertical: scale(25),
  },
  buttonStyles: {
    width: moderateScale(320),
    marginBottom: verticalScale(5),
    paddingVertical: scale(12),
    height: verticalScale(45)
  },
  modalButtonsContainer: {
    flexDirection: "row",
    paddingBottom: 10,
    justifyContent: "space-between",
    width: "75%"
  },
  customSignUpForm: {
    backgroundColor: '#fff',
    width: '100%',
    padding: 10,
    alignItems: 'center',
    flexDirection: 'row'
  }
});
