import React from "react";
import { View, Image, Alert, Text, ImageBackground, Dimensions, TouchableOpacity } from "react-native";

import FormInput from "../../UI/FormInput/FormInput";
import MultiSelect from 'react-native-multiple-select'
import TextLabel from "../../UI/TextLabel";

export default class MultiSelectFormInput extends React.Component {
    state = {
        selectedItems: [],
        currentId: '',
        memberships: [],
    }

    onSelectedItemsChange = selectedItems => {



        this.setState({
            selectedItems,

        });
    };


    render() {
        const {
            name,
            items,
            formik,
            customHandleBlur,
            multiSelectName,
            multiSelectPlaceHolder,
            displayKey,
            typeOfSelect
        } = this.props

        const selectedItemsToMap = this.state.selectedItems.map((item, i) => items.filter(i => i.id === item))

        return (
            <React.Fragment>
                <TextLabel
                    weight={"bold"}
                    size={16}
                    color={"black"}
                    additionalStyles={{ marginBottom: 6 }}
                >
                    {multiSelectName}
                </TextLabel>
                {selectedItemsToMap.map((item, index) => {
                    return item.map((fed, i) => {
                        return (
                            <View style={{
                                borderRadius: 30,
                                borderColor: "#8FB95F",
                                borderWidth: 2,
                                flexDirection: 'row',
                                padding: 0,
                                height: 40,
                                // width: 350,
                                alignContent: 'center',
                                alignItem: 'center',
                                justifyContent: 'space-between',
                                marginBottom: 10
                            }}>
                                <View style={{
                                    paddingLeft: 20,
                                    alignContent: 'center',
                                    alignItem: 'center',
                                    justifyContent: 'center'
                                }}>
                                    <Text style={{ color: "#000", }}>
                                        {fed[displayKey]}
                                    </Text>
                                </View>
                                <FormInput
                                    onChangeText={formik.handleChange(`${name[index]}`)}
                                    value={formik.values[`${name[index]}`]}
                                    name={`${name[index]}`}
                                    placeholder={"Code"}
                                    type="name"
                                    onBlur={e => {
                                        formik.handleBlur(`${name[index]}`)
                                        customHandleBlur(typeOfSelect, fed.id, formik.values[`${name[index]}`])
                                    }}
                                    additionalStyles={{
                                        width: '65%',
                                        height: '100%',
                                        margin: 0,
                                        borderWidth: 0,
                                        backgroundColor: "#8FB95F85",
                                        alignItem: 'flex-start'
                                    }}
                                    theme={'black'}
                                />
                            </View>
                        )
                    })
                })}


                <MultiSelect
                    items={items}
                    uniqueKey='id'
                    onSelectedItemsChange={this.onSelectedItemsChange}
                    selectedItems={this.state.selectedItems}
                    selectText={multiSelectPlaceHolder}
                    searchInputPlaceholderText='Search Federations...'
                    // onChangeInput={(text) => console.warn(text)}
                    tagRemoveIconColor='#000'
                    tagBorderColor='#000'
                    tagTextColor='#000'
                    selectedItemTextColor='#000'
                    selectedItemIconColor='#000'
                    itemTextColor='#000'
                    displayKey={displayKey}
                    searchInputStyle={{ color: '#000' }}
                    styleDropdownMenu={{ borderRadius: 25, backgroundColor: 'black' }}
                    styleDropdownMenuSubsection={{ borderRadius: 25, borderWidth: 1, borderColor: '#8FB95F85' }}
                    styleInputGroup={{ borderRadius: 25 }}
                    submitButtonColor='#000'
                    submitButtonText='Submit'
                    removeSelected={false}
                    hideTags={true}
                />

            </React.Fragment>
        )
    }
}