import { StyleSheet } from "react-native";
import { verticalScale, moderateScale, scale } from "../../../../utilities/ScalingScreen";
import { COLORS } from "../../../../config/constants";

export default StyleSheet.create({
    container: {
        width: '100%',
        marginVertical: verticalScale(5),
        borderColor: COLORS.airBnbLightGrey,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: verticalScale(60),
        padding: scale(10),
    },
    contentContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    title: {
        fontSize: moderateScale(16),
        fontWeight: '600',
        fontFamily: 'Rubik-Regular',
        width: moderateScale(220)
    },
    description: {
        fontSize: moderateScale(14),
        fontWeight: '400',
        fontFamily: "Lato-Regular",
        marginVertical: verticalScale(5),
        color: COLORS.softGrey
    },
    wrapper: {
        marginVertical: verticalScale(5)
    },
    priceTotal: {
        fontSize: moderateScale(16),
        fontWeight: '700',
        marginTop: verticalScale(5),
        color: COLORS.softGrey
    },
    price: {
        fontSize: moderateScale(22),
        fontWeight: 'bold',
        color: COLORS.softBlack   
    },
      radioButon: {
        width: 25,
        height: 25,
        borderRadius: 25 / 2,
        borderWidth: 0.5,
        borderColor: COLORS.airBnbLightGrey,
        marginRight: moderateScale(20),
        justifyContent: 'center',
        alignItems: 'center'
      },
      horizontalLine: {
        // width: '90%%',
        height: verticalScale(0.7),
        backgroundColor: COLORS.lightgrey,
        left: moderateScale(50)
      }
})
