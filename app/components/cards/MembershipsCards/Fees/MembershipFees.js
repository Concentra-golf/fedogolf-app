import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';

import styles from './Styles';
import TextLabel from '../../../UI/TextLabel';

import Ionicons from 'react-native-vector-icons/Ionicons';
import { verticalScale, moderateScale } from '../../../../utilities/ScalingScreen';
import { COLORS } from '../../../../config/constants';

class MembershipFees extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeIndex: 4,

        };
    }


    render() {
        const { data, index } = this.props;
        const amount = parseInt(data.amount);
        return (
            <TouchableOpacity
                onPress={() => this.props.segmentClicked(data)}
            >
                <View style={styles.container}>

                    <View style={styles.contentContainer}>
                        <View style={styles.saveContainer}>
                            <View style={styles.radioButon}>
                                {this.props.activeIndex === data.id &&
                                    <View style={{
                                        width: 18,
                                        height: 18,
                                        borderRadius: 18 / 2,
                                        backgroundColor: COLORS.green,
                                    }}
                                    />
                                }
                            </View>
                        </View>

                        <View style={styles.wrapper}>
                            <TextLabel additionalStyles={styles.title}>{data.alias}</TextLabel>
                            <Text style={styles.priceTotal}>${data.currency} <Text style={styles.price}>{amount.toLocaleString()}.00</Text></Text>
                        </View>
                    </View>

                    <TouchableOpacity>
                        <Ionicons
                            name='ios-information-circle-outline'
                            size={30}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.horizontalLine} />
            </TouchableOpacity>
        );
    }
}

export default MembershipFees;
