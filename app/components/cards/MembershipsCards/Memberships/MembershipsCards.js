import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { COLORS } from '../../../../config/constants';
import { scale, moderateScale, verticalScale, widthPercentageToDP } from '../../../../utilities/ScalingScreen';
import Ionicons from 'react-native-vector-icons/Ionicons';

import Moment from 'moment';
import { extendMoment } from 'moment-range';
import MembershipsInfo from '../../../../views/Settings/Membership/MembershipsInfo';
import RenewMembership from '../../../UI/Modals/RenewMembership';
import TextLabel from '../../../UI/TextLabel';

const moment = extendMoment(Moment);

import * as RNLocalize from 'react-native-localize';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../../translations/translation';


class MembershipsCards extends Component {
    constructor(props) {
        super(props);
        this.state = {
            expired: false,
            visible: false,
            msg: '',
            step: ''
        };
    }

    componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);
        const { name, expirationDate, allInfo, route } = this.props;
        let info = {}
        if (route === 'benefits') {
            info = allInfo
        } else {
            info = allInfo.users[0]
        }

        if (info.pivot.active === 1) {
            const text = translate('validUntil') + ":" + " " + `${moment(expirationDate).locale('en').format('DD MMMM YYYY')}`;
            this.setState({
                expired: false,
                msg: text
            })
        } else {
            if (this.props.allInfo.un_paid_request_payments.length > 0) {
                if (allInfo.un_paid_request_payments[0].approve === 0 && allInfo.un_paid_request_payments[0].transfers_uploaded === 0) {
                    this.setState({
                        expired: false,
                        msg: translate('ValidationMembership'),
                        step: 'validating'
                    })
                } else if (allInfo.un_paid_request_payments[0].transfers_uploaded === 0) {
                    this.setState({
                        expired: false,
                        msg: translate('PayMembership'),
                        step: 'pay'
                    })
                } else if (allInfo.un_paid_request_payments[0].transfers_uploaded === 1) {
                    this.setState({
                        expired: false,
                        msg: translate('ValidatingPay'),
                        step: 'valiMembership'
                    })
                } else {
                    this.setState({
                        expired: true,
                        msg: translate('Expired'),
                    })
                }
            } else {
                this.setState({
                    expired: true,
                    msg: translate('Expired'),
                })
            }
        }

    }

    showInfoModal(e) {
        this.setState({ visible: e })
    }


    onPressbutton() {
        const { route, allInfo } = this.props;

        let info = {}
        if (route === 'benefits') {
            info = allInfo
        } else {
            info = allInfo.users[0]
        }

        // WE USE ref ON EVERY STATE OF THE MEMBERSHIP TO VALIDATE THE CORRECT ROUTE.
        switch (this.state.step) {
            case 'validating':
                this.props.navigation.navigate('MembershipsInfo', {
                    name: this.props.allInfo.name,
                    expirationDate: info.pivot.validity,
                    allInfo: this.props.allInfo,
                    expired: false,
                    validating: true
                })
                break;
            case 'valiMembership':
                this.props.navigation.navigate('MembershipsInfo', {
                    name: this.props.allInfo.name,
                    expirationDate: info.pivot.validity,
                    allInfo: this.props.allInfo,
                    expired: false,
                    validating: true
                })
                break;
            case 'pay':
                this.props.navigation.navigate('OptionsSelector', {
                    name: this.props.allInfo.name,
                    expirationDate: info.pivot.validity,
                    allInfo: this.props.allInfo,
                    expired: this.state.expired,
                    route: "MembershipCard",
                    paymentData: this.props.allInfo.un_paid_request_payments[0]
                })
                break;
            default:
                this.props.navigation.navigate('MembershipsInfo', {
                    name: this.props.allInfo.name,
                    expirationDate: info.pivot.validity,
                    allInfo: this.props.allInfo,
                    expired: this.state.expired,
                    validating: false
                })
                break;
        }
    }

    render() {
        const { name, expirationDate, allInfo } = this.props;
        return (
            <TouchableOpacity
                onPress={this.props.route === 'benefits' ? (e) => this.props.onClick(e) : (e) => this.onPressbutton()}
                style={styles.membershipCard}
            >
                <View>
                    <Text style={styles.membershipsTitle}>{name}</Text>
                    <Text style={[styles.membershipValidity, this.state.expired ? { color: 'red', fontSize: moderateScale(15) } : {}]}>{this.state.msg}</Text>
                </View>

                <Ionicons
                    name='ios-arrow-forward'
                    size={30}
                    color={COLORS.airBnbLightGrey}
                />

                {/* <MembershipsInfo
                    visible={this.state.visible}
                    showInfoModal={(e) => this.showInfoModal(e)}
                    info={allInfo}
                    expired={this.state.expired}
                /> */}

            </TouchableOpacity >
        );
    }
}

export default MembershipsCards;

const styles = StyleSheet.create({
    membershipCard: {
        width: widthPercentageToDP('95%'),
        borderRadius: 3,
        padding: scale(10),
        marginBottom: verticalScale(10),
        borderColor: COLORS.darkgrey,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: COLORS.softGrey
    },
    membershipsTitle: {
        fontSize: moderateScale(16),
        fontWeight: '600',
        marginBottom: verticalScale(5),
        color: COLORS.airBnbGrey,
    },
    membershipValidity: {
        fontSize: moderateScale(13),
        fontWeight: '600',
        color: COLORS.softBlack
    },
    validatingContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    validatingText: {
        fontSize: moderateScale(14),
        fontWeight: '500'
    }
})