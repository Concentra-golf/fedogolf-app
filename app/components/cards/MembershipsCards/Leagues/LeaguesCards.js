import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';

import styles from './Styles';

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';

class FederationCards extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { data, user } = this.props;
    return (
      <TouchableOpacity
        style={styles.federationButton}
        // onPress={() => this.props.navigation.navigate('FederationMembershipDetail', {
        //   user,
        //   membership: data,
        // })}
      >
        <View style={styles.federationContainer}>

          <View style={styles.imageContainer}>
            <Image
              style={styles.federationIcon}
              resizeMode={"center"}
              source={{ uri: data.federation_img }}
            />
          </View>

          <View style={styles.textContainer}>
            <Text style={styles.title}>{data.name}</Text>
          </View>

        </View>
        <FontAwesomeIcon icon={faChevronRight} size={20} color={'#555'} />
      </TouchableOpacity>
    );
  }
}

export default FederationCards;
