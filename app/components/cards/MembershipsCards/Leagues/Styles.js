import { StyleSheet } from "react-native";
import { verticalScale, moderateScale } from "../../../../utilities/ScalingScreen";
import { COLORS } from "../../../../config/constants";

export default StyleSheet.create({
    container: {
        backgroundColor: "#fff"
    },
    federationButton: {
        width: "100%",
        height: verticalScale(90),
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: "space-between",
        borderBottomColor: '#5553',
        borderBottomWidth: 0.8,
        paddingHorizontal: moderateScale(10)
    },
    listStyles: {
        paddingTop: verticalScale(20),
        alignItems: 'center'
    },
    federationContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'center'
    },
    imageContainer: {
        paddingHorizontal: moderateScale(15),
        justifyContent: 'center',
        marginRight: moderateScale(5),
        width: 100,
        height: 100,
    },
    federationIcon: {
        width: 65,
        height: 65,
        marginTop: verticalScale(10),
        marginBottom: verticalScale(5)
    },
    textContainer: {
        width: moderateScale(200),
        height: verticalScale(50),
        justifyContent: 'center',
    },
    title: {
        fontSize: moderateScale(16),
        fontWeight: "bold",
        color: COLORS.airBnbGrey,
    }
})
