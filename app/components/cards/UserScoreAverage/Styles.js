import { StyleSheet, Platform } from "react-native";

import { COLORS } from "../../../config/constants";
import { verticalScale, scale, moderateScale } from "../../../utilities/ScalingScreen";

export default StyleSheet.create({
    container: {
        backgroundColor: COLORS.black,
    },
    scoresContainer: {
        paddingHorizontal: moderateScale(20),
        height: 120,
        width: '100%',
    },
    scoresInner: {
        flexDirection: 'row',
        justifyContent: "space-between"
    },
    scoreDataWrapper: {
        alignItems: 'center'
    },
    userImageContainer: {
        top: verticalScale(75),
        position: "absolute",
        alignItems: 'center',
    },
    imageContainer: {
        flexDirection: 'row',
        width: moderateScale(300),
        alignItems: 'center',
        justifyContent: 'center'
    },
    userImage: {
        width: 100,
        height: 100,
        marginBottom: 5,
        borderColor: '#fff',
        borderWidth: 3,
        borderRadius: 50,
    },
    badgeImageContainer: {
        borderRadius: scale(60 / 2),
        borderColor: "white",
        borderWidth: 1,
        width: 60,
        height: 60,
        marginTop: verticalScale(10),
        marginRight: moderateScale(10),
        marginLeft: moderateScale(10),
    },
    badgeImage: {
        borderColor: "white",
        backgroundColor: 'white',
        width: 60,
        height: 60,
        borderRadius: scale(60 / 2),
        // top: verticalScale(15)
    },
    badgeImageQr: {
        borderColor: "white",
        borderWidth: 1,
        width: 60,
        height: 60,
        marginTop: verticalScale(10),
        marginRight: moderateScale(10),
        marginLeft: moderateScale(10),
    },
    modalContainer: {
        position: 'absolute',
        backgroundColor: 'rgba(1, 1, 1, 0.5)',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%'
    },
    containerBox: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        width: moderateScale(300),
        height: verticalScale(200),
        borderRadius: scale(20)
    },
});
