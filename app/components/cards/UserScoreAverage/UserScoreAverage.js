import React, { Component } from 'react';
import { View, Image, Modal, TouchableOpacity, TouchableWithoutFeedback, StatusBar } from 'react-native';

// Components

import TextLabel from '../../UI/TextLabel';

// Styles

import styles from './Styles';
import UserQR from '../../UI/Modals/UserQR';

export default class UserScoreAverage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            user: {}
        };
        this.showModalQr = this.showModalQr.bind(this);
    }

    async componentDidMount() {
        StatusBar.setBarStyle('light-content', true);
        // const user = JSON.parse(await AsyncStorage.getItem('userInfo'));
        // this.setState({
        //     user: user
        // })

    }


    showModalQr(e) {
        this.setState({ showModal: e })
    }

    render() {
        const { data, user } = this.props;
        return (
            <View style={{ alignItems: "center" }}>
                <View style={styles.scoresContainer}>

                    <View style={styles.scoresInner}>

                        <ScoreData label={"Handicap"} data={'11'} />
                        {/* <ScoreData label={"Handicap"} data={user && user.handicap && user.handicap} /> */}
                        <ScoreData label={"Avg Score"} data={data.avg} />
                        <ScoreData label={"Rounds"} data={data.rounds} />

                    </View>

                </View>

                <View style={styles.userImageContainer}>
                    <View style={styles.imageContainer}>


                        {/* {user && user.federations.length > 0 ? (
                            <View style={styles.badgeImageContainer}>
                                {user.federations.slice(0, 1).map((item, index) =>
                                    <Image
                                        key={index}
                                        source={{ uri: item.federation_img }}
                                        style={styles.badgeImage}
                                        // source={{ uri: 'https://fedogolf.org.do/wp-content/uploads/2018/03/fedogolf-escudo.jpg' }}
                                        resizeMode='center'
                                    />
                                )}
                            </View>
                        ) : (
                                <View style={[styles.badgeImage, { borderWidth: 0 }]} />
                            )
                        } */}


                        <Image
                            style={styles.userImage}
                            source={{ uri: user && user.profile_img && user.profile_img }}
                        // source={{ uri: 'http://bo.golfertek.com/assets/images/avatars/golfer_male.png' }}
                        />

                        {/* <TouchableOpacity
                            onPress={() => this.showModalQr(true)}
                        >
                            <Image
                                source={{ uri: 'https://www.qr-code-generator.com/wp-content/themes/qr/new_structure/markets/core_market_full/generator/dist/generator/assets/images/websiteQRCode_noFrame.png' }}
                                style={styles.badgeImageQr}
                            />
                        </TouchableOpacity> */}

                    </View>
                    <TextLabel size={24} font={'secondarybold'} align={"center"}>{`${user && user.firstname && user.firstname} ${user && user.lastname && user.lastname}`}</TextLabel>
                </View>


                <Modal
                    transparent={true}
                    visible={this.state.showModal}
                    animationType={'fade'}
                    statusBarTranslucent
                    presentationStyle={'overFullScreen'}
                >
                    <View style={styles.modalContainer} />
                    <View style={{ justifyContent: 'center', alignItems: 'center', height: '100%' }}>
                        <View style={styles.containerBox}>

                            <UserQR
                                showModal={this.state.showModal}
                                showModalQr={(e) => this.showModalQr(e)}
                                user={user}
                            />

                        </View>
                    </View>

                </Modal>

            </View>
        )
    }
}



const ScoreData = ({ label, data }) => (
    <View style={styles.scoreDataWrapper}>
        <TextLabel size={35} color={'white'} font={'secondarybold'}>{data}</TextLabel>
        <TextLabel size={18} color={'white'} font={'secondarybold'} transform={"uppercase"}>{label}</TextLabel>
    </View>
);