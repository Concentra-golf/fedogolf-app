import { StyleSheet } from "react-native";

import { COLORS } from "../../../config/constants";
import { scale, verticalScale, moderateScale } from "../../../utilities/ScalingScreen";

export default StyleSheet.create({
    badgesContainer: {
        flexDirection: 'row',
        width: '100%',
        marginLeft: moderateScale(80),
        marginTop: verticalScale(30),
    },
    buttons: {
        borderBottomWidth: 1,
        borderBottomColor: "#D6D6D6",
        borderRadius: 0,
    },
    headerInner: {
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
        marginTop: verticalScale(5)
    },
    imageBackground: {
        width: '100%',
        height: verticalScale(180)
    },
    profileImage: {
        borderRadius: 100,
        borderColor: "white",
        borderWidth: 1,
        width: 73,
        height: 73,
        marginTop: verticalScale(10),
        marginBottom: verticalScale(5)
    },
    badgeImage: {
        borderRadius: scale(50 / 2),
        borderColor: "white",
        backgroundColor: 'white',
        borderWidth: 1,
        width: 45,
        height: 45,
        marginTop: verticalScale(10),
        marginRight: moderateScale(10),
        marginLeft: moderateScale(10),
        top: verticalScale(15)
    },
    badgeImageQr: {
        // borderRadius: scale(50 / 2),
        borderColor: "white",
        borderWidth: 1,
        width: 45,
        height: 45,
        marginTop: verticalScale(10),
        marginRight: moderateScale(10),
        marginLeft: moderateScale(10),
        top: verticalScale(15)
    },
    modalContainer: {
        position: 'absolute',
        backgroundColor: 'rgba(1, 1, 1, 0.5)',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%'
    },
    containerBox: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        width: moderateScale(300),
        height: verticalScale(200),
        borderRadius: scale(20)
    },
});
