import React, { Component } from 'react';
import { View, Image, TouchableOpacity, ImageBackground, Alert, Modal, TouchableWithoutFeedback, StatusBar } from 'react-native';


// Modules

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faGolfBall, faCamera } from '@fortawesome/free-solid-svg-icons';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as session from '../../../store/actions/session';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';
import * as RNLocalize from 'react-native-localize';

// Components

import TextLabel from '../../UI/TextLabel';
import ImagePicker from 'react-native-image-picker';
// Styles

import styles from './Styles';

// Images

import bgDrawerUserCard from './Images/bg-drawer-user-card.jpg';

// Constants

import { Base64, API_URL } from '../../../config/constants';
import SessionService from '../../../config/services/sessionService';
import UserQR from '../../UI/Modals/UserQR';
import { verticalScale } from '../../../utilities/ScalingScreen';



class SettingsUserCard extends Component {

    constructor(props) {
        super(props);
        setI18nConfig();
        this.state = {
            showModal: false,
            photo: '',
        };
        this.showModalQr = this.showModalQr.bind(this);
    }


    componentDidMount() {
        StatusBar.setBarStyle('light-content', true);
        RNLocalize.removeEventListener('change', handleLocalizationChange);
    }

    editProfilePic = async () => {
        const options = {
            title: 'Select Avatar',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
            maxWidth: 500,
            maxHeight: 500,
            quality: 1
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                console.log('response', response)
                this.handleUploadPhoto(`data:${response.type};base64,${response.data}`)
                Alert.alert('Successfuly edited profile picture', '', [{ text: 'Ok' }])
            }
        });
    }

    handleUploadPhoto = async (image_uri) => {
        let data = {
            profile_img: image_uri
        }
        const response = await this.props.updateProfile(this.props.user.id, this.props.session.accessToken, data)
    };

    showModalQr(e) {
        this.setState({ showModal: e })
    }

    render() {
        const { editBtn, linkBtn, editing } = this.props
        return (
            <View style={styles.container}>

                <ImageBackground
                    source={require('../../../assets/images/scroll-header-bg.png')}
                    resizeMode={"cover"}
                    style={styles.imageBackground}
                >

                    <View style={styles.headerInner}>

                        <View style={styles.badgesContainer}>
                            <TouchableOpacity onPress={() => this.editProfilePic()}>
                                <View>

                                    <Image
                                        source={{ uri: this.props.user && this.props.user.profile_img }}
                                        style={[styles.profileImage, editing && { width: 120, height: 120 }]}
                                    />

                                    {editing && (
                                        <TouchableOpacity onPress={() => {
                                            this.editProfilePic()
                                        }}>
                                            <View style={{
                                                position: 'absolute',
                                                bottom: -2,
                                                right: 0,
                                                backgroundColor: '#191E46',
                                                padding: 8,
                                                borderRadius: 30,
                                                borderWidth: 1,
                                                borderColor: '#fff'
                                            }}>
                                                <FontAwesomeIcon color={'#fff'} size={16} icon={faCamera}></FontAwesomeIcon>
                                            </View>
                                        </TouchableOpacity>
                                    )}
                                </View>
                            </TouchableOpacity>
                        </View>

                        <View
                            style={{
                                width: '100%',
                            }}
                        >
                            {editing == null && this.props.user &&
                                <TextLabel
                                    additionalStyles={{ fontWeight: 'bold', marginLeft: 40, }}
                                    size={22}
                                    color={'white'}
                                >
                                    {this.props.user.firstname} {this.props.user.lastname}
                                </TextLabel>
                            }
                        </View>

                        {editBtn && (
                            <TouchableOpacity
                                onPress={editBtn}
                                style={{
                                    paddingVertical: 8,
                                    paddingHorizontal: 12,
                                    marginTop: 5,
                                    borderRadius: 30,
                                    backgroundColor: '#50AF99'
                                }}>
                                <TextLabel color={'white'} size={14}>{translate('editProfile')}</TextLabel>
                            </TouchableOpacity>
                        )}

                        {linkBtn && (
                            <TouchableOpacity
                                onPress={linkBtn}
                                style={{
                                    paddingVertical: 8,
                                    paddingHorizontal: 20,
                                    marginTop: 5,
                                    borderRadius: 30,
                                    backgroundColor: '#50AF99'
                                }}>
                                <TextLabel color={'white'} size={14}>Link Federation</TextLabel>
                            </TouchableOpacity>
                        )}
                    </View>
                </ImageBackground>

                <Modal
                    transparent={true}
                    visible={this.state.showModal}
                    animationType={'fade'}
                    statusBarTranslucent
                    presentationStyle={'overFullScreen'}
                >
                    <View style={styles.modalContainer} />

                    <View style={{ justifyContent: 'center', alignItems: 'center', height: '100%' }}>
                        <View style={styles.containerBox}>
                            <UserQR
                                showModal={this.state.showModal}
                                showModalQr={(e) => this.showModalQr(e)}
                                user={this.props.user}
                            />
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        session: state.session,
        user: state.session.user.data
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators(session, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchProps,
)(SettingsUserCard);

