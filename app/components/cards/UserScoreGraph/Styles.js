import { StyleSheet, Dimensions } from 'react-native';

export default StyleSheet.create({
  wrapper: {
    width: Dimensions.get('window').width - 30,
    height: 150,
    backgroundColor: 'white',
  },
  chart: {
    height: '78%',
    // marginVertical: 10
    marginBottom: 10
  },
  axis: {
    // marginHorizontal: -10,
  },
});
