import * as React from 'react';
import { View } from 'react-native';
import { XAxis, AreaChart, Grid, LineChart } from 'react-native-svg-charts';
import * as shape from 'd3-shape';

import styles from './Styles';
// Styles


export default function UserScoreGraph({ data }) {
  return (
    <View style={styles.wrapper}>
      <LineChart
        data={data}
        // contentInset={{ top: 20, bottom: 20 }}
        animate={true}
        style={styles.chart}
        data={data} showGrid={false}
        numberOfTicks={0}
        svg={{ stroke: '#191E46' }}
      // svg={{ stroke: 'rgb(134, 65, 244)' }}
      >
        <Grid />
      </LineChart>
      <XAxis
        style={styles.axis}
        data={data}
        formatLabel={(value, index) => data[index]}
        contentInset={{ left: 10, right: 10 }}
        svg={{ fontSize: 16, fontWeight: 'bold', fill: 'black' }}
      />
    </View >

  );
}


// import React from 'react'
// import { View } from 'react-native';
// import { AreaChart, Grid, XAxis, LineChart } from 'react-native-svg-charts'
// import { Defs, LinearGradient, Stop, Path } from 'react-native-svg'
// import styles from './Styles';
// import * as shape from 'd3-shape'

// export default function UserScoreGraph({ data }) {

//   const Gradient = ({ index }) => (
//     <Defs key={index}>
//       <LinearGradient id={'gradient'} x1={'0%'} y1={'0%'} x2={'0%'} y2={'100%'}>
//         {/* <Stop offset={'0%'} stopColor={'#0075C3'} stopOpacity={0.25} /> */}
//         <Stop offset={'0%'} stopColor={'#29648B'} stopOpacity={0.40} />
//         <Stop offset={'50%'} stopColor={'#1CB586'} stopOpacity={0.2} />
//         <Stop offset={'100%'} stopColor={'#1CB586'} stopOpacity={0.01} />
//       </LinearGradient>
//     </Defs>
//   )

//   const Line = ({ line }) => (
//     <Path
//       key={'line'}
//       d={line}
//       stroke={'#191E46'}
//       fill={'none'}
//     />
//   )


//   return (
//     <View style={styles.wrapper}>
//       <AreaChart
//         style={styles.chart}
//         data={data}
//         numberOfTicks={0}
//         contentInset={{ top: 20, bottom: 20 }}
//         svg={{
//           fill: 'url(#gradient)',
//           stroke: 'rgb(25, 30, 70)',
//           strokeWidth: 2.5
//         }}
//       >
//         <Grid />
//         <Line />
//         <Gradient />
//       </AreaChart>
//       <XAxis
//         style={styles.axis}
//         data={data}
//         formatLabel={(value, index) => data[index]}
//         contentInset={{ left: 10, right: 10 }}
//         svg={{ fontSize: 16, fontWeight: 'bold', fill: 'black' }}
//       />
//     </View >
//   )
// }

