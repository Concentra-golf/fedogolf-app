import * as React from 'react';
import { View, ScrollView } from 'react-native';

// Components

import CourseInfoItem from './CourseInfoItem';

// Styles

import styles from './Styles';

export default function CourseInfo({ data }) {
  return (
    <View style={styles.container}>

      <CourseInfoItem icon={"user"} info={data.golf_club.contact_name} additionalStyles={styles.itemContainer} />
      <CourseInfoItem icon={"phone"} info={data.golf_club.phone} additionalStyles={styles.itemContainer} />
      <CourseInfoItem icon={"palette"} info={data.course_architect} additionalStyles={styles.itemContainer} />
      <CourseInfoItem icon={"mapmarker"} info={data.golf_club.address} additionalStyles={styles.itemContainer} />
      <CourseInfoItem icon={"golfball"} info={`${data.holes} holes`} additionalStyles={styles.itemContainer} />
      <CourseInfoItem icon={"web"} info={data.golf_club.website} additionalStyles={styles.itemContainer} />
      <CourseInfoItem icon={"clock"} info={"6:30 - 18:30"} additionalStyles={styles.itemContainer} />
      <CourseInfoItem icon={"email"} info={data.golf_club.email} additionalStyles={styles.itemContainer} link={true} />
      <CourseInfoItem icon={"sun"} info={"26°C"} additionalStyles={styles.itemContainer} />
      <CourseInfoItem icon={"expand"} info={`Par: ${data.par}`} additionalStyles={styles.itemContainer} />

    </View>
  );
}
