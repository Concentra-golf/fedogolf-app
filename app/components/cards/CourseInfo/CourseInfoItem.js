import React from 'react';
import {View, TouchableOpacity, Linking} from 'react-native';

// Modules

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import {
    faUser,
    faPalette,
    faGolfBall,
    faClock,
    faSun,
    faExpandAlt,
    faPhoneAlt,
    faMapMarkerAlt,
    faEnvelope,
    faGlobe
} from '@fortawesome/free-solid-svg-icons';

// Components

import TextLabel from '../../UI/TextLabel';

// Styles

import styles from './Styles';

// Constants

import { COLORS } from '../../../config/constants';

// Utilities

import { stripUrl } from '../../../utilities/helper-functions';

// Icons

const icons = {
    default: faUser,
    user: faUser,
    palette: faPalette,
    golfball: faGolfBall,
    clock: faClock,
    sun: faSun,
    expand: faExpandAlt,
    phone: faPhoneAlt,
    mapmarker: faMapMarkerAlt,
    email: faEnvelope,
    web: faGlobe
}

// Open Site

function goToSite (url) {
    Linking.canOpenURL(url).then(supported => {
        if (supported) {
            Linking.openURL(url);
        } else {
            console.log("Can't open URI: " + url);
        }
    });
};

export default function CourseInfoItem({ icon, info, link, additionalStyles, title }) {
    return (
        <View style={[styles.itemContainer, additionalStyles ? additionalStyles : {}]}>

            <FontAwesomeIcon icon={icon ? icons[icon] : icons.default} color={COLORS.logoGreen} size={21} style={styles.itemIcon} />

            {link ? (
                <TouchableOpacity onPress={() => goToSite(info)}>
                    {info &&
                        <TextLabel color={"darkblue"} additionalStyles={styles.itemInfo}>{stripUrl(info)}</TextLabel>
                    }
                </TouchableOpacity>
            ) : (
                <TextLabel color={"grey"} additionalStyles={styles.itemInfo}>{info || ''}</TextLabel>
            )}

        </View>
    )
}
