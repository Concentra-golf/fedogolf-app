import { StyleSheet } from 'react-native';
import { moderateScale } from '../../../utilities/ScalingScreen';

export default StyleSheet.create({
    container: {
        // flexDirection: 'row',
        // flexWrap: 'wrap'
    },
    itemContainer: {
        width: '46%',
        marginRight: 5,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    itemIcon: {
        marginRight: moderateScale(20)
    },
    itemInfo: {
        width: moderateScale(300),
        // backgroundColor: 'red'
    }
});
