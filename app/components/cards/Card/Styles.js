import { StyleSheet } from 'react-native';

import {COLORS} from '../../../config/constants';

export default StyleSheet.create({
    card: {
        backgroundColor: COLORS.white,
        marginLeft: 15,
        marginRight: 15,
        marginBottom: 30,
        shadowOffset: { width: 0, height: 2 },
        shadowColor: '#0000002B',
        shadowOpacity: 1.0,
        shadowRadius: 20,
        elevation: 6
    },
    cardTitle: {
        padding: 10,
    },

})