import React from 'react'
import {View} from 'react-native';

// Components

import TextLabel from '../../UI/TextLabel';

// Styles

import styles from './Styles';

export default function Card(props) {
    return (
        <View style={[styles.card, props.additionalStyles ? props.additionalStyles : {}]}>

            {props.title && 
            <TextLabel
                size={14}
                font={'primarybold'}
                transform={'uppercase'}
                additionalStyles={styles.cardTitle}
            >
                {props.title}
            </TextLabel>}

            {props.children}

        </View>
    )
}

