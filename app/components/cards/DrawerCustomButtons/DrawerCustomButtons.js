import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { DrawerItem } from '@react-navigation/drawer';

// Modules
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as session from '../../../store/actions/session';
import { COLORS } from '../../../config/constants';

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import {
    faQrcode,
} from '@fortawesome/free-solid-svg-icons';

class DrawerCustomButtons extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fedogolf: {},
            hasFedogolf: false
        };
    }

    componentDidMount() {

        const fedogolf = this.props.session.user.data.federations.find(x => x.id === 1)
        console.log(fedogolf)
        if (fedogolf !== {} && fedogolf !== undefined) {
            this.setState({
                fedogolf: fedogolf,
                hasFedogolf: true
            })
        } else {
            this.setState({
                fedogolf: {},
                hasFedogolf: false
            })
        }
    }

    render() {
        return (
            <View>
                {this.state.hasFedogolf &&
                    <DrawerItem
                        label={'Fedogolf QR'}
                        labelStyle={styles.drawerItemLabel}
                        style={styles.drawerItemWrapper}
                        onPress={() => this.props.navigation.navigate('MembershipQR', {
                            membership: this.state.fedogolf
                        })}
                        icon={() => <FontAwesomeIcon size={14} color={COLORS.black} icon={faQrcode} />}
                    />
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    drawerItemsContainer: {
        marginTop: 15,
    },
    drawerItemWrapper: {
        borderBottomColor: '#E8E8E8',
        borderBottomWidth: 1
    },
    drawerItemLabel: {
        fontFamily: 'Lato-Bold',
    },
})

function mapStateToProps(state) {
    return {
        ...state.membership,
        session: state.session,
        user: state.session.user.data,
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators({ session }, dispatch);
}


export default connect(
    mapStateToProps,
    mapDispatchProps
)(DrawerCustomButtons);