import { StyleSheet } from "react-native";

import { hasNotch } from 'react-native-device-info';

import { COLORS } from "../../../config/constants";

export default StyleSheet.create({
    container: {
        backgroundColor: COLORS.black,
    },
    backgroundImage: {
        flex: 1,
        justifyContent: 'center',
        height: hasNotch() ? 165 : 145
    },
    containerInner: {
        width: '90%',
        padding: 15,
        paddingTop: hasNotch() ? 35 : 15,
        flexDirection: 'row',
        alignSelf: 'center',
        justifyContent: 'center'
    },
    profileName: {
        marginBottom: 5
    },
    profileIcon: {
        width: 50,
        height: 50,
        marginRight: 15,
        borderRadius: 100,
        borderWidth: 1,
        borderColor: COLORS.white,
    },
    pointsContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    pointsIcon: {
        marginRight: 5
    }
});
