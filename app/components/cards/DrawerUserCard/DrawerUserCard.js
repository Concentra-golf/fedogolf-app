import React, { Component } from 'react';
import { View, Image, ImageBackground } from 'react-native';

// Modules

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faGolfBall } from '@fortawesome/free-solid-svg-icons';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as session from '../../../store/actions/session';

// Components

import TextLabel from '../../UI/TextLabel';

// Styles

import styles from './Styles';

// Images

import bgDrawerUserCard from './Images/bg-drawer-user-card.jpg';
import userIcon from './Images/img-user-icon-default.png';

// Constants

import { COLORS } from '../../../config/constants';

const userImage = 'https://www.diariolibre.com/binrepository/664x350/59c0/546d350/none/10904/KVJK/222222222222_12653040_20191119124853.jpg';

class DrawerUserCard extends Component {
    render() {
        const { user } = this.props
        return (
            <View style={styles.container}>

                <ImageBackground source={bgDrawerUserCard} resizeMode={"cover"} style={styles.backgroundImage}>

                    <View style={styles.containerInner}>

                        <Image
                            style={styles.profileIcon}
                            source={{ uri: user && user.profile_img && user.profile_img }}
                        // source={{ uri: 'http://bo.golfertek.com/assets/images/avatars/golfer_male.png' }}
                        />

                        {/* {userImage ? (
                            <Image source={{ url: userImage }} style={styles.profileIcon} resizeMode={'cover'} />
                        ) : (
                                <Image source={userIcon} style={styles.profileIcon} resizeMode={'cover'} />
                            )} */}

                        <View>
                            {user &&
                                <TextLabel
                                    font={"secondarybold"}
                                    color={"white"}
                                    additionalStyles={styles.profileName}>{user.firstname} {user.lastname}</TextLabel>
                            }

                            <View style={styles.pointsContainer}>
                                <FontAwesomeIcon size={14} icon={faGolfBall} color={COLORS.white} style={styles.pointsIcon} />
                                <TextLabel size={14} color={"white"}>17</TextLabel>
                            </View>
                        </View>

                    </View>

                </ImageBackground>

            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        session: state.session,
        user: state.session.user.data
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators(session, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchProps,
)(DrawerUserCard);
