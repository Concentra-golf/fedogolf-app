import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, ScrollView } from 'react-native';
import { widthPercentageToDP, heightPercentageToDP, moderateScale, verticalScale, scale } from '../../../utilities/ScalingScreen';
import { COLORS } from '../../../config/constants';

class ScoreCardValues extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasColor: false,
      color: 'white',
      textColor: 'black'
    };
  }

  componentDidMount() {
    this.getScore()
  }

  getScore() {
    const { data, score } = this.props;
    let scores = {};
    if (this.props.dafaultValue !== undefined && !isNaN(this.props.dafaultValue)) {
      scores = this.props.dafaultValue.toString();
    } else if (Object.keys(score).length > 0 && score.score !== 0) {
      scores = score.score.toString()
    } else if (score.score === 0) {
      scores = {}
    }
    return scores;
  }


  totalScores(value) {
    if (this.props.totalScores[this.props.playerSelected]) {
      if (this.props.index === 9) {
        return this.props.totalScores[this.props.playerSelected].out && this.props.totalScores[this.props.playerSelected].out.toString();
      } else if (this.props.index === 19) {
        return this.props.totalScores[this.props.playerSelected].in && this.props.totalScores[this.props.playerSelected].in.toString();
      } else if (this.props.index === 20) {
        return this.props.totalScores[this.props.playerSelected].total && this.props.totalScores[this.props.playerSelected].total.toString();
      }
    }
    return value
  }

  //FUNCTION TO SET THE KEYBOARD ON THE STARTING HOLE
  getStartHole() {
    let position = 1;
    if (this.props.index > 8) {
      position = 0
    }
    return this.props.playerHoleStart === this.props.index + position
  }

  render() {
    const { data, score } = this.props;
    const hole = this.props.data.name.split("Hole - ").pop(); // we eliminate the text, only leaving the numbers
    const value = this.totalScores(this.getScore());
    return (
      <View key={this.props.index}>
        {/* HOLES TITLE BOX */}
        <View style={[styles.nameBox, { backgroundColor: COLORS.darkblue }]}>
          <Text style={styles.holeTitle}>{hole}</Text>
        </View>
        {/* YARDS TITLE BOX */}
        <View style={styles.nameBox}>
          <Text>{parseInt(data.pivot.yards)}</Text>
        </View>
        {/* Handicap TITLE BOX */}
        <View style={styles.nameBox}>
          <Text>{data.pivot.handicap === undefined ? '' : parseInt(data.pivot.handicap)}</Text>
        </View>
        {/* PAR TITLE BOX */}
        <View style={styles.nameBox}>
          <Text>{data.pivot.par}</Text>
        </View>

        <View
          style={[this.props.color === undefined ? styles.nameBox : styles.nameBox, { backgroundColor: this.props.color !== undefined ? this.props.color.color : 'white' }]}
        >
          <TextInput
            autoFocus={this.getStartHole()}
            style={styles.cardInput}
            keyboardType='number-pad'
            maxLength={2}
            dafaultValue={value}
            value={value}
            editable={this.props.editable === undefined ? true : false}
            onChange={(e) => this.props.holesAndValues(e.nativeEvent.text)}
            onEndEditing={(e) => this.props.getScore(this.props.formField(e.nativeEvent.text, { name: `hole${this.props.index}` }, data.pivot.par, this.props.index))}
          />
        </View>

      </View>
    );
  }
}

export default ScoreCardValues;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: scale(10)
  },
  golfCourseTitle: {
    fontWeight: 'bold',
    fontSize: moderateScale(16),
    color: COLORS.darkblue
  },
  golfCoursesubtitle: {
    fontWeight: 'bold',
    fontSize: moderateScale(16),
    color: COLORS.darkblue,
    marginTop: verticalScale(5)
  },
  divider: {
    width: '100%',
    borderWidth: 0.5,
    backgroundColor: COLORS.airBnbLightGrey,
    marginVertical: verticalScale(10)
  },
  header: {
    height: heightPercentageToDP('7%'),
    width: '100%',
    padding: scale(10),
    flexDirection: 'row',
    backgroundColor: COLORS.darkblue,
    alignItems: 'center',
    justifyContent: 'center'
  },
  subTitle: {
    fontSize: moderateScale(16),
    color: COLORS.white,
    fontWeight: '600',
    marginHorizontal: moderateScale(5),
  },
  titles: {
    fontSize: moderateScale(14),
    color: COLORS.darkblue,
    fontWeight: '600'
  },
  scoreCardContainer: {
    height: heightPercentageToDP('30%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  nameBox: {
    height: 60,
    width: 60,
    borderWidth: 0.3,
    borderColor: COLORS.airBnbGrey,
    justifyContent: 'center',
    alignItems: 'center'
  },
  holeTitle: {
    fontSize: moderateScale(14),
    fontWeight: 'bold',
    color: COLORS.white
  },
  cardInput: {
    height: 45,
    width: 45,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    fontSize: moderateScale(14),
    color: 'black',
    fontWeight: 'bold'
  },
  buttonPost: {
    height: heightPercentageToDP('7%'),
    width: widthPercentageToDP('80%'),
    backgroundColor: COLORS.darkblue,
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: verticalScale(270),
    alignSelf: 'center'
  },
  buttonPostText: {
    color: COLORS.white,
    fontSize: moderateScale(14),
  }
})

const title = [
  { name: 'Hole' },
  { name: 'Yards' },
  { name: 'Par' },
  { name: 'Score' },
]