import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet, } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import Moment from 'moment';

import { verticalScale, scale, moderateScale, widthPercentageToDP } from '../../../utilities/ScalingScreen';
import { COLORS } from '../../../config/constants';

class ReceiptCards extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: ''
        };
    }

    componentDidMount() {
        const data = this.props.data;
        console.log(data);
        let name = '';

        switch (data.flow_type) {
            case 'standard':
                name = 'Standar';
                break;
            case 'SDCC':
                name = 'SDCC';
                break;
            case 'association':
                name = 'Association';
                break;
            case 'recurrent':
                name = 'Recurrent'
                break;
        }
        this.setState({
            name: name
        })

    }

    render() {
        const data = this.props.data;
        return (
            <TouchableOpacity
                style={styles.container}
                onPress={() => this.props.navigation.navigate('ReceiptDetails',{
                    data: data
                })}
            >
                <View style={styles.wrapper}>
                    <View style={styles.imageContainer}>
                        <Image
                            style={styles.federationIcon}
                            resizeMode={"center"}
                            source={{ uri: 'https://fedogolf.org.do/wp-content/uploads/2018/03/fedogolf-escudo.jpg' }}
                        />
                    </View>
                    <View style={styles.wrapperTwo}>
                        <View style={styles.textWrapper}>
                            <Text style={styles.title}>{data.membership.name}</Text>
                            <Text style={styles.paymentDetails}>{Moment(data.created_at).format('MMMM Do YYYY, h:mm:ss a')}</Text>
                            <Text style={styles.paymentDetails}>Online Payment</Text>
                        </View>
                        <FontAwesome5
                            name='arrow-right'
                            size={32}
                        />
                    </View>
                </View>


            </TouchableOpacity>
        );
    }
}

export default ReceiptCards;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: verticalScale(100),
        padding: scale(10),
        borderBottomWidth: 1,
        borderColor: COLORS.grey,
        justifyContent: 'center',
    },
    wrapper: {
        alignItems: 'center',
        flexDirection: 'row',
    },

    federationIcon: {
        width: scale(50),
        height: scale(50),
        marginRight: moderateScale(20)
    },
    title: {
        fontSize: moderateScale(18),
        fontWeight: 'bold',
        color: COLORS.lightBlack,
    },
    textWrapper: {
        justifyContent: 'space-evenly',
        height: verticalScale(60),
    },
    paymentDetails: {
        fontSize: moderateScale(14),
        color: COLORS.airBnbLightGrey
    },
    wrapperTwo: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: widthPercentageToDP('70%')
    }
})