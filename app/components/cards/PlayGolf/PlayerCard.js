import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { verticalScale, moderateScale, widthPercentageToDP } from '../../../utilities/ScalingScreen';
import { COLORS } from '../../../config/constants';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

class PlayerCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    onPress(e) {
        if (this.props.onPress) {
            if (this.props.item && this.props.item.id === this.props.user.id) {
                console.log('main player')
            } else {
                this.props.onPress(e)
            }
        } else {
            console.log('hey')
        }
    }

    render() {
        console.log(this.props.item)
        return (
            <TouchableOpacity
                style={styles.container}
                onPress={(e) => this.onPress(e)}
            >

                <FontAwesome5
                    name='user-alt'
                    size={27}
                    color={COLORS.softBlack}
                />

                <View style={styles.wrapper}>
                    <Text style={styles.name}>{this.props.firstname} {this.props.lastname}</Text>
                    <Text style={styles.mail}>{this.props.email}</Text>
                    {this.props.item && this.props.item.id !== this.props.user.id ? (
                        <View>
                            {this.props.route !== 'SearchPlayer' && <Text style={styles.tees}>Tees: <Text style={styles.teesInfo}>{this.props.tees}</Text></Text>}
                        </View>
                    ) : (
                            <View />
                        )
                    }

                    {this.props.item && this.props.item.id !== this.props.user.id ? (
                        <View>
                            {this.props.route !== 'SearchPlayer' && <Text style={styles.tees}>Starting Hole: <Text style={styles.teesInfo}>{this.props.holeStart}</Text></Text>}
                        </View>
                    ) : (
                            <View />
                        )
                    }
                </View>
            </TouchableOpacity>
        );
    }
}

export default PlayerCard;

const styles = StyleSheet.create({
    container: {
        width: widthPercentageToDP('88%'),
        // justifyContent: 'center',
        backgroundColor: 'white',
        paddingVertical: verticalScale(10),
        paddingHorizontal: moderateScale(10),
        marginVertical: verticalScale(10),
        elevation: 6,
        shadowColor: COLORS.airBnbLightGrey,
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.5,
        borderRadius: 5,
        flexDirection: 'row',
        alignItems: 'center'
    },
    wrapper: {
        marginLeft: moderateScale(10)
    },
    name: {
        fontSize: moderateScale(14),
        color: COLORS.softBlack,
        fontWeight: 'bold'
    },
    mail: {
        marginTop: verticalScale(5),
        fontSize: moderateScale(13),
        color: COLORS.softBlack
    },
    tees: {
        marginTop: verticalScale(5),
        fontSize: moderateScale(13),
        color: COLORS.softBlack
    },
    teesInfo: {
        fontWeight: 'bold'
    }
})