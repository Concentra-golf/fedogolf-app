import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Platform } from 'react-native';
import { widthPercentageToDP, heightPercentageToDP, moderateScale, scale, verticalScale } from '../../../utilities/ScalingScreen';
import { COLORS } from '../../../config/constants';

import Fontisto from 'react-native-vector-icons/Fontisto';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Moment from 'moment';

import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';

class OpenGame extends Component {
    constructor(props) {
        super(props);
        setI18nConfig();
        this.state = {
        };
    }

    render() {
        const { data, index } = this.props;
        return (
            <View key={this.props.index} style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.gameTitle}>{translate('gameTitle')} {this.props.index + 1}</Text>
                </View>

                <View style={styles.timeContainer}>
                    <View style={styles.timeWrapper}>
                        <Fontisto
                            name='date'
                            size={25}
                            color={COLORS.darkblue}
                        />
                        <Text style={styles.gameDate}>{Moment(this.props.date).locale('en').format('DD MMMM YYYY')}</Text>
                    </View>

                    {/* <View style={styles.usersContainer}>
                        <FontAwesome5
                            name='user'
                            size={25}
                            color={COLORS.darkblue}
                        />
                        <Text style={styles.usersQty}>{this.props.data.competitors.length}</Text>
                    </View> */}
                </View>

                <View style={styles.locationContainer}>
                    <View style={styles.timeWrapper}>
                        <MaterialIcons
                            name='location-on'
                            size={25}
                            color={COLORS.darkblue}
                        />
                        <View style={styles.wrapper}>
                            <Text style={styles.subTitle}>{translate('golfCourse')}:</Text>
                            <Text style={styles.locationName}>{this.props.data.golf_course.course_name}</Text>
                        </View>
                    </View>
                </View>

                <View style={styles.modalityContainer}>
                    <View style={styles.timeWrapper}>
                        <FontAwesome5
                            name='golf-ball'
                            size={25}
                            color={COLORS.darkblue}
                        />
                        <View style={styles.wrapper}>
                            <Text style={styles.subTitle}>{translate('gameModality')}</Text>
                            <Text style={styles.modalityName}>{this.props.data.type}</Text>
                        </View>
                    </View>
                </View>

                <TouchableOpacity
                    style={styles.openGameButton}
                    onPress={(e) => this.props.onPress(e)}
                >
                    <Text style={styles.openGameText}>{translate('openGames')}</Text>
                </TouchableOpacity>

            </View>
        );
    }
}

export default OpenGame;

const styles = StyleSheet.create({
    container: {
        borderWidth: 0.5,
        borderColor: 'rgba(0,0,0,0.3)',
        borderRadius: 10,
        backgroundColor: 'white',
        shadowColor: COLORS.airBnbLightGrey,
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.5,
        elevation: 6,
        marginBottom: Platform.OS === 'android' ? verticalScale(10) : 0,
        marginHorizontal: Platform.OS === 'android' ? moderateScale(5) : 0,
    },
    header: {
        width: '100%',
        height: 50,
        backgroundColor: COLORS.darkblue,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    gameTitle: {
        fontSize: moderateScale(16),
        color: COLORS.white,
        fontWeight: 'bold'
    },
    timeContainer: {
        borderBottomWidth: 1,
        padding: scale(10),
        borderColor: 'rgba(0,0,0,0.3)',
        backgroundColor: 'white',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    timeWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    gameDate: {
        marginLeft: moderateScale(10),
        fontSize: moderateScale(14),
        color: COLORS.softBlack
    },
    usersContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    usersQty: {
        fontSize: moderateScale(16),
        marginLeft: moderateScale(10)
    },
    locationContainer: {
        backgroundColor: 'white',
        borderBottomWidth: 1,
        padding: scale(10),
        borderColor: 'rgba(0,0,0,0.3)',
    },
    locationName: {
        fontSize: moderateScale(14),
        fontWeight: 'bold',
        color: COLORS.softBlack
    },
    modalityContainer: {
        borderBottomWidth: 1,
        padding: scale(10),
        borderColor: 'rgba(0,0,0,0.3)',
    },
    modalityName: {
        fontWeight: 'bold',
        fontSize: moderateScale(14),
        color: COLORS.softBlack
    },
    openGameButton: {
        width: '100%',
        height: verticalScale(50),
        justifyContent: 'center',
        alignItems: 'center'
    },
    openGameText: {
        fontSize: moderateScale(16),
        fontWeight: 'bold',
        color: COLORS.darkblue
    },
    wrapper: {
        marginLeft: moderateScale(10)
    },
    subTitle: {
        fontSize: moderateScale(14),
        color: COLORS.lightBlack,
    }
})