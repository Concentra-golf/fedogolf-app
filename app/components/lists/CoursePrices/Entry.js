import React, { Component } from 'react';
import { View, TouchableOpacity } from 'react-native';
import TextLabel from '../../UI/TextLabel';
import styles from './Styles';

class Entry extends Component {
    render() {
        const props = this.props;
        return (
            <TouchableOpacity activeOpacity={0.8} style={styles.item}>
                <View style={styles.info}>
                    {/* <TextLabel size={16} align={"right"}>{this.props.time}</TextLabel> */}
                    <TextLabel size={14} color={"midgreen"} align={"right"} >20.0 US$</TextLabel>
                </View>
            </TouchableOpacity>
        );
    }
}

export default Entry;
