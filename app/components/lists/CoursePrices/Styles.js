import { StyleSheet } from 'react-native';
import { verticalScale, scale, moderateScale } from '../../../utilities/ScalingScreen';
import { COLORS } from '../../../config/constants';

export default StyleSheet.create({
    container: {
        padding: scale(10),
        marginTop: verticalScale(10),
    },
    scrollContainer: {
        alignItems: 'center',
        paddingBottom: verticalScale(60)
    },
    entryContainer: {
        borderBottomColor: '#D8D8D8',
        paddingVertical: verticalScale(10),
        width: '100%',
        flexDirection: "row",
        justifyContent: "space-between",
        // marginTop: verticalScale(10)
    },
    title: {
        fontSize: moderateScale(16)
    },
    price: {
        fontSize: moderateScale(16),
        fontWeight: "bold",
        color: COLORS.midgreen
    },
    verticalLine: {
        marginTop: verticalScale(5),
        marginBottom: verticalScale(10),
        width: '95%',
        height: 0.5,
        backgroundColor: COLORS.grey
    },
    seasonTitle: {
        fontSize: moderateScale(14)

    },
    seasonPrice: {
        fontSize: moderateScale(14),
        fontWeight: 'bold',
        color: COLORS.midgreen,
    }
});
