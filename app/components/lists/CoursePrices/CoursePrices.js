import React from 'react';
import { View, FlatList, TouchableOpacity, ScrollView } from 'react-native';

// Components

import TextLabel from '../../UI/TextLabel';
import Entry from './Entry';

// Styles

import styles from './Styles';
import { Text } from 'native-base';
import { COLORS } from '../../../config/constants';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';

export default class CoursePrices extends React.Component {

    render() {
        const { data, action } = this.props;

        return (
            <ScrollView
                style={styles.container}
                contentContainerStyle={styles.scrollContainer}
            >

                {data.price !== '0.00' && data.price &&
                    <View style={{ width: '100%' }}>
                        <TouchableOpacity style={styles.entryContainer}>
                            <Text style={styles.title}>Standar Price</Text>
                            <Text style={styles.price}>{data.currency}$ {data.price || '0'}</Text>
                        </TouchableOpacity>
                        <View style={styles.verticalLine} />
                    </View>
                }


                {data.weekday_price !== '0.00' || '0' && data.weekday_price &&
                    <View style={{ width: '100%' }}>
                        <TouchableOpacity style={styles.entryContainer}>
                            <Text style={styles.title}>Weekday Price</Text>
                            <Text style={styles.price}>{data.currency}$ {data.weekday_price}</Text>
                        </TouchableOpacity>
                        <View style={styles.verticalLine} />
                    </View>
                }

                {data.weekend_price !== '0.00' || '0' && data.weekend_price &&
                    <View style={{ width: '100%' }}>
                        <TouchableOpacity style={styles.entryContainer}>
                            <Text style={styles.title}>Weekends Price</Text>
                            <Text style={styles.price}>{data.currency}$ {data.weekend_price}</Text>
                        </TouchableOpacity>
                        <View style={styles.verticalLine} />
                    </View>
                }


                {data.twilight_price !== '0.00' || '0' && data.twilight_price &&
                    <View style={{ width: '100%' }}>
                        <TouchableOpacity style={styles.entryContainer}>
                            <Text style={styles.title}>Twilight Price</Text>
                            <Text style={styles.price}>{data.currency}$ {data.twilight_price}</Text>
                        </TouchableOpacity>
                        <View style={styles.verticalLine} />
                    </View>
                }

                {data.club_price !== '0.00' && data.club_price &&
                    <View style={{ width: '100%' }}>
                        <TouchableOpacity style={styles.entryContainer}>
                            <Text style={styles.title}>Club Member Price</Text>
                            <Text style={styles.price}>{data.currency}$ {data.club_price}</Text>
                        </TouchableOpacity>
                        <View style={styles.verticalLine} />
                    </View>
                }

                {data.season_entities.length > 0 &&
                    <View style={styles.entryContainer}>
                        <Text style={styles.title}>Season Price</Text>
                        <Ionicons
                            name='ios-arrow-down'
                            size={30}
                            color={COLORS.midgreen}
                        />
                    </View>
                }

                {data.season_entities && data.season_entities.map((item, index) =>
                    <TouchableOpacity key={index} style={styles.entryContainer}>
                        <Text style={styles.seasonTitle}>{item.name}</Text>
                        <Text style={styles.seasonPrice}>{data.currency}$ {item.price}</Text>
                    </TouchableOpacity>
                )}

                {data.season_entities.length > 0 && <View style={styles.verticalLine} />}

                {data.foreign_price !== '0.00' && data.foreign_price &&
                    <View style={{ width: '100%' }}>
                        <TouchableOpacity style={styles.entryContainer}>
                            <Text style={styles.title}>Foreign Price</Text>
                            <Text style={styles.price}>{data.currency}$ {data.foreign_price}</Text>
                        </TouchableOpacity>
                        <View style={styles.verticalLine} />
                    </View>
                }


            </ScrollView>
        );
    }
}


