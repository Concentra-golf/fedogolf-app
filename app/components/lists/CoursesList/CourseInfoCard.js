import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text } from 'react-native';
import TextLabel from '../../UI/TextLabel';
import styles from './Styles';
import { verticalScale } from '../../../utilities/ScalingScreen';
import { COLORS } from '../../../config/constants';
import StarRating from 'react-native-star-rating';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

class CourseInfoCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const { item, showSchedule, action, index } = this.props
        return (
            <TouchableOpacity activeOpacity={0.8} style={styles.item} onPress={() => action(item.course_id)}>
                <View style={styles.container}>
                    <View style={styles.wrapper}>
                        <View style={styles.nameContainer}>
                            <TextLabel
                                size={16}
                                ellipsizeMode={'tail'}
                                numberOfLines={1}
                                additionalStyles={styles.clubName}>{item.club_name}</TextLabel>
                            <View style={styles.holesContainer}>
                                <TextLabel
                                    size={14}
                                    ellipsizeMode={'tail'}
                                    numberOfLines={1}
                                    additionalStyles={styles.name}>{item.course_name},</TextLabel>

                                <TextLabel size={12} color={"grey"} additionalStyles={styles.info}>{item.holes}/ {item.par}</TextLabel>

                                <MaterialIcons
                                    name='location-on'
                                    size={20}
                                    style={styles.holeIcon}
                                />
                            </View>
                            <TextLabel size={12} additionalStyles={styles.country}>{item.city}, {item.country}</TextLabel>
                        </View>
                        <TextLabel size={14} color={"grey"}>{item.distance.toFixed(2)} KM</TextLabel>
                    </View>
                    <StarRating
                        disabled={false}
                        maxStars={5}
                        rating={item.rating}
                        fullStarColor={COLORS.green}
                        starSize={20}
                        starStyle={styles.starStyle}
                        containerStyle={styles.starContainer}
                    />
                    <View style={styles.lineContainer}>
                        <View style={styles.horizontalLine} />
                    </View>
                </View>

            </TouchableOpacity>
        );
    }
}

export default CourseInfoCard;
