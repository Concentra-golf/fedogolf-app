import React from 'react';
import { View, FlatList, Image, TouchableOpacity } from 'react-native';

// Components
import TextLabel from '../../UI/TextLabel';

// Styles
import styles from './Styles';

import CourseInfoCard from './CourseInfoCard';
import { verticalScale } from '../../../utilities/ScalingScreen';

class CoursesList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            courseList: []
        };
    }

    componentDidMount() {
        const data = this.props.data;

        const distanceData = data.filter((item) => {
            let array = [];
            if (item.distance < 5000) {
                array.push(item);
                return array;
            }
        })
        this.setState({
            courseList: distanceData
        })

    }

    render() {
        const { data, showSchedule, action } = this.props;
        console.log(data)
        return (
            <FlatList
                data={this.state.courseList}
                refreshing={this.props.refreshing}
                onRefresh={this.props.handleRefresh}
                keyExtractor={(item, index) => index.toString()}
                contentContainerStyle={{ paddingBottom: verticalScale(30) }}
                renderItem={({ item }) =>
                    <CourseInfoCard
                        item={item}
                        showSchedule={showSchedule}
                        action={() => this.props.navigation.navigate('CourseDetail', {
                            courseData: item,
                            courseId: item.id
                        })}
                    />
                }
            />
        );
    }
}

export default (CoursesList);
