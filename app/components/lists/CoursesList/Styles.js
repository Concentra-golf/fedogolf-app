import { StyleSheet } from 'react-native';
import { verticalScale, moderateScale, widthPercentageToDP } from '../../../utilities/ScalingScreen';
import { COLORS } from '../../../config/constants';

export default StyleSheet.create({
    item: {
        alignSelf: 'stretch',
        backgroundColor: 'white',
        borderBottomColor: '#E8E8E8',
        marginVertical: 5,
        marginHorizontal: 10,
        paddingHorizontal: moderateScale(5)
    },
    container: {

    },
    wrapper: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    nameContainer: {
        flex: 1,
    },
    clubName: {
        marginBottom: 4,
        fontSize: moderateScale(16),
        color: '#333',
        fontWeight: 'bold',
        width: widthPercentageToDP('60%'),
    },
    name: {
        marginBottom: 4,
        color: COLORS.softBlack,
    },
    holesContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    holeIcon: {
        color: COLORS.airBnbGrey
    },
    info: {
        marginLeft: moderateScale(5)
    },
    country: {
        color: COLORS.airBnbLightGrey
    },
    price: {
        marginBottom: 4,
    },
    thumb: {
        width: 50,
        height: 50,
        borderRadius: 5,
        marginRight: 10,
    },
    lineContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: verticalScale(10)
    },
    horizontalLine: {
        height: verticalScale(0.4),
        width: '95%',
        backgroundColor: '#DBDBDB'
    },
    starContainer: {
        width: widthPercentageToDP('25%'),
        marginTop: verticalScale(5)
    },
    starStyle: {
        
    }
});
