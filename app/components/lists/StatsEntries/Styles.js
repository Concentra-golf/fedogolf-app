import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  statsRow: {
    borderColor: '#ddd',
    borderBottomWidth: 1,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: "center"
  },
});
