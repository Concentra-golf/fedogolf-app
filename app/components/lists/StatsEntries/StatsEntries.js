import React from 'react';
import { View, TouchableOpacity } from 'react-native';

// Modules

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faAngleRight } from '@fortawesome/free-solid-svg-icons'

// Components

import TextLabel from '../../UI/TextLabel';

// Styles

import styles from './Styles';

export default function StatsEntries({ data, action }) {
    return (
        <View>
            {data && data.map((item, i) => {
                return (
                  <Entry key={i} item={item} />
                );
              })}

              {action &&
              <TouchableOpacity onPress={() => action()}>
                <View style={styles.statsRow}>
                  <TextLabel size={12} font={'secondarybold'}>See All</TextLabel>
                  <FontAwesomeIcon icon={faAngleRight} />
                </View>
              </TouchableOpacity>}
        </View>
    )
}


const Entry = ({ item }) => (
    <View style={styles.statsRow}>
      <TextLabel size={12}>{item.name}</TextLabel>
  
      <TextLabel size={18} font={'secondarybold'}>
        {item.percentage} <TextLabel size={14}>%</TextLabel>
      </TextLabel>
    </View>
);  