import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { COLORS } from '../../../config/constants';
import { verticalScale, moderateScale, scale } from '../../../utilities/ScalingScreen';
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
class DocumentsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={styles.docMapContainer}>
                {!this.props.loadingDocument && !this.props.loading && this.props.requestData.payment_documents.length > 0 &&
                    <View style={styles.documentContainer}>
                        <Text style={styles.docTitle}>My Documents</Text>
                        <View style={styles.listContainer}>
                            {this.props.requestData.payment_documents.map((item, index) =>
                                <TouchableOpacity
                                    style={styles.selectButton}
                                    onPress={() => this.props.selecDocument(item)}
                                >
                                    <View style={styles.typeContainer}>
                                        <Text style={styles.typeName}>PDF</Text>
                                    </View>

                                    <View style={styles.wrapper}>
                                        <Text style={styles.documentTitle}>Document {index + 1}</Text>
                                        <Text style={styles.dateUpload}>March 25, 2020</Text>
                                    </View>

                                    <MaterialCommunityIcons
                                        name='arrow-collapse-down'
                                        size={28}
                                        style={{
                                            color: COLORS.lightblue,
                                            paddingRight: moderateScale(10),
                                        }}
                                    />

                                </TouchableOpacity>
                            )}
                        </View>
                    </View> 
                }
            </View>
        );
    }
}

export default DocumentsList;

const styles = StyleSheet.create({
    docMapContainer: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    documentContainer: {
        width: '100%',
        padding: scale(8),
        marginTop: verticalScale(20),
        paddingVertical: 10,
        // alignItems: 'center',
        // justifyContent: 'center'
    },
    docTitle: {
        fontSize: moderateScale(16),
        marginBottom: moderateScale(25),
        marginLeft: moderateScale(10),
        fontWeight: '600',
    },
    listContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    documentTitle: {
        fontSize: moderateScale(14),
        fontWeight: '500',
        color: COLORS.darkgrey,
    },
    dateUpload: {
        fontSize: moderateScale(12),
        marginTop: verticalScale(4)
    },
    selectButton: {
        width: moderateScale(310),
        marginBottom: scale(10),
        backgroundColor: 'white',
        height: verticalScale(55),
        padding: scale(8),
        borderWidth: 0.5,
        borderColor: COLORS.grey,
        alignItems: 'center',
        justifyContent: 'space-between',
        borderRadius: scale(3),
        shadowColor: '#000',
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.3,
        shadowRadius: 6,
        elevation: 6,
        flexDirection: 'row'
    },
    typeContainer: {
        backgroundColor: COLORS.lightblue,
        width: moderateScale(50),
        height: verticalScale(40),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: scale(5)
    },
    typeName: {
        color: COLORS.white,
        fontSize: moderateScale(14)
    },
    wrapper: {
        marginLeft: moderateScale(10),
        width: moderateScale(175)
    }
})