import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { verticalScale, moderateScale, scale } from '../../../utilities/ScalingScreen';
import { COLORS } from '../../../config/constants';

import * as RNLocalize from 'react-native-localize';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';


class CardPayment extends Component {
    constructor(props) {
        super(props);
        setI18nConfig();
        this.state = {
        };
    }


    componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);

    }

    render() {
        String.prototype.splice = function (idx, rem, str) {
            return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
        };

        const cardNumber = this.props.cardNumber.substring(9);
        const expiration = this.props.expiration.splice(4, 0, " / ");
        console.log(this.props.data)
        return (
            <TouchableOpacity
                style={styles.creditCardButton}
                onPress={(e) => this.props.onPress(e)}
            >
                {this.props.route === 'formSelect' &&
                    <View style={styles.radioButton} >
                        {this.props.activeIndex === this.props.data.id &&
                            <View style={{
                                height: 15,
                                width: 15,
                                borderRadius: 10,
                                backgroundColor: COLORS.green
                            }}
                            />
                        }
                    </View>
                }

                < View style={styles.wrapper}>
                    {this.props.type === 'MASTERCARD' ? (
                        <Image
                            source={require('../../../assets/images/creditCardsTypes/mastercard.png')}
                            style={styles.cardType}
                        />
                    ) : this.props.type === 'VISA' ? (
                        <Image
                            source={require('../../../assets/images/creditCardsTypes/visa.png')}
                            style={styles.cardType}
                        />
                    ) : this.props.type === 'DINERSCLUB' ? (
                        <Image
                            source={require('../../../assets/images/creditCardsTypes/discover.png')}
                            style={styles.cardType}
                        />
                    ) : (
                                    <Image
                                        source={require('../../../assets/images/creditCardsTypes/generic.png')}
                                        style={styles.cardType}
                                    />
                                )}
                    <View style={styles.creditCardInfoWrapper}>
                        <Text style={styles.creditCardNumber}>•••• {cardNumber}</Text>
                        <Text style={styles.expirationDate}>{translate('expiration')} {expiration}</Text>
                    </View>
                </View>
            </TouchableOpacity >
        );
    }
}

export default CardPayment;

const styles = StyleSheet.create({
    creditCardButton: {
        // backgroundColor: 'red',
        height: verticalScale(60),
        borderBottomWidth: 1,
        borderBottomColor: '#D8D8D8',
        paddingVertical: 10,
        width: '100%',
        flexDirection: "row",
        alignItems: 'center',
        paddingBottom: verticalScale(10),
    },
    radioButton: {
        borderWidth: 1,
        height: 20,
        width: 20,
        borderRadius: scale(10),
        marginRight: moderateScale(20),
        justifyContent: 'center',
        alignItems: 'center'
    },
    wrapper: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    creditCardInfoWrapper: {
        marginLeft: moderateScale(15),
        // marginTop: moderateScale(15)
    },
    expirationDate: {
        fontSize: moderateScale(13),
        color: COLORS.airBnbLightGrey,
        marginTop: verticalScale(5)
    },
    creditCardNumber: {
        fontWeight: '500',
        fontSize: moderateScale(18),
        color: COLORS.softBlack
    },
    cardType: {
        width: 60,
        height: 60
    },
})