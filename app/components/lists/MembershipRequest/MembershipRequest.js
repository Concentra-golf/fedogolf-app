import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native';

import { moderateScale, verticalScale } from '../../../utilities/ScalingScreen';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowUp, faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { COLORS } from '../../../config/constants';

import Moment from 'moment';
import 'moment/locale/es';

class MembershipRequest extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <TouchableOpacity
                style={styles.federationButton}
                onPress={(e) => this.props.onClick(e)}
            >
                <View style={styles.federationContainer}>
                    <View style={styles.imageContainer}>
                        <Image
                            style={styles.federationIcon}
                            resizeMode={"center"}
                            source={{ uri: 'https://fedogolf.org.do/wp-content/uploads/2018/03/fedogolf-escudo.jpg' }}
                        />
                    </View>
                    <View style={styles.textContainer}>
                        <Text numberOfLines={2} ellipsizeMode='tail' style={styles.title}>{this.props.name}</Text>
                        <Text style={styles.dateRequested}>{Moment(this.props.dateRequested).locale('en').format('DD MMMM YYYY')}</Text>
                    </View>
                </View>
                <Text
                    style={[
                        styles.statusStyle,
                        this.props.status === 'approve' ? { color: COLORS.green }
                            : this.props.status === 'disapprove' ? { color: COLORS.red }
                                : this.props.status === 'in_progress' ? { color: COLORS.airBnbLightGrey } : {}]}>{this.props.status.toUpperCase()}</Text>
            </TouchableOpacity>
        );
    }
}

export default MembershipRequest;

const styles = StyleSheet.create({
    federationButton: {
        width: "100%",
        height: verticalScale(90),
        paddingVertical: moderateScale(10),
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: "space-between",
        borderBottomColor: '#5553',
        borderBottomWidth: 0.8,
    },
    listStyles: {
        paddingTop: verticalScale(20),
        alignItems: 'center'
    },
    federationContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-start',
    },
    imageContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: moderateScale(5),
        // backgroundColor: 'red',
        width: moderateScale(70),
        height: verticalScale(70)
    },
    federationIcon: {
        width: 50,
        height: 50,
    },
    textContainer: {
        width: moderateScale(150)
    },
    title: {
        fontSize: moderateScale(15),
        fontWeight: "bold",
        marginBottom: verticalScale(5),
        color: COLORS.black
    },
    dateRequested: {
        fontSize: moderateScale(12),
        color: COLORS.darkgrey
    },
    statusStyle: {
        fontSize: moderateScale(12),
        color: COLORS.softBlack,
        marginRight: moderateScale(15)
    }
})
