import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

import { moderateScale, verticalScale, scale } from '../../../utilities/ScalingScreen'
import { COLORS } from '../../../config/constants';

class PricesByMembership extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={styles.container}>
                <Image
                    style={styles.federationIcon}
                    resizeMode={"center"}
                    source={{ uri: 'https://fedogolf.org.do/wp-content/uploads/2018/03/fedogolf-escudo.jpg' }}
                />
                <View style={styles.wrapper}>
                    <Text ellipsizeMode='tail' numberOfLines={1} style={styles.membershipTitle}>{this.props.membershipName.toLocaleUpperCase()}</Text>
                    <Text style={styles.price}>Price per member:  ${this.props.currency} {this.props.price}</Text>
                </View>
            </View>
        );
    }
}

export default PricesByMembership;

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFF',
        height: verticalScale(60),
        marginTop: verticalScale(5),
        marginHorizontal: moderateScale(10),
        // width: ''
        padding: scale(10),
        borderRadius: 10,
        alignItems: 'center',
        flexDirection: 'row',
        shadowColor: '#000',
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.3,
        shadowRadius: 6,
        elevation: 6
    },
    wrapper: {
        marginLeft: moderateScale(15)
    },
    membershipTitle: {
        color: COLORS.darkgrey,
        fontSize: moderateScale(14),
        fontWeight: '600',
        width: moderateScale(240)
    },
    price: {
        color: COLORS.midgreen,
        fontSize: moderateScale(14),
        marginTop: verticalScale(5)
    },
    federationIcon: {
        width: 50,
        height: 50,
    },
})