import React from 'react';
import { View, FlatList, TouchableOpacity, ScrollView } from 'react-native';

// Components

// Styles

import styles from './Styles';
import { Text } from 'native-base';
import PricesByMembership from './PricesByMembership';
import { verticalScale } from '../../../utilities/ScalingScreen';

export default class CoursePriceMembership extends React.Component {

    render() {
        const { data, action } = this.props;

        return (
            <View style={styles.wrapper}>
                <FlatList
                    data={data.memberships}
                    keyExtractor={(item, index) => index.toString()}
                    contentContainerStyle={{ paddingBottom: verticalScale(50),}}
                    renderItem={({ item, index }) =>
                        <PricesByMembership
                            key={index}
                            membershipName={item.name}
                            price={item.pivot.price}
                            currency={item.pivot.currency}
                        />
                    }

                />

            </View>
        );
    }
}

