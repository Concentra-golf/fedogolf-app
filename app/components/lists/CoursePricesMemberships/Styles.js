import { StyleSheet } from 'react-native';
import { verticalScale, scale } from '../../../utilities/ScalingScreen';

export default StyleSheet.create({
    wrapper: {
        // padding: scale(10),
    },
    item: {
        width: '50%',
        padding: 20,
        height: 80,
        backgroundColor: '#F9F9F9',
        margin: 5
    },
    name: {
        marginBottom: 4
    },
    price: {
        marginBottom: 4,
    },
    info: {
        alignItems: 'center'
    },

});
