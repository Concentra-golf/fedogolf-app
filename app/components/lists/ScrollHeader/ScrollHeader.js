import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    Dimensions,
    Animated,
    ScrollView,
    SafeAreaView,
    Platform,
    TouchableOpacity
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'
import { verticalScale, moderateScale, scale } from '../../../utilities/ScalingScreen';

const AnimatedTouchable = Animated.createAnimatedComponent(TouchableOpacity);

export default class ScrollHeader extends Component {

    constructor(props) {
        super(props);
        this.offset = 0;
        this.state = {
            scrollOffset: new Animated.Value(0),
            titleWidth: 0,
            titleTop: Platform.OS === 'android' ? new Animated.Value(50) : new Animated.Value(0),
            titleLeft: Platform.OS === 'android' ? new Animated.Value(10) : new Animated.Value(0),
        };
    }

    componentDidMount() {
        this.state.scrollOffset.addListener(({ value }) => (this.offset = value));

        const deviceHeight = Dimensions.get('window').height;
        const deviceWidth = Dimensions.get('window').width;
        console.log('Device Height', deviceHeight);
        console.log('Width', deviceWidth)

    }

    onScroll = e => {
        console.log(e)
        const scrollSensitivity = 4 / 6;
        const offset = e.nativeEvent.contentOffset.y / scrollSensitivity;
        console.log('Offset', offset);
        this.state.scrollOffset.setValue(offset);

        if (Platform.OS === 'android') {
            if (offset > 0) {
                Animated.spring(this.state.titleTop, { toValue: 20 }).start();
                Animated.spring(this.state.titleLeft, { toValue: 75 }).start();
                console.log('Moviendo')
            } else {
                Animated.spring(this.state.titleTop, { toValue: 70 }).start();
                Animated.spring(this.state.titleLeft, { toValue: 10 }).start();
                console.log('Normal')
            }
        } else {
            if (offset > 0) {
                Animated.spring(this.state.titleTop, { toValue: -10 }).start();
                Animated.spring(this.state.titleLeft, { toValue: 50 }).start();
                console.log('Moviendo')
            } else {
                Animated.spring(this.state.titleTop, { toValue: 0 }).start();
                Animated.spring(this.state.titleLeft, { toValue: 0 }).start();
                console.log('Normal')
            }
        }

    };

    getListItems = count => {
        const items = [];
        let i = 0;

        while (i < count) {
            i++;

            items.push(
                <View key={i} style={[styles.listItem, { backgroundColor: i % 2 === 0 ? '#eee5ff' : '#ceebfd' }]}>
                    <Text style={{ color: '#999' }}>{`List Item ${i}`}</Text>
                </View>
            );
        }

        return items;
    };

    setLayout = e => {
        if (this.offset === 0 && this.state.titleWidth === 0) {
            const titleWidth = e.nativeEvent.layout.width;
            console.log('titleWidth', titleWidth)
            this.setState({ titleWidth });
        }
    }

    render() {
        const { scrollOffset } = this.state;
        const screenWidth = Dimensions.get('window').width;
        return (
            <View style={styles.container}>
                <Animated.View
                    style={[
                        styles.header,
                        {
                            backgroundColor: this.props.headerColor,
                            paddingHorizontal: 8,
                            elevation: 10,
                            width: screenWidth,
                            height: scrollOffset.interpolate({
                                inputRange: [verticalScale(0), verticalScale(200)],
                                outputRange: [verticalScale(130), verticalScale(80)],
                                extrapolate: 'clamp',
                            }),
                        },
                    ]}
                >

                    <AnimatedTouchable
                        style={styles.iconStyle}
                        onPress={() => this.props.navigation.goBack()}
                    >
                        <Ionicons
                            size={32}
                            color="white"
                            name="md-arrow-back"
                        />
                    </AnimatedTouchable>

                    <Animated.Text
                        onLayout={(e) => this.setLayout(e)}
                        style={{
                            color: 'white',
                            top: this.state.titleTop,
                            left: this.state.titleLeft,
                            fontWeight: 'bold',
                            width: moderateScale(250),
                            fontSize: scrollOffset.interpolate({
                                inputRange: [0, 200],
                                outputRange: [26, moderateScale(20)],
                                extrapolate: 'clamp',
                            }),

                        }}>
                        {this.props.hederTitle}
                    </Animated.Text>

                    <Animated.View
                        style={{
                            // backgroundColor: 'black',
                            // height: 40,
                            width: scrollOffset.interpolate({
                                inputRange: [0, 200],
                                outputRange: [screenWidth * 0.9 - this.state.titleWidth, 0],
                                extrapolate: 'clamp',
                            }),
                        }}
                    />
                </Animated.View>


                <ScrollView
                    style={{ flex: 1, width: '100%', backgroundColor: 'white' }}
                    contentContainerStyle={{ width: '100%' }}
                    onScroll={this.onScroll}
                    scrollEventThrottle={20}
                >
                    {this.props.renderItem()}
                </ScrollView>


            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#ecf0f1',
    },
    header: {
        ...Platform.select({
            ios: {
                backgroundColor: '#1738B1',
                borderBottomWidth: 1,
                borderColor: 'gainsboro',
                flexDirection: 'row',
                alignItems: 'flex-end',
                justifyContent: 'center',
                paddingBottom: 8,
            },
            android: {
                backgroundColor: '#1738B1',
                paddingBottom: 8,

            },
        }),
    },
    iconStyle: {
        ...Platform.select({
            ios: {
                position: 'absolute',
                top: 55,
                left: 18
            },
            android: {
                top: verticalScale(30),
                left: moderateScale(15)
            }
        })
    },
    listItem: {
        height: 200,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
});
