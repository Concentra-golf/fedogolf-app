// with the score we calculate what type is
function getScoreCardType(score, holePar) {
    let scoreType = '';

    if (score === 1) {
        return scoreType = 'holeInOne'
    } else {
        if (score === holePar) {
            scoreType = 'par'
        } else if (score > holePar) {
            const calculateScore = score - holePar;
            if (calculateScore === 1) {
                scoreType = 'bodgey'
            } else if (calculateScore >= 2) {
                scoreType = 'doubleBodgey'
            }
        } else if (score < holePar && score !== 0) {
            const newScore = holePar - score;
            if (newScore === 1) {
                scoreType = 'birdie'
            } else if (newScore === 2 || newScore === 3) {
                scoreType = 'eagle'
            }
        } else if (score === 0) {
            scoreType = 'nothing'
        }
    }

    console.log('ScoreType =======', scoreType);
    return scoreType;
}

// when the score type is defined we assing a color
function identifyScoretypeColor(scoreType) {
    let color = 'white';
    if (scoreType === 'par') {
        color = 'white';
    } else if (scoreType === 'bodgey') {
        color = '#F6B72B';
    } else if (scoreType === 'doubleBodgey') {
        color = '#F56101';
    } else if (scoreType === 'birdie') {
        color = '#95D6E9';
    } else if (scoreType === 'eagle') {
        color = 'yellow'
    } else if (scoreType === 'nothing') {
        color = 'white';
    } else if (scoreType === 'holeInOne') {
        color = 'yellow'
    }
    return color
}

// Export Functions ===============================================================

export { identifyScoretypeColor, getScoreCardType };  