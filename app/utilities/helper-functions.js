// Formatting Functions ===============================================================

// Format Phone Number

function formatPhoneNumber(phone) {
  // Filter only numbers from the input

  let cleaned = ('' + phone).replace(/\D/g, '');

  // Check if the input is of correct length

  let match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);

  if (match) {
    return '(' + match[1] + ') ' + match[2] + '-' + match[3];
  }

  return null;
}

// Strip URL

function stripUrl(url) {
  return url.replace(/^(?:https?:\/\/)?(?:www\.)?/i, "").split('/')[0]
}

// Format Currency

function formatCurrency(number) {
  const fixedNumber = Number.parseFloat(number).toFixed(2);
  return '$' + String(fixedNumber).replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}


function getValueAndLabelFromArray(array, key) {
  const arrayToReturn = []

  array.map(element => {
    arrayToReturn.push({ value: element.id, label: element[key] })
  })

  return arrayToReturn
}

function removeDups(array) {
  let unique = {};
  array.forEach(function (i) {
    if (!unique[i]) {
      unique[i] = true;
    }
  });
  return Object.keys(unique);
}





// Export Functions ===============================================================

export { formatPhoneNumber, stripUrl, formatCurrency, getValueAndLabelFromArray, removeDups };
