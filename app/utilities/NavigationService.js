import { CommonActions } from '@react-navigation/native';

const config = {};
const mainConfig = {};

export function setNavigator(nav, route) {
    if (nav) {
      config.navigator = nav;
      config.navigator.initialRouteName = route;
    }
  }
  
export function mainPop(routeName) {
    config.navigator.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [
          { name: routeName },
        ],
      })
    );
}