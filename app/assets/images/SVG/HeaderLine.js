import * as React from "react"
import Svg, { Path } from "react-native-svg"

function HeaderLine(props) {
  return (
    <Svg
      width={props.width}
      height={props.height}
      viewBox="0 0 414.25 156.203"
      {...props}
    >
      <Path
        data-name="Path 406"
        d="M.143 0h414l.107 120.982s-22.2-10.813-78.822-19.919-97.532-3.984-146.12 20.987-89.684 33.242-109.987 34.038C30.865 157.989 0 135.68 0 135.68z"
        fill="#fff"
      />
    </Svg>
  )
}

export default HeaderLine;
