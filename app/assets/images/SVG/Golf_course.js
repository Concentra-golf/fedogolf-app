import * as React from "react"
import Svg, { Path } from "react-native-svg"
import { View } from "react-native"

function GolfCourse(props) {
    return (
        <View style={props.style}>

            <Svg
                color={props.color}
                height={props.size}
                viewBox="0 0 192 192"
                width={props.size}
                {...props}
            >
                <Path
                    fill={props.color}
                    d="M136 104c0-8.156 3.484-12.179 8.757-18.268C151.548 77.891 160 68.131 160 48c0-13.736-8.063-26.432-22.705-35.749A80.234 80.234 0 0096 0C53.617 0 8 22.531 8 72c0 31.813 9.447 61.929 26.6 84.8C51.377 179.17 73.757 192 96 192c36.539 0 56-27.876 56-48 0-16.063-6.364-23.858-11.011-29.55-3.46-4.236-4.989-6.285-4.989-10.45zm-64-8v56h-8V56h8v8l32 16z"
                    data-name="16-field"
                />
            </Svg>
        </View>
    )
}

export default GolfCourse
