import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, Platform, FlatList, Image } from 'react-native';
import { heightPercentageToDP, widthPercentageToDP, scale, moderateScale, verticalScale } from '../../utilities/ScalingScreen';
import TextLabel from '../../components/UI/TextLabel';
import { COLORS } from '../../config/constants';
import courses from '../../store/reducers/courses';
import Modal from 'react-native-modal';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import ScoreCardValues from '../../components/cards/ScoreCard/ScoreCardValues';
import { identifyScoretypeColor, getScoreCardType } from '../../utilities/scoreCard-helpers';

import * as playgolf from '../../store/actions/playgolf';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import GameRating from '../../components/UI/Modals/GameRating';

import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../translations/translation';

let totalScores = {};
class ScoreCard extends Component {
    constructor(props) {
        super(props);
        setI18nConfig();
        this.state = {
            holes: [],
            holesQty: 0,
            scoreType: '',
            gameInfo: [],
            selectedColor: [],
            loadingChange: false,
            scoreCardInfo: [],
            scores: [],
            players: [],
            showPlayerModal: false,
            playerSelected: 0,
            playerData: {},
            playerScore: [],
            totalScores: [],
            playerHoleStart: 1,
            loading: true,
            showRating: false,
            completeGameMsg: ""
        };
        this.getScore = this.getScore.bind(this);
        this.formField = this.formField.bind(this);
        this.multipleScoreCards = this.multipleScoreCards.bind(this);
    }

    componentDidMount() {
        if (this.props.route.params.route !== "PlayTab") {
            const courseData = this.props.route.params.courseData;
            const gameDetails = this.props.route.params.gameDetails;
            const players = this.props.route.params.players;
            const gameCreated = this.props.route.params.game;
            console.log(players);

            if (players.length > 1) {
                //We add the user tee value
                for (let index in players) {
                    if (index === '0') {
                        players[index]['tees'] = gameDetails.tees
                    }
                }
                this.setState({
                    players: players,
                    playerSelected: players[0].id,
                    playerData: players[0],
                })
                const player = players[0];

                this.multipleScoreCards(player);

            } else {

                for (let index in players) {
                    if (index === '0') {
                        players[index]['tees'] = gameDetails.tees,
                            players[index]['labelTees'] = gameDetails.labelTees
                    }
                }
                const player = players;

                this.setState({
                    players: players,
                    playerSelected: players[0].id,
                    playerData: players[0],
                    playerHoleStart: players[0].start,
                    courseData: courseData,
                    gameDetails: gameDetails,
                    loading: false,
                    holesQty: gameDetails.gameDetails.holes
                })

                this.settingScoreCard();
            }
        } else {
            const getPlayers = this.props.route.params.data.competitors[0];
            const getData = JSON.parse(getPlayers.pivot.score);
            console.log(getData);
            this.openGameScore(getData);
        }
    }

    // FUNCTION TO RESUME THE GAME, OPEN DE GAME FROM PLAY COMPONENT
    openGameScore(e) {
        const courseData = this.props.route.params.data.golf_course;
        const gameDetails = this.props.route.params.data;
        //Geting TEES Values
        const gettingTees = courseData.tees.filter((item) => {
            const array = [];
            if (gameDetails.tee_id === item.id) {
                array.push(item)
                return array;
            }
        })

        //Validated if holes selected are 18 or 9
        if (gameDetails.holes === 18) {
            var arr = gettingTees[0].holes;

            // We split array in two
            var first = arr.slice(0, 9); // we get the first 9 holes
            var second = arr.slice(9, 18); // we get the 10 to 18 holes

            //We Sum the yards to get the total of the first 9 holes
            const firstYards = first.reduce((value, item) => value + item.pivot.yards, 0)

            //We Sum the par's to get the total of the first 9 holes
            const firstPars = first.reduce((value, item) => value + item.pivot.par, 0)

            // We add the total for the first 9 holes
            var Out = {
                name: 'Out',
                editable: false,
                pivot: {
                    yards: firstYards,
                    par: firstPars
                }
            }
            first.push(Out);

            //We Sum the totalYards to get the total of the last 9 holes
            const lastYards = second.reduce((value, item) => value + item.pivot.yards, 0)

            //We Sum the par's to get the total of the last 9 holes
            const lastPars = second.reduce((value, item) => value + item.pivot.par, 0)

            // We add the total for the last 9 holes
            var In = {
                name: 'In',
                editable: false,
                index: 9,
                pivot: {
                    yards: lastYards,
                    par: lastPars
                }
            }
            second.push(In);

            // We Sum all the total yards
            const yardsTotal = firstYards + lastYards;

            // We Sum all the total yards
            const parTotal = firstPars + lastPars;

            // We add the total for the 18 holes
            var total = {
                name: 'Total',
                editable: false,
                pivot: {
                    yards: yardsTotal,
                    par: parTotal
                }
            }
            second.push(total);

            // We add merge the arrays again.
            const holes = first.concat(second)

            //WE SET THE DATA ON THE STATES TO RESUME THE GAME
            this.setState({
                holes: holes,
                holesQty: gameDetails.holes,
                courseData: this.props.route.params.data.golf_course,
                gameDetails: gameDetails,
                scoreCardInfo: e.score,
                scores: e.score,
                selectedColor: e.scoreColor,
                totalScores: e.totalScores,
                playerData: this.props.route.params.data.competitors[0].user,
                playerSelected: this.props.route.params.data.competitors[0].user.id,
                playerHoleStart: parseInt(this.props.route.params.data.competitors[0].pivot.start),
                loading: false,
            })
        } else {
            //If the user only wants 9 holes
            var arr = gettingTees[0].holes;
            var first = arr.slice(0, 9); // we get the first 9 holes

            //We Sum the yards to get the total of the first 9 holes
            const firstYards = first.reduce((value, item) => value + item.pivot.yards, 0)

            //We Sum the par's to get the total of the first 9 holes
            const firstPars = first.reduce((value, item) => value + item.pivot.par, 0)

            // We add the total for the first 9 holes
            var Out = {
                name: 'Out',
                editable: false,
                pivot: {
                    yards: firstYards,
                    par: firstPars
                }
            }
            first.push(Out);

            // We add the total for the 9 holes
            var total = {
                name: 'Total',
                editable: false,
                pivot: {
                    yards: firstYards,
                    par: firstPars
                }
            }
            first.push(total);

            //WE SET THE DATA ON THE STATES TO RESUME THE GAME
            this.setState({
                holes: first,
                holesQty: gameDetails.holes,
                courseData: this.props.route.params.data.golf_course,
                gameDetails: gameDetails,
                scoreCardInfo: e.score,
                scores: e.score,
                selectedColor: e.scoreColor,
                totalScores: e.totalScores,
                playerData: this.props.route.params.data.competitors[0].user,
                playerSelected: this.props.route.params.data.competitors[0].user.id,
                playerHoleStart: parseInt(this.props.route.params.data.competitors[0].pivot.start),
                loading: false,
            })
        }
    }

    // FUNCTION TO OPEN THE SCORE CARD WITH MULTIPLE USERS (NOT CURRENLY AVAILABLE)
    multipleScoreCards(e) {
        const courseData = this.props.route.params.courseData;
        const gameDetails = this.props.route.params.gameDetails;
        const player = e;

        //Geting TEES Values
        const gettingTees = courseData.tees.filter((item) => {
            const array = [];
            if (player.tees === item.id) {
                array.push(item)
                return array;
            }
        })

        //Validated if holes selected are 18 or 9
        if (gameDetails.holes.length === 18) {
            var arr = gettingTees[0].holes;

            // We split array in two
            var first = arr.slice(0, 9); // we get the first 9 holes
            var second = arr.slice(9, 18); // we get the 10 to 18 holes

            //We Sum the yards to get the total of the first 9 holes
            const firstYards = first.reduce((value, item) => value + item.pivot.yards, 0)

            //We Sum the par's to get the total of the first 9 holes
            const firstPars = first.reduce((value, item) => value + item.pivot.par, 0)

            // We add the total for the first 9 holes
            var Out = {
                name: 'Out',
                editable: false,
                pivot: {
                    yards: firstYards,
                    par: firstPars
                }
            }
            first.push(Out);

            //We Sum the totalYards to get the total of the last 9 holes
            const lastYards = second.reduce((value, item) => value + item.pivot.yards, 0)

            //We Sum the par's to get the total of the last 9 holes
            const lastPars = second.reduce((value, item) => value + item.pivot.par, 0)

            // We add the total for the last 9 holes
            var In = {
                name: 'In',
                editable: false,
                index: 9,
                pivot: {
                    yards: lastYards,
                    par: lastPars
                }
            }
            second.push(In);

            // We Sum all the total yards
            const yardsTotal = firstYards + lastYards;

            // We Sum all the total yards
            const parTotal = firstPars + lastPars;

            // We add the total for the 18 holes
            var total = {
                name: 'Total',
                editable: false,
                pivot: {
                    yards: yardsTotal,
                    par: parTotal
                }
            }
            second.push(total);

            // We add merge the arrays again.
            const holes = first.concat(second)
            this.setState({
                holes: holes
            })
        } else {
            //If the user only wants 9 holes
            var arr = gettingTees[0].holes;
            var first = arr.slice(0, 9); // we get the first 9 holes

            //We Sum the yards to get the total of the first 9 holes
            const firstYards = first.reduce((value, item) => value + item.pivot.yards, 0)

            //We Sum the par's to get the total of the first 9 holes
            const firstPars = first.reduce((value, item) => value + item.pivot.par, 0)

            // We add the total for the first 9 holes
            var Out = {
                name: 'Out',
                editable: false,
                pivot: {
                    yards: firstYards,
                    par: firstPars
                }
            }
            first.push(Out);

            // We add the total for the 9 holes
            var total = {
                name: 'Total',
                editable: false,
                pivot: {
                    yards: firstYards,
                    par: firstPars
                }
            }
            first.push(total);


            this.setState({
                holes: first
            })
        }
    }

    // FUNTION TO SET THE NEW GAME.
    settingScoreCard() {
        const courseData = this.props.route.params.courseData;
        const gameDetails = this.props.route.params.gameDetails;
        const players = this.props.route.params.players;
        //Geting TEES Values
        const gettingTees = courseData.tees.filter((item) => {
            const array = [];
            if (gameDetails.tees === item.id) {
                array.push(item)
                return array;
            }
        })

        //Validated if holes selected are 18 or 9
        if (gameDetails.holes.length === 18) {
            var arr = gettingTees[0].holes;

            // We split array in two
            var first = arr.slice(0, 9); // we get the first 9 holes
            var second = arr.slice(9, 18); // we get the 10 to 18 holes

            //We Sum the yards to get the total of the first 9 holes
            const firstYards = first.reduce((value, item) => value + item.pivot.yards, 0)

            //We Sum the par's to get the total of the first 9 holes
            const firstPars = first.reduce((value, item) => value + item.pivot.par, 0)

            // We add the total for the first 9 holes
            var Out = {
                name: 'Out',
                editable: false,
                pivot: {
                    yards: firstYards,
                    par: firstPars
                }
            }
            first.push(Out);

            //We Sum the totalYards to get the total of the last 9 holes
            const lastYards = second.reduce((value, item) => value + item.pivot.yards, 0)

            //We Sum the par's to get the total of the last 9 holes
            const lastPars = second.reduce((value, item) => value + item.pivot.par, 0)

            // We add the total for the last 9 holes
            var In = {
                name: 'In',
                editable: false,
                index: 9,
                pivot: {
                    yards: lastYards,
                    par: lastPars
                }
            }
            second.push(In);

            // We Sum all the total yards
            const yardsTotal = firstYards + lastYards;

            // We Sum all the total yards
            const parTotal = firstPars + lastPars;

            // We add the total for the 18 holes
            var total = {
                name: 'Total',
                editable: false,
                pivot: {
                    yards: yardsTotal,
                    par: parTotal
                }
            }
            second.push(total);

            // We add merge the arrays again.
            const holes = first.concat(second)
            this.setState({
                holes: holes
            })
        } else {
            //If the user only wants 9 holes
            var arr = gettingTees[0].holes;
            var first = arr.slice(0, 9); // we get the first 9 holes

            //We Sum the yards to get the total of the first 9 holes
            const firstYards = first.reduce((value, item) => value + item.pivot.yards, 0)

            //We Sum the par's to get the total of the first 9 holes
            const firstPars = first.reduce((value, item) => value + item.pivot.par, 0)

            // We add the total for the first 9 holes
            var Out = {
                name: 'Out',
                editable: false,
                pivot: {
                    yards: firstYards,
                    par: firstPars
                }
            }
            first.push(Out);

            // We add the total for the 9 holes
            var total = {
                name: 'Total',
                editable: false,
                pivot: {
                    yards: firstYards,
                    par: firstPars
                }
            }
            first.push(total);


            this.setState({
                holes: first
            })
        }
    }

    formField(e, field, i, index) {
        return { target: { ...field, value: parseInt(e === "" ? 0 : e), par: i, index: index } };
    }

    // FUNTION TO SET SCORE ON EACH FIELD
    async getScore(e) {
        const holeName = e.target.name;
        const holePar = e.target.par;
        const index = e.target.index;
        const score = e.target.value === '' ? 0 : parseInt(e.target.value);
        const playerSelected = this.state.playerData;

        let players = this.state.playerScore;
        let selectedColor = this.state.selectedColor;
        let scores = this.state.scores

        let scoreCardInfo = this.state.scoreCardInfo;

        if (!scoreCardInfo[playerSelected.id]) {
            scoreCardInfo[playerSelected.id] = []
        }

        // with the score we calculate what type is

        const scoreType = await getScoreCardType(score, holePar);
        console.log(scoreType);

        // when the score type is defined we assing a color

        const color = identifyScoretypeColor(scoreType);
        console.log(color);

        if (!selectedColor[playerSelected.id]) {
            selectedColor[playerSelected.id] = []
        }

        const validArray = selectedColor[playerSelected.id].filter(x => x !== undefined)

        if (validArray.find(x => x.index === index)) {
            // if the user edit t3he score we identify witch id belongs 
            const arrayIndex = selectedColor[playerSelected.id].findIndex(x => x.hole === holeName);
            selectedColor[playerSelected.id][arrayIndex] = { index: index, color: color, hole: holeName }
        } else {
            if (!selectedColor[playerSelected.id]) {
                selectedColor[playerSelected.id] = [];
            }
            if (!selectedColor[playerSelected.id].find(x => x.index === index)) {
                selectedColor[playerSelected.id].push({ index, color, hole: holeName });
            }
        }

        // Creating the object we are goin to use for sent the data
        if (scoreCardInfo[playerSelected.id].find(x => x.hole === holeName)) {
            // if the user edit the score we identify witch id belongs 
            const arrayIndex = scoreCardInfo[playerSelected.id].findIndex(x => x.hole === holeName);
            scoreCardInfo[playerSelected.id][arrayIndex] = { index: index, hole: holeName, par: holePar, score: score, }

        } else {
            if (!scoreCardInfo[playerSelected.id]) {
                scoreCardInfo[playerSelected.id] = [];
            }
            if (!scoreCardInfo[playerSelected.id].find(x => x.index === index)) {
                scoreCardInfo[playerSelected.id].push({ index: index, hole: holeName, par: holePar, score: score, });
            }
        }

        // setting values for each user
        players[playerSelected.id] = { playerId: playerSelected.id, holes: [...scoreCardInfo[playerSelected.id]] };
        let PlayerList = players.filter(Boolean);
        console.log(PlayerList)

        // If the play is for 18 holes
        const holes = this.state.holes;
        if (holes.length === 21) {
            // to get the first 9 holes total
            for (const key in holes) {
                getScore = holes[key];
                if (key === "9") {
                    let firstTotal = 0;
                    if (index <= 8) {
                        const filterScores = scoreCardInfo[playerSelected.id].filter(item => {
                            let array = []
                            if (item.index <= 8) {
                                return array
                            }
                        })
                        firstTotal = filterScores.reduce((accumulator, item) => accumulator + parseInt(item.score), 0)
                        totalScores = {
                            ...this.state.totalScores[playerSelected.id],
                            ...totalScores,
                            out: firstTotal
                        };
                        holes[key]['total'] = parseInt(firstTotal)

                    }
                    // to get the last 9 holes total
                } else if (key === "19") {
                    let secondTotal = 0;
                    if (index >= 9) {
                        const filterScores02 = scoreCardInfo[playerSelected.id].filter(item => {
                            let array = []
                            if (item.index >= 9) {
                                return array
                            }
                        })
                        secondTotal = filterScores02.reduce((accumulator, item) => accumulator + parseInt(item.score), 0)
                        totalScores = {
                            ...this.state.totalScores[playerSelected.id],
                            ...totalScores,
                            in: secondTotal
                        };
                        holes[key]['total'] = parseInt(secondTotal)
                    }
                } else if (key === "20") { // Total of the 18 holes
                    let total = 0;
                    total = scoreCardInfo[playerSelected.id].reduce((accumulator, item) => accumulator + parseInt(item.score), 0)
                    totalScores = {
                        ...this.state.totalScores[playerSelected.id],
                        ...totalScores,
                        total: total
                    };
                    holes[key]['total'] = parseInt(total)
                }
            }

        } else {
            let getScore = [];
            // if the use only select 9 Holes
            for (const key in holes) {
                getScore = holes[key];
                if (key === "9") {
                    let firstTotal = 0;
                    if (index <= 8) {
                        const forze01 = scoreCardInfo[playerSelected.id].filter(item => {
                            let array = []
                            if (item.index <= 8) {
                                return array
                            }
                        })
                        firstTotal = forze01.reduce((accumulator, item) => accumulator + parseInt(item.score), 0)
                        totalScores = {
                            ...this.state.totalScores[playerSelected.id],
                            ...totalScores,
                            out: firstTotal
                        };
                        holes[key]['total'] = firstTotal
                    }
                    // to get the last 9 holes total
                } else if (key === "10") { // Total of the 9 holes
                    let total = 0;
                    total = scoreCardInfo[playerSelected.id].reduce((accumulator, item) => accumulator + parseInt(item.score), 0)
                    totalScores = {
                        ...this.state.totalScores[playerSelected.id],
                        ...totalScores,
                        total: total
                    };
                    holes[key]['total'] = parseInt(total)
                }
            }
        }

        let playesTotal = this.state.totalScores;
        playesTotal[playerSelected.id] = totalScores;
        totalScores = {};

        this.setState({
            ...this.state,
            scoreType: scoreType,
            selectedColor: selectedColor,
            scoreCardInfo: scoreCardInfo,
            totalScores: playesTotal
        })


        //we validate the routes to set gameDetail, 
        let gameDetail = {};
        if (this.props.route.params.game) {
            gameDetail = this.props.route.params.game.game;
        } else {
            gameDetail = this.props.route.params.data;
        }

        //SETING THE DATA TO SEND 
        const scoreObject = {
            score: scoreCardInfo,
            scoreColor: selectedColor,
            totalScores: playesTotal,
        }
        const finalScore = { score: JSON.stringify(scoreObject) }

        // WE SAVE THE POST SCORE IN THE SERVER
        await this.props.postGame(finalScore, gameDetail.id, gameDetail.competitors[0].id, this.props.session.accessToken);

        if (this.props.playGolf.postGame.success) {
            console.log('Success')
        } else {
            console.log('Error')
        }

    }

    //FUNTION TO MANAGE MULTIPLES PLAYERS
    showPlayerModal(e) {
        this.setState({ showPlayerModal: e })
    }

    selectPlayer(e, playerData) {
        this.setState({
            playerSelected: e,
            playerData: playerData,
        })
        this.multipleScoreCards(playerData);
    }

    //FUNCTION TO HANDLE THE CHANGE ON EACH TEXT INPUT.
    holesAndValues(value, holePosition) {
        const scoreCardInfo = this.state.scoreCardInfo
        // Creating the object we are goin to use for sent the data
        const array = scoreCardInfo[this.state.playerSelected] && scoreCardInfo[this.state.playerSelected]
        if (scoreCardInfo[this.state.playerSelected] && array.find(x => x !== undefined && x !== null && x.index === holePosition)) {
            //111 the user edit the score we identify witch id belongs 
            scoreCardInfo[this.state.playerSelected] && scoreCardInfo[this.state.playerSelected].map((hole, index) => {
                if (hole.index === holePosition) {
                    scoreCardInfo[this.state.playerSelected][index] = { ...scoreCardInfo[this.state.playerSelected][index], score: parseInt(value === "" ? 0 : value) }
                }
            })

            this.setState({
                ...this.state,
                scoreCardInfo: scoreCardInfo
            })
        }
    }

    showRatingModal(visible) {
        //We validete if the game is complete to post the score
        if (this.state.holesQty === 18) {
            if (this.state.scoreCardInfo[this.state.playerData.id].length === 18) {
                this.setState({ showRating: visible })
            } else {
                this.setState({
                    completeGameMsg: translate('completeGame')
                })
                console.log('Completa el juego')
            }
        } else {
            //this is with 9 holes
            if (this.state.scoreCardInfo[this.state.playerData.id].length === 9) {
                this.setState({ showRating: visible })
            } else {
                this.setState({
                    completeGameMsg: translate('completeGame')
                })
            }
        }
    }

    render() {
        return (
            <KeyboardAwareScrollView
                extraScrollHeight={verticalScale(40)}
                style={styles.container}
            >
                {!this.state.loading &&
                    <View>
                        <Text style={styles.golfCourseTitle}>{this.state.courseData.course_name}</Text>
                        <View style={styles.courseWrapper}>
                            <Text style={styles.golfCoursesubtitle}>Holes: {this.state.gameDetails.holes.length || this.state.gameDetails.holes}</Text>
                            <View style={styles.courseDivider} />
                            <Text style={styles.golfCoursesubtitle}>Par: {this.state.courseData.par}</Text>
                        </View>


                        {/* <View style={styles.divider} />

                        <TouchableOpacity
                            onPress={players.length === 1 ? console.log('hey') : () => this.showPlayerModal(true)}
                            style={styles.playerInfoContainer}
                        >
                            <Image source={{ uri: 'http://bo.golfertek.com/assets/images/avatars/golfer_male.png' }} style={styles.image} />
                            <View style={styles.wrapper}>
                                <Text style={styles.playerName}>{this.state.playerData.firstname} {this.state.playerData.lastname}</Text>
                                <Text style={styles.playerTees}>Tees: {this.state.playerData.labelTees}</Text>
                            </View>
                        </TouchableOpacity> */}
                    </View>
                }

                <View style={styles.divider} />

                <View style={{ paddingVertical: verticalScale(20), }}>
                    <ScrollView
                        horizontal
                        contentContainerStyle={styles.scoreCardContainer}
                    >
                        <View style={{ flexDirection: 'column' }}>
                            {title.map((item, key) => (
                                <View key={key} style={styles.nameBox}>
                                    <Text style={styles.titles}>{item.name}</Text>
                                </View>
                            ))}
                        </View>

                        {!this.state.loadingChange && this.state.holes.map((item, index) => {
                            let holeData = {}
                            let colors = {}
                            if (this.state.scoreCardInfo) {
                                let values = this.state.scoreCardInfo[this.state.playerSelected];
                                if (values !== undefined && values !== null) {
                                    values = values.filter(x => x !== undefined && x !== null)
                                    holeData = values.find(x => x.index === index);
                                    if (holeData === undefined) {
                                        holeData = {};
                                    }
                                }
                                console.log(holeData)
                            }
                            if (this.state.selectedColor[this.state.playerSelected]) {
                                const array = this.state.selectedColor[this.state.playerSelected].filter(x => x !== undefined && x !== null)
                                colors = array.find(x => x.index == index)
                            }
                            return (
                                <ScoreCardValues
                                    playerHoleStart={this.state.playerHoleStart}
                                    totalScores={this.state.totalScores}
                                    playerSelected={this.state.playerSelected}
                                    key={index}
                                    score={holeData}
                                    index={index}
                                    customIndex={this.state.index}
                                    getScore={(e) => this.getScore(e)}
                                    data={item}
                                    holesAndValues={(text) => this.holesAndValues(text, index)}
                                    scoreType={this.state.scoreType}
                                    formField={this.formField}
                                    editable={item.editable}
                                    dafaultValue={item.total}
                                    color={colors}
                                />
                            )
                        }
                        )}
                    </ScrollView>
                </View>

                <View style={styles.legendContainer}>
                    {legend.map((item, key) => (
                        <View key={key} style={styles.legend}>
                            <View style={[styles.legentColor, { backgroundColor: item.color }]} />
                            <Text style={styles.legendTitle}>{item.name}</Text>
                        </View>
                    ))}
                </View>

                    <Text style={styles.errorMsg}>{this.state.completeGameMsg}</Text>

                <TouchableOpacity
                    style={styles.buttonPost}
                    onPress={() => this.showRatingModal(true)}
                // onPress={() => this.props.navigation.popToTop()}
                >
                    <Text style={styles.buttonPostText}>Post Score</Text>
                </TouchableOpacity>



                <Modal
                    isVisible={this.state.showRating}
                // onBackdropPress={() => this.showRatingModal(false)}
                >
                    <GameRating
                        close={(e) => this.showRatingModal(e)}
                        golfCourse={this.state.courseData}
                        gameDetails={this.state.gameDetails}
                        route={this.props.route.params.route}
                        navigation={this.props.navigation}
                    />
                </Modal>



                {/* SELECT PLAYERS MODAL */}

                <Modal
                    isVisible={this.state.showPlayerModal}
                    style={styles.playerModal}
                    onBackdropPress={() => this.showPlayerModal(false)}
                >
                    <View style={styles.modalContainer}>
                        <Text style={styles.modalTitle}>Choose a player</Text>
                        <FlatList
                            data={this.state.players}
                            extraData={this.state.players}
                            keyExtractor={(item, index) => index.toString()}
                            contentContainerStyle={styles.contentList}
                            renderItem={({ item, index }) =>
                                <TouchableOpacity
                                    onPress={() => this.selectPlayer(item.id, item)}
                                    style={styles.playerCardContainer}
                                >
                                    <View style={styles.wrapperTwo}>
                                        <Image source={{ uri: 'http://bo.golfertek.com/assets/images/avatars/golfer_male.png' }} style={styles.playerImageSelector} />
                                        <Text style={styles.playerNameSelector}>{item.firstname} {item.lastname}</Text>
                                    </View>

                                    <TouchableOpacity
                                        style={styles.checkbox}
                                    >
                                        {this.state.playerSelected === item.id &&
                                            <View
                                                style={{
                                                    height: 15,
                                                    width: 15,
                                                    borderRadius: 15 / 2,
                                                    backgroundColor: COLORS.green
                                                }}
                                            />
                                        }
                                    </TouchableOpacity>
                                </TouchableOpacity>
                            }
                        />
                    </View>
                </Modal>

            </KeyboardAwareScrollView>
        );
    }
}


function mapStateToProps(state) {
    return {
        session: state.session,
        user: state.session.user.data,
        playGolf: state.playgolf
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators(playgolf, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchProps
)(ScoreCard);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: scale(10),
    },
    playerInfoContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    image: {
        borderWidth: 0.5,
        borderColor: COLORS.airBnbGrey,
        height: 60,
        width: 60,
        borderRadius: 60 / 2,
        marginRight: moderateScale(15)
    },
    wrapper: {
        flexDirection: 'column'
    },
    playerName: {
        fontSize: moderateScale(17),
        fontWeight: '600',
        marginBottom: verticalScale(5),
        color: COLORS.airBnbGrey
    },
    playerTees: {
        fontSize: moderateScale(14),
        color: COLORS.airBnbGrey
    },
    golfCourseTitle: {
        fontWeight: 'bold',
        fontSize: moderateScale(20),
        color: COLORS.softBlack,
        textAlign: 'center'
    },
    courseWrapper: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: "center"
    },
    golfCoursesubtitle: {
        fontWeight: 'bold',
        fontSize: moderateScale(16),
        color: COLORS.darkblue,
        marginTop: verticalScale(5)
    },
    courseDivider: {
        height: verticalScale(20),
        width: 1,
        backgroundColor: COLORS.darkblue,
        marginHorizontal: moderateScale(20),
        marginTop: verticalScale(5)
    },
    divider: {
        width: '100%',
        borderWidth: Platform.OS === 'ios' ? 1 : 0.3,
        borderColor: 'rgba(0,0,0,0.2)',
        marginVertical: verticalScale(15)
    },
    header: {
        height: heightPercentageToDP('7%'),
        width: '100%',
        padding: scale(10),
        flexDirection: 'row',
        backgroundColor: COLORS.darkblue,
        alignItems: 'center',
        justifyContent: 'center'
    },
    subTitle: {
        fontSize: moderateScale(16),
        color: COLORS.white,
        fontWeight: '600',
        marginHorizontal: moderateScale(5),
    },
    titles: {
        fontSize: moderateScale(14),
        color: COLORS.darkblue,
        fontWeight: '600',
        // textAlign: 'center'
    },
    scoreCardContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    nameBox: {
        height: 60,
        width: 80,
        borderWidth: 0.3,
        borderColor: COLORS.airBnbGrey,
        justifyContent: 'center',
        alignItems: 'center'
    },
    holeTitle: {
        fontSize: moderateScale(14),
        fontWeight: 'bold',
        color: COLORS.white
    },
    cardInput: {
        height: 45,
        width: 45,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center'
    },
    buttonPost: {
        height: heightPercentageToDP('7%'),
        width: widthPercentageToDP('80%'),
        backgroundColor: COLORS.darkblue,
        borderRadius: 6,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: verticalScale(20),
        marginBottom: verticalScale(20)
    },
    buttonPostText: {
        color: COLORS.white,
        fontSize: moderateScale(14),
    },
    legend: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    legendContainer: {
        width: '100%',
        flexWrap: 'wrap',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    legentColor: {
        height: 15,
        width: 15,
        borderWidth: 0.5,
        borderColor: COLORS.lightgrey,
        borderRadius: (15 / 2)
    },
    legendTitle: {
        fontSize: moderateScale(14),
        margin: scale(10)
    },
    //
    playerModal: {
        justifyContent: "flex-end",
        margin: 0,
    },
    modalContainer: {
        height: heightPercentageToDP('30%'),
        width: '100%',
        backgroundColor: 'white',
        padding: scale(10)
    },
    modalTitle: {
        fontWeight: 'bold',
        fontSize: moderateScale(16),
        color: COLORS.airBnbGrey,
        marginVertical: verticalScale(15)
    },
    playerCardContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: verticalScale(10),
        justifyContent: 'space-between'
    },
    wrapperTwo: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    playerImageSelector: {
        height: 50,
        width: 50,
        borderWidth: 1,
        borderRadius: 50 / 2,
        marginRight: moderateScale(15)
    },
    playerNameSelector: {
        fontSize: moderateScale(14),
        color: COLORS.softBlack
    },
    checkbox: {
        height: 20,
        width: 20,
        borderWidth: 1,
        borderRadius: 20 / 2,
        marginRight: moderateScale(10),
        justifyContent: 'center',
        alignItems: 'center'
    },
    errorMsg: {
        fontSize: moderateScale(15),
        color: COLORS.red,
        textAlign: 'center'
    }
})

const title = [
    { name: 'Hole' },
    { name: 'Yards' },
    { name: 'Handicap' },
    { name: 'Par' },
    { name: 'Score' },
]

const legend = [
    { key: '1', name: 'Albatross', color: 'green' },
    { key: '2', name: 'Eagle', color: 'yellow' },
    { key: '3', name: 'Birdie', color: '#95D6E9' },
    { key: '4', name: 'Par', color: 'white' },
    { key: '5', name: 'Bodgey', color: '#F6B72B' },
    { key: '6', name: 'Double Bodgey or Worse', color: '#F56101' },
]