//Modules
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ScrollView, Modal, Alert, ActivityIndicator } from 'react-native';


import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Moment from 'moment';
import * as RNLocalize from 'react-native-localize';
import { Toast } from 'native-base';

//Redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as courses from '../../store/actions/courses';
import * as playGolf from '../../store/actions/playgolf';

//Components
import PickerSelect from '../../components/UI/Picker/PickerSelect';
import FormInput from "../../components/UI/FormInput/FormInput";
import TextLabel from '../../components/UI/TextLabel';
import PlayerCard from '../../components/cards/PlayGolf/PlayerCard';
import AddPlayer from '../../components/UI/Modals/AddPlayer';
import PlayerSelected from '../../components/forms/AddPlayer/PlayerSelected';
import AddGuest from '../../components/forms/AddPlayer/AddGuest';

//Helpers
import { COLORS } from '../../config/constants';
import { scale, heightPercentageToDP, verticalScale, moderateScale, widthPercentageToDP } from '../../utilities/ScalingScreen';
import { translate, setI18nConfig, handleLocalizationChange } from '../../translations/translation';
import { getValueAndLabelFromArray } from '../../utilities/helper-functions';
const date = Date.now()


class PlayGolf extends Component {
    constructor(props) {
        super(props);
        setI18nConfig();
        this.state = {
            golfCourse: [],
            course: {},
            holeSelected: 1,
            holes: holes,
            activeIndex: 1,
            tees: [],
            loading: true,
            teesValue: '',
            startingHole: [],
            msg: '',
            date: Moment(date).format('DD-MM-YYYY'),
            dateValue: Moment(date).format('DD-MM-YYYY'),
            playersList: [],
            addPlayer: false,
            privacyGame: 1,
            playerSelected: false,
            radioButton: false,
            addGuest: false,
            gameModality: [],
            loadingModal: false,
            showTees: false
        };
    }

    async componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);

        if (this.props.route.params.route !== 'Play') {
            this.getCourseData();
        } else {
            this.getCourseDataFromPlay();
        }

    }

    async getCourseDataFromPlay() {
        await this.props.getCourses(this.props.session.accessToken);

        if (this.props.courses.allCourses.success) {
            const coursesList = getValueAndLabelFromArray(this.props.courses.allCourses.data, 'course_name');

            let playersList = this.state.playersList;
            const playerData = {
                firstname: this.props.user.firstname,
                lastname: this.props.user.lastname,
                email: this.props.user.email,
                gender: this.props.user.gender,
                id: this.props.user.id
            }

            playersList.push(playerData);

            this.setState({
                loading: false,
                refreshing: false,
                coursesList: coursesList,
                playersList: playersList
            })
        }
    }


    async getCourseData() {
        const courseData = this.props.route.params.courseData;
        await this.props.getCourses(this.props.session.accessToken);

        if (this.props.courses.allCourses.success) {
            const coursesList = getValueAndLabelFromArray(this.props.courses.allCourses.data, 'course_name')
            const tees = getValueAndLabelFromArray(courseData.tees, 'color');

            let playersList = this.state.playersList;
            const user = this.props.user;

            const playerData = {
                firstname: user.firstname,
                lastname: user.lastname,
                email: user.email,
                gender: user.gender,
                id: user.id
            }

            playersList.push(playerData);

            this.setState({
                loading: false,
                refreshing: false,
                coursesList: coursesList,
                tees: tees,
                golfCourse: courseData.id,
                playersList: playersList,
                showTees: true
            })
        }
    }

    componentWillUnmount() {
        RNLocalize.removeEventListener('change', handleLocalizationChange);
    }

    selectHole(e) {
        this.setState({ activeIndex: e })
        if (e === 2) {
            const halfHoles = holes.slice(0, 9);
            this.setState({
                holes: halfHoles
            })
        } else {
            this.setState({
                holes: holes
            })
        }
    }

    privacySetting(e) {
        this.setState({ privacyGame: e })
    }

    setValues(item, e) {
        switch (e) {
            case 'golfCourse':
                this.setState({ golfCourse: item })
                this.getTeessValues(item)
                break;
            case 'tees':
                this.setState({ teesValue: item })
                break;
            case 'startingHole':
                this.setState({ startingHole: item });
                break;
            case 'date':
                this.setState({ dateValue: item });
                break;
            case 'gameModality':
                this.setState({ gameModality: item });
                break;
        }
    }


    getTeessValues(golfCourse_id) {
        if (golfCourse_id !== null) {
            const getTees = this.props.courses.allCourses.data.filter(item => golfCourse_id === item.id);
            const tees = getValueAndLabelFromArray(getTees[0].tees, 'color');
            this.setState({
                showTees: true,
                tees: tees,
                course: getTees[0]
            })
        }
    }

    async playGolf(e, f) {
        const { golfCourse, tees, startingHole, date, gameModality } = this.state;

        if (golfCourse !== null && tees !== null && startingHole !== null && gameModality !== null) {

            this.setState({ loadingModal: true })
            const { courseData } = this.props.route.params;

            //We add the hole value to the main user
            for (const key in this.state.playersList) {
                if (key === "0") {
                    const hole = parseInt(this.state.startingHole.slice(-2));
                    this.state.playersList[key]['start'] = hole
                }
            }

            //Object to create the game
            const gameData = {
                users: this.state.playersList,
                holes: this.state.holes.length,
                tee: this.state.teesValue,
                date: Moment(this.state.dateValue, "DD-MM-YYYY").format('YYYY-MM-DD HH:mm:ss'),
                type: this.state.gameModality
            }

            await this.props.createGolfGame(this.state.golfCourse, gameData, this.props.session.accessToken);
            if (this.props.playGolf.golfGame.success) {
                this.setState({ loadingModal: false })
                const getTeesName = this.state.tees.find(x => x.value === this.state.teesValue);

                //We created a object to manage the data on the score's components.
                const gameDetails = {
                    golfCourse: this.state.golfCourse,
                    gameDetails: this.props.playGolf.golfGame.data.game,
                    tees: this.state.teesValue,
                    labelTees: getTeesName.label,
                    startingHole: this.state.startingHole,
                    holes: this.state.holes
                }

                this.setState({ msg: '' })

                // We use the playtype to manage the game screen 
                if (this.props.route.params.playType === 'HoleByHole') {
                    this.props.navigation.navigate('ScoreCard', {
                        courseData: courseData || this.state.course,
                        gameDetails: gameDetails,
                        players: this.state.playersList,
                        game: this.props.playGolf.golfGame.data
                    })
                } else if (this.props.route.params.playType === 'TotalScore') {
                    this.props.navigation.navigate('TotalScore', {
                        courseData: courseData,
                        gameDetails: gameDetails,
                        players: this.state.playersList,
                        game: this.props.playGolf.golfGame.data
                    })
                }

            } else {
                console.log('error with api')
                this.setState({ loadingModal: false })
                Toast.show({
                    type: 'danger',
                    text: "Se ha producido un error al crear el juego.",
                    duration: 3000,
                    buttonText: 'OK'
                })
            }
        } else {
            console.log('requertidos')
        }
    }

    getPlayers(e) {
        const players = this.state.playersList;
        const playersAdded = e

        //We validate if the player is already selected
        const validateArray = players.filter((item) => {
            if (item.id === playersAdded.id) {
                return true
            }
            return false
        });

        //we validate if the user is already selected
        if (validateArray.length === 0) {
            //with the tees ID We identify the name 
            let labelTees = this.state.tees.find((item) => {
                if (item.value === e.tees) {
                    return item
                }
            })
            const newPlayerInfo = Object.assign(playersAdded, { labelTees: labelTees.label }); //We add the tee label
            players.push(newPlayerInfo); // we add the selected player to the array list
            this.setState({ playersList: players }) // we update the list with the new values
            this.openPlayerModal(false) // we close the modal
        } else {
            Alert.alert('This user is already selected', '', [{ text: 'Ok' }])
        }
    }

    openPlayerModal(visible) {
        this.setState({ addPlayer: visible })
    }

    playerSelectedModal(visible, data) {
        this.setState({
            playerSelected: visible,
            selectedPlayer: data
        })
    }

    removeSelectedPlayer(e) {
        const playerSelected = e;
        const getPlayerOnList = this.state.playersList.findIndex(x => x.id === playerSelected.id);
        this.state.playersList.splice(getPlayerOnList, 1);

        this.playerSelectedModal(false)
    }

    addGuest(visible) {
        this.setState({ addGuest: visible })
    }

    render() {
        return (
            <ScrollView style={styles.container}>

                <View style={styles.coursePicker}>
                    <TextLabel additionalStyles={styles.title}>{translate('golfCourse')}</TextLabel>

                    {!this.state.loading &&
                        <View style={styles.pickerContainer}>
                            <PickerSelect
                                data={this.state.coursesList}
                                value={this.state.golfCourse}
                                placeholder={'Please select your course'}
                                action={(value) => this.setValues(value, 'golfCourse')}
                            />

                            <FontAwesome
                                name='caret-down'
                                size={scale(15)}
                                color={COLORS.softBlack}
                            />
                        </View>
                    }

                    {/* <TextLabel size={13} color={'danger'} additionalStyles={{ marginBottom: 10 }}>
                        {props.errors.golfCourse}
                    </TextLabel> */}
                </View>

                <View style={styles.teesContainer}>
                    <TextLabel additionalStyles={styles.title}>{translate('gameModality')}</TextLabel>
                    <View style={styles.pickerContainer}>
                        <PickerSelect
                            data={gameModality}
                            value={this.state.gameModality}
                            placeholder={translate('modalityPlaceholder')}
                            action={(value) => this.setValues(value, 'gameModality')}
                        />
                        <FontAwesome
                            name='caret-down'
                            size={scale(15)}
                            color={COLORS.softBlack}
                        />
                    </View>
                    {/* <TextLabel size={13} color={'danger'} additionalStyles={{ marginBottom: 10 }}>
                        {props.errors.gameModality}
                    </TextLabel> */}
                </View>

                <View style={styles.holesContainer}>
                    <TextLabel additionalStyles={styles.title}>{translate('holeNumber')}</TextLabel>
                    <View style={styles.buttonContainer}>
                        <TouchableOpacity
                            style={[styles.holesSelect, this.state.activeIndex === 1 ? { backgroundColor: COLORS.darkblue } : {}]}
                            onPress={() => this.selectHole(1)}
                        >
                            <MaterialIcons
                                name='golf-course'
                                size={27}
                                color={this.state.activeIndex === 1 ? COLORS.white : COLORS.darkblue}
                            />
                            <Text style={[styles.holesSelectText, this.state.activeIndex === 1 ? { color: COLORS.white } : {}]}>18 {translate('holes')}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={[styles.holesSelect, this.state.activeIndex === 2 ? { backgroundColor: COLORS.darkblue } : {}]}
                            onPress={() => this.selectHole(2)}
                        >
                            <MaterialIcons
                                name='golf-course'
                                size={27}
                                color={this.state.activeIndex === 2 ? COLORS.white : COLORS.darkblue}
                            />
                            <Text style={[styles.holesSelectText, this.state.activeIndex === 2 ? { color: COLORS.white } : {}]}>9 {translate('holes')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={styles.holesContainer}>
                    <TextLabel additionalStyles={styles.title}>{translate('privacyTitle')}</TextLabel>
                    <View style={styles.buttonContainer}>
                        <TouchableOpacity
                            style={[styles.holesSelect, this.state.privacyGame === 1 ? { backgroundColor: COLORS.darkblue } : {}]}
                            onPress={() => this.privacySetting(1)}
                        >
                            <Text style={[styles.holesSelectText, this.state.privacyGame === 1 ? { color: COLORS.white } : {}]}>{translate('publicGame')}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={[styles.holesSelect, this.state.privacyGame === 2 ? { backgroundColor: COLORS.darkblue } : {}]}
                            onPress={() => this.privacySetting(2)}
                        >
                            <Text style={[styles.holesSelectText, this.state.privacyGame === 2 ? { color: COLORS.white } : {}]}>{translate('privateGame')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                {/* <View style={styles.playerContainer}>
                                    <TextLabel additionalStyles={styles.title}>{translate('players')}</TextLabel>
                                    <View style={styles.playersListContainer}>

                                        {!this.state.loading && this.state.playersList.map((item, index) => (
                                            <PlayerCard
                                                key={index}
                                                item={item}
                                                firstname={item.firstname}
                                                lastname={item.lastname}
                                                email={item.email}
                                                tees={item.labelTees}
                                                onPress={() => this.playerSelectedModal(true, item)}
                                                user={this.props.user}
                                                holeStart={item.start}
                                            />
                                        ))}

                                        <TouchableOpacity
                                            style={styles.frequentButton}
                                            onPress={() => this.setState({ radioButton: !this.state.radioButton })}
                                        >
                                            <View style={styles.radioButton} >
                                                {this.state.radioButton &&
                                                    <View style={{
                                                        height: 15,
                                                        width: 15,
                                                        borderRadius: 10,
                                                        backgroundColor: COLORS.green
                                                    }}
                                                    />
                                                }
                                            </View>
                                            <Text style={styles.radioText}>{translate('frecuentButton')}</Text>
                                        </TouchableOpacity>

                                        <View style={styles.buttonWrapper}>
                                            <TouchableOpacity
                                                style={styles.addPlayer}
                                                onPress={() => this.openPlayerModal(true)}
                                            >
                                                <FontAwesome5
                                                    name='user-plus'
                                                    size={27}
                                                    color={COLORS.airBnbLightGrey}
                                                />
                                                <Text style={styles.buttonText}>{translate('addPlayer')}</Text>
                                            </TouchableOpacity>

                                            <TouchableOpacity
                                                style={styles.addPlayer}
                                                onPress={() => this.addGuest(true)}
                                            >
                                                <FontAwesome5
                                                    name='user-plus'
                                                    size={27}
                                                    color={COLORS.airBnbLightGrey}
                                                />
                                                <Text style={styles.buttonText}>{translate('addGuest')}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View> */}

                {this.state.showTees &&
                    <View style={styles.teesContainer}>
                        <TextLabel additionalStyles={styles.title}>{translate('playerTess')}</TextLabel>
                        <View style={styles.pickerContainer}>
                            <PickerSelect
                                data={this.state.tees}
                                value={this.state.teesValue}
                                placeholder={translate('playerTessPlaceholder')}
                                action={(value) => this.setValues(value, 'tees')}
                            />
                            <FontAwesome
                                name='caret-down'
                                size={scale(15)}
                                color={COLORS.softBlack}
                            />
                        </View>
                        {/* <TextLabel size={13} color={'danger'} additionalStyles={{ marginBottom: 10 }}>
                        {props.errors.tees}
                    </TextLabel> */}
                    </View>
                }

                <View style={styles.daysContainer}>
                    <TextLabel additionalStyles={styles.title}>{translate('playDate')}</TextLabel>
                    <View style={styles.pickerContainer}>
                        <FormInput
                            onChangeText={(value) => this.setValues(value, 'date')}
                            value={this.state.dateValue}
                            theme={"light"}
                            type={'date'}
                            placeholder="Select a Date"
                            additionalStyles={styles.inputs}
                            data={this.state.date}
                            editable={false}
                        />
                    </View>
                    {/* <TextLabel size={13} color={'danger'} additionalStyles={{ marginBottom: 10 }}>
                        {props.errors.date}
                    </TextLabel> */}
                </View>

                <View style={styles.startingContainer}>
                    <TextLabel additionalStyles={styles.title}>{translate('startingHole')}</TextLabel>
                    <View style={styles.pickerContainer}>
                        <PickerSelect
                            data={this.state.holes}
                            value={this.state.startingHole}
                            placeholder={translate('startingHoleDes')}
                            action={(value) => this.setValues(value, 'startingHole')}
                        />
                        <FontAwesome
                            name='caret-down'
                            size={scale(15)}
                            color={COLORS.softBlack}
                        />
                    </View>
                    {/* <TextLabel size={13} color={'danger'} additionalStyles={{ marginBottom: 10 }}>
                        {props.errors.startHole}
                    </TextLabel> */}
                </View>


                <View style={styles.startButtonContainer}>
                    <TouchableOpacity
                        onPress={() => this.playGolf()}
                        // onPress={() => props.handleSubmit()}
                        style={styles.startButton}
                    >
                        {!this.state.loadingModal ? (
                            <Text style={styles.startButtonText}>{translate('startRound')}</Text>
                        ) : (
                                <ActivityIndicator size='large' color='white' />
                            )
                        }
                    </TouchableOpacity>
                </View>

                {/* ================================ MODALS COMPONENTS ================================ */}

                <Modal
                    visible={this.state.addPlayer}
                    animationType='slide'
                >
                    <AddPlayer
                        close={(e) => this.openPlayerModal(e)}
                        getPlayers={(e) => this.getPlayers(e)}
                        tees={this.state.tees}
                        holes={this.state.holes}
                    />
                </Modal>

                <Modal
                    visible={this.state.playerSelected}
                    animationType='slide'
                >
                    <PlayerSelected
                        close={(e) => this.playerSelectedModal(e)}
                        deletePlayer={(e) => this.removeSelectedPlayer(e)}
                        playerInfo={this.state.selectedPlayer}
                        route={'MainSetting'}
                        tees={this.state.tees}
                        holes={this.state.holes}
                    />
                </Modal>

                <Modal
                    visible={this.state.addGuest}
                    animationType='slide'
                >
                    <AddGuest
                        close={(e) => this.addGuest(e)}
                        getPlayers={(e) => this.getPlayers(e)}
                        tees={this.state.tees}
                        holes={this.state.holes}
                    />
                </Modal>


            </ScrollView>
        );
    }
}

const gameModality = [
    { label: "Individual", value: "individual" },
    { label: "Scramble", value: "scramble" },
    { label: "Stroke Play", value: "strokeplay" },
    { label: "Best-Ball", value: "bestball" },
]

function mapStateToProps(state) {
    return {
        session: state.session,
        user: state.session.user.data,
        courses: state.courses,
        playGolf: state.playgolf
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators({ ...courses, ...playGolf }, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchProps,
)(PlayGolf);


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.white,
        padding: scale(10)
    },
    coursePicker: {

    },
    title: {
        fontSize: moderateScale(16),
        color: COLORS.airBnbGrey,
        fontWeight: '600'
    },
    pickerContainer: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        width: '100%',
        // backgroundColor: 'red',
        borderColor: COLORS.airBnbGrey,
        height: heightPercentageToDP('7%'),
        marginTop: verticalScale(10),
        padding: scale(5),
        alignItems: 'center',
    },
    playerContainer: {
        width: '100%',
        marginTop: verticalScale(20),
        justifyContent: 'center',
        paddingHorizontal: scale(10),
        paddingVertical: scale(10),
    },
    playersListContainer: {
        width: '100%',
    },
    addPlayer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    buttonText: {
        marginLeft: moderateScale(10),
        fontSize: moderateScale(13),
    },
    holesContainer: {
        width: '100%',
        marginTop: verticalScale(20)
    },
    buttonContainer: {
        flexDirection: 'row',
        marginTop: verticalScale(10),
        justifyContent: 'space-evenly',
        alignItems: 'center'
    },
    holesSelect: {
        width: widthPercentageToDP('40%'),
        height: heightPercentageToDP('7%'),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: COLORS.darkblue,
        flexDirection: 'row'
    },
    holesSelectText: {
        fontSize: moderateScale(14),
        color: COLORS.darkblue,
        fontWeight: '600',
        marginLeft: moderateScale(5)
    },
    teesContainer: {
        width: '100%',
        marginTop: verticalScale(20)
    },
    daysContainer: {
        width: '100%',
        marginTop: verticalScale(20)
    },
    startingContainer: {
        width: '100%',
        marginTop: verticalScale(20)
    },
    startButtonContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: verticalScale(20),
        paddingBottom: verticalScale(20)
    },
    startButton: {
        width: widthPercentageToDP('80%'),
        height: heightPercentageToDP('7%'),
        backgroundColor: COLORS.darkblue,
        borderRadius: 7,
        justifyContent: 'center',
        alignItems: 'center'
    },
    startButtonText: {
        fontSize: moderateScale(16),
        color: COLORS.white
    },
    inputs: {
        width: moderateScale(330),
        backgroundColor: 'transparent',
        color: COLORS.airBnbLightGrey,
        borderWidth: 0,
        fontSize: moderateScale(14),
        paddingHorizontal: moderateScale(20),
        marginTop: 15,
        borderRadius: 0,
        textAlign: 'left',
    },
    frequentButton: {
        flexDirection: 'row',
        marginTop: verticalScale(10)
    },
    radioButton: {
        borderWidth: 1,
        height: 20,
        width: 20,
        borderRadius: scale(10),
        marginRight: moderateScale(20),
        justifyContent: 'center',
        alignItems: 'center'
    },
    radioText: {
        fontSize: moderateScale(14)
    },
    buttonWrapper: {
        flexDirection: 'row',
        // backgroundColor: 'red',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        marginTop: verticalScale(15)

    }
})

const holes = [
    { label: 'Hole 1', value: 'hole01' },
    { label: 'Hole 2', value: 'hole02' },
    { label: 'Hole 3', value: 'hole03' },
    { label: 'Hole 4', value: 'hole04' },
    { label: 'Hole 5', value: 'hole05' },
    { label: 'Hole 6', value: 'hole06' },
    { label: 'Hole 7', value: 'hole07' },
    { label: 'Hole 8', value: 'hole08' },
    { label: 'Hole 9', value: 'hole09' },
    { label: 'Hole 10', value: 'hole010' },
    { label: 'Hole 11', value: 'hole011' },
    { label: 'Hole 12', value: 'hole012' },
    { label: 'Hole 13', value: 'hole013' },
    { label: 'Hole 14', value: 'hole014' },
    { label: 'Hole 15', value: 'hole015' },
    { label: 'Hole 16', value: 'hole016' },
    { label: 'Hole 17', value: 'hole017' },
    { label: 'Hole 18', value: 'hole018' },
]


