import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { COLORS } from '../../config/constants';
import { verticalScale, scale, moderateScale } from '../../utilities/ScalingScreen';

import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../translations/translation';
import * as RNLocalize from 'react-native-localize';

class SelectPlayMethod extends Component {
    constructor(props) {
        super(props);
        setI18nConfig();
        this.state = {
        };
    }

    componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);
    }

    componentWillUnmount() {
        RNLocalize.removeEventListener('change', handleLocalizationChange);
    }

    playHolebyHole() {
        if (this.props.route.params.route !== 'Play') {
            this.props.navigation.navigate('PlayGolf', {
                courseData: this.props.route.params.courseData,
                playType: 'HoleByHole'
            })
        } else {
            this.props.navigation.navigate('PlayGolf', {
                route: this.props.route.params.route,
                playType: 'HoleByHole'
            })
        }
    }

    totalScore() {
        if (this.props.route.params.route !== 'Play') {
            this.props.navigation.navigate('PlayGolf', {
                courseData: this.props.route.params.courseData,
                playType: 'TotalScore'
            })
        } else {
            this.props.navigation.navigate('PlayGolf', {
                route: this.props.route.params.route,
                playType: 'TotalScore'
            })
        }
    }

    render() {
        return (
            <View style={styles.container}>

                <Text style={styles.title}>{translate('roundTitle')}</Text>

                <TouchableOpacity
                    style={styles.button}
                    onPress={() => this.playHolebyHole()}
                >
                    <Text style={styles.buttonTitle}>{translate('holeByHole')}</Text>
                </TouchableOpacity>

                <Text style={styles.description}>{translate('holebyHoleDes')}</Text>

                <TouchableOpacity
                    onPress={() => this.totalScore()}
                    style={styles.button}
                >
                    <Text style={styles.buttonTitle}>{translate('totalScore')}</Text>
                </TouchableOpacity>

                <Text style={styles.description}>{translate('totalScoreDes')}</Text>

            </View>
        );
    }
}

export default SelectPlayMethod;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.white,
        padding: scale(10),
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: moderateScale(22),
        fontWeight: 'bold',
        color: COLORS.darkblue,

    },
    button: {
        height: verticalScale(45),
        backgroundColor: COLORS.darkblue,
        width: '100%',
        marginTop: verticalScale(10),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: scale(5)
    },
    buttonTitle: {
        fontSize: moderateScale(14),
        fontWeight: '600',
        color: COLORS.white
    },
    description: {
        fontSize: moderateScale(14),
        margin: scale(10),
        color: COLORS.darkblue
    }
})