import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ScrollView, Modal, Alert, TextInput } from 'react-native';
import { COLORS } from '../../config/constants';
import PickerSelect from '../../components/UI/Picker/PickerSelect';
import FormInput from "../../components/UI/FormInput/FormInput";

import DatePicker from 'react-native-datepicker'
import { scale, heightPercentageToDP, verticalScale, moderateScale, widthPercentageToDP } from '../../utilities/ScalingScreen';

import TextLabel from '../../components/UI/TextLabel';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { getValueAndLabelFromArray } from '../../utilities/helper-functions';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as courses from '../../store/actions/courses';
import PlayerCard from '../../components/cards/PlayGolf/PlayerCard';
import AddPlayer from '../../components/UI/Modals/AddPlayer';
import PlayerSelected from '../../components/forms/AddPlayer/PlayerSelected';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import Moment from 'moment';
import { extendMoment } from 'moment-range';

const date = Date.now()

class TotalScore extends Component {
    constructor(props) {
        super(props);
        this.state = {
            frontValue: 0,
            backValue: 0,
            playerSelected: 0,
            playerData: {},
        };
    }
    async componentDidMount() {
        const courseData = this.props.route.params.courseData;
        const gameDetails = this.props.route.params.gameDetails;
        const players = this.props.route.params.players;
        console.log(players);

        if (players.length > 1) {
            //We add the user tee value
            for (let index in players) {
                if (index === '0') {
                    players[index]['tees'] = gameDetails.tees
                }
            }
            this.setState({
                players: players,
                playerSelected: players[0].id,
                playerData: players[0]
            })
            const player = players[0];

            this.multipleScoreCards(player);

        } else {
            for (let index in players) {
                if (index === '0') {
                    players[index]['tees'] = gameDetails.tees,
                        players[index]['labelTees'] = gameDetails.labelTees
                }
            }
            const player = players;

            this.setState({
                players: players,
                playerSelected: players[0].id,
                playerData: players[0]
            })

            this.settingScoreCard();
        }
    }


    async configureScore(position, value) {
        let frontVal = 0;
        let backVal = 0;
        switch (position) {
            case 'front':
                frontVal = value
                this.setState({
                    frontValue: value,
                })
                break;
            case 'back':
                backVal = value
                this.setState({
                    backValue: value
                })
                break;
        }

        const frontValue = await this.state.frontValue === 0 ? parseInt(frontVal) : parseInt(this.state.frontValue);
        const backValue = await this.state.backValue === 0 ? parseInt(backVal) : parseInt(this.state.backValue);

        const total = frontValue + backValue;
        console.log(total);

        if (!isNaN(total)) {
            this.setState({
                total: total.toString()
            })
        }

    }

    render() {
        const { courseData, gameDetails, players } = this.props.route.params;
        return (
            <KeyboardAwareScrollView
                extraScrollHeight={verticalScale(40)}
                style={styles.container}
            >

                <Text style={styles.golfCourseTitle}>{courseData.course_name}</Text>
                <View style={styles.courseWrapper}>
                    <Text style={styles.golfCoursesubtitle}>Holes: {gameDetails.holes.length}</Text>
                    <View style={styles.courseDivider} />
                    <Text style={styles.golfCoursesubtitle}>Par: {courseData.par}</Text>
                </View>
                {/* <Text style={styles.golfCoursesubtitle}>Handicap: {courseData.par}</Text> */}

                <View style={styles.divider} />

                <View style={styles.totalContainer}>
                    <TextLabel additionalStyles={styles.title}>Score</TextLabel>

                    <View style={styles.scoreContainer}>
                        <View style={styles.scoreWrapper}>
                            <Text style={styles.scoreCardTitle}>Front 9</Text>
                            <View style={styles.nameBox}>
                                <TextInput
                                    style={styles.cardInput}
                                    keyboardType='number-pad'
                                    maxLength={2}
                                    onEndEditing={(e) => this.configureScore('front', e.nativeEvent.text)}
                                />
                            </View>
                        </View>

                        <View style={styles.scoreWrapper}>
                            <Text style={styles.scoreCardTitle}>Back 9</Text>
                            <View style={styles.nameBox}>
                                <TextInput
                                    style={styles.cardInput}
                                    keyboardType='number-pad'
                                    maxLength={2}
                                    onEndEditing={(e) => this.configureScore('back', e.nativeEvent.text)}
                                />
                            </View>
                        </View>

                        <View style={styles.scoreWrapper}>
                            <Text style={styles.scoreCardTitle}>Total Score</Text>
                            <View style={styles.nameBox}>
                                <TextInput
                                    style={styles.cardInput}
                                    keyboardType='number-pad'
                                    maxLength={2}
                                    defaultValue={this.state.total}
                                    value={this.state.total}
                                    editable={false}
                                />
                            </View>
                        </View>

                    </View>
                </View>

                <TouchableOpacity
                    style={styles.buttonPost}
                    onPress={() => this.props.navigation.popToTop()}
                >
                    <Text style={styles.buttonPostText}>Post Score</Text>
                </TouchableOpacity>

            </KeyboardAwareScrollView>
        );
    }
}

function mapStateToProps(state) {
    return {
        session: state.session,
        user: state.session.user.data,
        courses: state.courses
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators(courses, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchProps,
)(TotalScore);


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.white,
        padding: scale(10)
    },
    golfCourseTitle: {
        fontWeight: 'bold',
        fontSize: moderateScale(20),
        color: COLORS.softBlack,
        textAlign: 'center'
    },
    courseWrapper: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: "center"
    },
    golfCoursesubtitle: {
        fontWeight: 'bold',
        fontSize: moderateScale(16),
        color: COLORS.darkblue,
        marginTop: verticalScale(5)
    },
    courseDivider: {
        height: verticalScale(20),
        width: 1,
        backgroundColor: COLORS.darkblue,
        marginHorizontal: moderateScale(20),
        marginTop: verticalScale(5)
    },
    divider: {
        width: '100%',
        borderWidth: Platform.OS === 'ios' ? 1 : 0.3,
        borderColor: 'rgba(0,0,0,0.2)',
        marginVertical: verticalScale(15)
    },
    totalContainer: {
        width: '100%',
        marginTop: verticalScale(20),
        marginBottom: verticalScale(20)
    },
    scoreContainer: {
        flexDirection: 'row',
        marginTop: verticalScale(10),
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    nameBox: {
        height: 60,
        width: 100,
        borderWidth: 0.3,
        borderColor: COLORS.airBnbGrey,
        justifyContent: 'center',
        alignItems: 'center'
    },
    cardInput: {
        height: 45,
        width: 45,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        fontSize: moderateScale(14),
        color: 'black',
        fontWeight: 'bold'
    },
    scoreCardTitle: {
        fontSize: moderateScale(14),
        color: COLORS.airBnbGrey,
        marginBottom: verticalScale(10)
    },
    scoreWrapper: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonPost: {
        height: heightPercentageToDP('7%'),
        width: widthPercentageToDP('80%'),
        backgroundColor: COLORS.darkblue,
        borderRadius: 6,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: verticalScale(20),
        marginBottom: verticalScale(40)

    },
    buttonPostText: {
        color: COLORS.white,
        fontSize: moderateScale(14),
    },
})

const gameModality = [
    { label: "Individual", value: "1" },
    { label: "Scramble", value: "2" },
    { label: "Stroke Play", value: "3" },
    { label: "Best-Ball", value: "4" },
]

const holes = [
    { label: 'Hole 1', value: 'hole01' },
    { label: 'Hole 2', value: 'hole02' },
    { label: 'Hole 3', value: 'hole03' },
    { label: 'Hole 4', value: 'hole04' },
    { label: 'Hole 5', value: 'hole05' },
    { label: 'Hole 6', value: 'hole06' },
    { label: 'Hole 7', value: 'hole07' },
    { label: 'Hole 8', value: 'hole08' },
    { label: 'Hole 9', value: 'hole09' },
    { label: 'Hole 10', value: 'hole010' },
    { label: 'Hole 11', value: 'hole011' },
    { label: 'Hole 12', value: 'hole012' },
    { label: 'Hole 13', value: 'hole013' },
    { label: 'Hole 14', value: 'hole014' },
    { label: 'Hole 15', value: 'hole015' },
    { label: 'Hole 16', value: 'hole016' },
    { label: 'Hole 17', value: 'hole017' },
    { label: 'Hole 18', value: 'hole018' },
]