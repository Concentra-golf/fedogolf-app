import * as React from 'react';

// Modules

import { createStackNavigator } from '@react-navigation/stack';

// Views

import Courses from '../../Courses/Courses';
import CourseDetail from '../../Courses/CourseDetail';
import ListSeach from '../../Courses/ListSeach';

// Components

import MenuButton from '../../../components/UI/HeaderButtons/MenuButton/MenuButton';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';
import CustomBack from '../../../components/UI/ScoreCardsComponent/CustomBack';

// Options

import { commonHeaderOptions, settingsScreens } from './NavigatorOptions';

// Constants

import { COLORS } from '../../../config/constants';
import { TouchableOpacity, Text } from 'react-native';
import ScoreCard from '../../PlayGolf/ScoreCard';

// Courses Stack ===================================================================================================================

const Stack = createStackNavigator();

export function CoursesNavigator({ navigation, route }) {
  setI18nConfig();
  if (route.state && route.state.index > 0) {
    navigation.setOptions({ tabBarVisible: false, })
  } else {
    navigation.setOptions({ tabBarVisible: true })
  }
  return (
    <Stack.Navigator initialRouteName="Courses" screenOptions={{ ...commonHeaderOptions }}>
      <Stack.Screen
        name="Courses"
        component={Courses}
        options={({ navigation, route }) => ({
          headerLeft: () => (
            <MenuButton action={() => navigation.toggleDrawer()} />
          ),
          headerTintColor: COLORS.white,
          headerTitle: translate('coursesStack'),
          headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: 22,
          },
        })}
      />
      <Stack.Screen
        name="Search"
        component={ListSeach}
        // options={{ headerTransparent: false, headerTintColor: COLORS.darkgrey }}
        options={({ navigation, route }) => ({
          headerTransparent: false,
          headerTintColor: COLORS.darkgrey,
          headerShown: false
        })}
      />

      <Stack.Screen
        name="ScoreCard"
        component={ScoreCard}
        options={({ navigation, route }) => ({
          headerLeft: () => (
            <CustomBack action={() => navigation.goBack()} />
          ),
          gestureEnabled: false,
          headerShown: true,
          headerTransparent: false,
          headerTitle: 'Score Card',
          headerTintColor: COLORS.white,
          headerStyle: {
            backgroundColor: COLORS.darkblue,
            borderColor: COLORS.logoGreen,
          },
        })}
      />

      <Stack.Screen name="CourseDetail" component={CourseDetail} options={{ tabBarVisible: false, headerShown: false }} />

      {/* Settings Screens  */}

      {settingsScreens.map((route, i) => {
        return (
          <Stack.Screen key={i} name={route.name} component={route.screen} options={{ ...route.options }} />
        );
      })}


    </Stack.Navigator>
  );
}