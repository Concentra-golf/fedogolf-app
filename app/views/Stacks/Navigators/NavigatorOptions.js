// Constants

import { COLORS } from "../../../config/constants";

// Common Settings Screens

import Account from '../../Settings/Account';
import Events from '../../Events/Events';
import Notifications from '../../Settings/Notifications';
import UserSettings from '../../Settings/UserSettings/UserSettings';
import AccountOptions from '../../Settings/AccountOptions'
import PersonalInformation from '../../Settings/UsetDetails/PersonalInformation/PersonalInformation';
import PIEdit from '../../Settings/UsetDetails/PersonalInformation/PIEdit';
import GolferInformation from '../../Settings/UsetDetails/GolferInformation/GolferInformation';
import GIEdit from '../../Settings/UsetDetails/GolferInformation/GIEdit';
import GolferQrCode from '../../Settings/GolferQrCode';
import BenefitsList from '../../../views/Settings/Benefits/BenefitsList';
import MembershipOptions from '../../Settings/Membership/MembershipOptions';
import FederationMembershipList from '../../Settings/Membership/Federation/FederationMembershipList';
import FederationMembershipDetail from '../../Settings/Membership/Federation/FederationMembershipDetail';
import BenefitsByMembership from '../../Settings/Benefits/BenefitsByMembership';
import ManageRequest from '../../Settings/MembershipRequest/ManageRequest';
import FedoGolfSignUpForm from '../../../components/forms/FedoGolfSignUpForm/FedoGolfSignUpForm';
import ManageRequestDetails from '../../Settings/MembershipRequest/ManageRequestDetails';
import FormDetails from '../../Settings/MembershipRequest/FormDetails';
import CreditCardPayment from '../../Settings/Payment/CreditCardPayment';
import MembershipsInfo from '../../Settings/Membership/MembershipsInfo';
import MembershipBenefitsDetails from '../../Settings/Benefits/MembershipBenefitsDetails';
import CoursePrices from '../../Courses/CoursePrices';
import MembershipsPrices from '../../Courses/MembershipsPrices';
import AsociationMembershipList from '../../Settings/Membership/Asociation/AsociationMembershipList';
import LeagueMembershipList from '../../Settings/Membership/League/LeagueMembershipList';
import OptionsSelector from '../../Settings/Membership/Renew/OptionsSelector';
import FederationSignUp from '../../Settings/Membership/FederationSignUp';
import ReceiptList from '../../Settings/Receipt/ReceiptList';
import ReceiptDetails from '../../Settings/Receipt/ReceiptDetails';
import MembershipQR from '../../Settings/Membership/MembershipQR';
import CourseCommodities from '../../Courses/CourseCommodities';
import PlayGolf from '../../PlayGolf/PlayGolf';
import ScoreCard from '../../PlayGolf/ScoreCard';
import MyCards from '../../Settings/Payment/MyCards';
import ConfigurationScreen from '../../Settings/Membership/Configuration/ConfigurationScreen';
import SelectPlayMethod from '../../PlayGolf/SelectPlayMethod';
import TotalScore from '../../PlayGolf/TotalScore';
import MyRounds from '../../Courses/MyRounds';
import LanguageSelector from '../../Se../../Settings/Membership/Configuration/LanguageSelector'

export const settingsScreens = [
    { name: 'Account', screen: Account, options: {} },
    { name: 'Events', screen: Events, options: {} },
    { name: 'Notifications', screen: Notifications, options: {} },
    { name: 'UserSettings', screen: UserSettings, options: { headerShown: false, headerTitle: '' } },
    { name: 'AccountOptions', screen: AccountOptions, options: { headerTitle: '', headerTintColor: COLORS.white } },
    { name: 'PersonalInformation', screen: PersonalInformation, options: { headerTitle: '', headerTintColor: COLORS.white } },
    { name: 'PIEdit', screen: PIEdit, options: { headerTitle: '', headerTintColor: COLORS.white } },
    { name: 'GolferInformation', screen: GolferInformation, options: { headerTitle: '', headerTintColor: COLORS.white } },
    { name: 'GIEdit', screen: GIEdit, options: { headerTitle: '', headerTintColor: COLORS.white } },
    { name: 'GolferQrCode', screen: GolferQrCode, options: { headerTitle: '', headerTintColor: COLORS.white } },
    { name: 'BenefitsList', screen: BenefitsList, options: { headerTitle: '', headerTintColor: 'black', } },
    { name: 'BenefitsByMembership', screen: BenefitsByMembership, options: { headerTitle: '', headerTintColor: 'black', } },
    { name: 'MembershipOptions', screen: MembershipOptions, options: { headerTitle: '', headerTintColor: COLORS.white } },

    { name: 'FederationMembershipList', screen: FederationMembershipList, options: { headerShown: false } },
    { name: 'FederationMembershipDetail', screen: FederationMembershipDetail, options: { headerShown: false } },
    { name: 'ManageRequest', screen: ManageRequest, options: { headerShown: false } },
    { name: 'FedoGolftSignUp', screen: FedoGolfSignUpForm, options: { headerTitle: '', headerTintColor: COLORS.black, headerBackTitleVisible: true, headerTintColor: COLORS.white } },
    { name: 'ManageRequestDetails', screen: ManageRequestDetails, options: { headerTitle: '', headerTintColor: COLORS.black, headerBackTitleVisible: false, headerTintColor: COLORS.black } },
    { name: 'FormDetails', screen: FormDetails, options: { headerTitle: '', headerTintColor: COLORS.black, headerBackTitleVisible: true, headerTintColor: COLORS.white } },
    { name: 'CreditCardPayment', screen: CreditCardPayment, options: { headerTitle: '', headerTintColor: COLORS.black, headerBackTitleVisible: false, headerTintColor: COLORS.airBnbGrey, } },
    { name: 'MembershipsInfo', screen: MembershipsInfo, options: { headerShown: true, headerTransparent: true, headerTitle: '' } },
    { name: 'OptionsSelector', screen: OptionsSelector, options: { headerShown: true, headerTransparent: true, headerTitle: '' } },
    { name: 'MembershipsBenefitsDetails', screen: MembershipBenefitsDetails, options: { headerTitle: '', headerBackTitleVisible: false, headerTintColor: COLORS.black, } },
    { name: 'CoursePrices', screen: CoursePrices, options: { headerTitle: '', headerTransparent: false, } },
    { name: 'CourseCommodities', screen: CourseCommodities, options: { headerTitle: '', headerTransparent: false, } },
    { name: 'MembershipsPrices', screen: MembershipsPrices, options: { headerTitle: '', headerTransparent: false, } },

    { name: 'AsociationMembershipList', screen: AsociationMembershipList, options: { headerTitle: '', headerTintColor: COLORS.white } },
    { name: 'LeagueMembershipList', screen: LeagueMembershipList, options: { headerTitle: '', headerTintColor: COLORS.white } },
    { name: 'FederationSignUp', screen: FederationSignUp, options: { headerShown: true, headerTransparent: false, headerTitle: '', headerTintColor: COLORS.black } },
    { name: 'ReceiptList', screen: ReceiptList, options: { headerShown: false } },
    { name: 'MyCards', screen: MyCards, options: { headerShown: false } },
    { name: 'ReceiptDetails', screen: ReceiptDetails, options: { headerShown: true, headerTransparent: false, headerTitle: 'Receipt Summary', headerTintColor: COLORS.white, headerStyle: { backgroundColor: COLORS.darkblue, borderColor: COLORS.green, } } },
    { name: 'MembershipQR', screen: MembershipQR, options: { headerShown: true, headerTitle: '', headerTintColor: COLORS.white } },
    { name: 'PlayGolf', screen: PlayGolf, options: { headerShown: true, headerTransparent: false, headerTitle: 'Round Setup', headerTintColor: COLORS.white, headerStyle: { backgroundColor: COLORS.darkblue, borderColor: COLORS.logoGreen, } } },
    { name: 'TotalScore', screen: TotalScore, options: { headerShown: true, headerTransparent: false, headerTitle: 'Total Score', headerTintColor: COLORS.white, headerStyle: { backgroundColor: COLORS.darkblue, borderColor: COLORS.logoGreen, } } },
    { name: 'Configuration', screen: ConfigurationScreen, options: { headerTitle: '', headerTintColor: COLORS.white, } },
    { name: 'MyRounds', screen: MyRounds, options: { headerTitle: 'My Rounds', headerTransparent: false, headerTintColor: COLORS.black, } },
    { name: 'SelectPlayMethod', screen: SelectPlayMethod, options: { headerShown: true, headerTransparent: false, headerTitle: 'Round Setup', headerTintColor: COLORS.white, headerStyle: { backgroundColor: COLORS.darkblue, borderColor: COLORS.logoGreen, } } },
    { name: 'LanguageSelector', screen: LanguageSelector, options: { headerShown: true, headerTransparent: false, headerTitle: 'Language', headerTintColor: COLORS.white, headerStyle: { backgroundColor: COLORS.darkblue, borderColor: COLORS.logoGreen, } } },


    /// Screens whit more settings 
    // {
    //     name: 'ScoreCard', screen: ScoreCard,
    //     options: {
    //         gestureEnabled: false,
    //         headerShown: true,
    //         headerTransparent: false,
    //         headerTitle: 'Score Card',
    //         headerTintColor: COLORS.white,
    //         headerStyle: {
    //             backgroundColor: COLORS.darkblue,
    //             borderColor: COLORS.logoGreen,
    //         },
    //     }
    // },
]
// Common Header Options

export const commonHeaderOptions = {
  headerTransparent: true,
  headerTintColor: COLORS.darkgrey,
  headerBackTitleVisible: false
  // headerShown: false
};

export const defaulOptions = {
  headerTransparent: false,
  headerTintColor: COLORS.darkgrey
};

export const commonWhiteHeaderOptions = {
  headertintColor: COLORS.white
};
