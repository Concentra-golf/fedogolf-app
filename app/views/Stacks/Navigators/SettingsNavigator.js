import * as React from 'react';

// Modules

import { createStackNavigator } from '@react-navigation/stack';
import ListSeach from '../../Courses/ListSeach';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';

// Options

import { commonHeaderOptions, settingsScreens } from './NavigatorOptions';
import { COLORS } from '../../../config/constants';

// Settings Stack ===================================================================================================================

const Stack = createStackNavigator();

export function SettingsNavigator() {
  setI18nConfig(); 
  return (
    <Stack.Navigator initialRouteName="UserSettings" screenOptions={{ ...commonHeaderOptions }}>

      <Stack.Screen name="Search" component={ListSeach} options={{ headerTransparent: false, headerTintColor: COLORS.darkgrey }} />

      {/* Settings Screens */}

      {settingsScreens.map((route, i) => {
        return (
          <Stack.Screen key={i} name={route.name} component={route.screen} options={{ ...route.options }} />
        );
      })}
    </Stack.Navigator>
  );
}

