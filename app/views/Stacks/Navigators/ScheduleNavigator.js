import * as React from 'react';

// Modules

import { createStackNavigator } from '@react-navigation/stack';

// Views

import Schedule from '../../Schedule/Schedule';
import CourseDetail from '../../Courses/CourseDetail';
import ListSeach from '../../Courses/ListSeach';

// Components

import MenuButton from '../../../components/UI/HeaderButtons/MenuButton/MenuButton';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';

// Options

import { commonHeaderOptions, settingsScreens } from './NavigatorOptions';

// Constants

import { COLORS } from '../../../config/constants';

// Schedules Stack ===================================================================================================================

const Stack = createStackNavigator();

export function ScheduleNavigator() {
  setI18nConfig(); 

  return (
    <Stack.Navigator initialRouteName="Schedule" screenOptions={{ ...commonHeaderOptions }}>
      <Stack.Screen
        name={translate('scheduleStack')}
        component={Schedule}
        options={({ navigation, route }) => ({
          headerLeft: () => (
            <MenuButton action={() => navigation.toggleDrawer()} />
          ),
          headerTintColor: COLORS.white,
          headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: 22,
          },
        })}
      />
      <Stack.Screen name="Search" component={ListSeach} options={{ headerTransparent: false, headerTintColor: COLORS.darkgrey}}/>
      <Stack.Screen name="CourseDetail" component={CourseDetail} options={{
        headerTintColor: COLORS.white,
        headerTitle: '',
        headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 22,
        },
      }} />


      {/* Settings Screens  */}

      {settingsScreens.map((route, i) => {
        return (
          <Stack.Screen key={i} name={route.name} component={route.screen} options={{ ...route.options }}/>
        );
      })}
    </Stack.Navigator>
  );
}