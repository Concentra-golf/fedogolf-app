import * as React from 'react';

// Modules

import { createStackNavigator } from '@react-navigation/stack';

// Views

import Play from '../../Play/Play';

// Options

import { commonHeaderOptions, settingsScreens } from './NavigatorOptions';
import SmallHeader from '../../../components/UI/SmallHeader/SmallHeader';
import MenuButton from '../../../components/UI/HeaderButtons/MenuButton/MenuButton';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';


// Constants
import { COLORS } from '../../../config/constants';
import { Dimensions, View, ImageBackground } from 'react-native';
import AsociationMembershipList from '../../Settings/Membership/Asociation/AsociationMembershipList';
import FederationMembershipList from '../../Settings/Membership/Federation/FederationMembershipList';
import ScoreCard from '../../PlayGolf/ScoreCard';
import CustomBack from '../../../components/UI/ScoreCardsComponent/CustomBack';

// Play Stack ===================================================================================================================

const Stack = createStackNavigator();

export function PlayNavigator({ navigation, route }) {
  setI18nConfig();
  if (route.state && route.state.index > 0) {
    navigation.setOptions({ tabBarVisible: false, })
  } else {
    navigation.setOptions({ tabBarVisible: true })
  }
  return (
    <Stack.Navigator initialRouteName="Play">
      <Stack.Screen
        name="Play"
        component={Play}
        options={({ navigation, route }) => ({
          headerTintColor: COLORS.white,
          headerTitle: translate('playStack'),
          headerLeft: () => <MenuButton action={() => navigation.toggleDrawer()} />,
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerStyle: { backgroundColor: COLORS.darkblue }
        })}
      />

      <Stack.Screen
        name="ScoreCard"
        component={ScoreCard}
        options={({ navigation, route }) => ({
          headerLeft: () => (
            <CustomBack action={() => navigation.goBack()} />
          ),
          gestureEnabled: false,
          headerShown: true,
          headerTransparent: false,
          headerTitle: 'Score Card',
          headerTintColor: COLORS.white,
          headerStyle: {
            backgroundColor: COLORS.darkblue,
            borderColor: COLORS.logoGreen,
          },
        })}
      />


      {/* Settings Screens  */}
      {settingsScreens.map((route, i) => {
        return (
          <Stack.Screen key={i} name={route.name} component={route.screen} options={{ ...route.options }} />
        );
      })}

    </Stack.Navigator>
  );
}
