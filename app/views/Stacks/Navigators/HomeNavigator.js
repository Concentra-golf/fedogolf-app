import * as React from 'react';
import { TouchableOpacity, View, Text } from 'react-native';
// Modules

import { createStackNavigator } from '@react-navigation/stack';

// Views

import Home from '../../Home/Home';

// Components

import MenuButton from '../../../components/UI/HeaderButtons/MenuButton/MenuButton';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';

// Options

import { commonHeaderOptions, settingsScreens } from './NavigatorOptions';

// Constants

import { COLORS } from '../../../config/constants';
import FederationQr from '../../../components/UI/HeaderButtons/FederationQr/FederationQr';

// Home Stack ===================================================================================================================

const Stack = createStackNavigator();

export function HomeNavigator() {
  setI18nConfig(); 
  return (
    <Stack.Navigator initialRouteName="Home" screenOptions={{ ...commonHeaderOptions }}>
      <Stack.Screen
        name="Home"
        component={Home}
        options={({ navigation, route }) => ({
          headerLeft: () => (
            <MenuButton action={() => navigation.toggleDrawer()} />
          ),
          headerRight: () => (
            <FederationQr navigation={navigation} />
          ),
          headerTintColor: COLORS.white,
          headerTitle: 'GolferTek',
          headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: 22,
          },
          headerTitleAlign: 'center',
          headerStyle: {
            elevation: 0,
          },
        })}
      />

      {/* Settings Screens  */}

      {settingsScreens.map((route, i) => {
        return (
          <Stack.Screen key={i} name={route.name} component={route.screen} options={{ ...route.options }} />
        );
      })}
    </Stack.Navigator>
  );
}