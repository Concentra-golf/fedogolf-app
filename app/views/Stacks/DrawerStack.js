import * as React from 'react';
import { View, Platform } from 'react-native';

// Modules
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as session from '../../store/actions/session';

import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList, DrawerItem } from '@react-navigation/drawer';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import {
  faUsers,
  faUserAlt,
  faChartBar,
  faCreditCard,
  faBell,
  faCogs,
  faGolfBall,
  faQrcode,
  faHandHoldingUsd,
} from '@fortawesome/free-solid-svg-icons';
// Stacks

import { TabsStack } from './TabsStack';

// Components

import DrawerUserCard from '../../components/cards/DrawerUserCard/DrawerUserCard';
import * as RNLocalize from 'react-native-localize';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../translations/translation';

// Styles

import styles from './Styles';

// Constants

import { COLORS } from '../../config/constants';
import DrawerCustomButtons from '../../components/cards/DrawerCustomButtons/DrawerCustomButtons';

// Drawer Stack ===================================================================================================================

const Drawer = createDrawerNavigator();

function DrawerContent(props) {
  setI18nConfig(); 
  var items =
    [
      {
        label: translate('request'),
        screen: 'ManageRequest',
        icon: faUsers
      },
      // {
      //   label: 'Memberships',
      //   screen: 'MembershipOptions',
      //   icon: faGolfBall,
      // },
      // {
      //   label: 'My Qr',
      //   screen: 'GolferQrCode',
      //   icon: faQrcode,
      // },
      // {
      //   label: 'LogOut',
      //   action: 'LogOut',
      //   icon: faSignOutAlt,
      // },
    ];

  return (
    <DrawerContentScrollView {...props}>

      <DrawerUserCard />

      {/* <DrawerItemList {...props} /> */}

      <View style={styles.drawerItemsContainer}>
        {items.map((item, i) => {
          return (
            <NaVItem
              label={item.label}
              icon={item.icon}
              action={() => item.screen ? props.navigation.navigate(item.screen, { route: 'drawer' }) : item.action}
              key={i}
              {...props}
            />
          )
        })}

        <DrawerCustomButtons 
          navigation={props.navigation}
        />

      </View>

    </DrawerContentScrollView>
  );
}


const NaVItem = ({ label, icon, light, action }) => (
  <DrawerItem
    label={label}
    labelStyle={styles.drawerItemLabel}
    style={styles.drawerItemWrapper}
    onPress={action}
    icon={() => <FontAwesomeIcon size={14} color={light ? COLORS.lightgrey : COLORS.black} icon={icon} />}
  />
);



export function DrawerStack() {
  return (
    <Drawer.Navigator
      initialRouteName="Tabs"
      drawerContent={props => DrawerContent(props)}
      drawerContentOptions={{
        contentContainerStyle: { paddingTop: 0 }
      }}>
      <Drawer.Screen name="Tabs" component={TabsStack} />
    </Drawer.Navigator>
  );
}


function mapStateToProps(state) {
  return {
    ...state.membership,
    session: state.session,
    user: state.session.user.data,
  };
}

function mapDispatchProps(dispatch) {
  return bindActionCreators({ session }, dispatch);
}


export default connect(
  mapStateToProps,
  mapDispatchProps
)(DrawerStack);