import * as React from 'react';
import { SafeAreaView } from 'react-native';

// Modules
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

// Navigators
import { HomeNavigator } from './Navigators/HomeNavigator';
import { PlayNavigator } from './Navigators/PlayNavigator';
import { CoursesNavigator } from './Navigators/CoursesNavigator';
import { ScheduleNavigator } from './Navigators/ScheduleNavigator';
import { SettingsNavigator } from './Navigators/SettingsNavigator';

// Components

import TabNavIcon from '../../components/UI/TabNavIcon/TabNavIcon';
import { heightPercentageToDP } from '../../utilities/ScalingScreen';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../translations/translation';

// Tabs Stack ===================================================================================================================

const Tab = createBottomTabNavigator();

export function TabsStack() {
  setI18nConfig();
  return (
    <Tab.Navigator
      initialRouteName="Home"
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused }) => {
          let routeName = '';

          if (route.name === 'Inicio' || route.name === 'Home') {
            routeName = 'Home'
          } else if (route.name === 'Campos' || route.name === 'Courses') {
            routeName = 'Courses'
          } else if (route.name === 'Agenda' || route.name === 'Schedule') {
            routeName = 'Schedule'
          } else if (route.name === 'Configuración' || route.name === 'Settings') {
            routeName = 'Settings'
          } else if (route.name === 'Jugar' || route.name === 'Play') {
            routeName = 'Play'
          }

          return <TabNavIcon name={routeName.toLowerCase()} large={focused} route={route} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: 'white',
        inactiveTintColor: '#CCCCCC',
        tabStyle: {
          backgroundColor: 'black',
        },
        labelStyle: {
          textTransform: 'uppercase',
        },
      }}>
      <Tab.Screen name={translate('homeStack')} component={HomeNavigator} />
      <Tab.Screen name={translate('coursesStack')} component={CoursesNavigator} />
      <Tab.Screen name={translate('playStack')} component={PlayNavigator} />
      <Tab.Screen name={translate('settingStack')} component={SettingsNavigator} />
      {/* <Tab.Screen name="Schedule" component={ScheduleNavigator} /> */}

    </Tab.Navigator>
  );
}