import { StyleSheet } from "react-native";

import { COLORS } from "../../config/constants";

export default StyleSheet.create({
    drawerItemsContainer: {
        marginTop: 15,
    },
    drawerItemWrapper: {
        borderBottomColor: '#E8E8E8',
        borderBottomWidth: 1
    },
    drawerItemLabel: {
        fontFamily: 'Lato-Bold',
        // textTransform: 'uppercase',
    },
});
