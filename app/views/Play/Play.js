import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, ScrollView, Dimensions, RefreshControl, ActivityIndicator } from 'react-native';

import { moderateScale, scale, verticalScale, heightPercentageToDP } from '../../utilities/ScalingScreen';
import OpenGame from '../../components/cards/PlayGolf/OpenGame';
import { COLORS } from '../../config/constants';

import Carousel, { Pagination } from 'react-native-snap-carousel';

// Modules
import * as RNLocalize from 'react-native-localize';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as playgolf from '../../store/actions/playgolf';
import Loader from '../../components/UI/Loader/Loader';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../translations/translation';


const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.7);


class Play extends Component {

  constructor(props) {
    super(props)
    setI18nConfig();
    this.state = {
      loading: true,
      openList: [],
      index: 0,
      refreshing: false
    }
    this.continueGame = this.continueGame.bind(this);
    //We Re-render the screen 
    props.navigation.addListener('focus', e => {
      this.getOpenGames();
      console.log('updating', e)
    });
  }

  componentDidMount() {
    RNLocalize.addEventListener('change', handleLocalizationChange);
    this.getOpenGames();
  }

  async getOpenGames() {
    await this.props.getOpenPlays(this.props.session.accessToken);

    if (this.props.playgolf.getOpenPlays.success) {
      this.setState({
        loading: false,
        openList: this.props.playgolf.getOpenPlays.data.games.data
      })
    } else {
      this.setState({
        loading: false,
        openList: []
      })
    }
  }

  async continueGame(e) {
    console.log(e);

    await this.props.continueGame(e.id, this.props.session.accessToken);

    if (this.props.playgolf.continueGame.success) {
      this.props.navigation.navigate('ScoreCard', {
        data: this.props.playgolf.continueGame.data,
        route: 'PlayTab',
      })
    } else {
      console.log('error')
    }

  }

  setPagination() {
    const { index } = this.state;
    return (
      <Pagination
        dotsLength={this.state.openList.length}
        activeDotIndex={index}
        containerStyle={{ backgroundColor: 'white' }}
        dotStyle={{
          width: 15,
          height: 15,
          borderRadius: 15 / 2,
          backgroundColor: COLORS.darkblue,
        }}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
    );
  }

  onRefresh = () => {
    this.setState({ refreshing: true });
    this.getOpenGames().then(() => {
      this.setState({ refreshing: false });
    });
  }

  render() {
    return (
      <View
        style={styles.container}>

        <View style={styles.content}>
          <Text style={styles.title}>{translate('openGames')}</Text>
          {!this.state.loading && this.state.openList.length > 0 &&
            <ScrollView
              // contentContainerStyle={{ height: '100%' }}
              showsVerticalScrollIndicator={false}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this.onRefresh}
                />
              }
            >
              <Carousel
                ref={(c) => this.carousel = c}
                data={this.state.openList}
                sliderWidth={SLIDER_WIDTH}
                itemWidth={ITEM_WIDTH}
                containerCustomStyle={styles.carouselContainer}
                inactiveSlideShift={0}
                onSnapToItem={(index) => this.setState({ index })}
                useScrollView={true}
                renderItem={({ item, index }) =>
                  <OpenGame
                    index={index}
                    data={item}
                    onPress={(e) => this.continueGame(item)}
                  />
                }
              />
              {this.setPagination()}
            </ScrollView>
          }
        </View>

        {!this.state.loading && this.state.openList.length <= 0 &&
          <Loader
            nothing={true}
            nothingMessage={'No open games available'}
          />
        }

        {this.state.loading &&
          <ActivityIndicator size='large' color={COLORS.darkblue} />
        }


        <TouchableOpacity
          style={styles.newGameButton}
          onPress={() => this.props.navigation.navigate('SelectPlayMethod', {
            route: 'Play'
          })}
        >
          <Text style={styles.buttonTitle}>{translate('newGame')}</Text>
        </TouchableOpacity>

      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
    user: state.session.user.data,
    playgolf: state.playgolf
  };
}

function mapDispatchProps(dispatch) {
  return bindActionCreators(playgolf, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchProps,
)(Play);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  content: {
    padding: scale(10),
  },
  title: {
    fontSize: moderateScale(20),
    fontWeight: 'bold',
    marginTop: verticalScale(10),
    color: COLORS.softBlack
  },
  listContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  carouselContainer: {
    marginTop: verticalScale(25),
    paddingVertical: verticalScale(20),

  },
  list: {
    height: heightPercentageToDP('30%'),
    alignItems: 'center',
    justifyContent: 'center',
    flexGrow: 1,
  },
  newGameButton: {
    backgroundColor: COLORS.darkblue,
    height: heightPercentageToDP('10%'),
    width: '100%',
    position: 'absolute',
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  buttonTitle: {
    fontSize: moderateScale(16),
    color: COLORS.white,
    fontWeight: 'bold'
  }
})

const openGame = [
  {
    key: 1,
    gameName: 'Game 1',
    date: "09/02/2020",
    players: "2",
    golfCourse: 'Santo Domingo Country Club',
    modality: 'Hole by Hole',
  },
  {
    key: 2,
    gameName: 'Game 2',
    date: "09/02/2020",
    players: "2",
    golfCourse: 'Bonao',
    modality: 'Hole by Hole',
  }
]