import { StyleSheet } from 'react-native';
import { verticalScale, scale } from '../../utilities/ScalingScreen';

export default StyleSheet.create({
    infoScreenContainer: {
        flex: 1,
        padding: scale(15),
    },
    mapStyles: {
        // paddingBottom: verticalScale(100)
    },
    mapsContainer: {
        height: 200,
        paddingBottom: verticalScale(250),
    },
    coursesPricesContainer: {
        flex: 1,
        backgroundColor: 'white'
    },
    coursesPriceMemebershipContainer: {
        flex: 1, 
        backgroundColor: 'white' 
    },
    tabContainer: {
        flex: 1,
        backgroundColor: 'white'
    }
});
