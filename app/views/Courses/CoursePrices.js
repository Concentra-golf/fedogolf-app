import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import TextLabel from '../../components/UI/TextLabel';
import { scale, verticalScale } from '../../utilities/ScalingScreen';
import { COLORS } from '../../config/constants';
import CoursePricesList from '../../components/lists/CoursePrices/CoursePrices';

class CoursePrices extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { data } = this.props.route.params
    return (
      <View style={styles.container}>

        <TextLabel
          additionalStyles={{ marginTop: verticalScale(20), }}
          size={30}
          color={COLORS.airBnbGrey}>
          Prices
            </TextLabel>

        <CoursePricesList
          data={data}
        />

      </View>
    );
  }
}

export default CoursePrices;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: scale(10)
  }
})