import React, { Component } from 'react';
import { View, Text, ScrollView, StyleSheet, TouchableOpacity, Platform, ActivityIndicator } from 'react-native';

// Modules
import HeaderImageScrollView, { TriggeringView } from 'react-native-image-header-scroll-view';
import Ionicons from 'react-native-vector-icons/Ionicons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import Entypo from 'react-native-vector-icons/Entypo'
import StarRating from 'react-native-star-rating';
import * as RNLocalize from 'react-native-localize';
import { DotIndicator } from 'react-native-indicators';

// Components
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../translations/translation';
import TextLabel from '../../components/UI/TextLabel';
import Moment from 'moment';
import FixedForeground from '../../components/UI/HeaderButtons/ImageHeader/FixedForeGround';

// Constants

import { COLORS } from '../../config/constants';

// Google Maps
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { scale, verticalScale, moderateScale, heightPercentageToDP, widthPercentageToDP } from '../../utilities/ScalingScreen';
import CourseInfoItem from '../../components/cards/CourseInfo/CourseInfoItem';

//Redux

import * as session from '../../store/actions/session';
import * as courses from '../../store/actions/courses';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class Courses extends Component {
  constructor(props) {
    super(props);
    setI18nConfig();
    this.state = {
      loading: true,
      error: false,
      ratings: [],
      courseData: {},
      latitude: '',
      longitude: '',
      ratings: [],
      loadRatingList: true
    };
  }

  async componentDidMount() {
    RNLocalize.addEventListener('change', handleLocalizationChange);
    const courseData = this.props.route.params.courseData;
    await this.props.getCourseById(courseData.course_id, this.props.session.accessToken);
    if (this.props.course.coursesDetails.success) {
      console.log(this.props.course.coursesDetails.data);
      this.setState({
        courseData: this.props.course.coursesDetails.data,
        loading: false,
        latitude: parseFloat(this.props.course.coursesDetails.data.golf_club.latitude),
        longitude: parseFloat(this.props.course.coursesDetails.data.golf_club.longitude)
      })
      this.getRating();
    } else {
      this.setState({
        courseData: [],
        loading: false,
        error: true
      })
    }
  }

  async getRating() {
    await this.props.getCoursesRatings(this.state.courseData.id, this.props.session.accessToken);
    if (this.props.course.coursesRating.success) {
      this.setState({
        rating: this.props.course.coursesRating.data,
        loadRatingList: false
      })
      console.log(this.state.rating)
    } else {
      console.log('fallo')
    }
  }

  render() {
    const { courseData, rating } = this.state;
    return (
      <View style={{ flex: 1 }}>
        {!this.state.loading && !this.state.error &&
          <HeaderImageScrollView
            maxHeight={MAX_HEIGHT}
            minHeight={MIN_HEIGHT}
            maxOverlayOpacity={1}
            minOverlayOpacity={0}
            useNativeDriver={true}
            headerImage={require('../../assets/images/scroll-header-bg.png')}
            overlayColor={'white'}
            renderFixedForeground={() => <FixedForeground title={''} navigation={this.props.navigation} />}
          >
            <View style={styles.container}>
              <TriggeringView>
                <TextLabel size={30} additionalStyles={styles.title}>{translate('courseTitle')} {courseData.course_name}</TextLabel>
                <TextLabel additionalStyles={styles.subtitle} size={15}>{courseData.golf_club.address}</TextLabel>
                <View style={styles.horizontalLineShort} />

                <View style={styles.subMenuContainer}>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('MyRounds')}
                    style={styles.subMenu}>
                    <FontAwesome
                      name='history'
                      size={32}
                      color={COLORS.darkblue}
                    />
                    <Text style={styles.subMenuTitle}>{translate('rounds')} </Text>
                  </TouchableOpacity>
                  <View style={styles.verticalLine} />
                  <TouchableOpacity style={styles.subMenu}>
                    <Entypo
                      name='bar-graph'
                      size={32}
                      color={COLORS.darkblue}
                    />
                    <Text style={styles.subMenuTitle}>{translate('stadistics')} </Text>
                  </TouchableOpacity>
                </View>

                {/* <TextLabel additionalStyles={{ marginTop: verticalScale(10), }} size={25} color={COLORS.airBnbGrey}>Contact Name</TextLabel>
            <TextLabel additionalStyles={{ marginTop: verticalScale(5), }} size={16} color={COLORS.airBnbLightGrey}>{courseData.golf_club.contact_name}</TextLabel> */}

                <View style={styles.horizontalLine} />

                <View style={styles.aditionaInfoContainer}>
                  <TextLabel additionalStyles={{ marginTop: verticalScale(20), }} size={25} color={COLORS.airBnbGrey}>{translate('aditionalInfo')} </TextLabel>
                  <View style={styles.aditionaInfoWrapper}>
                    {courseData.golf_club.phone &&
                      <CourseInfoItem icon={"phone"} title={'Phone'} info={courseData.golf_club.phone} additionalStyles={styles.itemContainer} />
                    }

                    {courseData.golf_club.email &&
                      <CourseInfoItem icon={"email"} info={courseData.golf_club.email} additionalStyles={styles.itemContainer} link={true} />
                    }
                    <CourseInfoItem icon={"clock"} info={`${courseData.open || '6:00 AM'} - ${courseData.closed || '8:00 PM'}`} additionalStyles={styles.itemContainer} />
                    <CourseInfoItem icon={"sun"} info={`${courseData.temperature || '26'}°C`} additionalStyles={styles.itemContainer} />
                    {courseData.holes &&
                      <CourseInfoItem icon={"golfball"} info={`${courseData.holes} holes`} additionalStyles={styles.itemContainer} />
                    }
                    {courseData.par &&
                      <CourseInfoItem icon={"expand"} info={` Par: ${courseData.par}`} additionalStyles={styles.itemContainer} />
                    }
                  </View>
                </View>

                <View style={styles.horizontalLine} />

                {/* Commodities */}
                <TouchableOpacity
                  style={styles.pricesContainerButtons}
                  onPress={() => this.props.navigation.navigate('CourseCommodities', {
                    data: courseData
                  })}
                >
                  <View style={styles.pricesWrapper}>
                    <TextLabel additionalStyles={{ marginTop: verticalScale(20), }} size={25} color={COLORS.airBnbGrey}>{translate('commodities')}</TextLabel>
                    <TextLabel additionalStyles={{ marginTop: verticalScale(10), }} size={14} color={COLORS.airBnbLightGrey}>Driving Range,Putting Green and more</TextLabel>
                  </View>
                  <FontAwesome5
                    name='arrow-right'
                    size={28}
                    style={{ marginTop: verticalScale(25) }}
                  />
                </TouchableOpacity>

                <View style={styles.horizontalLine} />

                {/* Courses Prices */}
                <View style={styles.pricesContainer}>
                  <TouchableOpacity
                    style={styles.pricesContainerButtons}
                    onPress={() => this.props.navigation.navigate('CoursePrices', {
                      data: courseData
                    })}
                  >
                    <View style={styles.pricesWrapper}>
                      <TextLabel additionalStyles={{ marginTop: verticalScale(20), }} size={25} color={COLORS.airBnbGrey}>{translate('prices')}</TextLabel>
                      <TextLabel additionalStyles={{ marginTop: verticalScale(10), }} size={14} color={COLORS.airBnbLightGrey}>{translate('priceDetails')}</TextLabel>
                    </View>
                    <FontAwesome5
                      name='arrow-right'
                      size={28}
                      style={{ marginTop: verticalScale(25) }}
                    />
                  </TouchableOpacity>

                  <View style={styles.horizontalLine} />

                  {/* Membership Prices */}
                  <TouchableOpacity
                    style={styles.pricesContainerButtons}
                    onPress={() => this.props.navigation.navigate('MembershipsPrices', {
                      data: courseData
                    })}
                  >
                    <View style={styles.pricesWrapper}>
                      <TextLabel additionalStyles={{ marginTop: verticalScale(20), }} size={25} color={COLORS.airBnbGrey}>{translate('membershipPrice')}</TextLabel>
                      <TextLabel additionalStyles={{ marginTop: verticalScale(10), }} size={14} color={COLORS.airBnbLightGrey}>{translate('membershipDetails')}</TextLabel>
                    </View>

                    <FontAwesome5
                      name='arrow-right'
                      size={28}
                      style={{ marginTop: verticalScale(25) }}
                    />

                  </TouchableOpacity>
                </View>
                <View style={styles.horizontalLine} />

                {/* Map Component */}
                <View style={styles.locationContainer}>
                  <TextLabel additionalStyles={{ marginTop: verticalScale(10), }} size={25} color={COLORS.airBnbGrey}>{translate('location')}</TextLabel>
                  {courseData &&
                    <MapView
                      provider={PROVIDER_GOOGLE}
                      style={{ padding: '35%', marginTop: verticalScale(20), borderRadius: scale(10) }}
                      initialRegion={{
                        latitude: this.state.latitude,
                        longitude: this.state.longitude,
                        latitudeDelta: 0.0050,
                        longitudeDelta: 0.0002,
                      }}
                    >
                      <Marker
                        coordinate={{ latitude: this.state.latitude, longitude: this.state.longitude }}
                        title={courseData.course_name}
                        description={"Golf Course"}
                      />
                    </MapView>
                  }

                </View>

                <View style={styles.horizontalLine} />

                {/* COURSE RATINGS =================================================== */}

                <View style={styles.ratingContainer}>
                  <TextLabel size={25} color={COLORS.airBnbGrey}>{translate('courseScore')}</TextLabel>
                  <View style={styles.wrapper}>
                    <StarRating
                      disabled={false}
                      maxStars={5}
                      rating={courseData.rating_total}
                      fullStarColor={COLORS.green}
                      starSize={25}
                      starStyle={styles.starStyle}
                      containerStyle={styles.starContainer}
                    />
                    <Text style={styles.ratingListText}>{courseData.rating_total} / 5</Text>
                  </View>

                  {!this.state.loadRatingList && rating.slice(0, 3).map((item, index) =>
                    <View key={index} style={styles.scoreList}>
                      <Text style={styles.scoreName}>{item.user.firstname} {item.user.lastname}</Text>
                      <View style={styles.wrapper}>
                        <StarRating
                          disabled={false}
                          maxStars={5}
                          rating={item.rating}
                          fullStarColor={COLORS.green}
                          starSize={15}
                          starStyle={styles.starStyle}
                          containerStyle={styles.starListContainer}
                        />
                        <Text style={styles.ratingListText}>{item.rating} / 5</Text>
                      </View>
                      {item.comment !== 'NA' &&
                        <Text style={styles.scoreDescrip}>{item.comment}</Text>
                      }
                    </View>
                  )}

                  <View style={styles.buttonContainer}>
                    {!this.state.loadRatingList && rating.length > 3 &&
                      <TouchableOpacity
                        style={styles.reviewButton}
                      >
                        <Text style={styles.reviewButtonText}>Show all reviews</Text>
                      </TouchableOpacity>
                    }
                  </View>

                </View>

              </TriggeringView>
            </View>

          </HeaderImageScrollView>
        }

        {!this.state.loading &&
          <View style={styles.playGolfContainer}>
            {/* <Text style={styles.price}>${courseData.club_price} per game</Text> */}
            <TouchableOpacity
              style={styles.playButton}
              onPress={() => this.props.navigation.navigate('SelectPlayMethod', {
                courseData: courseData
              })}
            >
              <TextLabel additionalStyles={styles.buttonText}>{translate('playGolf')}</TextLabel>
            </TouchableOpacity>
          </View>
        }

        {this.state.loading &&
          <DotIndicator color={COLORS.darkblue} />
        }

      </View>
    );
  }
}


function mapStateToProps(state) {
  return {
    session: state.session,
    user: state.session.user.data,
    course: state.courses
  };
}

function mapDispatchProps(dispatch) {
  return bindActionCreators({ ...session, ...courses }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchProps
)(Courses);




const MIN_HEIGHT = Platform.OS === 'ios' ? verticalScale(100) : verticalScale(80);
const MAX_HEIGHT = moderateScale(220);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: scale(20),
    paddingBottom: verticalScale(80)
  },
  title: {
    color: COLORS.airBnbGrey,
    fontWeight: '600'
  },
  subtitle: {
    marginTop: verticalScale(5),
    color: COLORS.darkblue
  },
  horizontalLine: {
    height: verticalScale(1),
    width: '100%',
    backgroundColor: 'rgba(72, 72, 72, 0.2)',
    marginTop: verticalScale(15)
  },
  horizontalLineShort: {
    height: verticalScale(0.5),
    width: moderateScale(60),
    backgroundColor: 'rgba(72, 72, 72, 0.2)',
    marginTop: verticalScale(15)
  },
  subMenuContainer: {
    height: heightPercentageToDP('10%'),
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center'
  },
  subMenu: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  subMenuTitle: {
    fontSize: moderateScale(16),
    marginTop: verticalScale(5),
    color: COLORS.softBlack
  },
  verticalLine: {
    height: verticalScale(40),
    width: 1,
    backgroundColor: 'rgba(72, 72, 72, 0.2)',
  },
  locationContainer: {
    marginTop: verticalScale(10)
  },
  headerContainer: {
    height: verticalScale(100),
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor: 'red',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: moderateScale(20),
    paddingVertical: verticalScale(10)
  },
  headerBackButton: {
    height: 40,
    width: 40,
    backgroundColor: 'white',
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: verticalScale(15)
  },
  itemContainer: {
    // backgroundColor: 'red',
    width: '100%',
    marginRight: 5,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  aditionaInfoContainer: {
  },
  aditionaInfoWrapper: {
    marginTop: verticalScale(20)
  },
  pricesContainerButtons: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  pricesWrapper: {
    flexDirection: 'column'
  },
  playGolfContainer: {
    width: '100%',
    height: '10%',
    backgroundColor: 'white',
    position: 'absolute',
    bottom: 0,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: scale(20),
    borderTopWidth: 0.5,
    borderColor: 'rgba(72, 72, 72, 0.2)',
  },
  price: {
    fontSize: moderateScale(14),
    fontWeight: '500',
    color: COLORS.darkblue
  },
  playButton: {
    backgroundColor: COLORS.darkblue,
    height: verticalScale(40),
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  },
  buttonText: {
    color: COLORS.white
  },
  ratingContainer: {
    width: '100%',
    marginTop: verticalScale(15)
    // backgroundColor: 'red',
    // height: heightPercentageToDP('10%')
  },
  starContainer: {
    width: widthPercentageToDP('35%'),
    marginTop: verticalScale(10)
  },
  scoreList: {
    width: '100%',
    marginTop: verticalScale(10),
    justifyContent: 'center'
  },
  scoreName: {
    fontSize: moderateScale(14),
    fontWeight: '600',
    color: COLORS.airBnbGrey
  },
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  starListContainer: {
    width: widthPercentageToDP('20%'),
    marginVertical: verticalScale(5)
  },
  ratingListText: {
    fontSize: moderateScale(12),
    marginLeft: moderateScale(5)
  },
  scoreDescrip: {
    fontSize: moderateScale(14),
  },
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: verticalScale(15),
  },
  reviewButton: {
    width: widthPercentageToDP('70%'),
    height: heightPercentageToDP('6%'),
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7,
    borderColor: COLORS.airBnbLightGrey
  },
  reviewButtonText: {
    fontSize: moderateScale(14),
    color: COLORS.airBnbGrey
  }
})
