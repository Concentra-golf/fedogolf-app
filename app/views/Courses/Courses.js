import React, { Component } from 'react';
import { Text, View } from 'react-native';

// Modules

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as courses from '../../store/actions/courses';
import GetLocation from 'react-native-get-location'

// Components
import CoursesList from '../../components/lists/CoursesList/CoursesList';
import ViewHeader from '../../components/UI/ViewHeader/ViewHeader'
import SearchBar from '../../components/forms/SearchBar/SearchBar'
import Loader from '../../components/UI/Loader/Loader';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../translations/translation';
import * as RNLocalize from 'react-native-localize';

class Courses extends Component {
  constructor(props) {
    super(props);
    setI18nConfig(); 
    this.state = {
      loading: true,
      stopLoading: false,
      refreshing: false,
    };
  }


  async componentDidMount() {
    RNLocalize.addEventListener('change', handleLocalizationChange);
    this.requestData()
  }

  async requestData(e) {
    const token = this.props.session.accessToken;

    GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
    })
      .then(async (location) => {
        console.log(location);

        const locations = {
          "latitude": location.latitude,
          "longitude": location.longitude,
        }

        const request = await this.props.getCoursesByLocation(locations, token);
        // const request = await this.props.getCourses(token);
        if (this.props.courses.courses.success) {
          this.setState({
            loading: false,
            refreshing: false,
            coursesList: this.props.courses.courses.data
          })
        } else {
          console.log('no hay na')
          this.setState({
            loading: false,
            refreshing: false,
            coursesList: []
          })
        }
      })
      .catch(error => {
        const { code, message } = error;
        console.warn(code, message);
      })
  }

  handleRefresh = () => {
    this.setState({
      refreshing: true,
    }, () => { this.requestData() });
  }


  render() {
    const { navigation, courses } = this.props
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <ViewHeader >
          <SearchBar
            navigation={navigation}
            data={this.state.coursesList}
            placeholder={translate('courseSearchBar')}
            isButton={true}
            autoFocus={true}
          />
        </ViewHeader>
        {/* <SectionHeading align={"center"}>Next Courses</SectionHeading> */}

        {!this.state.loading &&
          <CoursesList
            data={courses.courses.data}
            navigation={this.props.navigation}
            handleRefresh={this.handleRefresh}
            refreshing={this.state.refreshing}
          />
        }

        {this.state.loading &&
          <Loader
            loadingMessage={translate('loadingCourses')}
          />
        }

        {/* {this.state.coursesList === [] &&
          <Loader
            nothing={true}
            nothingMessage={'Loading Courses List'}
          />
        } */}

        {/* {courses.courses.data.length === 0 &&
          <Loader
            nothing={true}
            nothingMessage={'No courses available'}
          />
        } */}

      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
    user: state.session.user.data,
    courses: state.courses
  };
}

function mapDispatchProps(dispatch) {
  return bindActionCreators(courses, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchProps,
)(Courses);

