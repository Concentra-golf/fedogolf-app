import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import TextLabel from '../../components/UI/TextLabel';
import { scale, verticalScale } from '../../utilities/ScalingScreen';
import { COLORS } from '../../config/constants';
import CoursePricesList from '../../components/lists/CoursePrices/CoursePrices';
import CoursePricesMembership from '../../components/lists/CoursePricesMemberships/CoursePriceMembership';

class MembershipsPrices extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const { data } = this.props.route.params
        return (
            <View style={styles.container}>

                <TextLabel
                    additionalStyles={styles.title}
                    size={30}
                    color={COLORS.airBnbGrey}>
                    Memberships Prices
            </TextLabel>

                <CoursePricesMembership
                    data={data}
                />

            </View>
        );
    }
}

export default MembershipsPrices;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: scale(10)
    },
    title: {
        marginTop: verticalScale(20),
        marginBottom: verticalScale(20)
    }
})