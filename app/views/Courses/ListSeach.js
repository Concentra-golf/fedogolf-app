import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, SafeAreaView, StatusBar } from 'react-native';
import * as RNLocalize from 'react-native-localize';

import { COLORS } from '../../config/constants';
import SearchBar from '../../components/forms/SearchBar/SearchBar';
import { verticalScale, moderateScale, scale, heightPercentageToDP } from '../../utilities/ScalingScreen';

import CourseInfoCard from '../../components/lists/CoursesList/CourseInfoCard';
import Loader from '../../components/UI/Loader/Loader';
import BenefitsCards from '../../components/UI/BenefitsComponents/BenefitsCards';
import CourseFilter from '../../components/forms/CourseFilter/CourseFilter';
import GetLocation from 'react-native-get-location'
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../translations/translation';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as courses from '../../store/actions/courses';

class ListSeach extends Component {
  static navigationOptions = ({ navigation }) => {
    const params = navigation.state.params || {};
    return {
      headerLeft: (
        <Button
          onPress={() => navigation.navigate('MyModal')}
          title="Info"
          color="#fff"
        />
      ),
    };
  };

  constructor(props) {
    super(props);
    setI18nConfig();
    this.state = {
      infoList: [],
      loading: true,
      value: '',
      text: '',
      filterModal: false
    };
    this.arrayList = []
    this.searchFilterFunction = this.searchFilterFunction.bind(this);
    this.clearSearch = this.clearSearch.bind(this);
  }

  componentDidMount() {
    const { navigation, route } = this.props;
    RNLocalize.addEventListener('change', handleLocalizationChange);

    if (route.params.data.length > 0) {
      this.setState({
        infoList: route.params.data,
        loading: false
      }, () => { this.arrayList = route.params.data })
    }
  }


  searchFilterFunction = async (e) => {
    if (e !== '') {
      const newData = this.arrayList.filter(item => {
        const data = `${item.course_name} ${item.club_name}`;
        const itemData = data ? data.toUpperCase() : '';
        const textData = e.toUpperCase();
        return result = itemData.indexOf(textData) > -1;
      });
      await this.setState({
        infoList: newData,
        text: e,
        delete: true,
        loading: false
      });
    } else {
      this.setState({
        text: '',
        delete: false,
        infoList: this.props.route.params.data
      })
      // this.loadContent();
    }
  };

  clearSearch() {
    this.setState({
      infoList: this.arrayList,
      delete: false,
      text: ''
    })
  }

  filterOptions(country, city, rating) {
    this.setState({ loading: true })

    GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
    })
      .then(async (location) => {
        const filter = {
          "country": country,
          "city": city,
          "rating": rating,
          "latitude": location.latitude,
          "longitude": location.longitude,
        }

        await this.props.getFilter(filter, this.props.session.accessToken);

        if (this.props.courses.filter.success) {
          this.setState({
            infoList: this.props.courses.filter.data,
            loading: false
          }, () => { this.arrayList = this.props.courses.filter.data })
          console.log('success')
        }
      })
      .catch(error => {
        const { code, message } = error;
        console.warn(code, message);
      })

    console.log(this.state.infoList)
  }

  showFilterModal(e) {
    this.setState({
      filterModal: e
    })
  }

  render() {
    const { navigation, route, type } = this.props;
    let title = ''
    switch (this.props.route.params.type) {
      case 'Benefits':
        title = 'Active Benefits';
        break;
      default:
        title = 'GOLF COURSES';
        break;
    }

    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle='dark-content' />

        <View style={styles.searchContainer}>
          <SearchBar
            action={this.searchCoursesNearMe}
            query={route.params.searchQuery}
            placeholder={translate('courseSearchBar')}
            lightTheme={true}
            isButton={false}
            delete={this.state.delete}
            clearText={this.clearSearch}
            value={this.state.text}
            itemSearch={(e) => this.searchFilterFunction(e)}
            ref={(input) => { this.nameInput = input }}
            navigation={this.props.navigation}
            showFilterModal={(e) => this.showFilterModal(e)}
          />
        </View>

        {/* <Text style={styles.title}>{title}</Text> */}

        {!this.state.loading && this.state.infoList.length > 0 &&
          <FlatList
            data={this.state.infoList}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{ paddingBottom: verticalScale(30), paddingHorizontal: moderateScale(10) }}
            renderItem={({ item, index }) => {
              if (this.props.route.params.type === 'benefits') {
                return (
                  <BenefitsCards
                    key={index}
                    title={item.name}
                    description={item.description}
                    counter={item.pivot.qty}
                    OnClick={this.props.route === 'federation' ? () => console.log('nada') : (e) => this.showDetailsModal(true, item)}
                  />
                )
              } else {
                return (
                  <CourseInfoCard
                    item={item}
                    action={() => this.props.navigation.navigate('CourseDetail', {
                      courseData: item,
                      courseId: item.id,
                      index: index
                    })}
                  />
                )
              }
            }
            }
          />
        }

        {!this.state.loading && this.state.infoList.length === 0 &&
          <Loader
            nothing={true}
            nothingMessage={'The Golf course you are looking for, is not available'}
          />
        }

        <CourseFilter
          visible={this.state.filterModal}
          showFilterModal={(e) => this.showFilterModal(e)}
          filterOptions={(e, w) => this.filterOptions(e, w)}
          data={this.props.route.params.data}
        />

      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
    user: state.session.user.data,
    courses: state.courses
  };
}

function mapDispatchProps(dispatch) {
  return bindActionCreators(courses, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchProps,
)(ListSeach);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  headerContainer: {
    height: heightPercentageToDP('10%'),
    backgroundColor: 'red'
  },
  searchContainer: {
    // backgroundColor: 'red',
    marginTop: verticalScale(20)
  },
  title: {
    fontSize: moderateScale(15),
    fontWeight: 'bold',
    color: COLORS.airBnbGrey,
    marginLeft: moderateScale(20),
    marginBottom: verticalScale(10),
  }
})