import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar, ScrollView } from 'react-native';
import TextLabel from '../../components/UI/TextLabel';
import { scale, verticalScale, moderateScale, heightPercentageToDP } from '../../utilities/ScalingScreen';
import { COLORS } from '../../config/constants';
import CoursePricesList from '../../components/lists/CoursePrices/CoursePrices';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import GolfCaddy from '../../assets/images/SVG/Golf_caddy';
import GolfCar from '../../assets/images/SVG/Golf_car';
import GolfCourse from '../../assets/images/SVG/Golf_course';
import Golfer from '../../assets/images/SVG/Golfer';
import DrivingRange from '../../assets/images/SVG/DrivingRange';
import ClubFitting from '../../assets/images/SVG/Golf_clubFitting';
import GolfBunker from '../../assets/images/SVG/Golf_bunker';
import Reception from '../../assets/images/SVG/Reception';

class CourseCommodities extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const { data } = this.props.route.params;
        console.log(data)
        return (
            <View style={styles.container}>
                <StatusBar barStyle='dark-content' />
                <TextLabel
                    additionalStyles={{ marginTop: verticalScale(20), }}
                    size={30}
                    color={COLORS.airBnbGrey}>
                    Commodities
            </TextLabel>

                <ScrollView contentContainerStyle={styles.contentScroll} style={styles.content}>

                    {data.golf_club.changing_room === 1 &&
                        <View style={styles.commoditieContainer}>
                            <MaterialCommunityIcons
                                name='locker'
                                size={30}
                                color={COLORS.logoGreen}
                                style={{ marginRight: moderateScale(10) }}
                            />
                            <TextLabel additionalStyles={styles.commoditiesTitle}>Changing Room</TextLabel>
                        </View>
                    }

                    {data.golf_club.club_fitting === 1 &&
                        <View style={styles.commoditieContainer}>
                            <ClubFitting
                                size={32}
                                color={COLORS.logoGreen}
                                style={{ marginRight: moderateScale(2) }}
                            />
                            <TextLabel additionalStyles={styles.commoditiesTitle}>Club Fitting</TextLabel>
                        </View>
                    }

                    {data.golf_club.motor_cart === 1 &&
                        <View style={styles.commoditieContainer}>
                            <GolfCar
                                size={32}
                                color={COLORS.logoGreen}
                                style={{ marginRight: moderateScale(2) }}
                            />
                            <TextLabel additionalStyles={styles.commoditiesTitle}>Golf Car</TextLabel>
                        </View>
                    }

                    {data.golf_club.caddie_hire === 1 &&
                        < View style={styles.commoditieContainer}>
                            <GolfCaddy
                                size={32}
                                color={COLORS.logoGreen}
                                style={{ marginRight: moderateScale(5) }}
                            />
                            <TextLabel additionalStyles={styles.commoditiesTitle}>Caddy Hire</TextLabel>
                        </View>
                    }

                    {data.golf_club.chipping_green === 1 &&
                        <View style={styles.commoditieContainer}>
                            <GolfCourse
                                size={32}
                                color={COLORS.logoGreen}
                                style={{ marginRight: moderateScale(5) }}
                            />
                            <TextLabel additionalStyles={styles.commoditiesTitle}>Chipping Green</TextLabel>
                        </View>
                    }

                    {data.golf_club.driving_range === 1 &&
                        <View style={styles.commoditieContainer}>
                            <DrivingRange
                                size={32}
                                color={COLORS.logoGreen}
                                style={{ marginRight: moderateScale(5) }}
                            />
                            <TextLabel additionalStyles={styles.commoditiesTitle}>Driving Range</TextLabel>
                        </View>
                    }

                    {data.golf_club.golf_lessons === 1 &&
                        <View style={styles.commoditieContainer}>
                            <Golfer
                                size={32}
                                color={COLORS.logoGreen}
                                style={{ marginRight: moderateScale(5) }}
                            />

                            <TextLabel additionalStyles={styles.commoditiesTitle}>Golf Lessons</TextLabel>
                        </View>
                    }

                    {data.golf_club.practice_bunker === 1 &&
                        < View style={styles.commoditieContainer}>
                            <GolfBunker
                                size={32}
                                color={COLORS.logoGreen}
                                style={{ marginRight: moderateScale(5) }}
                            />
                            <TextLabel additionalStyles={styles.commoditiesTitle}>Practice Bunker</TextLabel>
                        </View>
                    }

                    {data.golf_club.pro_shop === 1 &&
                        <View style={styles.commoditieContainer}>
                            <Entypo
                                name='shop'
                                size={30}
                                color={COLORS.logoGreen}
                                style={{ marginRight: moderateScale(10) }}
                            />
                            <TextLabel additionalStyles={styles.commoditiesTitle}>Pro Shop</TextLabel>
                        </View>
                    }

                    {data.golf_club.putting_green === 1 &&
                        <View style={styles.commoditieContainer}>
                            <MaterialIcons
                                name='golf-course'
                                size={30}
                                color={COLORS.logoGreen}
                                style={{ marginRight: moderateScale(10) }}
                            />
                            <TextLabel additionalStyles={styles.commoditiesTitle}>Putting Green</TextLabel>
                        </View>
                    }

                    {data.golf_club.restaurant === 1 &&
                        <View style={styles.commoditieContainer}>
                            <MaterialIcons
                                name='restaurant'
                                size={30}
                                color={COLORS.logoGreen}
                                style={{ marginRight: moderateScale(10) }}
                            />
                            <TextLabel additionalStyles={styles.commoditiesTitle}>Restaurant</TextLabel>
                        </View>
                    }

                    {data.golf_club.reception_hall === 1 &&
                        <View style={styles.commoditieContainer}>
                            <Reception
                                size={32}
                                color={COLORS.logoGreen}
                                style={{ marginRight: moderateScale(5) }}
                            />
                            <TextLabel additionalStyles={styles.commoditiesTitle}>Reception Hall</TextLabel>
                        </View>
                    }

                </ScrollView>

            </View >
        );
    }
}

export default CourseCommodities;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: scale(10)
    },
    content: {
        marginTop: verticalScale(15),
    },
    contentScroll: {
        alignItems: 'center'
    },
    commoditieContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: verticalScale(10),
        width: '90%',
        height: heightPercentageToDP('8%'),
        paddingHorizontal: moderateScale(10),
        borderBottomWidth: 0.5,
        borderColor: COLORS.airBnbLightGrey
    },
    commoditiesTitle: {
        fontSize: moderateScale(16),
        fontWeight: '600',
        color: COLORS.softBlack,
        marginLeft: moderateScale(5)
    }
})