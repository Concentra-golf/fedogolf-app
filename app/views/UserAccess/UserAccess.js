import * as React from 'react';
import {
  View,
  Text,
  Button,
  Image,
  ScrollView,
  TouchableOpacity,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
  ImageBackground,
  Dimensions,
  SafeAreaView,
  Alert
} from 'react-native';

// Modules

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { LoginManager, LoginButton, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../translations/translation';

// Constants

import { FACEBOOK_LOGIN_PERMISSIONS, FACEBOOK_LOGIN_PERMISSIONS_REQUEST } from '../../config/constants';
import * as RNLocalize from 'react-native-localize';

// Actions
import * as session from '../../store/actions/session';

// Components
import ForgotPassword from '../../components/forms/ForgotPassword/ForgotPassword'
import Welcome from '../../components/UI/Welcome/Welcome';
import SignIn from '../../components/forms/SignIn/SignIn';
import SignUp from '../../components/forms/SignUp/SignUp'

import styles from './Styles';
import SignUpGolferInformation from '../../components/forms/SignUpGolferInformation/SignUpGolferInformation';

class UserAccess extends React.Component {
  constructor(props) {
    super(props);
    setI18nConfig(); // set initial config
    this.state = {
      showForm: 'welcome',
      // showForm: 'signup',
      // showForm: 'signUpGolferInformationForm',
    };
  }

  componentDidMount = () => {
    RNLocalize.addEventListener('change', handleLocalizationChange);
    this.props.cleanSessionStore();
  };

  userSignInWithFacebook = async (result) => {


    const data = {
      firstname: result.first_name,
      lastname: result.last_name,
      email: result.email,
      facebook_id: result.id,
    }

    await this.props.facebookSignIn(data)

    // Close Form if the user is logged in

    console.log('session', this.props.session)

    if (this.props.session.user.new_user) {
      this.switchForms('signUpGolferInformationForm')
    } else if (this.props.session.user.isLoggedIn) {
      this.props.navigation.navigate('Tabs');
    } else {
      Alert.alert('Algo fallo', '', [{ text: 'Ok' }])
    }
  }

  faceBookLogin = () => {

    console.log('1')

    const self = this;
    LoginManager.setLoginBehavior(Platform.OS === 'ios' ? 'browser' : 'web_only')
    LoginManager.logInWithPermissions(FACEBOOK_LOGIN_PERMISSIONS).then(
      function (result) {
        if (!result.isCancelled) {
          self.accessFacebookUserTokenData();
        }
      },
      function (error) {
        console.log('Login fail with error: ' + error);
      }
    );
  }

  // Access Facebook User Token Data

  accessFacebookUserTokenData = () => {

    console.log('2')

    this.setState({ loadingTokenData: true });

    const self = this;

    AccessToken.getCurrentAccessToken().then(data => {
      console.log('the token', data.accessToken.toString());

      const processRequest = new GraphRequest(FACEBOOK_LOGIN_PERMISSIONS_REQUEST, null, self.getFacebookUserData);
      // Start the graph request.
      new GraphRequestManager().addRequest(processRequest).start(); ``

    }).catch(error => {
      // console.log('the token error', error);
      self.facebookLogout();
    });

  }

  // Get the user data from Facebook
  getFacebookUserData = async (error, result) => {


    if (error) {

      console.log('error fetching data', error);

      this.setState({ loadingTokenData: false });

    } else {

      this.setState({ user: result, hasData: true, loadingTokenData: false });
      await this.userSignInWithFacebook(result)

    }

  }

  // Log out the app from Faceboook

  facebookLogout = () => {

    LoginManager.logOut();

    this.setState({ userData: {}, hasData: false, loadingTokenData: false, userStore: null });

  }

  // Switch between Login, Password Recovery and Register forms

  switchForms = form => {
    this.setState({
      showForm: form,
    });
  };

  render() {
    const { session, navigation, signIn, facebookSignIn, signUp, user, forgotPassword, updateProfile } = this.props;
    const { showForm } = this.state;
    if (!true) {
      return (

        <Welcome switchForms={this.switchForms} ></Welcome>
      )
    } else
      return (
        <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }} >

          <KeyboardAvoidingView
            style={showForm === 'login' ?
              styles.loginContainer : showForm === 'signup' ?
                styles.signUpContainer : showForm === 'password' ?
                  styles.signUpContainer : showForm === 'signUpGolferInformationForm' ?
                    styles.signUpContainer : styles.loginContainer
            }
            behavior={Platform.OS === 'ios' ? 'padding' : ''}
          >
            <TouchableOpacity activeOpacity={1.0} onPress={Keyboard.dismiss}>
              <ScrollView >
                <View style={styles.scrollViewWrapper}>

                  {/* Welcome Screen */}
                  {showForm === 'welcome' && (
                    <Welcome switchForms={this.switchForms} ></Welcome>
                  )}

                  {/* Login Form */}
                  {showForm === 'login' && (
                    <SignIn
                      isLoading={session.user.isLoading}
                      error={session.user.error}
                      session={session.user}
                      navigation={navigation}
                      switchForms={this.switchForms}
                      signIn={signIn}
                      facebookSignIn={facebookSignIn}
                      user={user}
                      facebookLogin={this.faceBookLogin}
                    />
                  )}



                  {/* Password Reset Form */}

                  {showForm === 'password' && (
                    <ForgotPassword
                      isLoading={session.passwordRecovery.isLoading}
                      error={session.passwordRecovery.error}
                      session={session.passwordRecovery}
                      navigation={navigation}
                      switchForms={this.switchForms}
                      forgotPassword={forgotPassword}
                    />
                  )}

                  {/* Register Form */}

                  {showForm === 'signup' && (
                    <SignUp
                      isLoading={session.user.isLoading}
                      error={session.user.error}
                      session={session.user}
                      navigation={navigation}
                      switchForms={this.switchForms}
                      signUp={signUp}
                      setLoginForm={this.setLoginForm}
                      facebookLogin={this.faceBookLogin}
                    />
                  )}

                  {showForm === 'signUpGolferInformationForm' && (
                    <SignUpGolferInformation
                      isLoading={session.user.isLoading}
                      error={session.user.error}
                      session={session.user}
                      navigation={navigation}
                      switchForms={this.switchForms}
                      signUp={signUp}
                      setLoginForm={this.setLoginForm}
                      updateProfile={updateProfile}
                    />
                  )}

                  <View
                    style={{
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    {/* {this.state.showForm !== 'signUpGolferInformationForm' ? (
                      <Button
                        title="Go to Tabs"
                        onPress={() => this.props.navigation.navigate('Tabs')}
                      />
                    ) : null} */}

                    {/* <Button
                    title="Test action"
                    onPress={() => this.props.signIn()}
                  /> */}
                  </View>

                </View>
              </ScrollView>
            </TouchableOpacity>
          </KeyboardAvoidingView>
        </SafeAreaView>

      );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
    user: state.session.data
  };
}

function mapDispatchProps(dispatch) {
  return bindActionCreators(session, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchProps,
)(UserAccess);
