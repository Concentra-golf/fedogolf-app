import React, { Component } from "react";
import { Text, View } from "react-native";
import SettingsUserCard from "../../components/cards/SettingsUserCard/SettingsUserCard";
import SettingsButton from "../../components/UI/SettingsButton";

// Modules

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as RNLocalize from 'react-native-localize';

import * as session from '../../store/actions/session';

import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../translations/translation';

class AccountOptions extends Component {
  constructor(props) {
    super(props);
    setI18nConfig();
    this.state = {
    };
  }

  async componentDidMount() {
    RNLocalize.addEventListener('change', handleLocalizationChange);
    const oldToken = this.props.session.accessToken;
    await this.props.refreshToken(oldToken);
    const request = this.props.user.federations;
  }

  componentWillUnmount() {
    RNLocalize.removeEventListener('change', handleLocalizationChange);
  }

  render() {
    const { user } = this.props.route.params
    return (
      <View >
        <SettingsUserCard user={this.props.session.user} />

        <SettingsButton
          label={translate('personalInfo')}
          icon="altUser"
          action={() =>
            this.props.navigation.navigate("PersonalInformation", {
              user
            })
          }
        />

        <SettingsButton
          label={translate('golferInfo')}
          icon="golf"
          action={() =>
            this.props.navigation.navigate("GolferInformation", {
              user
            })
          }
        />

        <SettingsButton
          label={translate('manageRequest')}
          theme={"settingsButton"}
          action={() => this.props.navigation.navigate('ManageRequest', {
            user
          })}
          icon={'membership'}
        />


        <SettingsButton
          label={translate('myReceipst')}
          icon="receipt"
          action={() =>
            this.props.navigation.navigate("ReceiptList", {
              user
            })
          }
        />

      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
    user: state.session.user.data
  };
}

function mapDispatchProps(dispatch) {
  return bindActionCreators(session, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchProps
)(AccountOptions);
