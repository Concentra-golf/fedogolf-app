import React, { Component } from "react";
import { Text, View, StyleSheet } from "react-native";
import SettingsUserCard from "../../../components/cards/SettingsUserCard/SettingsUserCard";

import { ScrollView } from "react-native-gesture-handler";

// Modules

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as session from "../../..//store/actions/session";
import AboutService from "../../../config/services/AboutService";
import Loader from "../../../components/UI/Loader/Loader";

class TermsOfUse extends Component {
  state = {
    loading: true,
    termsOfUse: {}
  };

  async componentDidMount() {
    const oldToken = this.props.session.accessToken;
    const token = await this.props.refreshToken(oldToken);
    await this.getTermsOfUse(this.props.session.accessToken);
    this.setState({ loading: false });
  }

  getTermsOfUse = async accessToken => {
    const response = await AboutService.getTermsOfUse(accessToken);

    this.setState({ termsOfUse: response.data[0] });
  };

  render() {
    const { user } = this.props.route.params;
    return (
      <ScrollView>
        <View>
          <SettingsUserCard
            editBtn={() =>
              this.props.navigation.navigate("PIEdit", {
                user
              })
            }
            user={
              this.props.session &&
              this.props.session.user &&
              this.props.session.user.data &&
              user
            }
          />
          <View style={{ alignItems: "center" }}>
            <View>
              <Text style={styles.title}>Terms of Use</Text>
            </View>
          </View>
          <View style={styles.infoContainer}>
            <View>
              <Text style={styles.info}>
                Version:{" "}
                {this.state.termsOfUse && this.state.termsOfUse.version}
              </Text>
            </View>
          </View>

          <View>
            <Text style={styles.description}>
              {this.state.termsOfUse && this.state.termsOfUse.description}
            </Text>
          </View>
        </View>

        {this.state.loading && <Loader loadingMessage={"Loading"} />}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 18,
    marginTop: 20,
    fontWeight: "bold"
  },
  infoContainer: {
    alignItems: "flex-end",
    justifyContent: "flex-end",
    padding: 10,
    fontSize: 14
  },
  info: {
    fontWeight: "bold",
    fontSize: 14
  },
  description: {
    fontSize: 14,
    padding: 10
  }
});

function mapStateToProps(state) {
  return {
    session: state.session,
    user: state.session.user.data
  };
}

function mapDispatchProps(dispatch) {
  return bindActionCreators(session, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchProps
)(TermsOfUse);
