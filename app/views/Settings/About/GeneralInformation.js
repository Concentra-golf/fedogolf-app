import React, { Component } from "react";
import { Text, View, StyleSheet } from "react-native";
import SettingsUserCard from "../../../components/cards/SettingsUserCard/SettingsUserCard";

import { ScrollView } from "react-native-gesture-handler";

// Modules

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as session from "../../..//store/actions/session";
import AboutService from "../../../config/services/AboutService";
import Loader from "../../../components/UI/Loader/Loader";

class GeneralInformation extends Component {
  state = {
    loading: true,
    generalInformation: {}
  };

  async componentDidMount() {
    const oldToken = this.props.session.accessToken;
    const token = await this.props.refreshToken(oldToken);
    await this.getGeneralInformation(this.props.session.accessToken);
    this.setState({ loading: false });
  }

  getGeneralInformation = async accessToken => {
    const response = await AboutService.getGeneralInformation(accessToken);

    this.setState({ generalInformation: response.data[0] });
  };

  render() {
    const { user } = this.props.route.params;
    return (
      <ScrollView>
        <View>
          <SettingsUserCard
            editBtn={() =>
              this.props.navigation.navigate("PIEdit", {
                user
              })
            }
            user={
              this.props.session &&
              this.props.session.user &&
              this.props.session.user.data &&
              user
            }
          />
          <View style={{ alignItems: "center" }}>
            <View>
              <Text style={styles.title}>General Information</Text>
            </View>
          </View>
          <View style={styles.infoContainer}>
            <View>
              <Text style={styles.info}>
                Name:{" "}
                {this.state.generalInformation &&
                  this.state.generalInformation.name}
              </Text>
            </View>
            <View>
              <Text style={styles.info}>
                Version:{" "}
                {this.state.generalInformation &&
                  this.state.generalInformation.version}
              </Text>
            </View>
          </View>

          <View>
            <Text style={styles.description}>
              {this.state.generalInformation &&
                this.state.generalInformation.description}
            </Text>
          </View>
        </View>

        {this.state.loading && <Loader loadingMessage={"Loading"} />}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 18,
    marginTop: 20,
    fontWeight: "bold"
  },
  infoContainer: {
    alignItems: "flex-end",
    justifyContent: "flex-end",
    padding: 10,
    fontSize: 14
  },
  info: {
    fontWeight: "bold",
    fontSize: 14
  },
  description: {
    fontSize: 14,
    padding: 10
  }
});

function mapStateToProps(state) {
  return {
    session: state.session,
    user: state.session.user.data
  };
}

function mapDispatchProps(dispatch) {
  return bindActionCreators(session, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchProps
)(GeneralInformation);
