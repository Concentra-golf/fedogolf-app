import React, { Component } from "react";
import { Text, View } from "react-native";
// import SettingsUserCard from "../../../components/cards/SettingsUserCard/SettingsUserCard";
import SettingsUserCard from "../../../components/cards/SettingsUserCard/SettingsUserCard";
import SettingsButton from "../../../components/UI/SettingsButton";

// Modules

import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import * as session from "../../../store/actions/session";

class AboutOptions extends Component {
  async componentDidMount() {
    const oldToken = this.props.session.accessToken;
    await this.props.refreshToken(oldToken);

    const request = this.props.user.federations;
  }

  render() {
    const { user } = this.props.route.params;
    // console.log('user', user)
    return (
      <View>
        <SettingsUserCard user={user} />

        <SettingsButton
          label="Terms of Use"
          icon="termsOfUse"
          action={() =>
            this.props.navigation.navigate("TermsOfUse", {
              user
            })
          }
        />

        <SettingsButton
          label="Privacy Policies"
          icon="privacyPolicies"
          action={() =>
            this.props.navigation.navigate("PrivacyPolicies", {
              user
            })
          }
        />

        <SettingsButton
          label={"General Information"}
          theme={"settingsButton"}
          action={() =>
            this.props.navigation.navigate("GeneralInformation", {
              user
            })
          }
          icon={"aboutGolfertek"}
          // additionalStyles={styles.btns}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
    user: state.session.user.data
  };
}

function mapDispatchProps(dispatch) {
  return bindActionCreators(session, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchProps
)(AboutOptions);
