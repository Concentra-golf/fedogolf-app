import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, StatusBar, Platform, SafeAreaView } from 'react-native';

import * as session from '../../../store/actions/session';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as RNLocalize from 'react-native-localize';

import SettingsUserCard from '../../../components/cards/SettingsUserCard/SettingsUserCard';
import UploadDocs from '../../../components/UI/UploadDocs/UploadDocs';
import FederationService from '../../../config/services/FederationService'
import Button from "../../../components/UI/Button";

import Moment from 'moment';
import 'moment/locale/es';
import { moderateScale, verticalScale, scale } from '../../../utilities/ScalingScreen';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { DotIndicator } from 'react-native-indicators';

import RNFS from 'react-native-fs';
import FileViewer from 'react-native-file-viewer';
import { COLORS } from '../../../config/constants';
import Loader from '../../../components/UI/Loader/Loader';
import DocumentsList from '../../../components/lists/MembershipsDocuments/DocumentsList';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';

import { getValueAndLabelFromArray } from '../../../utilities/helper-functions'
import FormDetails from './FormDetails';

class ManageRequestDetails extends Component {
    constructor(props) {
        super(props);
        setI18nConfig();
        this.state = {
            requestData: {},
            loading: true,
            loadingDocument: false,
            federations: [],
            associations: [],
            leagues: [],
            golf_clubs: [],
            requestValues: {},
            showFedoGolfRegister: false,
        };
        this.reRender02 = this.reRender02.bind(this);
    }

    componentDidUpdate() {
        StatusBar.setBarStyle('light-content', true);
    }

    async componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);
        this.loadData();
    }

    async loadData() {
        const info = this.props.route.params.requestData
        let request = await this.props.requestMembershipsById(info.id, this.props.session.accessToken);

        if (this.props.session.membershipsItem.success) {
            const values = this.props.session.membershipsItem.data;

            let value = {
                firstname: values.firstname,
                lastname: values.lastname,
                id: values.id_number,
                dob: values.dob,
                address: values.address,
                nationality: values.nationality,
                mobile_phone: values.mobile_phone,
                office_phone: values.office_phone,
                home_phone: values.home_phone,
                email: values.email,
                golf_club_id: values.golf_club_id,
                ghin_number: values.ghin_number,
                association_id: values.association_id,
                league_id: values.league_id,
                profession: values.profession,
                company_name: values.company_name,
                job_title: values.job_title,
                sdcc: values.sdcc
            }
            this.setState({
                requestData: this.props.session.membershipsItem.data,
                requestValues: value,
                loading: false,
            })
            this.getFedogolftData();
        }

        console.log(this.state.requestData)
    }

    async getFedogolftData() {
        const response = await FederationService.getAllFederations(this.props.session.accessToken)
        if (response.status === 200) {
            this.setState({
                federations: response.data.federations,
                associations: response.data.asociations,
                leagues: response.data.leagues,
                golf_clubs: response.data.golf_clubs,
            })
        }
    }

    async selecDocument(e) {
        const doc = 'document.pdf'
        const localFile = `${RNFS.DocumentDirectoryPath}/${e.name}`;
        this.setState({ loadingDocument: true })
        const options = {
            // fromUrl: 'http://api.golfertek.com/previewpdf',
            fromUrl: e.url,
            toFile: localFile
        };

        RNFS.downloadFile(options).promise
            .then((e) => {
                this.setState({ loadingDocument: false })
                FileViewer.open(localFile)

            }).catch(error => {
                this.setState({
                    loadingDocument: false
                })
            });
    }

    async reRender02() {
        console.log('reRendering')
        await this.loadData()
    }

    render() {
        const { requestData } = this.state;
        const { session, editfedogolfSignUp } = this.props
        return (
            <SafeAreaView style={styles.container}>

                {this.state.loading && <DotIndicator color={COLORS.darkblue} />}

                {!this.state.loading &&
                    <ScrollView style={styles.content}>

                        <View style={styles.headerContainer}>
                            <Text style={styles.mainTitle}>{requestData.federation.name}</Text>
                        </View>

                        <View style={styles.listConatiner}>

                            <Text style={styles.infoTitle}>{translate('requestDetails')}</Text>

                            <View style={styles.requestMainInformation}>
                                <View style={styles.rowContainer}>
                                    <Text style={styles.title}>{translate('federation')}</Text>
                                    <Text style={styles.subtitle}>{requestData.federation.name}</Text>
                                </View>

                                {this.state.requestData.request_payment && requestData.request_payment.payment_type && requestData.request_payment.is_paid === 1 &&
                                    <View style={styles.rowContainer}>
                                        <Text style={styles.title}>{translate('paymentMethod')}</Text>
                                        <Text style={styles.subtitle}>{requestData.request_payment.payment_type === 'card' ? translate('creditCard') : translate('transfer')}</Text>
                                    </View>
                                }

                                {this.state.requestData.request_payment && requestData.request_payment.transaction_id &&
                                    <View style={styles.rowContainer}>
                                        <Text style={styles.title}>{translate('paymentStatus')}</Text>
                                        <Text style={styles.subtitle}>{requestData.request_payment.transaction_id !== null ? translate('Paid') : translate('waitingPay')}</Text>
                                    </View>
                                }

                                <View style={styles.rowContainer}>
                                    <Text style={styles.title}>{translate('dateCreated')}</Text>
                                    <Text style={styles.subtitle}>{Moment(requestData.created_at).locale('en').format('DD MMMM YYYY')}</Text>
                                </View>

                                <View style={styles.rowContainer}>
                                    <Text style={styles.title}>{translate('status')}</Text>
                                    <Text
                                        style={[styles.subtitle,
                                        requestData.status === 'approve' ? { color: COLORS.green } :
                                            requestData.status === 'in_progress' ? { color: COLORS.airBnbLightGrey } :
                                                requestData.status === 'disapprove' ? { color: COLORS.red } :
                                                    {}]}
                                    >
                                        {requestData.status === 'approve' ? translate('aprove') : requestData.status === 'in_progress' ? translate('progress') : translate('decline')}
                                    </Text>
                                </View>

                                <TouchableOpacity
                                    style={styles.submitButton}
                                    onPress={() => this.setState({ showFedoGolfRegister: true })}
                                >
                                    <Text style={styles.submitButtonText}>{translate('moreDetails')}</Text>
                                </TouchableOpacity>

                                {requestData.request_payment &&
                                    requestData.status !== "approve" &&
                                    requestData.request_payment.approve !== 0 &&
                                    requestData.request_payment.is_paid !== 1 &&
                                    requestData.request_payment.transfers_uploaded === 0 &&
                                    <TouchableOpacity
                                        style={styles.submitPaymentButton}
                                        onPress={() => this.props.navigation.navigate('OptionsSelector', {
                                            data: requestData,
                                            paymentData: requestData.request_payment,
                                            reRender: this.props.route.params.reRender,
                                            reRender02: this.reRender02,
                                            route: 'RequestDetails'
                                        })}
                                    >
                                        <Text style={styles.submitButtonText}>{translate('payMembership')}</Text>
                                    </TouchableOpacity>
                                }

                            </View>

                            <DocumentsList
                                loadingDocument={this.state.loadingDocument}
                                loading={this.state.loading}
                                requestData={this.state.requestData}
                                selecDocument={(e) => this.selecDocument(e)}
                            />

                            {this.state.loadingDocument &&
                                <View style={styles.documentContainer}>
                                    <Loader
                                        loadingMessage={'Downloading File'}
                                    />
                                </View>
                            }

                            {!this.state.loading &&
                                <FormDetails
                                    user={this.state.requestValues}
                                    status={requestData.status}
                                    aplication={this.props.session.membershipsItem}
                                    reRender02={this.reRender02}
                                    ghinValidated={requestData.is_ghin_number_valid}
                                    navigation={this.props.navigation}
                                    visible={this.state.showFedoGolfRegister}
                                    associations={getValueAndLabelFromArray(this.state.associations, 'name')}
                                    leagues={getValueAndLabelFromArray(this.state.leagues, 'name')}
                                    golf_clubs={getValueAndLabelFromArray(this.state.golf_clubs, 'club_name')}
                                    accessToken={session.accessToken}
                                    session={session}
                                    editfedogolfSignUp={editfedogolfSignUp}
                                    fedogolfUser={session.fedogolfUser}
                                    closeModal={() => { this.setState({ showFedoGolfRegister: false }) }
                                    }
                                />
                            }


                        </View>
                    </ScrollView>
                }
            </SafeAreaView>
        );
    }
}

function mapStateToProps(state) {
    return {
        session: state.session,
        user: state.session.user.data,
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators(session, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchProps
)(ManageRequestDetails);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    content: {
        width: '100%',
        height: '100%',
        // padding: scale(20),
    },
    headerContainer: {
        marginTop: verticalScale(30),
        width: '100%',
        padding: scale(20),
    },
    mainTitle: {
        fontSize: moderateScale(25),
        fontWeight: Platform.OS === 'ios' ? '600' : 'bold',
        color: COLORS.black,
    },
    listConatiner: {
        // alignItems: "center"
    },
    infoTitle: {
        fontSize: moderateScale(18),
        fontWeight: 'bold',
        color: COLORS.airBnbGrey,
        marginLeft: moderateScale(20)
    },
    requestMainInformation: {
        alignItems: 'center'
    },
    rowContainer: {
        marginTop: verticalScale(20),
        borderBottomWidth: 1,
        borderBottomColor: '#D8D8D8',
        paddingVertical: 10,
        width: '90%',
        flexDirection: "row",
        justifyContent: "space-between"
    },
    title: {
        fontSize: moderateScale(13)
    },
    subtitle: {
        fontSize: moderateScale(14),
        fontWeight: "bold",
        color: COLORS.softBlack
    },
    submitButton: {
        width: moderateScale(320),
        height: verticalScale(50),
        marginTop: verticalScale(15),
        backgroundColor: COLORS.darkblue,
        alignItems: 'center',
        justifyContent: 'center'
    },
    submitButtonText: {
        fontSize: moderateScale(16),
        fontWeight: 'bold',
        color: 'white'
    },
    submitPaymentButton: {
        width: moderateScale(320),
        height: verticalScale(50),
        marginTop: verticalScale(15),
        backgroundColor: COLORS.red,
        alignItems: 'center',
        justifyContent: 'center'
    },
    editButton: {
        height: verticalScale(40),
        width: moderateScale(250),
        backgroundColor: COLORS.lightblue,
        marginTop: verticalScale(15),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20
    },
    editButtonText: {
        fontSize: moderateScale(14),
        color: COLORS.white,
        fontWeight: 'bold'
    }
})

