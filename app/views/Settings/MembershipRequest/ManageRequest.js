import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Dimensions, StatusBar, TouchableOpacity } from 'react-native';
import * as Animatable from 'react-native-animatable';

import { FlatList } from 'react-native-gesture-handler';

import Ionicons from 'react-native-vector-icons/Ionicons'
import HeaderImageScrollView, { TriggeringView } from 'react-native-image-header-scroll-view';
import { Header } from 'react-native/Libraries/NewAppScreen';

import MembershipRequest from '../../../components/lists/MembershipRequest/MembershipRequest';
import { verticalScale, moderateScale, scale } from '../../../utilities/ScalingScreen';
import { COLORS } from '../../../config/constants';

import * as session from '../../../store/actions/session';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Loader from '../../../components/UI/Loader/Loader';

import FixedForeground from '../../../components/UI/HeaderButtons/ImageHeader/FixedForeGround';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';
import * as RNLocalize from 'react-native-localize';

class ManageRequest extends Component {
    constructor(props) {
        super(props);
        setI18nConfig(); 
        this.state = {
            showModal: false,
            membershipsList: [],
            loading: true,
            refreshing: false,
            showNavTitle: false,
        };
        this.loadData = this.loadData.bind(this);
        this.reRender = this.reRender.bind(this);
    }

    showModalVisible(e) {
        this.setState({
            showModal: e
        })
    }

    async componentDidMount() {
        RNLocalize.removeEventListener('change', handleLocalizationChange);
        this.loadData();
        this.props.navigation.addListener(
            'didFocus',
            payload => {
                this.loadData();
            }
        );
    }


    async loadData() {
        const token = this.props.session.accessToken;
        let request = await this.props.requestMemberships(token);
        const membershipsList = this.props.session.membershipsList.data;
        if (this.props.session.membershipsList.success) {
            this.setState({
                membershipsList: membershipsList,
                loading: false,
                refreshing: false,
            })
        } else {
            this.setState({
                membershipsList: [],
                loading: false,
                error: true,
                refreshing: false,
            })
        }

    }

    handleRefresh = () => {
        this.setState({
            refreshing: true,
        }, () => { this.loadData(); }
        );
    }

    reRender = async () => {
        await this.loadData();
    }


    render() {
        return (
            <View style={{ flex: 1 }}>
                <StatusBar barStyle="light-content" />
                <HeaderImageScrollView
                    maxHeight={MAX_HEIGHT}
                    minHeight={MIN_HEIGHT}
                    maxOverlayOpacity={0.3}
                    minOverlayOpacity={0.2}
                    fadeOutForeground
                    headerImage={require('../../../assets/images/scroll-header-bg.png')}
                    renderFixedForeground={() => <FixedForeground title={translate('manageRequest')} navigation={this.props.navigation} />}
                >
                    <TriggeringView
                        style={styles.section}

                    >
                        {!this.state.loading && this.state.membershipsList.length > 0 &&
                            <FlatList
                                data={this.state.membershipsList}
                                refreshing={this.state.refreshing}
                                onRefresh={() => this.handleRefresh()}
                                keyExtractor={(item, index) => item.id.toString()}
                                renderItem={({ item }) =>
                                    <MembershipRequest
                                        name={item.federation.name}
                                        dateRequested={item.created_at}
                                        status={item.status}
                                        onClick={() => this.props.navigation.navigate('ManageRequestDetails', {
                                            requestData: item,
                                            reRender: this.reRender
                                        })}
                                    />
                                }
                            />
                        }

                        {this.state.loading &&
                            <Loader
                                loadingMessage={translate('loadingRequest')}
                            />
                        }

                        {!this.state.loading && this.state.membershipsList.length === 0 &&
                            <Loader
                                nothing={true}
                                nothingMessage={translate('noRequest')}
                            />
                        }

                        {!this.state.loading && this.state.error &&
                            <Loader
                                errorState={true}
                                errorMessage={translate('errorRequest')}
                            />
                        }
                    </TriggeringView>
                </HeaderImageScrollView>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        session: state.session,
        user: state.session.user.data,
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators(session, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchProps
)(ManageRequest);

const MIN_HEIGHT = verticalScale(100);
const MAX_HEIGHT = moderateScale(220);

const styles = StyleSheet.create({
    image: {
        height: MAX_HEIGHT,
        width: '100%',
        resizeMode: 'stretch',
    },
    section: {
        padding: 20,
        borderBottomColor: '#cccccc',
        backgroundColor: 'white',
    },
    headerContainer: {
        height: verticalScale(100),
        justifyContent: "center",
        alignItems: "center",
        justifyContent: 'space-between',
        flexDirection: 'row',
        paddingHorizontal: moderateScale(20),
        paddingVertical: verticalScale(10)
    },
    imageTitle: {
        color: 'white',
        fontSize: moderateScale(24),
        marginLeft: moderateScale(20),
        marginTop: verticalScale(20),
        fontWeight: 'bold'
    },
});