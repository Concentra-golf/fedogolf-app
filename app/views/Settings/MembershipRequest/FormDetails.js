import React from "react";
import {
    View,
    ScrollView,
    Alert,
    Modal,
    Text,
    TouchableOpacity,
    Dimensions,
    SafeAreaView,
    StatusBar
} from "react-native";

// Modules
import { Formik } from "formik";
import * as yup from "yup";

// Components
import AlertMessage from "../../../components/UI/AlertMessage/AlertMessage";
import Button from "../../../components/UI/Button";
import FormInput from "../../../components/UI/FormInput/FormInput";
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';
import * as RNLocalize from 'react-native-localize';


// Styles
import styles from "./Styles";

// Images
import Ionicons from 'react-native-vector-icons/Ionicons'


const ratio = Math.min(
    Dimensions.get('window').width / 414,
    Dimensions.get('window').height / 420,
)

const height = 420 * ratio;

export default class FormDetails extends React.Component {
    constructor(props) {
        super(props);
        setI18nConfig(); 
        this.state = {
            showSuccesModal: false,
            isPassport: false,
            gender: 'male',
            values: {
                gender: 1,
                homeClub: ''
            }
        };
        this.fedoGolfSignUp = this.fedoGolfSignUp.bind(this);
        this.switchGender = this.switchGender.bind(this);
    }

    componentDidUpdate() {
        RNLocalize.addEventListener('change', handleLocalizationChange);
        StatusBar.setBarStyle('dark-content', true);
    }

    componentWillUnmount() {
        StatusBar.setBarStyle('dark-content', true);
    }

    // Sign in the user

    switchGender = (e) => {
        console.log(e);
        this.setState({ gender: e })
        this.state.values.gender === 1
            ? this.setState({ values: { gender: 2 } })
            : this.setState({ values: { gender: 1 } })
    }

    async fedoGolfSignUp(values, actions) {
        const gender = this.state.gender;

        const data = {
            firstname: values.firstname,
            lastname: values.lastname,
            id_number: values.id_number,
            email: values.email,
            dob: values.dob,
            nationality: values.nationality,
            home_phone: values.home_phone,
            mobile_phone: values.mobile_phone,
            golf_club_id: values.golf_club_id,
            ghin_number: values.ghin_number,
            association_id: values.association_id,
            league_id: values.league_id,
            profession: values.profession,
            office_phone: values.office_phone,
            address: values.address,
            company_name: values.company_name,
            gender: gender,
            job_title: values.job_title,
            sdcc: values.sdcc || false,
            federation_id: 1
        };
        console.log('data from fedogolf', data)

        const aplicationId = this.props.session.membershipsItem.data.id;

        await this.props.editfedogolfSignUp(this.props.accessToken, aplicationId, data);

        // Close Form if the user is logged in

        if (this.props.session.fedogolfUser.data) {
            Alert.alert('Aplication updated succesfully', '', [{ text: 'Ok', onPress: () => this.props.closeModal() }])
            this.props.reRender02();

        } else {
            Alert.alert('Something went wrong registering user', '', [{ text: 'Ok' }])
        }
    };

    onClick() {
        switch (this.props.route) {
            case 'register':
                this.props.closeModal();
                break;
            case 'logued':
                this.props.closeModal();
                this.props.navigation.navigate('ManageRequest');
                break;
            default:
                this.props.closeModal();
                break;
        }

    }


    render() {
        const { user, visible, error, associations, leagues, golf_clubs, ghinValidated } = this.props;
        return (
            <Modal
                presentationStyle='fullScreen'
                animationType="slide"
                visible={visible}
                onRequestClose={() => {
                    this.props.closeModal()
                    // Alert.alert("Modal has been closed.");
                }}
                onTouchOutside={() => {
                    this.props.closeModal()
                }}
            >
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={styles.header}>
                        <TouchableOpacity
                            onPress={() => this.props.closeModal()}
                            style={styles.iconButton}
                        >
                            <Ionicons
                                name={'md-close'}
                                size={25}
                            />
                        </TouchableOpacity>
                        <Text style={styles.headerTitle}>{translate('requestDetails')}</Text>
                    </View>

                    <ScrollView>

                        <View style={styles.wrapper}>
                            <View style={styles.inputContainer}>

                                {/* Form Error Message */}

                                {error && <AlertMessage type={"danger"} message={error} />}

                                {/* Form */}

                                {!this.state.showSuccesModal &&
                                    <Formik
                                        initialValues={user}
                                        validationSchema={validationSchema}
                                        onSubmit={(values, actions) => this.fedoGolfSignUp(values, actions)}
                                    >
                                        {props => {
                                            return (
                                                <React.Fragment>
                                                    {/* Form Inputs */}

                                                    <FormInput
                                                        onChangeText={props.handleChange("firstname")}
                                                        onBlur={props.handleBlur("firstname")}
                                                        value={props.values.firstname}
                                                        placeholder={translate('firstName')}
                                                        additionalStyles={styles.fedogolfInputs}
                                                        theme={'black'}
                                                        error={props.touched.firstname && props.errors.firstname}
                                                        theme={"#00000094"}
                                                    />

                                                    <FormInput
                                                        onChangeText={props.handleChange("lastname")}
                                                        onBlur={props.handleBlur("lastname")}
                                                        value={props.values.lastname}
                                                        placeholder={translate('lastName')}
                                                        additionalStyles={styles.fedogolfInputs}
                                                        theme={'black'}
                                                        error={props.touched.lastname && props.errors.lastname}
                                                        theme={"#00000094"}
                                                    />

                                                    <FormInput
                                                        onChangeText={props.handleChange("id_number")}
                                                        onBlur={props.handleBlur("id_number")}
                                                        value={props.values.id}
                                                        placeholder={translate('idPassport')}
                                                        // keyboardType="number-pad"
                                                        additionalStyles={styles.fedogolfInputs}
                                                        theme={'black'}
                                                        error={props.touched.id_number && props.errors.id_number}
                                                        theme={"#00000094"}
                                                    />

                                                    <FormInput
                                                        onChangeText={value => props.setFieldValue('dob', value)}
                                                        value={props.values.dob}
                                                        theme={"#00000094"}
                                                        type={'date'}
                                                        placeholder={translate('birthday')}
                                                        additionalStyles={{ ...styles.fedogolfInputs, height: 44 }}
                                                        data={this.state.countries}
                                                        editable={false}
                                                        error={props.touched.dob && props.errors.dob}
                                                    />

                                                    <FormInput
                                                        onChangeText={props.handleChange("address")}
                                                        onBlur={props.handleBlur("address")}
                                                        value={props.values.address}
                                                        placeholder={translate('address')}
                                                        additionalStyles={styles.fedogolfInputs}
                                                        theme={'black'}
                                                        error={props.touched.nationality && props.errors.address}
                                                        theme={"#00000094"}
                                                    />

                                                    <FormInput
                                                        onChangeText={props.handleChange("nationality")}
                                                        onBlur={props.handleBlur("nationality")}
                                                        value={props.values.nationality}
                                                        placeholder={translate('nationality')}
                                                        additionalStyles={styles.fedogolfInputs}
                                                        theme={'black'}
                                                        error={props.touched.nationality && props.errors.nationality}
                                                        theme={"#00000094"}
                                                    />

                                                    <FormInput
                                                        onChangeText={props.handleChange("mobile_phone")}
                                                        onBlur={props.handleBlur("mobile_phone")}
                                                        value={props.values.mobile_phone}
                                                        placeholder={translate('mobilePhone')}
                                                        additionalStyles={styles.fedogolfInputs}
                                                        theme={'black'}
                                                        error={props.touched.mobile_phone && props.errors.mobile_phone}
                                                        theme={"#00000094"}
                                                    />

                                                    <FormInput
                                                        onChangeText={props.handleChange("office_phone")}
                                                        onBlur={props.handleBlur("office_phone")}
                                                        value={props.values.office_phone}
                                                        placeholder={translate('officePhone')}
                                                        additionalStyles={styles.fedogolfInputs}
                                                        theme={'black'}
                                                        error={props.touched.office_phone && props.errors.office_phone}
                                                        theme={"#00000094"}
                                                    />
                                                    <FormInput
                                                        onChangeText={props.handleChange("home_phone")}
                                                        onBlur={props.handleBlur("home_phone")}
                                                        value={props.values.home_phone}
                                                        placeholder={translate('homePhone')}
                                                        // type="telephoneNumber"
                                                        keyboardType="number-pad"
                                                        additionalStyles={styles.fedogolfInputs}
                                                        error={props.touched.home_phone && props.errors.home_phone}
                                                        theme={"#00000094"}
                                                    />

                                                    <FormInput
                                                        onChangeText={props.handleChange("email")}
                                                        onBlur={props.handleBlur("email")}
                                                        value={props.values.email}
                                                        placeholder={translate('email')}
                                                        type="emailAddress"
                                                        additionalStyles={styles.fedogolfInputs}
                                                        editable={false}
                                                        error={props.touched.email && props.errors.email}
                                                        theme={"#00000094"}
                                                    />
                                                    <View>
                                                        <FormInput
                                                            onChangeText={value => props.setFieldValue('golf_club_id', value)}
                                                            value={props.values.golf_club_id}
                                                            type={'picker'}
                                                            theme={'light'}
                                                            placeholder="Home Club *"
                                                            additionalStyles={styles.fedogolfInputs}
                                                            data={golf_clubs}
                                                            editable={false}
                                                            error={props.touched.golf_club_id && props.errors.golf_club_id}
                                                        />
                                                    </View>
                                                    {
                                                        ghinValidated ? (
                                                            <View style={styles.ghinValidatedContainer}>
                                                                <Text style={styles.ghinTitle}>Ghin Number</Text>
                                                                <Text style={styles.ghinNumber}>{props.values.ghin_number}</Text>
                                                            </View>
                                                        ) : (
                                                                <FormInput
                                                                    onChangeText={props.handleChange('ghin_number')}
                                                                    value={props.values.ghin_number}
                                                                    theme={"#00000094"}
                                                                    maxLength={7}
                                                                    placeholder="Ghin Number"
                                                                    keyboardType="number-pad"
                                                                    additionalStyles={styles.fedogolfInputs}
                                                                    error={props.touched.ghin_number && props.errors.ghin_number}
                                                                />
                                                            )
                                                    }

                                                    <FormInput
                                                        // key={'association_name'}
                                                        onChangeText={value => props.setFieldValue('association_id', value)}
                                                        value={props.values.association_id}
                                                        type={'picker'}
                                                        theme={'light'}
                                                        placeholder={translate('association')}
                                                        additionalStyles={styles.fedogolfInputs}
                                                        data={associations}
                                                        editable={false}
                                                        error={props.touched.association_id && props.errors.association_id}
                                                    />
                                                    <FormInput
                                                        // key={'team_name'}
                                                        onChangeText={value => props.setFieldValue('league_id', value)}
                                                        value={props.values.league_id}
                                                        data={leagues}
                                                        items={[]}
                                                        type={'picker'}
                                                        theme={'light'}
                                                        placeholder={translate('league')}
                                                        additionalStyles={styles.fedogolfInputs}
                                                        editable={false}
                                                        error={props.touched.league_id && props.errors.league_id}
                                                    />
                                                    <FormInput
                                                        onChangeText={props.handleChange("profession")}
                                                        onBlur={props.handleBlur("profession")}
                                                        value={props.values.profession}
                                                        placeholder={translate('profession')}
                                                        additionalStyles={styles.fedogolfInputs}
                                                        theme={'black'}
                                                        error={props.touched.profession && props.errors.profession}
                                                        theme={"#00000094"}
                                                    />

                                                    <FormInput
                                                        onChangeText={props.handleChange("company_name")}
                                                        onBlur={props.handleBlur("company_name")}
                                                        value={props.values.company_name}
                                                        placeholder={translate('companyName')}
                                                        additionalStyles={styles.fedogolfInputs}
                                                        error={props.touched.company_name && props.errors.company_name}
                                                        theme={"#00000094"}
                                                    />

                                                    <FormInput
                                                        onChangeText={props.handleChange("job_title")}
                                                        onBlur={props.handleBlur("job_title")}
                                                        value={props.values.job_title}
                                                        placeholder={translate('jobTitle')}
                                                        additionalStyles={styles.fedogolfInputs}
                                                        error={props.touched.job_title && props.errors.job_title}
                                                        theme={"#00000094"}
                                                    />

                                                    <FormInput
                                                        // onChangeText={handleChange("privacyPolicy")}
                                                        onValueChange={value => props.setFieldValue('sdcc', value)}
                                                        value={props.values.sdcc}
                                                        onBlur={props.handleBlur("sdcc")}
                                                        type="switch"
                                                        error={props.touched.sdcc && props.errors.sdcc}
                                                        theme="light"
                                                        placeholder={translate('areYouMemberSDCC')}
                                                    // action={() => console.log}
                                                    />

                                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 20, paddingVertical: 15 }}>
                                                        <Button
                                                            label={translate('male')}
                                                            theme={"lightblue"}
                                                            additionalStyles={{ ...styles.genderBtn, ...(this.state.values.gender === 1 ? styles.genderActiveBtn : {}) }}
                                                            action={() => this.switchGender('male')}
                                                        />
                                                        <Button
                                                            label={translate('female')}
                                                            theme={"lightblue"}
                                                            additionalStyles={{ ...styles.genderBtn, ...(this.state.values.gender === 2 ? styles.genderActiveBtn : {}) }}
                                                            action={() => this.switchGender('female')}
                                                        />
                                                    </View>

                                                    {/* Submit Buttons */}
                                                    {this.props.status !== 'approve' &&
                                                        <Button
                                                            label={translate('update')}
                                                            isLoading={this.state.loading}
                                                            theme={"darkblue"}
                                                            additionalStyles={styles.submitButton}
                                                            // action={() => props.handleSubmit()}
                                                            action={() => this.fedoGolfSignUp(props.values)}
                                                        />
                                                    }


                                                </React.Fragment>
                                            )
                                        }}
                                    </Formik>
                                }

                                {/* {this.state.showSuccesModal &&
                                    <View style={styles.successContainer}>
                                        <SuccessMsg
                                            onClick={() => this.onClick()}
                                            route={this.props.route}
                                        // onClick={() => this.props.closeModal()}
                                        />
                                    </View>
                                } */}

                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </Modal>
        );
    }
}

// Form initial values and validaiton schemas

const initialValues = {

};

const validationSchema = yup.object().shape({
    firstname: yup.string().required("This field is required"),
    lastname: yup.string().required("This field is required"),
    ghin: yup.string().matches(/^\d+$/, 'This field is numeric only'),
    golf_club_id: yup.string().required('This field is required'),
    association_id: yup.string().required('This field is required'),
    league_id: yup.string().required('This field is required'),
    home_phone: yup.string().matches(/^\d+$/, 'This field is numeric only'),
    mobile_phone: yup.string().matches(/^\d+$/, 'This field is numeric only').required('This field is required'),
    office_phone: yup.string().matches(/^\d+$/, 'This field is numeric only'),
    dob: yup.string().required('This field is required')
    // phone: yup.string().matches(/^\d+$/, 'This field is numeric only').required('This field is required'),
});
