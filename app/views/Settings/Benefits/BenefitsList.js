import React, { Component } from 'react';
import { View, Text, FlatList, Modal, StyleSheet, TouchableWithoutFeedback, SafeAreaView, Platform } from 'react-native';
import { Container, Header, Content, Tab, Tabs, ScrollableTab, TabHeading } from 'native-base';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as benefits from '../../../store/actions/benefits';
import MembershipDetailHeader from '../../../components/UI/MembershipDetailHeader/MembershipDetailHeader';
import BenefitsListData from '../../../components/UI/BenefitsComponents/BenefitsListData';
import UsedBenefitsList from '../../../components/UI/BenefitsComponents/UsedBenefitsList';
import FederatesBenefitsList from '../../../components/UI/BenefitsComponents/FederatesBenefitsList';
import FilterButtons from '../../../components/UI/CustomTabs/FilterButtons';
import { verticalScale, scale, heightPercentageToDP, moderateScale } from '../../../utilities/ScalingScreen';
import { COLORS } from '../../../config/constants';



class BenefitsList extends Component {
  static navigationOptions = {
    headerShown: false
  }
  constructor(props) {
    super(props);
    this.state = {
      benefitsList: [],
      loading: true,
      showModal: false,
      modalDetail: {},
      refreshing: false,
      activeIndex: 1
    };
    this.renderList = this.renderList.bind(this);
    this.getBenefitsList = this.getBenefitsList.bind(this);
  }

  componentDidMount() {
    this.getBenefitsList();
  }

  // async componentDidUpdate() {
  //   const response = await this.props.getAllBenefits(this.props.user.id, this.props.session.accessToken);

  //   if (this.state.benefitsList.length !== response.length) {
  //     this.setState({
  //       benefitsList: response,
  //       loading: false,
  //       refreshing: false
  //     })
  //   }

  // }

  async getBenefitsList() {
    const response = await this.props.getAllBenefits(this.props.user.id, this.props.session.accessToken);

    if (this.props.benefits.allbenefits.success) {
      this.setState({
        benefitsList: this.props.benefits.allbenefits.data,
        loading: false,
        refreshing: false
      })
      console.log(this.state.benefitsList);
    } else {
      this.setState({
        benefitsList: [],
        loading: false,
        refreshing: false
      })
    }
  }

  handleRefresh = () => {
    this.setState({
      refreshing: true,
      benefitsList: [...this.state.benefitsList]
    }, () => {
      this.getBenefitsList();
    }
    );
  }

  reRender() {
    this.getBenefitsList();
  }

  renderList(index) {
    this.setState({
      activeIndex: index,
    });
    console.log('index', index)
  }

  render() {
    return (
      <View style={styles.container}>

        <View style={styles.header}>
          <Text style={styles.headerTitle}>My Benefits</Text>
        </View>

        <FilterButtons
          data={filterButtons}
          activeIndex={this.state.activeIndex}
          renderList={(e) => this.renderList(e)}
        />

        <View style={styles.content}>
          {this.state.activeIndex === 1 ? (
            <FederatesBenefitsList
              membershipList={this.props.session.user.data}
              userData={this.props.session.user}
              benefitsList={this.state.benefitsList}
              refreshing={this.state.refreshing}
              onRefresh={this.handleRefresh}
              loading={this.state.loading}
              navigation={this.props.navigation}
              reRender={() => this.reRender()}
              activeFilter={0}
            />
          ) : this.state.activeIndex === 2 ? (
            <BenefitsListData
              benefitsList={this.state.benefitsList}
              userData={this.props.session.user}
              refreshing={this.state.refreshing}
              onRefresh={this.handleRefresh}
              loading={this.state.loading}
              reRender={() => this.reRender()}
              activeFilter={0}
            />
          ) : (
                <UsedBenefitsList
                  benefitsList={this.state.benefitsList}
                  userData={this.props.session.user}
                  refreshing={this.state.refreshing}
                  onRefresh={this.handleRefresh}
                  loading={this.state.loading}
                  reRender={() => this.reRender()}
                  activeFilter={1}
                />
              )
          }
        </View>

      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
    user: state.session.user.data,
    benefits: state.benefits
  };
}

function mapDispatchProps(dispatch) {
  return bindActionCreators(benefits, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchProps
)(BenefitsList);

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  header: {
    width: '100%',
    height: Platform.OS === 'ios' ? heightPercentageToDP('20%') : heightPercentageToDP('17%'),
    padding: scale(10),
    flexDirection: 'row',
    alignContent: "center",
    // backgroundColor: 'red'
  },
  headerTitle: {
    fontSize: Platform.OS === 'ios' ? moderateScale(30) : moderateScale(25),
    marginTop: Platform.OS === 'ios' ? verticalScale(70) : verticalScale(50),
    marginLeft: Platform.OS === 'ios' ? verticalScale(5) : verticalScale(5),
    fontWeight: 'bold',
    color: COLORS.softBlack
  },
  content: {
    backgroundColor: 'white',
    width: '100%',
    height: heightPercentageToDP('68%')
  }
})


const filterButtons = [
  {
    key: 1,
    id: '1',
    name: 'Federated',
  },
  {
    key: 2,
    id: '2',
    name: 'Others',
  },
  {
    key: 3,
    id: '3',
    name: 'Used',
  }
]