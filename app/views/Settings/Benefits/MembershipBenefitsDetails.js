import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, ScrollView, FlatList, TouchableOpacity, Image } from 'react-native';
import { moderateScale, verticalScale, scale } from '../../../utilities/ScalingScreen';
import { COLORS } from '../../../config/constants';
import Ionicons from "react-native-vector-icons/dist/Ionicons";

import Modal from 'react-native-modal';
import PricesByMembership from '../../../components/lists/CoursePricesMemberships/PricesByMembership';

class MembershipBenefitsDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            infoPrice: false,
            data: {}
        };
        this.showPriceModal = this.showPriceModal.bind(this);
    }

    showPriceModal(visible, data) {
        this.setState({
            infoPrice: visible,
            data: data
        })
    }

    render() {
        const { allInfo } = this.props.route.params;
        console.log(allInfo)
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView contentContainerStyle={styles.contentScroll} style={styles.content}>

                    <View style={styles.header} />

                    <Text style={styles.title}>{allInfo.name}</Text>

                    <Text style={styles.subtitle}>Membership Courses Price: </Text>

                    <FlatList
                        data={allInfo.golf_courses}
                        keyExtractor={(item) => item.id.toString()}
                        contentContainerStyle={{ paddingBottom: verticalScale(20) }}
                        renderItem={({ item }) =>
                            <PricesByMembership
                                //     onPress={() => this.showPriceModal(true, item)}
                                membershipName={item.course_name}
                                price={item.pivot.price}
                                currency={item.pivot.currency}
                            />
                        }
                    />

                    <Modal
                        isVisible={this.state.infoPrice}
                        style={styles.modalContainer}
                    >
                        <View style={styles.modalContent}>

                            <TouchableOpacity
                                onPress={(e) => this.props.close(e)}
                                style={{
                                    position: 'absolute',
                                    top: verticalScale(0),
                                    right: moderateScale(15),
                                    zIndex: 1
                                }}
                            >
                                <Ionicons
                                    name='ios-close'
                                    size={moderateScale(32)}
                                />
                            </TouchableOpacity>

                            <Text style={styles.title}>{this.state.data.course_name}</Text>
                            {/* 
                            <View style={styles.wrapper} >
                                <Text style={styles.membershipTitle}>Weekends Price</Text>
                                <Text style={styles.price}>Price: {this.state.data.pivot.weekend}</Text>
                            </View> */}

                        </View>
                    </Modal>

                </ScrollView>
            </SafeAreaView>
        );
    }
}

export default MembershipBenefitsDetails;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    header: {
        // width: '100%',
        height: verticalScale(60),
        paddingHorizontal: moderateScale(20),
        paddingVertical: verticalScale(10)
    },
    content: {
        // backgroundColor: 'red',
        // width: '100%',
        // height: '100%',
        // paddingHorizontal: scale(20)
    },
    contentScroll: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: moderateScale(20),
        fontWeight: '600',
        color: COLORS.black,
    },
    subtitle: {
        fontSize: moderateScale(18),
        color: COLORS.airBnbGrey,
        marginBottom: verticalScale(10),
        marginTop: verticalScale(20)
    },
    wrapper: {
        marginLeft: moderateScale(15)
    },
    membershipTitle: {
        color: COLORS.darkgrey,
        fontSize: moderateScale(14),
        fontWeight: '600'
    },
    price: {
        color: COLORS.midgreen,
        fontSize: moderateScale(14),
        marginTop: verticalScale(5)
    },
    modalContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        borderRadius: 6,
        padding: scale(10),
    },
    modalContent: {
        backgroundColor: '#fff',
        padding: 15,
        width: moderateScale(300),
        margin: 25,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 6
    },
})