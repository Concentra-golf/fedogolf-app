import React, { Component } from 'react';
import { Text, View } from 'react-native';
import SettingsUserCard from '../../components/cards/SettingsUserCard/SettingsUserCard';
import { ScrollView } from 'react-native-gesture-handler';
import QRCode from 'react-native-qrcode-svg';

// Modules

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as session from '../../store/actions/session';

class GolferQrCode extends Component {
  render() {
    const { user } = this.props
    return (
      <ScrollView>
        <View >
          <SettingsUserCard user={user} />
          <View style={{ flex: 1, alignItems: 'center', marginTop: 100 }}>
            <QRCode
              value={'123456'}
              // value={JSON.stringify(user)}
              size={200}
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
    user: state.session.user.data
  };
}

function mapDispatchProps(dispatch) {
  return bindActionCreators(session, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchProps,
)(GolferQrCode);

