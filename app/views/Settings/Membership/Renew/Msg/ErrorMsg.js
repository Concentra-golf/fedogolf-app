import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal'
import { COLORS } from '../../../../../config/constants';
import { moderateScale, scale, verticalScale } from '../../../../../utilities/ScalingScreen';
import ErrorPayment from '../../../../../components/UI/LottieAnimation/ErrorPayment';
import Error from '../../../../../components/UI/LottieAnimation/Error';
import { DotIndicator } from 'react-native-indicators';

import * as RNLocalize from 'react-native-localize';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../../../translations/translation';

class ErrorMsg extends Component {
    constructor(props) {
        super(props);
        setI18nConfig(); 
        this.state = {
        };
    }

    componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);
      }

    render() {
        const { errorType } = this.props;
        let title = '';
        let msg = '';
        let button = '';
        if (errorType === 'payment') {
            title = translate('paymentError')
            msg = translate('paymentErrorMsg')
            button = translate('paymentErrorBtn')
        } else if (errorType === 'form') {
            title = translate('signUpError')
            msg = translate('signYpErrorMsg')
            button = translate('signUpErrorBtn')
        }
        return (
            <Modal
                isVisible={this.props.isVisible}
                style={styles.modalContainer}
            >
                {this.props.isLoading ? (
                    <DotIndicator color={COLORS.red} />
                ) : (
                        <View style={styles.container}>
                            {errorType === 'payment' ? (
                                <ErrorPayment />
                            ) : (
                                    <Error />
                                )}

                            <Text style={styles.title}>{title}</Text>
                            <Text style={styles.subTitle}>{msg}</Text>

                            {errorType === 'form' ? (
                                <View style={styles.buttonContainer}>
                                    <TouchableOpacity
                                        onPress={() => this.props.action()}
                                        style={[styles.doneButton, { backgroundColor: 'white', borderColor: COLORS.red, borderWidth: 1 }]}
                                    >
                                        <Text style={[styles.doneButtonText, { color: COLORS.red }]}>{button}</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        onPress={() => this.props.action('later')}
                                        style={styles.doneButton}
                                    >
                                        <Text style={styles.doneButtonText}>{translate('tryLater')}</Text>
                                    </TouchableOpacity>
                                </View>
                            ) : (
                                    <TouchableOpacity
                                        onPress={() => this.props.action()}
                                        style={styles.doneButtonTwo}
                                    >
                                        <Text style={styles.doneButtonText}>{button}</Text>
                                    </TouchableOpacity>
                                )
                            }
                        </View>
                    )
                }
            </Modal>
        );
    }
}

export default ErrorMsg;

const styles = StyleSheet.create({
    modalContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    container: {
        backgroundColor: COLORS.white,
        width: moderateScale(320),
        // height: '90%',
        borderRadius: scale(20),
        alignItems: 'center',
        padding: scale(20)
    },
    title: {
        fontSize: moderateScale(20),
        marginVertical: verticalScale(10),
        fontWeight: 'bold',
        color: COLORS.red
    },
    subTitle: {
        fontSize: moderateScale(14),
        marginVertical: verticalScale(5),
        textAlign: 'center',
        color: COLORS.airBnbLightGrey
    },
    doneButton: {
        backgroundColor: COLORS.red,
        width: moderateScale(130),
        height: verticalScale(40),
        marginVertical: verticalScale(20),
        marginHorizontal: moderateScale(10),
        justifyContent: 'center',
        alignItems: 'center'
    },
    doneButtonTwo: {
        backgroundColor: COLORS.red,
        width: moderateScale(250),
        height: verticalScale(40),
        marginVertical: verticalScale(20),
        justifyContent: 'center',
        alignItems: 'center'
    },
    doneButtonText: {
        color: COLORS.white,
        fontWeight: 'bold',
        fontSize: moderateScale(16)
    },
    buttonContainer: {
        flexDirection: 'row'
    }
})