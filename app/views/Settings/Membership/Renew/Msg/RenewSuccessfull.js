import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal'
import { COLORS } from '../../../../../config/constants';
import { moderateScale, scale, verticalScale } from '../../../../../utilities/ScalingScreen';
import Success from '../../../../../components/UI/LottieAnimation/Success';
import { DotIndicator } from 'react-native-indicators';

import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../../../translations/translation';
import * as RNLocalize from 'react-native-localize';

class RenewSuccessfull extends Component {
    constructor(props) {
        super(props);
        setI18nConfig();
        this.state = {
        };
    }

    componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);
    }

    render() {
        const { route } = this.props;
        let msg = '';
        if (route === 'RequestDetails') {
            msg = translate('requestDetailsMsg')
        } else if (route === 'MembershipCard' || route === 'renew') {
            msg = translate('renewMsg')
        } else {
            msg = translate('paymentSuccessMsg')
        }
        return (
            <Modal
                isVisible={this.props.isVisible}
                style={styles.modalContainer}
            >
                {this.props.isLoading ? (
                    <DotIndicator color={COLORS.green} />
                ) : (
                        <View style={styles.container}>
                            <Success />

                            <Text style={styles.title}>{translate('success')}</Text>
                            <Text style={styles.subTitle}>{msg}</Text>

                            <TouchableOpacity
                                onPress={() => this.props.action()}
                                style={styles.doneButton}
                            >
                                <Text style={styles.doneButtonText}>{translate('done')}</Text>
                            </TouchableOpacity>
                        </View>
                    )
                }
            </Modal>
        );
    }
}

export default RenewSuccessfull;

const styles = StyleSheet.create({
    modalContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    container: {
        backgroundColor: COLORS.white,
        width: moderateScale(320),
        // height: '90%',
        borderRadius: scale(20),
        alignItems: 'center',
        padding: scale(20)
    },
    title: {
        fontSize: moderateScale(20),
        marginVertical: verticalScale(10),
        fontWeight: 'bold',
        color: COLORS.green
    },
    subTitle: {
        fontSize: moderateScale(14),
        marginVertical: verticalScale(5),
        textAlign: 'center',
        color: COLORS.airBnbLightGrey
    },
    doneButton: {
        backgroundColor: COLORS.green,
        width: moderateScale(250),
        height: verticalScale(40),
        marginVertical: verticalScale(20),
        justifyContent: 'center',
        alignItems: 'center'
    },
    doneButtonText: {
        color: COLORS.white,
        fontWeight: 'bold',
        fontSize: moderateScale(16)
    }
})