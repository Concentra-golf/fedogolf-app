import React, { Component } from 'react';
import { View, Text, StyleSheet, Platform, SafeAreaView, ScrollView } from 'react-native';
import { moderateScale, verticalScale, scale } from '../../../../utilities/ScalingScreen';
import { COLORS } from '../../../../config/constants';

import * as payment from '../../../../store/actions/payment';
import * as session from '../../../../store/actions/session';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import StepIndicator from 'react-native-step-indicator';
import Modal from 'react-native-modal';
import { DotIndicator } from 'react-native-indicators';

import MembershipFees from './Modules/MembershipFees';
import RenewPayment from './Modules/RenewPayment';
import MessageScreen from './Modules/MessageScreen';
import RenewSuccessfull from './Msg/RenewSuccessfull';
import ConfirmAssociationModal from '../../../../components/UI/Modals/ConfirmAssociationModal';
import ErrorMsg from './Msg/ErrorMsg';

import { translate, setI18nConfig, handleLocalizationChange } from '../../../../translations/translation';
import * as RNLocalize from 'react-native-localize';

class OptionsSelector extends Component {
    constructor(props) {
        super(props);
        setI18nConfig(); 
        this.state = {
            title: translate('membershipTitle'),
            step: 0,
            loading: this.props.renewPayment.isPaymentSuccess.isLoading,
            success: false,
            goToCheckout: false,
            recurrent: false,
            feesType: null,
            stepCount: 3,
            assoConfirm: false,
            feedInfo: {},
            gender: 'male',
            errorType: 'form',
            error: false,
        };
        this.renderStpes = this.renderStpes.bind(this);
        this.selectedItem = this.selectedItem.bind(this);
    }

    //CONFIGURING MEMBERSHIPS FEES ROUTES
    componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);
        const route = this.props.route.params.route;
        if (route === 'RequestDetails' || route === 'MembershipCard') {
            this.setState({
                step: 1,
                title: translate('paymentMethodTitle'),
                stepCount: 2
            })
        }
    }


    //RENDER SCREENS STEPS
    renderStpes(e) {
        if (e === 0) {
            this.setState({
                step: 0,
                title: translate('membershipTitle'),
            })
        } else if (e === 1) {
            this.setState({
                step: 1,
                title: translate('paymentMethodTitle'),
            })
        } else if (e === 2) {
            this.setState({
                step: 2,
                title: 'Checkout'
            })
        }
    }

    //CONFIGURING MEMBERSHIPS FEES 
    selectedItem(e) {
        switch (e.type) {
            case 'standard':
                this.setState({
                    goToCheckout: false,
                    recurrent: false,
                })
                break;
            case 'SDCC':
                this.setState({
                    goToCheckout: true
                })
                break;
            case 'association':
                this.setState({
                    goToCheckout: true
                })
                break;
            case 'recurrent':
                this.setState({
                    goToCheckout: false,
                    recurrent: true
                })
                break;
        }

        this.setState({
            feesType: e.type,
            selectedItem: e
        })
    }

    paymentMethod(e) {
        console.log(e)
        this.setState({
            paymentMethod: e
        })
    }

    async renewPaymemt(e) {
        this.setState({ loading: true })

        const { data, renewPaymentInfo, } = this.props.route.params;
        console.log(e)

        const requestPayment_id = renewPaymentInfo.id;
        const paymentInfo = e

        let paymentObject = {};
        if (paymentInfo.paymentMethod === 'card') {
            paymentObject = {
                amount: paymentInfo.amount,
                paymentType: paymentInfo.type,
                type: paymentInfo.paymentMethod,
                cardNumber: this.state.paymentMethod.cardNumber,
                expiration: this.state.paymentMethod.expiration,
                cvc: this.state.paymentMethod.cvc,
                card_token: this.state.paymentMethod.token === undefined ? '' : this.state.paymentMethod.token,
                reason: "renew_membership",
            }
        } else {
            paymentObject = {
                type: '',
                amount: paymentInfo.amount,
                paymentType: paymentInfo.type,
                reason: "renew_membership",
                file: paymentInfo.file,
            }
        }

        await this.props.simulatePayment(requestPayment_id, null, paymentObject, this.props.session.accessToken);

        if (this.props.renewPayment.simulatePaymentResponse.success) {
            this.setState({
                loading: false,
            })
            this.props.paymentSuccess(true);
        } else {
            this.setState({
                loading: false,
                errorType: 'payment',
                error: true,
            })
            // Alert.alert('Something went wrong with the payment process', '', [{ text: 'Ok' }])
        }
    }

    async sentRequetPayment(e, renewPaymentInfo) {
        console.log(e, renewPaymentInfo)
        if (this.props.route.params.route === 'renew') {
            this.setState({ loading: true })
            const flowType = e.type;
            const renewPaymentInfo = this.props.route.params.renewPaymentInfo.id;
            await this.props.renewPaymentRequest(this.props.session.user.data.id, this.props.route.params.data.id, flowType, renewPaymentInfo, this.props.session.accessToken);
            if (this.props.renewPayment.renewPaymentResponse.success) {
                // TRIGGER SUCCESS MODAL WHEN PAYMENT
                this.setState({ loading: false })
                this.props.paymentSuccess(true);
            } else {
                this.setState({ loading: false })
                this.setState({
                    loading: false,
                    errorType: 'payment',
                    error: true,
                })
                // Alert.alert('Something went wrong whit the payment process', '', [{ text: 'Ok' }])
            }
        } else {
            if (e.type === 'association') {
                this.setState({ assoConfirm: true, feedInfo: e })
            } else {
                this.setState({ loading: true })
                this.fedoGolfSignUp(e)
            }
        }
    }

    async fedoGolfSignUp(paymentInfo) {
        console.log('FEDERATION STARTING VALIDATION')

        const route = this.props.route.params.route;

        const gender = this.state.gender;
        const values = route === 'RequestDetails' ? this.props.route.params.data : this.props.route.params.formValues;
        const payment = paymentInfo;

        const data = {
            firstname: values.firstname,
            lastname: values.lastname,
            id_number: values.id_number,
            email: values.email,
            dob: values.dob,
            nationality: values.nationality,
            home_phone: values.home_phone,
            mobile_phone: values.mobile_phone,
            golf_club_id: values.golf_club_id,
            ghin_number: values.ghin_number,
            association_id: payment.type === 'association' ? paymentInfo.association_id : values.association_id,
            league_id: values.league_id,
            profession: values.profession,
            office_phone: values.office_phone,
            address: values.address,
            company_name: values.company_name,
            gender: gender,
            job_title: values.job_title,
            sdcc: values.sdcc || false,
            federation_id: 1,
            membership_id: values.memberType,
            amount: payment.amount,
            flow_type: payment.type,
        };
        console.log('data from fedogolf', data)
        await this.props.fedogolfSignUp(this.props.session.user.accessToken, data);
        // Close Form if the user is logged in\
        if (this.props.session.fedogolfUser.success) {
            console.log('FEDERATION SIGN UP ========== SUCCESS')
            // SIMULATING PAYMENT PROCESS FOR STANDAR AND RECURRENT FEES
            if (payment.type === 'standard' || payment.type === 'recurrent') {
                const requestPayment = this.props.session.fedogolfUser.data.requestPayment;
                const application_id = this.props.session.fedogolfUser.data.application.id;
                let payments = {};
                if (payment.paymentMethod === 'card') {
                    payments = {
                        type: 'card',
                        amount: paymentInfo.amount,
                        paymentType: payment.type, // the payment type is what you select on first (standar, sdcc, etc)
                        cardNumber: this.state.paymentMethod.cardNumber,
                        expiration: this.state.paymentMethod.expiration,
                        cvc: this.state.paymentMethod.cvc,
                        card_token: this.state.paymentMethod.token === undefined ? '' : this.state.paymentMethod.token,
                        reason: "get_membership",
                    }
                } else {
                    payments = {
                        type: '',
                        amount: paymentInfo.amount,
                        paymentType: payment.type,
                        reason: "get_membership",
                        file: paymentInfo.file,
                    }
                }
                // SIMULATING PAYMENT PROCESS
                console.log('PAYMENT ==========', payments)

                // await this.props.simulatePayment(requestPayment.id, application_id, this.props.session.accessToken);
                await this.props.simulatePayment(requestPayment.id, application_id, payments, this.props.session.accessToken);
                if (this.props.renewPayment.simulatePaymentResponse.success) {
                    this.setState({ loading: false })
                    console.log('FEDERATION PAYMENT SIGN UP ========== SUCCESS')
                    await this.props.paymentSuccess(true);
                } else {
                    this.setState({
                        loading: false,
                        errorType: 'payment',
                        error: true,
                    })
                    // Alert.alert('Something went wrong with the payment process', '', [{ text: 'Ok' }])
                }
            } else {
                this.setState({ loading: false })
                // SENT FROM WITHOUT PAY
                await this.props.paymentSuccess(true);
            }
        } else {
            this.setState({
                loading: false,
                error: true,
                errorType: 'form'
            })
            // Alert.alert('Something went wrong registering user', '', [{ text: 'Ok' }])
        }
    };


    validateAsosociation(e) {
        console.log(e)
        const feesSelected = this.state.feedInfo;
        const association_id = e.association_id;
        const association = {
            ...feesSelected,
            ...e
        }
        console.log(association);
        this.setState({ assoConfirm: false, loading: true })

        this.fedoGolfSignUp(association)
    }

    // WHEN THE USER IS PAYING FROM THE COMPONENT ManageRequestDetails
    async sentRequetsPaymentFromDetails(e) {
        this.setState({ loading: true })
        const paymentData = this.props.route.params.paymentData;
        const data = this.props.route.params.data;
        let payment = {};
        payments = {
            type: e.paymentMethod,
            amount: e.amount,
            reason: "get_membership",
            file: e.file,
            cardNumber: this.state.paymentMethod.cardNumber,
            expiration: this.state.paymentMethod.expiration,
            cvc: this.state.paymentMethod.cvc,
            card_token: this.state.paymentMethod.token === undefined ? '' : this.state.paymentMethod.token,
            paymentType: e.paymentType,

        }

        // SET ROUTE WHEN IS RENEWING MEMBERSHIP FROM COMPONENT FederationMembershipDetail
        if (this.props.route.params.route === "MembershipCard") {
            payments = {
                type: e.paymentMethod,
                amount: e.amount,
                reason: e.reason,
                file: e.file,
                cardNumber: this.state.paymentMethod.cardNumber,
                expiration: this.state.paymentMethod.expiration,
                cvc: this.state.paymentMethod.cvc,
                card_token: this.state.paymentMethod.token === undefined ? '' : this.state.paymentMethod.token,
                paymentType: e.paymentType,
            }
            await this.props.simulatePayment(paymentData.id, null, payments, this.props.session.accessToken);
        } else {
            await this.props.simulatePayment(data.request_payment_id, data.id, payments, this.props.session.accessToken);
        }

        if (this.props.renewPayment.simulatePaymentResponse.success) {
            // TRIGGER SUCCESS MODAL WHEN PAYMENT
            this.setState({ loading: false })
            this.props.paymentSuccess(true);
        } else {
            this.setState({
                loading: false,
                errorType: 'payment',
                error: true,
            })
            // Alert.alert('Something went wrong whit the payment process', '', [{ text: 'Ok' }])
        }
    }


    async onDone() {
        // SET ROUTE WHEN IS FROM COMPONENT ManageRequestDetails
        if (this.props.route.params.route === 'RequestDetails') {
            await this.props.paymentSuccess(false);
            this.props.route.params.reRender()
            this.props.route.params.reRender02();
            this.props.navigation.goBack()
        } else if (this.props.route.params.route === 'SignUpGolferInformation') {
            await this.props.paymentSuccess(false);
            this.props.navigation.popToTop()
        } else {
            // SET ROUTE WHEN IS FROM COMPONENT FederationSignUp Or FederationMembershipDetail
            await this.props.paymentSuccess(false);
            this.props.navigation.navigate('MembershipOptions')
        }
    }

    async onError(e) {
        console.log(e)
        console.log(this.state.errorType);
        if (this.state.errorType === 'form') {
            if (e === 'later') {
                this.props.navigation.navigate('FederationMembershipList')
            } else {
                this.setState({ error: false })
            }
        } else {
            this.setState({ error: false })
            this.props.navigation.popToTop()
            // this.props.navigation.navigate('ManageRequest');

        }

    }

    render() {
        const { data, renewPaymentInfo, route, memberType, paymentData } = this.props.route.params;
        return (
            <SafeAreaView style={styles.container}>
                <Text style={styles.title}>{this.state.title}</Text>
                <View style={styles.stepsContainer}>
                    <StepIndicator
                        customStyles={customStyles}
                        currentPosition={this.state.step}
                        stepCount={this.state.stepCount}
                    />
                </View>

                {this.state.step === 0 ? (
                    <View style={{ flex: 1 }}>
                        {this.state.loading ? (
                            <DotIndicator color={COLORS.green} />
                        ) : (
                                <MembershipFees
                                    data={data}
                                    renewPaymentInfo={renewPaymentInfo}
                                    changeSteps={(e) => this.renderStpes(e)}
                                    selectedItem={(e) => this.selectedItem(e)}
                                    route={route}
                                    memberType={memberType}
                                    goToCheckout={this.state.goToCheckout}
                                    recurrent={this.state.recurrent}
                                    sentRequetPayment={(e) => this.sentRequetPayment(e)}
                                />
                            )
                        }
                    </View>
                ) : this.state.step === 1 ? (
                    <ScrollView>
                        <RenewPayment
                            data={data}
                            renewPaymentInfo={renewPaymentInfo}
                            changeSteps={(e) => this.renderStpes(e)}
                            sentDatatoPay={(e) => this.sentDatatoPay(e)}
                            selectedItem={this.state.selectedItem}
                            paymentMethod={(e) => this.paymentMethod(e)}
                            memberType={memberType}
                            route={route}
                            goToCheckout={this.state.goToCheckout}
                            recurrent={this.state.recurrent}
                            feesType={this.state.feesType}
                            paymentData={paymentData}
                            sentRequetsPaymentFromDetails={(e) => this.sentRequetsPaymentFromDetails(e)}
                            sentRequetPayment={(e) => this.sentRequetPayment(e)}
                        />
                    </ScrollView>
                ) : (
                            <View style={{ flex: 1 }}>
                                {
                                    this.state.loading ? (
                                        <DotIndicator color={COLORS.green} />
                                    ) : (
                                            <MessageScreen
                                                data={data}
                                                renewPaymentInfo={renewPaymentInfo}
                                                changeSteps={(e) => this.renderStpes(e)}
                                                paymentMethod={this.state.paymentMethod}
                                                selectedItem={this.state.selectedItem}
                                                renewPaymemt={(e) => this.renewPaymemt(e)}
                                                loading={this.props.renewPayment.isPaymentSuccess.isLoading}
                                                memberType={memberType}
                                                route={route}
                                                sentRequetPayment={(e) => this.sentRequetPayment(e)}
                                                goToCheckout={this.state.goToCheckout}
                                                recurrent={this.state.recurrent}
                                                feesType={this.state.feesType}
                                                paymentData={paymentData}
                                                sentRequetsPaymentFromDetails={(e) => this.sentRequetsPaymentFromDetails(e)}
                                            />
                                        )
                                }
                            </View>
                        )
                }

                {/* SET SUCCESS MODAL */}
                <RenewSuccessfull
                    isVisible={this.props.renewPayment.isPaymentSuccess.success}
                    isLoading={this.props.loading}
                    navigation={this.props.navigation}
                    action={() => this.onDone()}
                    route={route}
                />

                <ErrorMsg
                    isVisible={this.state.error}
                    // isVisible={true}
                    isLoading={this.props.loading}
                    navigation={this.props.navigation}
                    action={(e) => this.onError(e)}
                    route={route}
                    errorType={this.state.errorType}
                />

                <Modal
                    isVisible={this.state.assoConfirm}
                    style={styles.modalContainer}
                >
                    <ConfirmAssociationModal
                        session={this.props.session}
                        validateAsosociation={(e) => this.validateAsosociation(e)}
                        close={() => this.setState({ assoConfirm: false })}
                    />
                </Modal>

            </SafeAreaView>
        );
    }
}


function mapStateToProps(state) {
    return {
        session: state.session,
        user: state.session.user.data,
        membershipfees: state.membership,
        renewPayment: state.payment
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators({ ...session, ...payment }, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchProps
)(OptionsSelector);



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    title: {
        fontSize: moderateScale(25),
        fontWeight: Platform.OS === 'ios' ? '600' : 'bold',
        color: COLORS.black,
        marginTop: verticalScale(50),
        width: '100%',
        textAlign: 'center'
    },
    stepsContainer: {
        marginTop: verticalScale(25),
        width: '100%',
    },
    stepCircle: {
        backgroundColor: 'blue',
        height: 30,
        width: 30,
        borderRadius: (30 / 2),
    },
    stepLine: {
        width: 50,
        height: 4,
        backgroundColor: 'black'
    },
    //Modal
    modal: {
        backgroundColor: COLORS.white,
        width: moderateScale(320),
        height: verticalScale(100)
    },
    modalContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        borderRadius: 6,
        padding: scale(10),
    },
})

const customStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize: 30,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: COLORS.green,
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: COLORS.green,
    stepStrokeUnFinishedColor: '#dedede',
    separatorFinishedColor: COLORS.green,
    separatorUnFinishedColor: '#dedede',
    stepIndicatorFinishedColor: COLORS.green,
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 0,
    currentStepIndicatorLabelFontSize: 0,
    stepIndicatorLabelCurrentColor: 'transparent',
    stepIndicatorLabelFinishedColor: 'transparent',
    stepIndicatorLabelUnFinishedColor: 'transparent',
    labelColor: '#999999',
    labelSize: 13,
    currentStepLabelColor: '#7eaec4',
}