import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Platform, SafeAreaView, ScrollView, FlatList, Image, TextInput } from 'react-native';
import { moderateScale, verticalScale, scale } from '../../../../../utilities/ScalingScreen';
import { COLORS } from '../../../../../config/constants';

import MaterialCommunityIcons from "react-native-vector-icons/dist/MaterialCommunityIcons";
import { DotIndicator } from 'react-native-indicators';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../../../translations/translation';
import * as RNLocalize from 'react-native-localize';

class MessageScreen extends Component {
    constructor(props) {
        super(props);
        setI18nConfig(); 
        this.state = {
            loading: false
        };
    }

    finishTransaction(e) {
        const { data, renewPaymentInfo, selectedItem, paymentMethod, feesType, paymentData } = this.props;
        let finishPaymet = {}

        if (this.props.route === "RequestDetails" || this.props.route === "MembershipCard") {
            switch (paymentMethod.method) {
                case "Credit Card":
                    finishPaymet = {
                        paymentMethod: 'card',
                        type: feesType,
                        amount: paymentData.amount,
                        reason: "renew_membership",
                        paymentType: paymentData.flow_type
                    }
                    break;
                case "Upload Voucher":
                    finishPaymet = {
                        paymentMethod: '',
                        type: feesType,
                        amount: paymentData.amount,
                        reason: "renew_membership",
                        file: paymentMethod.docFile,
                        paymentType: paymentData.flow_type
                    }
                    break;
            }
        } else {

            switch (paymentMethod.method) {
                case "Credit Card":
                    finishPaymet = {
                        paymentMethod: 'card',
                        type: feesType,
                        amount: selectedItem.amount,
                        item: selectedItem.id,
                        reason: "renew_membership",
                        flow_type: selectedItem.alias,
                        cardInfo: paymentMethod
                    }
                    break;
                case "Upload Voucher":
                    finishPaymet = {
                        paymentMethod: '',
                        type: feesType,
                        amount: selectedItem.amount,
                        item: selectedItem.id,
                        reason: "renew_membership",
                        file: paymentMethod.docFile,
                        flow_type: selectedItem.alias
                    }
                    break;
            }
        }

        if (this.props.route === 'FederationSignUp') {
            this.props.sentRequetPayment(finishPaymet)
        } else if (this.props.route === 'SignUpGolferInformation') {
            this.props.sentRequetPayment(finishPaymet)
        } else if (this.props.route === "RequestDetails" || this.props.route === "MembershipCard") {
            this.props.sentRequetsPaymentFromDetails(finishPaymet)
        } else {
            this.props.renewPaymemt(finishPaymet);
        }

    }

    render() {
        const { data, renewPaymentInfo, selectedItem, paymentMethod, loading, paymentData } = this.props;
        let paymentAmount = null;
        let alias = null;
        if (this.props.route === "RequestDetails" || this.props.route === "MembershipCard") {
            paymentAmount = paymentData.amount;
            alias = paymentData.flow_type
        } else {
            paymentAmount = selectedItem.amount;
            alias = selectedItem.alias
        }
        return (
            <View style={styles.container}>


                <View style={styles.content}>

                    <View style={styles.amountContainer}>
                        <Text style={styles.title}>{translate('amount')}</Text>
                        <Text style={styles.price}>RD$ {paymentAmount}</Text>
                    </View>

                    <View style={styles.amountContainer}>
                        <Text style={styles.title}>{translate('subscriptionType')}</Text>
                        <Text style={styles.subtitle}>{alias}</Text>
                    </View>

                    <View style={styles.amountContainer}>
                        <Text style={styles.title}>{translate('paymentMethod')}</Text>
                        <Text style={[styles.subtitle, { marginTop: 10 }]}>{paymentMethod.method}</Text>

                        {/* SETTING PAYMENT METHOD TYPE */}
                        {paymentMethod.method === 'Credit Card' ? (
                            // {/* SETTING CARD TYPE */
                            <View style={styles.creditCardContainer}>
                                {paymentMethod.type === 'MASTERCARD' ? (
                                    <Image
                                        source={require('../../../../../assets/images/creditCardsTypes/mastercard.png')}
                                        style={styles.cardType}
                                    />
                                ) : paymentMethod.type === 'VISA' ? (
                                    <Image
                                        source={require('../../../../../assets/images/creditCardsTypes/visa.png')}
                                        style={styles.cardType}
                                    />
                                ) : paymentMethod.type === 'DINERSCLUB' ? (
                                    <Image
                                        source={require('../../../../../assets/images/creditCardsTypes/discover.png')}
                                        style={styles.cardType}
                                    />
                                ) : (
                                                <Image
                                                    source={require('../../../../../assets/images/creditCardsTypes/generic.png')}
                                                    style={styles.cardType}
                                                />
                                            )
                                }
                                <Text style={styles.creditCardNumber}>{paymentMethod.number}</Text>
                            </View>
                        ) : (
                                // {/* SETTING VOUCHER */
                                <View style={styles.voucherContainer}>
                                    <Text style={styles.voucherSubtitle}>{translate('voucherAmount')}: <Text style={styles.voucherValue}>{paymentMethod.amount}</Text></Text>

                                    <View style={styles.fileContainer}>
                                        <View style={styles.fileContainerWrapper}>
                                            <MaterialCommunityIcons name='receipt' style={styles.fileIcon} />
                                            <Text style={styles.fileTitle}>{paymentMethod.docFile.name}</Text>
                                        </View>
                                    </View>
                                </View>
                            )
                        }
                    </View>

                </View>

                <View style={styles.buttonContainer}>
                    <TouchableOpacity
                        style={[styles.nextButton, { backgroundColor: 'white', borderWidth: 1, borderColor: COLORS.green }]}
                        onPress={(e) => this.props.changeSteps(1)}
                    >
                        <Text style={[styles.nextButtonText, { color: COLORS.green }]}>{translate('goBack')}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.nextButton}
                        onPress={(e) => this.finishTransaction(e)}
                    >
                        <Text style={styles.nextButtonText}>{translate('finish')}</Text>
                    </TouchableOpacity>
                </View>

            </View>
        );
    }
}

export default MessageScreen;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: scale(20)
    },
    amountContainer: {
        width: '100%',
        marginVertical: verticalScale(10)
    },
    title: {
        fontSize: moderateScale(16),
        marginVertical: moderateScale(10),
        fontWeight: 'bold',
        color: COLORS.grey
    },
    price: {
        fontSize: moderateScale(25),
        fontWeight: 'bold',
        color: COLORS.green,
    },
    subtitle: {
        color: COLORS.softBlack,
        fontWeight: '600',
        fontSize: moderateScale(14)
    },
    creditCardContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: verticalScale(10)
    },
    cardType: {
        width: 50,
        height: 50
    },
    creditCardNumber: {
        fontWeight: '500',
        fontSize: moderateScale(14),
        marginLeft: moderateScale(10),
    },
    ///VOUCHER INFO
    voucherContainer: {
        marginTop: verticalScale(10),
    },
    voucherSubtitle: {
        fontSize: moderateScale(14),
        color: '#333'
    },
    voucherValue: {
        fontSize: moderateScale(15),
        color: COLORS.green,
        fontWeight: "bold"
    },
    fileContainer: {
        borderWidth: 0.5,
        borderColor: '#D8D8D8',
        width: '100%',
        padding: scale(10),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: moderateScale(15),
        marginTop: verticalScale(10)
    },
    fileContainerWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    fileIcon: {
        marginRight: moderateScale(15),
        fontSize: moderateScale(30),
        color: '#333'
    },
    fileTitle: {
        fontSize: moderateScale(14),
        color: '#333'
    },
    //BUTTONS STYLES
    buttonContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    nextButton: {
        marginTop: verticalScale(20),
        marginHorizontal: moderateScale(10),
        height: verticalScale(45),
        width: moderateScale(155),
        backgroundColor: COLORS.green,
        justifyContent: 'center',
        alignItems: 'center'
    },
    nextButtonText: {
        fontSize: moderateScale(16),
        fontWeight: 'bold',
        color: 'white'
    }

})

