import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Platform, SafeAreaView, ScrollView, FlatList } from 'react-native';
import { moderateScale, verticalScale, scale } from '../../../../../utilities/ScalingScreen';
import { COLORS } from '../../../../../config/constants';

import * as membership from '../../../../../store/actions/membership';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MembershipFeesCard from '../../../../../components/cards/MembershipsCards/Fees/MembershipFees';
import { DotIndicator } from 'react-native-indicators';

import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../../../translations/translation';
import * as RNLocalize from 'react-native-localize';

class MembershipFees extends Component {
    constructor(props) {
        super(props);
        setI18nConfig();
        this.state = {
            feesList: [],
            loading: true,
            buttonActive: false,
            membershipInfo: {}
        };
        this.segmentClicked = this.segmentClicked.bind(this);
    }

    async componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);
        if (this.props.route === 'FederationSignUp' || this.props.route === 'SignUpGolferInformation') {
            await this.props.getMembershipsTypesById(this.props.memberType, this.props.session.user.accessToken)

            if (this.props.membershipfees.membershipfeesById.success) {
                const membershipFees = this.props.membershipfees.membershipfeesById.membershipDetails.rates
                console.log(membershipFees)
                this.setState({
                    membershipDetails: membershipFees,
                    loading: false
                })
                console.log(this.state.membershipDetails)
            }
        } else {
            const { data, renewPaymentInfo } = this.props;
            await this.props.getAllMembershipFees(data.id)

            if (this.props.membershipfees.membershipfees.success) {
                const membershipFees = this.props.membershipfees.membershipfees.membershipDetails.rates
                this.setState({
                    membershipDetails: membershipFees,
                    loading: false
                })
                console.log(this.state.membershipDetails)
            }
        }
    }

    segmentClicked(index) {
        this.setState({
            activeIndex: index.id,
            buttonActive: true,
            membershipInfo: index
        });
        this.props.selectedItem(index)
    }

    render() {
        const { data, renewPaymentInfo, goToCheckout, recurrent } = this.props;
        return (
            <ScrollView style={styles.container}>

                {this.state.loading &&
                    <DotIndicator
                        color={COLORS.green}
                    />
                }

                {!this.state.loading &&
                    <View style={styles.content}>
                        <FlatList
                            data={this.state.membershipDetails}
                            showsVerticalScrollIndicator={false}
                            extraData={this.state.activeIndex}
                            contentContainerStyle={{ marginTop: verticalScale(10) }}
                            keyExtractor={(item, index) => item.id.toString()}
                            renderItem={({ item, index }) =>
                                <MembershipFeesCard
                                    data={item}
                                    index={index}
                                    activeIndex={this.state.activeIndex}
                                    segmentClicked={(e) => this.segmentClicked(e)}
                                />
                            }
                        />

                        {goToCheckout === true ? (
                            <View style={styles.buttonContainer}>
                                <TouchableOpacity
                                    style={[styles.nextButton, this.state.buttonActive ? {} : styles.nextButtonDisable]}
                                    onPress={this.state.buttonActive ? (e) => this.props.sentRequetPayment(this.state.membershipInfo) : console.log('No')}
                                >
                                    <Text style={styles.nextButtonText}>{translate('sentRequest')}</Text>
                                </TouchableOpacity>
                            </View>
                        ) : (
                                <View style={styles.buttonContainer}>
                                    <TouchableOpacity
                                        style={[styles.nextButton, this.state.buttonActive ? {} : styles.nextButtonDisable]}
                                        onPress={this.state.buttonActive ? (e) => this.props.changeSteps(1) : console.log('No')}
                                    >
                                        <Text style={styles.nextButtonText}>{translate('continue')}</Text>
                                    </TouchableOpacity>
                                </View>
                            )
                        }

                    </View>
                }

            </ScrollView>
        );
    }
}


function mapStateToProps(state) {
    return {
        session: state.session,
        user: state.session.user.data,
        membershipfees: state.membership,
        renewPayment: state.payment
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators(membership, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchProps
)(MembershipFees);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    content: {
        width: '100%',
        padding: scale(20),
        justifyContent: 'center',
    },
    buttonContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: verticalScale(30),
    },
    nextButtonDisable: {
        marginTop: verticalScale(20),
        height: verticalScale(45),
        width: moderateScale(310),
        backgroundColor: COLORS.softGrey,
        justifyContent: 'center',
        alignItems: 'center'
    },
    nextButton: {
        marginTop: verticalScale(20),
        height: verticalScale(45),
        width: moderateScale(310),
        backgroundColor: COLORS.green,
        justifyContent: 'center',
        alignItems: 'center'
    },
    nextButtonText: {
        fontSize: moderateScale(16),
        fontWeight: 'bold',
        color: 'white'
    }
})