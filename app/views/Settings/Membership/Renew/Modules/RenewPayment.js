import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Platform, SafeAreaView, ScrollView, FlatList, Image, TextInput, Modal } from 'react-native';
import { moderateScale, verticalScale, scale } from '../../../../../utilities/ScalingScreen';
import { COLORS } from '../../../../../config/constants';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import MaterialCommunityIcons from "react-native-vector-icons/dist/MaterialCommunityIcons";
import DocumentPicker from 'react-native-document-picker';
import CreditCardModal from '../../../../../components/UI/Payment/CreditCardModal';
import CardPayment from '../../../../../components/lists/CardPayment/CardPayment';

import * as payment from '../../../../../store/actions/payment';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../../../translations/translation';
import * as RNLocalize from 'react-native-localize';

class RenewPayment extends Component {
    constructor(props) {
        super(props);
        setI18nConfig();
        this.state = {
            hasFile: false,
            buttonActive: false,
            activeIndex: null,
            voucherValue: '',
            voucherObject: {},
            creditCard: false,
            creditCardValue: {},
            cardList: [],
            loading: true,
            addingCard: false,
        };
        this.cardSelected = this.cardSelected.bind(this);
        this.setVoucherData = this.setVoucherData.bind(this);
        this.getCreditCardValues = this.getCreditCardValues.bind(this);
    }

    componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);
        this.getMyCard();
    }

    async getMyCard() {
        const userid = this.props.session.user.data.id;
        await this.props.getUserPayments(userid, this.props.session.accessToken);

        if (this.props.payment.userPaymentCards.success) {
            console.log(this.props.payment.userPaymentCards.data.data);
            this.setState({
                cardList: this.props.payment.userPaymentCards.data.data,
                loading: false
            })
        } else {
            this.setState({
                cardList: [],
                loading: false,
                error: true,
            })
        }
    }

    async uploadDocument() {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            })
            const name = res.name;
            const ext = name.substring(name.length - 3);
            const permittedFiles = ['jpg', 'jpeg', 'png', 'pdf'];
            const load = permittedFiles.find((item) => item === ext);

            if (load) {
                console.log('doc')
                this.setState({
                    hasFile: true,
                    docName: res.name,
                    docFile: res,
                    select: false,
                    fileSelected: true,
                    activeIndex: null,
                    buttonActive: false
                })
            } else {
                this.setState({
                    typeError: true,
                    msg: 'Please upload file with this extentions jpg, jpeg, png, pdf'
                })
            }
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
            } else {
                throw err;
            }
        }
    }

    // We manage the selected card
    cardSelected(index) {
        if (this.state.hasFile) {
            this.deleteFile()
        }
        this.setState({
            activeIndex: index.id,
            buttonActive: true
        });

        const creditCard = {
            cardNumber: index.card_number,
            expiration: index.expiration,
            cvc: index.cvc,
            method: 'Credit Card',
            type: index.card_brand,
            token: index.token
        }

        // we sent the credit card values to paymentMethod
        this.props.paymentMethod(creditCard)
    }

    // SETING VOUCHER AMOUNT
    setVoucherData() {
        const voucherValue = this.state.voucherValue;
        const { selectedItem, paymentData, route } = this.props;

        // SETING VOUCHER AMOUNT WITH THIS ROUTES
        // RequestDetails FROM COMPONENT ManageRequestDetails
        // MembershipCard FROM COMPONENT FederationMembershipDetail
        if (route === "RequestDetails" || route === "MembershipCard") {
            const selectedValue = parseInt(this.props.paymentData.amount);
            const valueInt = parseInt(voucherValue);
            console.log(selectedValue);

            if (selectedValue <= valueInt && selectedValue == valueInt) {
                const voucherObject = {
                    amount: voucherValue,
                    // membershipId: this.props.data.id,
                    method: 'Upload Voucher',
                    docFile: this.state.docFile
                }

                this.setState({
                    voucherObject: voucherObject,
                    buttonActive: true,
                    msg: false
                })

                this.props.paymentMethod(voucherObject)
            } else {
                this.setState({
                    msg: translate('voucherErrorMsg'),
                    buttonActive: false,
                })
            }
        } else {
            const selectedValue = parseInt(this.props.selectedItem.amount);
            const valueInt = parseInt(voucherValue);
            console.log(selectedValue);

            if (selectedValue <= valueInt && selectedValue == valueInt) {
                const voucherObject = {
                    amount: voucherValue,
                    // membershipId: this.props.data.id,
                    method: 'Upload Voucher',
                    docFile: this.state.docFile
                }

                this.setState({
                    voucherObject: voucherObject,
                    buttonActive: true,
                    msg: false
                })

                this.props.paymentMethod(voucherObject)
            } else {
                this.setState({
                    msg: translate('voucherErrorMsg'),
                    buttonActive: false,
                })
            }
        }
    }

    deleteFile() {
        console.log('deleting file')
        this.setState({
            hasFile: false,
            docName: '',
            docFile: {},
            select: true,
            voucherObject: {},
            voucherValue: null,
            buttonActive: false
        })
    }

    addCreditCardModal(visible) {
        this.setState({ creditCard: visible })
    }

    //We receive the new credit card values
    async getCreditCardValues(e, saveCard) {
        if (e !== {}) {
            this.setState({ addingCard: true })
            let cardList = this.state.cardList;

            //We set the card values to AZUL Structure
            let creditCard = e.number.replace(/ /g, ''); //we eliminate the spaces of the cards

            let expirationMonth = e.expiry.substring(0, 2)
            let expirationYear = e.expiry.substring(3)
            let setExpDate = '20' + expirationYear + expirationMonth; // we create the AZUL format for the dates (202012)

            const newCreditCard = {
                cardNumber: creditCard,
                cvc: e.cvc,
                expiration: setExpDate
            }

            //We Validate if the user wants tu save te creditCard
            if (saveCard) {
                const request = await this.props.saveUserCard(newCreditCard, this.props.session.accessToken);
                if (this.props.payment.saveUserCards.success) {
                    this.setState({ addingCard: false })
                    this.getMyCard();
                    this.addCreditCardModal(false)
                } else {
                    console.log('Error adding the card')
                }
            } else {
                //We Set the new Card localy on the list.
                const newCard = {
                    card_brand: e.type.toUpperCase(),
                    card_number: creditCard,
                    expiration: setExpDate,
                    cvc: e.cvc,
                    name: e.name
                }
                cardList.push(newCard);
                this.setState({ addingCard: false, loading: false, cardList: cardList })
                this.addCreditCardModal(false)

            }

        }
    }

    render() {
        const { selectedItem, recurrent, route } = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.paymentContainer}>

                    {!this.state.loading && this.state.cardList.length !== 0 ? (
                        <View>
                            {this.state.cardList.map((item, i) =>
                                <CardPayment
                                    key={i}
                                    data={item}
                                    type={item.card_brand}
                                    cardNumber={item.card_number}
                                    expiration={item.expiration}
                                    route={'formSelect'} // We set The route to use the component
                                    activeIndex={this.state.activeIndex}
                                    onPress={() => this.cardSelected(item)}
                                />
                            )}
                        </View>
                    ) : (
                            <View style={styles.creditCardContainer}>
                                <FontAwesome 
                                    name='credit-card'
                                    size={scale(30)}
                                    color={COLORS.airBnbGrey}
                                />
                                <Text style={styles.cardMsg}>No hay tarjetas registradas</Text>
                            </View>
                        )
                    }

                    <TouchableOpacity
                        style={styles.addNewCreditCard}
                        onPress={() => this.addCreditCardModal(true)}
                    >
                        <MaterialCommunityIcons
                            name='credit-card'
                            size={scale(20)}
                            style={{ color: COLORS.red }}
                        />
                        <Text style={styles.addNewCreditCardText}>{translate('addNewCC')}</Text>
                    </TouchableOpacity>

                    {!recurrent &&
                        <Text style={[styles.title, { marginTop: verticalScale(20) }]}>{translate('uploadVoucher')}</Text>
                    }

                    {!this.state.hasFile && !recurrent &&
                        <TouchableOpacity
                            style={styles.rowContainer}
                            onPress={() => this.uploadDocument()}
                        >
                            <Text style={styles.title}>{translate('selectVoucher')}</Text>
                            <MaterialCommunityIcons
                                name='receipt'
                                size={33}
                                style={{ color: 'black' }}
                            />
                        </TouchableOpacity>
                    }

                    {this.state.hasFile && !recurrent &&
                        <View style={styles.voucherWrapper}>
                            <View style={styles.fileContainer}>
                                <View style={styles.fileContainerWrapper}>
                                    <MaterialCommunityIcons name='receipt' style={styles.fileIcon} />
                                    <Text style={styles.fileTitle}>{this.state.docName}</Text>
                                </View>
                                <TouchableOpacity
                                    onPress={() => this.deleteFile()}
                                >
                                    <MaterialCommunityIcons name='close-circle-outline' style={styles.fileIcon} />
                                </TouchableOpacity>
                            </View>

                            <View style={styles.inputContainer}>
                                <TextInput
                                    style={styles.input}
                                    placeholder='Voucher Mount'
                                    placeholderTextColor='black'
                                    onChangeText={(voucherValue) => this.setState({ voucherValue })}
                                />
                                <TouchableOpacity
                                    style={styles.sentButton}
                                    onPress={() => this.setVoucherData()}
                                >
                                    <Text style={styles.sentButtonText}>Ok</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    }
                    <Text style={styles.errorMsg}>{this.state.msg}</Text>
                </View>

                <View style={styles.buttonContainer}>
                    {route === 'FederationSignUp' &&
                        <TouchableOpacity
                            style={[styles.nextButton, { backgroundColor: 'white', borderWidth: 1, borderColor: COLORS.green }]}
                            onPress={(e) => this.props.changeSteps(0)}
                        >
                            <Text style={[styles.nextButtonText, { color: COLORS.green }]}>{translate('goBack')}</Text>
                        </TouchableOpacity>
                    }
                    <TouchableOpacity
                        style={[styles.nextButton, this.state.buttonActive ? {} : styles.nextButtonDisable]}
                        onPress={this.state.buttonActive ? (e) => this.props.changeSteps(2) : console.log('No')}
                    >
                        <Text style={styles.nextButtonText}>{translate('continue')}</Text>
                    </TouchableOpacity>

                </View>

                <Modal
                    visible={this.state.creditCard}
                    animationType='slide'
                >
                    <CreditCardModal
                        close={(e) => this.addCreditCardModal(e)}
                        getCreditCardValues={(e, save) => this.getCreditCardValues(e, save)}
                        addingCard={this.state.addingCard}

                    />
                </Modal>

            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        session: state.session,
        user: state.session.user.data,
        payment: state.payment
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators(payment, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchProps
)(RenewPayment);


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: scale(20)
    },
    creditCardButton: {
        marginTop: verticalScale(20),
        height: verticalScale(60),
        borderBottomWidth: 1,
        borderBottomColor: '#D8D8D8',
        paddingVertical: 10,
        width: '100%',
        flexDirection: "row",
        alignItems: 'center',
        paddingBottom: verticalScale(10),
    },
    wrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    creditCardInfoWrapper: {
        marginLeft: moderateScale(10),
        marginTop: moderateScale(15)
    },
    cardType: {
        width: 50,
        height: 50
    },
    expirationDate: {
        fontSize: moderateScale(12),
        color: COLORS.airBnbLightGrey,
        marginTop: verticalScale(5)
    },
    creditCardNumber: {
        fontWeight: '500',
        fontSize: moderateScale(14),
    },
    addNewCreditCard: {
        marginTop: verticalScale(10),
        flexDirection: 'row',
        width: '100%',
        height: verticalScale(30),
        alignItems: 'center'
    },
    addNewCreditCardText: {
        fontSize: moderateScale(16),
        color: COLORS.red,
        marginLeft: moderateScale(5)
    },
    rowContainer: {
        marginTop: verticalScale(20),
        height: verticalScale(60),
        borderBottomWidth: 1,
        borderBottomColor: '#D8D8D8',
        paddingVertical: 10,
        width: '100%',
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: 'center',
        paddingBottom: verticalScale(10),
        // backgroundColor: 'red'
    },
    paymentContainer: {
        paddingBottom: Platform.OS === 'ios' ? verticalScale(10) : verticalScale(50)
    },
    title: {
        fontSize: moderateScale(17),
        color: COLORS.airBnbGrey,
        fontWeight: 'normal'
    },
    subtitle: {
        fontSize: moderateScale(14),
        fontWeight: "bold"
    },
    paymentButtonContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    submitButton: {
        width: moderateScale(280)
    },
    voucherWrapper: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: verticalScale(20)
    },
    fileContainer: {
        borderWidth: 0.5,
        borderColor: '#D8D8D8',
        width: '100%',
        padding: scale(10),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: moderateScale(15),
    },
    fileContainerWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    fileIcon: {
        marginRight: moderateScale(15),
        fontSize: moderateScale(30),
        color: '#333'
    },
    fileTitle: {
        fontSize: moderateScale(14),
        color: '#333'
    },
    inputContainer: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    input: {
        width: '80%',
        height: verticalScale(40),
        borderWidth: 1,
        borderRadius: scale(10),
        borderColor: '#D8D8D8',
        padding: scale(10)
    },
    sentButton: {
        width: moderateScale(50),
        height: verticalScale(40),
        backgroundColor: COLORS.red,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: scale(10)
    },
    sentButtonText: {
        color: COLORS.white,
        fontWeight: 'bold'
    },
    errorMsg: {
        fontWeight: '600',
        color: COLORS.red,
        marginTop: verticalScale(5),
        textAlign: 'center'
    },
    buttonContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        paddingBottom: verticalScale(20)
    },
    nextButton: {
        marginTop: verticalScale(20),
        marginHorizontal: moderateScale(10),
        height: verticalScale(45),
        width: moderateScale(155),
        backgroundColor: COLORS.green,
        justifyContent: 'center',
        alignItems: 'center'
    },
    nextButtonDisable: {
        marginTop: verticalScale(20),
        marginHorizontal: moderateScale(10),
        height: verticalScale(45),
        width: moderateScale(155),
        backgroundColor: COLORS.softGrey,
        justifyContent: 'center',
        alignItems: 'center'
    },
    nextButtonText: {
        fontSize: moderateScale(16),
        fontWeight: 'bold',
        color: 'white'
    },
    creditCardContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    cardMsg: {
        fontWeight: '600',
        fontSize: moderateScale(16),
        marginTop: verticalScale(10),
        color: COLORS.airBnbGrey
    }
})


var creditCardInfo = [
    {
        key: 1,
        name: 'American Express',
        cardNumber: '**** 9344',
        expiration: '07/24',
        image: require('../../../../../assets/images/creditCardsTypes/american.png'),
        method: 'Credit Card',
        type: 'amex'
    },
    {
        key: 2,
        name: 'Visa',
        cardNumber: '**** 9345',
        expiration: '07/22',
        image: require('../../../../../assets/images/creditCardsTypes/visa.png'),
        method: 'Credit Card',
        type: 'visa'
    },
    {
        key: 3,
        name: 'Visa',
        cardNumber: '5426064000424979',
        expiration: '202412',
        cvc: '979',
        image: require('../../../../../assets/images/creditCardsTypes/visa.png'),
        method: 'Credit Card',
        type: 'visa'
    }
]