
import React, { Component } from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import * as RNLocalize from 'react-native-localize';

import { verticalScale, moderateScale, scale } from '../../../utilities/ScalingScreen';
import SettingsUserCard from '../../../components/cards/SettingsUserCard/SettingsUserCard';
import SettingsButton from '../../../components/UI/SettingsButton';
import { COLORS } from '../../../config/constants';
import LinkMembership from './LinkMembership/LinkMembership';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as session from '../../../store/actions/session';
 class MembershipOptions extends Component {
    constructor(props) {
        super(props);
        setI18nConfig();
        this.state = {
            showLinkFederation: false,
        };
        props.navigation.addListener('focus', e => {
            this.refreshScreen();
            console.log('updating', e)
          });
    }

    async componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);
    }

    async refreshScreen() {
        const oldToken = this.props.session.accessToken;
        await this.props.refreshToken(oldToken);
    }

    showLinkModal(e) {
        this.setState({
            showLinkFederation: e
        })
    }

    render() {
        const user = this.props
        const federationId = { id: 1 }
        return (
            <View style={styles.container}>
                <SettingsUserCard user={user} />

                <SettingsButton
                    // label="Federation"
                    label={translate('federation')}
                    icon="users"
                    action={() => this.props.navigation.navigate('FederationMembershipList', {
                        user,
                        type: 'federation'
                    })}
                />

                <SettingsButton
                    // label="Asociation"
                    label={translate('association')}
                    icon="users"
                    action={() => this.props.navigation.navigate('AsociationMembershipList', {
                        user,
                    })}
                />

                <SettingsButton
                    // label="League"
                    label={translate('league')}
                    icon="users"
                    action={() => this.props.navigation.navigate('LeagueMembershipList', {
                        user,
                    })}
                />

                {this.props.route.params.user.federations.length === 0 &&
                    <View style={styles.joinButtonContainer}>
                        <View style={styles.button}>
                            <Image
                                style={styles.imageStyle}
                                resizeMode="cover"
                                source={{ uri: 'http://api.golfertek.com/img/profile/fedogolf.jpg' }}
                            />
                            <Text style={styles.buttonTitle}>Become a member now</Text>
                        </View>

                        <TouchableOpacity
                            onPress={() => this.setState({ showLinkFederation: true })}
                            style={styles.registerButton}
                        >
                            <Text style={styles.registerButtonText}>Register Now</Text>
                        </TouchableOpacity>
                    </View>
                }

                {this.state.showLinkFederation &&
                    <LinkMembership
                        close={() => this.setState({ showLinkFederation: false })}
                        navigation={this.props.navigation}
                        federationSelected={federationId}
                    />
                }

            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        ...state.membership,
        session: state.session,
        user: state.session.user.data,
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators(session, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchProps
)(MembershipOptions);

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#fff",
        flex: 1
    },
    joinButtonContainer: {
        width: '100%',
        position: 'absolute',
        bottom: verticalScale(20),
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        padding: moderateScale(10),
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonTitle: {
        fontSize: moderateScale(20),
        marginTop: moderateScale(14),
        fontWeight: 'bold'
    },
    imageStyle: {
        height: 80,
        width: 150
    },
    registerButton: {
        backgroundColor: COLORS.lightblue,
        width: moderateScale(190),
        height: verticalScale(35),
        marginTop: verticalScale(15),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: scale(20)
    },
    registerButtonText: {
        fontSize: moderateScale(16),
        color: COLORS.white,
    }
})