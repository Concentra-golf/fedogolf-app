import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    FlatList,
    Alert,
    Modal,
    TouchableOpacity
} from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as session from '../../../../store/actions/session';
import * as membership from '../../../../store/actions/membership';

import SettingsUserCard from '../../../../components/cards/SettingsUserCard/SettingsUserCard';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowDown } from '@fortawesome/free-solid-svg-icons';
import { moderateScale, verticalScale, scale } from '../../../../utilities/ScalingScreen';
import { COLORS } from '../../../../config/constants';
import AsociationsCards from '../../../../components/cards/MembershipsCards/Asociations/AsociationsCards';


class LeagueMembershipList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            leaguesList: [],
        };
    }

    async componentDidMount() {
        const oldToken = this.props.session.accessToken;
        await this.props.refreshToken(oldToken);

        const request = this.props.user.leagues;

        this.setState({
            leaguesList: request
        })
    }


    render() {
        const { user } = this.props.session;
        return (
            <View style={styles.container}>
                <SettingsUserCard />

                {this.state.leaguesList.length == 0 &&
                    <View style={styles.linkMsgContainer}>
                        <Text style={styles.linkMsgText}>You don't have linked leagues account</Text>
                        <View style={styles.horizontalLine} />
                        <Text style={styles.linkMsgTextTwo}>Tap here to link your leagues account</Text>
                        <FontAwesomeIcon icon={faArrowDown} size={30} />
                    </View>
                }

                <FlatList
                    data={this.state.leaguesList}
                    keyExtractor={(item) => item.id.toString()}
                    refreshing={this.props.refreshing}
                    onRefresh={this.props.handleRefresh}
                    renderItem={({ item }) => (
                        <AsociationsCards
                            data={item}
                            navigation={this.props.navigation}
                            user={user}
                        />
                    )}
                />

                <View style={styles.buttonContainer}>
                    <TouchableOpacity
                        style={styles.addFederationButton}
                    >
                        <Text style={{ fontSize: moderateScale(14), color: COLORS.white }}>Add leagues</Text>
                    </TouchableOpacity>
                </View>

            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        ...state.membership,
        session: state.session,
        user: state.session.user.data,
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators({ ...session, ...membership }, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchProps
)(LeagueMembershipList);


const styles = StyleSheet.create({
    container: {
        backgroundColor: "#fff",
        flex: 1
    },
    buttonContainer: {
        width: '100%',
        height: verticalScale(70),
        position: 'absolute',
        bottom: 0,
        backgroundColor: 'white',
        paddingVertical: scale(10),
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: '#000',
    },
    addFederationButton: {
        backgroundColor: '#50AF99',
        width: moderateScale(220),
        height: verticalScale(40),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },
    linkMsgContainer: {
        width: '100%',
        paddingTop: 45,
        alignItems: "center",
    },
    linkMsgText: {
        fontSize: moderateScale(18),
        width: moderateScale(170),
        textAlign: 'center'
    },
    horizontalLine: {
        width: "65%",
        height: 1,
        backgroundColor: '#0003',
        marginVertical: 30
    },
    linkMsgTextTwo: {
        fontSize: moderateScale(14),
        marginBottom: verticalScale(30)
    }
})
