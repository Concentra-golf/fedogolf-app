import React, { Component } from 'react';
import { View, Text, SafeAreaView, TouchableOpacity, StyleSheet } from 'react-native';

import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import { verticalScale, moderateScale, scale } from '../../../../utilities/ScalingScreen';
import { COLORS } from '../../../../config/constants';

class FederationList extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>

                <TouchableOpacity
                    onPress={() => this.props.showModalList(false)}
                    style={styles.closeButton}
                >
                    <Ionicons
                        name='ios-close'
                        size={35}
                        color={COLORS.black}
                    />
                </TouchableOpacity>

                {this.props.membership.federations && this.props.membership.federations.map((item, index) =>
                    <TouchableOpacity
                        key={index}
                        onPress={() => this.props.federationSelected(item)}
                        style={styles.buttonContainer}
                    >
                        <Text style={styles.buttonText}>{item.name}</Text>
                    </TouchableOpacity>
                )}

            </SafeAreaView>
        );
    }
}

export default FederationList;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    closeButton: {
     width: moderateScale(50),
     padding: scale(10),
     marginLeft: moderateScale(5),
     justifyContent: 'center',
     alignItems: 'center'
    },
    buttonContainer: {
        paddingHorizontal: 20,
        paddingVertical: 12,
        borderBottomColor: "#3332",
        borderBottomWidth: 1,
        width: '100%',
        height: verticalScale(40),
        justifyContent: 'center'
    },
    buttonText: {
        fontSize: moderateScale(16),
    }
})
