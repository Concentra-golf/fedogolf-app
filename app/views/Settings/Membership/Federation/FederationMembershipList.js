import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    FlatList,
    Alert,
    Modal,
    Dimensions
} from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as session from '../../../../store/actions/session';
import * as membership from '../../../../store/actions/membership';
import * as RNLocalize from 'react-native-localize';

import { verticalScale, moderateScale, scale, heightPercentageToDP } from '../../../../utilities/ScalingScreen';
import LinkMembership from '../LinkMembership/LinkMembership';
import FederationList from './FederationList';
import FederationCards from '../../../../components/cards/MembershipsCards/Federations/FederationCards';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { COLORS } from '../../../../config/constants';
import Loader from '../../../../components/UI/Loader/Loader';

import FixedForeground from '../../../../components/UI/HeaderButtons/ImageHeader/FixedForeGround';
import HeaderImageScrollView, { TriggeringView } from 'react-native-image-header-scroll-view';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../../translations/translation';

class FederationMembershipList extends Component {
    constructor(props) {
        super(props);
        setI18nConfig();
        this.state = {
            membershipList: [],
            showLinkFederation: false,
            showAllFederations: false,
            selectedFederation: {},
        };
        this.showModalList = this.showModalList.bind(this);
    }

    async componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);
        const oldToken = this.props.session.accessToken;
        await this.props.refreshToken(oldToken);

        const request = this.props.user.federations;

        this.setState({
            membershipList: request
        })
    }

    callMembership = async () => {
        if (!this.props.membership[this.props.route.params.type])
            await this.props.getAllFederations();
        if (this.props.membership.error)
            Alert.alert(
                'Error',
                'An error has occurred. Please try again',
                [
                    { text: 'OK' }
                ],
                { cancelable: false }
            );
        else
            this.setState({ showAllFederations: true });

    }

    federationSelected(e) {
        console.log(e);
        this.setState({
            selectedFederation: e,
            showAllFederations: false,
            showLinkFederation: true
        })
    }

    showModalList(e) {
        this.setState({
            showAllFederations: e
        })
    }

    render() {
        const { type } = this.props.route.params;
        const { user } = this.props.session;
        return (
            <View style={styles.container}>

                <HeaderImageScrollView
                    maxHeight={MAX_HEIGHT}
                    minHeight={MIN_HEIGHT}
                    maxOverlayOpacity={0.3}
                    minOverlayOpacity={0.2}
                    fadeOutForeground
                    headerImage={require('../../../../assets/images/scroll-header-bg.png')}
                    renderFixedForeground={() => <FixedForeground title={translate('federationTitle')} navigation={this.props.navigation} />}
                >
                    <TriggeringView style={styles.section}>
                        {this.state.membershipList.length == 0 &&
                            <Loader
                                nothing={true}
                                nothingMessage={translate('linkedMsg')}
                            />
                        }

                        <FlatList
                            data={this.state.membershipList}
                            keyExtractor={(item) => item.id.toString()}
                            refreshing={this.props.refreshing}
                            onRefresh={this.props.handleRefresh}
                            renderItem={({ item }) => (
                                <FederationCards
                                    data={item}
                                    navigation={this.props.navigation}
                                    user={user}
                                />
                            )}
                        />

                        {this.state.showLinkFederation &&
                            <LinkMembership
                                close={() => this.setState({ showLinkFederation: false })}
                                navigation={this.props.navigation}
                                federationSelected={this.state.selectedFederation}
                            />
                        }

                        <Modal
                            presentationStyle="fullScreen"
                            visible={this.state.showAllFederations}
                        >
                            <FederationList
                                membership={this.props.membership}
                                showModalList={(e) => this.showModalList(e)}
                                federationSelected={(e) => this.federationSelected(e)}
                                navigation={this.props.navigation}
                            />
                        </Modal>
                    </TriggeringView>

                    <View style={styles.buttonContainer}>
                        <TouchableOpacity
                            style={styles.addFederationButton}
                            onPress={() => this.callMembership()}
                        >
                            <Text style={{ fontSize: moderateScale(14), color: COLORS.white }}>{translate('addFederation')}</Text>
                        </TouchableOpacity>
                    </View>

                </HeaderImageScrollView>
            </View>
        );
    }
}

function mapStateToProps(state) {
    console.log(state);

    return {
        ...state.membership,
        session: state.session,
        user: state.session.user.data,
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators({ ...session, ...membership }, dispatch);
}


export default connect(
    mapStateToProps,
    mapDispatchProps
)(FederationMembershipList);

const MIN_HEIGHT = verticalScale(100);
const MAX_HEIGHT = moderateScale(220);

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#fff",
        flex: 1
    },
    buttonContainer: {
        width: '100%',
        height: verticalScale(70),
        position: 'absolute',
        bottom: heightPercentageToDP('20%'),
        backgroundColor: 'white',
        paddingVertical: scale(10),
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        shadowColor: '#000',
    },
    section: {
        height: Dimensions.get('window').height / 1.6,
        padding: scale(10),
        borderBottomColor: '#cccccc',
        backgroundColor: 'white',
    },
    addFederationButton: {
        backgroundColor: '#50AF99',
        width: moderateScale(220),
        height: verticalScale(40),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },
    linkMsgContainer: {
        width: '100%',
        paddingTop: 45,
        alignItems: "center",
    },
    linkMsgText: {
        fontSize: moderateScale(18),
        width: moderateScale(170),
        textAlign: 'center'
    },
    horizontalLine: {
        width: "65%",
        height: 1,
        backgroundColor: '#0003',
        marginVertical: 30
    },
    linkMsgTextTwo: {
        fontSize: moderateScale(14),
        marginBottom: verticalScale(30)
    }
})
