import React, { Component } from 'react';
import { Text, Image, View, ImageBackground, StyleSheet, SafeAreaView, ScrollView, StatusBar } from 'react-native';

import { TouchableOpacity } from 'react-native-gesture-handler';
import { verticalScale, moderateScale, scale, heightPercentageToDP, widthPercentageToDP } from '../../../../utilities/ScalingScreen';
import { COLORS } from '../../../../config/constants';
import RenewMsg from '../../../../components/UI/Memberships/Msg/RenewMsg';
import FixedForeground from '../../../../components/UI/HeaderButtons/ImageHeader/FixedForeGround';

import MembershipsCards from '../../../../components/cards/MembershipsCards/Memberships/MembershipsCards';

import HeaderImageScrollView, { TriggeringView } from 'react-native-image-header-scroll-view';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import TextLabel from '../../../../components/UI/TextLabel';

import * as RNLocalize from 'react-native-localize';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../../translations/translation';

export default class FederationMembershipDetail extends Component {

    constructor(props) {
        super(props);
        setI18nConfig();
        this.state = {
            viewModal: false
        };
    }

    componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);
    }

    render() {
        const { membership } = this.props.route.params;
        return (
            <View style={styles.container}>
                <StatusBar barStyle="light-content" />
                <HeaderImageScrollView
                    maxHeight={MAX_HEIGHT}
                    minHeight={MIN_HEIGHT}
                    maxOverlayOpacity={0.3}
                    minOverlayOpacity={0.2}
                    fadeOutForeground
                    headerImage={require('../../../../assets/images/scroll-header-bg.png')}
                    renderFixedForeground={() => <FixedForeground title={membership.name} navigation={this.props.navigation} />}
                >
                    <TriggeringView style={styles.section}>

                        <TouchableOpacity
                            style={styles.membershipDetail}
                            onPress={() => this.props.navigation.navigate('MembershipQR', {
                                membership: membership
                            })}
                        >
                            <FontAwesome
                                name='qrcode'
                                size={moderateScale(35)}
                                style={{ marginRight: moderateScale(20) }}
                            />
                            <View>
                                <TextLabel additionalStyles={styles.qrTitle}>{translate('scanQrTitle')}</TextLabel>
                                <TextLabel additionalStyles={styles.qrsubtitle}>{translate('scanQrMsg')}</TextLabel>
                            </View>
                        </TouchableOpacity>

                        <View style={styles.dividerContainer}>
                            <View style={styles.divider} />
                        </View>


                        <View style={styles.membershipsContainer}>
                            <Text style={styles.membershipTitle}>{translate('myMemberships')}</Text>

                            <View style={styles.membershipList}>
                                {membership.memberships.map((item, index) =>
                                    <MembershipsCards
                                        key={index}
                                        allInfo={item}
                                        name={item.name}
                                        expirationDate={item.users[0].pivot.validity}
                                        navigation={this.props.navigation}
                                        expired={this.state.expired}
                                    />
                                )}


                            </View>
                        </View>


                        <View style={styles.buttonContainer}>
                            <TouchableOpacity
                                style={styles.benefitsButton}
                                onPress={() => this.props.navigation.navigate('BenefitsByMembership', {
                                    name: membership.name
                                })}
                            >
                                <Text style={styles.benefitsButtonText}>{translate('seeAllBenefits')}</Text>
                            </TouchableOpacity>
                        </View>

                        <RenewMsg
                            modelVisible={this.state.viewModal}
                        />

                    </TriggeringView>
                </HeaderImageScrollView>
            </View>
        );
    }
}
const MIN_HEIGHT = verticalScale(100);
const MAX_HEIGHT = moderateScale(220);


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    section: {
        flex: 1,
        padding: scale(10),
    },
    membershipDetail: {
        width: '100%',
        height: heightPercentageToDP('10%'),
        flexDirection: 'row',
        alignItems: 'center',
        padding: scale(10)
    },
    qrTitle: {
        fontSize: moderateScale(16),
        marginBottom: verticalScale(5),
        fontWeight: 'bold',
        color: COLORS.softBlack
    },
    qrsubtitle: {
        fontSize: moderateScale(14),
        color: COLORS.softBlack
    },
    divider: {
        height: 0.5,
        width: widthPercentageToDP('90%'),
        backgroundColor: COLORS.airBnbGrey
    },
    dividerContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },

    membershipsContainer: {
        width: '100%',
        marginTop: verticalScale(15),
    },
    membershipTitle: {
        color: COLORS.softBlack,
        fontSize: moderateScale(18),
        fontWeight: '600'
    },
    membershipList: {
        marginTop: verticalScale(10),
        justifyContent: 'center',
        alignItems: 'center'
    },
    membershipsTitle: {
        fontSize: moderateScale(17),
        fontWeight: '600',
        marginBottom: verticalScale(5),
        color: COLORS.airBnbGrey,
    },
    buttonContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    benefitsButton: {
        alignItems: 'center',
        justifyContent: 'center',
        width: moderateScale(280),
        height: heightPercentageToDP('8%'),
        backgroundColor: COLORS.green,
        paddingVertical: verticalScale(10),
        borderRadius: scale(5),
        marginTop: verticalScale(30)
    },
    benefitsButtonText: {
        fontSize: moderateScale(16),
        color: COLORS.white,
        fontWeight: 'bold'
    },
})