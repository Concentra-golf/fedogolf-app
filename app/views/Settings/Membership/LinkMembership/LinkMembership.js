import React, { Component } from 'react';
import { Text, Modal, Image, View, ActivityIndicator, TouchableOpacity } from 'react-native';

import styles from './Styles';

import FederationService from '../../../../config/services/FederationService'


import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getValueAndLabelFromArray } from '../../../../utilities/helper-functions'
import * as session from '../../../../store/actions/session';
import FedoGolfSignUpForm from '../../../../components/forms/FedoGolfSignUpForm/FedoGolfSignUpForm';
import { moderateScale, verticalScale } from '../../../../utilities/ScalingScreen';
import Step01 from '../../../../components/forms/LinkMembershipsSteps/Step01';
import Steps02 from '../../../../components/forms/LinkMembershipsSteps/Steps02';
import BottomButtons from '../../../../components/forms/LinkMembershipsSteps/BottomButtons';
import SuccessLink from '../../../../components/forms/LinkMembershipsSteps/Msg/SuccessLink';


import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../../translations/translation';
import * as RNLocalize from 'react-native-localize';

class LinkMembership extends Component {
    constructor(props) {
        super(props);
        setI18nConfig(); 
        this.state = {
            showModal: true,
            showFedogolfVerification: false,
            showFedoGolfRegister: false,
            showFedoGolfBenefits: false,
            loading: false,
            msg: '',
            federations: [],
            associations: [],
            leagues: [],
            golf_clubs: [],
        }
        this.fedogolfValidation = this.fedogolfValidation.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    async componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);
        const { session } = this.props
        const response = await FederationService.getAllFederations(session.accessToken)

        response.data.golf_clubs.unshift({ id: 0, club_name: "Ninguna" });
        response.data.leagues.unshift({ id: 0, name: "Ninguna" });
        response.data.asociations.unshift({ id: 0, name: "Ninguna" });

        this.setState({
            federations: response.data.federations,
            associations: response.data.asociations,
            leagues: response.data.leagues,
            golf_clubs: response.data.golf_clubs,
        })
        console.log(this.state)
    }

    async fedogolfValidation(values, actions) {
        this.setState({ loading: true, msg: '' })
        console.log(values);

        const data = {
            number: values.id_number,
            email: values.membership_number,
            type: 'id_number',
            membership: this.props.federationSelected.id
        }

        try {
            let request = await this.props.fedogolfValidation(data, this.props.session.accessToken);
            if (this.props.session.fedogolfUser.success) {
                this.setState({
                    success: true,
                    loading: false,
                    msg: translate('succesValidatingAccount')
                    // msg: 'An confimation mail has been sent, please check your email'
                })
            } else {
                this.setState({
                    error: true,
                    loading: false,
                    msg: translate('errorValidatingAccount')
                    // msg: 'Error validating account'
                })
            }
        } catch (error) {
            this.setState({ loading: false, msg: translate('errorValidatingAccount') })
        }
    }

    closeModal() {
        this.setState({ msg: '' });
        this.props.close();
    }

    gotoForm() {
        this.closeModal()
        this.props.navigation.navigate('FederationSignUp', {
            user: this.props.session.user.data,
            navigation: this.props.navigation,
            associations: getValueAndLabelFromArray(this.state.associations, 'name'),
            leagues: getValueAndLabelFromArray(this.state.leagues, 'name'),
            golf_clubs: getValueAndLabelFromArray(this.state.golf_clubs, 'club_name'),
            accessToken: this.props.session.accessToken,
            session: this.props.session,
            fedogolfSignUp: this.props.fedogolfSignUp,
            fedogolfUser: this.props.session.fedogolfUser
        })
    }

    render() {
        const { session, fedogolfSignUp, fedogolfUser } = this.props
        return (
            <Modal
                // presentationStyle='overFullScreen'
                animationType="fade"
                transparent={true}
                visible={this.state.showModal}
                onRequestClose={() => this.props.close()}
                onTouchOutside={() => this.props.close()}
            >
                {!this.state.success ? (
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <View style={{ alignItems: "center", width: '100%' }}>

                                {/* Sign up Button */}
                                <Step01
                                    showFedogolfVerification={this.state.showFedogolfVerification}
                                    firstAction={() => this.setState({ showFedogolfVerification: true })}
                                    secondAction={() => this.gotoForm()}
                                // secondAction={() => this.setState({ showFedoGolfRegister: true })}
                                />

                                <Steps02
                                    showFedogolfVerification={this.state.showFedogolfVerification}
                                    fedogolfValidation={(values, actions) => this.fedogolfValidation(values, actions)}
                                    loading={this.state.loading}
                                    props={this.props}
                                    fedogolf_user={this.props.session.fedogolf_user}
                                    msg={this.state.msg}
                                    success={this.state.success}
                                    goBack={() => this.setState({ showFedogolfVerification: false })}
                                />


                                <BottomButtons
                                    showFedogolfVerification={this.state.showFedogolfVerification}
                                    closeModal={(e) => this.closeModal(e)}
                                    actionButton={(e) => this.setState({ showFedoGolfBenefits: true })}
                                />

                            </View>
                        </View>
                    </View>
                ) : (
                        <View style={styles.centeredView}>
                            <SuccessLink
                                msg={'Close Modal'}
                                onClick={(e) => this.closeModal(e)}
                            />
                        </View>
                    )
                }

                <FedoGolfSignUpForm
                    user={session.user.data}
                    navigation={this.props.navigation}
                    visible={this.state.showFedoGolfRegister}
                    associations={getValueAndLabelFromArray(this.state.associations, 'name')}
                    leagues={getValueAndLabelFromArray(this.state.leagues, 'name')}
                    golf_clubs={getValueAndLabelFromArray(this.state.golf_clubs, 'club_name')}
                    accessToken={session.accessToken}
                    session={session}
                    fedogolfSignUp={fedogolfSignUp}
                    fedogolfUser={session.fedogolfUser}
                    closeModal={
                        () => {
                            this.setState({ showFedoGolfRegister: false })
                            this.props.close()
                        }
                    }
                    route={'logued'}
                />
            </Modal >
        );
    }
}

function mapStateToProps(state) {
    return {
        session: state.session,
        user: state.session.user.data
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators(session, dispatch);
}

export default connect(mapStateToProps, mapDispatchProps)(LinkMembership)
