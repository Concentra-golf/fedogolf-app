import React from "react";
import { View, Image, ScrollView, CheckBox, Alert, Modal, Text, TouchableOpacity, ImageBackground, Dimensions, StyleSheet, SafeAreaView } from "react-native";
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons'

// Modules

import { Formik } from "formik";
import * as yup from "yup";

import * as session from '../../../store/actions/session';
import * as payment from '../../../store/actions/payment';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';


// Components

import RNPickerSelect from 'react-native-picker-select';
import AlertMessage from "../../../components/UI/AlertMessage/AlertMessage";
import Button from "../../../components/UI/Button";
import FormInput from "../../../components/UI/FormInput/FormInput";

import { getValueAndLabelFromArray } from "../../../utilities/helper-functions";
import { moderateScale, verticalScale, scale, widthPercentageToDP } from "../../../utilities/ScalingScreen";
import { COLORS } from "../../../config/constants";
import TextLabel from "../../../components/UI/TextLabel";

import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';
import * as RNLocalize from 'react-native-localize';

class FederationSignUp extends React.Component {
    constructor(props) {
        super(props);
        setI18nConfig();
        this.state = {
            showSuccesModal: false,
            isPassport: false,
            gender: 'male',
            memberType: [],
            formValues: {},
            values: {
                gender: 1,
                homeClub: ''
            }
        };
        this.fedoGolfSignUp = this.fedoGolfSignUp.bind(this);
        this.switchGender = this.switchGender.bind(this);
    }

    async componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);
        const props = this.props.getMembershipsTypes(1, this.props.session.user.accessToken);
        if (this.props.session.membershipTypes.success) {
            this.setState({
                memberType: this.props.session.membershipTypes.data
            })
        } else {
            console.log('error')
        }
    }

    // Sign in the user

    switchGender = (e) => {
        console.log(e);
        this.setState({ gender: e })
        this.state.values.gender === 1
            ? this.setState({ values: { gender: 2 } })
            : this.setState({ values: { gender: 1 } })
    }

    async fedoGolfSignUp(paymentInfo) {
        console.log('FEDERATION STARTING VALIDATION')

        const gender = this.state.gender;
        const values = this.state.formValues;
        const payment = paymentInfo;

        const data = {
            firstname: values.firstname,
            lastname: values.lastname,
            id_number: values.id_number,
            email: values.email,
            dob: values.dob,
            nationality: values.nationality,
            home_phone: values.home_phone,
            mobile_phone: values.mobile_phone,
            golf_club_id: values.golf_club_id,
            ghin_number: values.ghin_number,
            association_id: payment.type === 'association' ? paymentInfo.association_id : values.association_id,
            league_id: values.league_id,
            profession: values.profession,
            office_phone: values.office_phone,
            address: values.address,
            company_name: values.company_name,
            gender: gender,
            job_title: values.job_title,
            sdcc: values.sdcc || false,
            federation_id: 1,
            membership_id: values.memberType,
            amount: payment.amount,
            flow_type: payment.type,

        };
        console.log('data from fedogolf', data)
        await this.props.fedogolfSignUp(this.props.session.user.accessToken, data);
        // Close Form if the user is logged in\
        if (this.props.session.fedogolfUser.success) {
            console.log('FEDERATION SIGN UP ========== SUCCESS')
            // SIMULATING PAYMENT PROCESS FOR STANDAR AND RECURRENT FEES
            if (payment.type === 'standard' || payment.type === 'recurrent') {
                const requestPayment = this.props.session.fedogolfUser.data.requestPayment;
                const application_id = this.props.session.fedogolfUser.data.application.id;
                let payments = {};
                if (payment.paymentMethod === 'card') {
                    payments = {
                        type: 'card',
                        amount: paymentInfo.amount,
                        reason: "get_membership",
                    }
                } else {
                    payments = {
                        type: '',
                        amount: paymentInfo.amount,
                        reason: "get_membership",
                        file: paymentInfo.file,
                    }
                }
                // SIMULATING PAYMENT PROCESS

                await this.props.simulatePayment(requestPayment.id, application_id, payments, this.props.session.accessToken);
                if (this.props.renewPayment.simulatePaymentResponse.success) {
                    console.log('FEDERATION PAYMENT SIGN UP ========== SUCCESS')
                    await this.props.paymentSuccess(true);
                } else {
                    Alert.alert('Something went wrong whit the payment process', '', [{ text: 'Ok' }])
                }
            } else {
                // SENT FROM WITHOUT PAY
                await this.props.paymentSuccess(true);
            }
        } else {
            Alert.alert('Something went wrong registering user', '', [{ text: 'Ok' }])
        }
    };


    payMembership(values, actions) {
        this.setState({ formValues: values })

        if (this.props.route.params.route === 'SignUpGolferInformation') {
            this.props.navigation.navigate('OptionsSelector', {
                route: 'SignUpGolferInformation',
                memberType: values.memberType,
                fedoGolfSignUp: this.fedoGolfSignUp,
                formValues: values
            })
        } else {
            this.props.navigation.navigate('OptionsSelector', {
                route: 'FederationSignUp',
                memberType: values.memberType,
                fedoGolfSignUp: this.fedoGolfSignUp,
                formValues: values
            })
        }
    }

    render() {
        const { user, visible, error, associations, leagues, golf_clubs, fedogolfUser } = this.props.route.params;

        return (
            <SafeAreaView style={styles.container}>
                <ScrollView>
                    <View style={styles.wrapper}>

                        <View style={styles.customHeader}>
                            <Text style={styles.customTitle}>{translate('titleFederationSignUp')}</Text>
                            <Text style={styles.customSubtitle}>Federacion Dominicana de Golf</Text>
                            <View style={styles.horizontalLine} />
                        </View>


                        <View style={styles.inputContainer}>
                            {error &&
                                <AlertMessage
                                    type={"danger"}
                                    message={error}
                                />
                            }

                            <Formik
                                initialValues={user}
                                validationSchema={validationSchema}
                                onSubmit={(values, actions) => this.payMembership(values, actions)}
                            >
                                {props => {
                                    return (
                                        <React.Fragment>
                                            {/* Form Inputs */}

                                            <FormInput
                                                onChangeText={props.handleChange("firstname")}
                                                value={props.values.firstname}
                                                placeholder={translate('firstName')}
                                                additionalStyles={styles.fedogolfInputs}
                                                theme={'black'}
                                                error={props.errors.firstname}
                                                theme={"#00000094"}
                                            />

                                            <FormInput
                                                onChangeText={props.handleChange("lastname")}
                                                value={props.values.lastname}
                                                placeholder={translate('lastName')}
                                                additionalStyles={styles.fedogolfInputs}
                                                theme={'black'}
                                                error={props.errors.lastname}
                                                theme={"#00000094"}
                                            />

                                            <FormInput
                                                onChangeText={props.handleChange("id_number")}
                                                value={props.values.id}
                                                placeholder={translate('idPassport')}
                                                keyboardType="number-pad"
                                                additionalStyles={styles.fedogolfInputs}
                                                theme={'black'}
                                                error={props.errors.id_number}
                                                theme={"#00000094"}
                                            />

                                            <FormInput
                                                onChangeText={value => props.setFieldValue('dob', value)}
                                                value={props.values.dob}
                                                theme={"#00000094"}
                                                type={'date'}
                                                placeholder={translate('birthday')}
                                                additionalStyles={{ ...styles.fedogolfInputs, height: 44 }}
                                                data={this.state.countries}
                                                editable={false}
                                            // error={props.errors.dob}
                                            />

                                            {props.errors.dob &&
                                                <TextLabel size={13} color={'danger'} additionalStyles={{ marginBottom: 10 }}>This field is requiered</TextLabel>
                                            }


                                            <FormInput
                                                onChangeText={props.handleChange("address")}
                                                // onBlur={props.handleBlur("address")}
                                                value={props.values.address}
                                                placeholder={translate('address')}
                                                additionalStyles={styles.fedogolfInputs}
                                                theme={'black'}
                                                error={props.errors.address}
                                                theme={"#00000094"}
                                            />

                                            <FormInput
                                                onChangeText={props.handleChange("nationality")}
                                                // onBlur={props.handleBlur("nationality")}
                                                value={props.values.nationality}
                                                placeholder={translate('nationality')}
                                                additionalStyles={styles.fedogolfInputs}
                                                theme={'black'}
                                                error={props.errors.nationality}
                                                theme={"#00000094"}
                                            />

                                            <FormInput
                                                onChangeText={props.handleChange("mobile_phone")}
                                                // onBlur={props.handleBlur("mobile_phone")}
                                                value={props.values.mobile_phone}
                                                placeholder={translate('mobilePhone')}
                                                additionalStyles={styles.fedogolfInputs}
                                                theme={'black'}
                                                error={props.errors.mobile_phone}
                                                theme={"#00000094"}
                                            />

                                            <FormInput
                                                onChangeText={props.handleChange("office_phone")}
                                                // onBlur={props.handleBlur("office_phone")}
                                                value={props.values.office_phone}
                                                placeholder={translate('officePhone')}
                                                additionalStyles={styles.fedogolfInputs}
                                                theme={'black'}
                                                error={props.errors.office_phone}
                                                theme={"#00000094"}
                                            />
                                            <FormInput
                                                onChangeText={props.handleChange("home_phone")}
                                                // onBlur={props.handleBlur("home_phone")}
                                                value={props.values.home_phone}
                                                placeholder={translate('homePhone')}
                                                // type="telephoneNumber"
                                                keyboardType="number-pad"
                                                additionalStyles={styles.fedogolfInputs}
                                                error={props.errors.home_phone}
                                                theme={"#00000094"}
                                            />

                                            <FormInput
                                                onChangeText={props.handleChange("email")}
                                                // onBlur={props.handleBlur("email")}
                                                value={props.values.email}
                                                placeholder={translate('email')}
                                                type="emailAddress"
                                                additionalStyles={styles.fedogolfInputs}
                                                editable={false}
                                                error={props.errors.email}
                                                theme={"#00000094"}
                                            />
                                            <View>
                                                <FormInput
                                                    onChangeText={value => props.setFieldValue('golf_club_id', value)}
                                                    value={props.values.golf_club_id}
                                                    type={'picker'}
                                                    theme={'light'}
                                                    placeholder="Home Club *"
                                                    additionalStyles={styles.fedogolfInputs}
                                                    data={golf_clubs}
                                                    editable={false}
                                                    error={props.errors.golf_club_id}
                                                />
                                            </View>
                                            <FormInput
                                                onChangeText={props.handleChange('ghin_number')}
                                                value={props.values.ghin_number}
                                                theme={"#00000094"}
                                                maxLength={7}
                                                placeholder="Ghin Number"
                                                keyboardType="number-pad"
                                                additionalStyles={styles.fedogolfInputs}
                                                error={props.errors.ghin_number}
                                            />

                                            <FormInput
                                                // key={'association_name'}
                                                onChangeText={value => props.setFieldValue('association_id', value)}
                                                value={props.values.association_id}
                                                type={'picker'}
                                                theme={'light'}
                                                placeholder={translate('association')}
                                                additionalStyles={styles.fedogolfInputs}
                                                data={associations}
                                                editable={false}
                                                error={props.errors.association_id}
                                            />
                                            <FormInput
                                                // key={'team_name'}
                                                onChangeText={value => props.setFieldValue('league_id', value)}
                                                value={props.values.league_id}
                                                data={leagues}
                                                items={[]}
                                                type={'picker'}
                                                theme={'light'}
                                                placeholder={translate('league')}
                                                additionalStyles={styles.fedogolfInputs}
                                                editable={false}
                                                error={props.errors.league_id}
                                            />
                                            <FormInput
                                                onChangeText={props.handleChange("profession")}
                                                // onBlur={props.handleBlur("profession")}
                                                value={props.values.profession}
                                                placeholder={translate('profession')}
                                                additionalStyles={styles.fedogolfInputs}
                                                theme={'black'}
                                                error={props.errors.profession}
                                                theme={"#00000094"}
                                            />

                                            <FormInput
                                                onChangeText={props.handleChange("company_name")}
                                                // onBlur={props.handleBlur("company_name")}
                                                value={props.values.company_name}
                                                placeholder={translate('companyName')}
                                                additionalStyles={styles.fedogolfInputs}
                                                error={props.touched.company_name && props.errors.company_name}
                                                theme={"#00000094"}
                                            />

                                            <FormInput
                                                onChangeText={props.handleChange("job_title")}
                                                // onBlur={props.handleBlur("job_title")}
                                                value={props.values.job_title}
                                                placeholder={translate('jobTitle')}
                                                additionalStyles={styles.fedogolfInputs}
                                                error={props.errors.job_title}
                                                theme={"#00000094"}
                                            />

                                            <FormInput
                                                // key={'association_name'}
                                                onChangeText={(value) => props.setFieldValue('memberType', (value))}
                                                value={props.values.memberType}
                                                type={'picker'}
                                                theme={'light'}
                                                placeholder={translate('membershioType')}
                                                additionalStyles={styles.fedogolfInputs}
                                                data={getValueAndLabelFromArray(this.state.memberType, 'name')}
                                                editable={false}
                                                error={props.errors.memberType}
                                            />

                                            <FormInput
                                                // onChangeText={handleChange("privacyPolicy")}
                                                onValueChange={value => props.setFieldValue('sdcc', value)}
                                                value={props.values.sdcc}
                                                // onBlur={props.handleBlur("sdcc")}
                                                type="switch"
                                                error={props.errors.sdcc}
                                                theme="light"
                                                placeholder={translate('areYouMemberSDCC')}
                                            // action={() => console.log}
                                            />

                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 20, paddingVertical: 15 }}>
                                                <Button
                                                    label={translate('male')}
                                                    theme={"lightblue"}
                                                    additionalStyles={{ ...styles.genderBtn, ...(this.state.values.gender === 1 ? styles.genderActiveBtn : {}) }}
                                                    action={() => this.switchGender('male')}
                                                />
                                                <Button
                                                    label={translate('female')}
                                                    theme={"lightblue"}
                                                    additionalStyles={{ ...styles.genderBtn, ...(this.state.values.gender === 2 ? styles.genderActiveBtn : {}) }}
                                                    action={() => this.switchGender('female')}
                                                />
                                            </View>

                                            {/* Submit Buttons */}
                                            <TouchableOpacity
                                                style={styles.sentRequestCotainer}
                                                // onPress={(actions) => this.payMembership(props.values, actions)}
                                                onPress={() => props.handleSubmit()}
                                            >
                                                <Text style={styles.sentRequestText}>{translate('membershioType')}</Text>
                                            </TouchableOpacity>


                                        </React.Fragment>
                                    )
                                }}
                            </Formik>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFF'
    },
    wrapper: {
        width: "100%",
        marginTop: verticalScale(10),
        paddingTop: verticalScale(10)
    },
    customHeader: {
        width: '100%',
        padding: scale(20)
    },
    customTitle: {
        fontWeight: '600',
        fontSize: moderateScale(16),
        color: COLORS.airBnbGrey,
        marginBottom: verticalScale(5)
    },
    customSubtitle: {
        fontWeight: 'bold',
        fontSize: moderateScale(16),
        color: '#333',
        marginBottom: verticalScale(5)
    },
    horizontalLine: {
        width: '100%',
        height: verticalScale(1),
        backgroundColor: COLORS.airBnbLightGrey,
        marginVertical: verticalScale(10)
    },
    inputContainer: {
        marginLeft: 30,
        marginRight: 30
    },

    backgroundImage: {
        width: 414,
        height: 403,
        flex: 1,
    },

    iconButton: {
        position: 'absolute',
        left: moderateScale(20),
        top: verticalScale(20)
    },
    fedogolfInputs: {
        backgroundColor: 'transparent',
        borderColor: '#8E8E93',
        borderWidth: 1,
        color: 'black',
        fontSize: moderateScale(14),
        textAlign: 'left',
        paddingHorizontal: moderateScale(20),
        height: 52,
        borderRadius: 24,
    },
    genderBtn: {
        width: '45%',
        marginBottom: 20,
        backgroundColor: "transparent",
        borderWidth: 0,
        borderRadius: 10,
        backgroundColor: COLORS.grey,

    },
    genderActiveBtn: {
        backgroundColor: COLORS.lightblue,
    },
    sentRequestCotainer: {
        marginTop: verticalScale(10),
        marginHorizontal: moderateScale(10),
        height: verticalScale(45),
        width: widthPercentageToDP('80%'),
        backgroundColor: COLORS.green,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: verticalScale(30)
    },
    sentRequestText: {
        fontSize: moderateScale(16),
        fontWeight: 'bold',
        color: 'white'
    }
});


function mapStateToProps(state) {
    return {
        session: state.session,
        user: state.session.user.data,
        renewPayment: state.payment
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators({ ...session, ...payment }, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchProps
)(FederationSignUp);


const validationSchema = yup.object().shape({
    firstname: yup.string().required(translate('fieldReq')),
    lastname: yup.string().required(translate('fieldReq')),
    ghin_number: yup.string().matches(/^\d+$/, translate('numericOnly')).min(7).matches(/^\d+$/, translate('charactersLimit7')).nullable(true),
    golf_club_id: yup.string().required(translate('fieldReq')).nullable(true),
    association_id: yup.string().required(translate('fieldReq')).nullable(true),
    league_id: yup.string().required(translate('fieldReq')).nullable(true),
    home_phone: yup.string().matches(/^\d+$/, translate('numericOnly')).nullable(true),
    mobile_phone: yup.string().required(translate('fieldReq')).matches(/^\d+$/, translate('numericOnly')).nullable(true),
    office_phone: yup.string().matches(/^\d+$/, translate('numericOnly')).nullable(true),
    dob: yup.string().required(translate('fieldReq')).nullable(true),
    memberType: yup.string().required(translate('fieldReq')).nullable(true),
    // phone: yup.string().matches(/^\d+$/, 'This field is numeric only').required('This field is required'),
});
