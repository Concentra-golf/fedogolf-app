import React, { Component } from 'react';
import { View, Text, ImageBackground, StyleSheet, Image } from 'react-native';
import TextLabel from '../../../components/UI/TextLabel';
import { verticalScale, moderateScale, widthPercentageToDP, heightPercentageToDP, scale } from '../../../utilities/ScalingScreen';
import { COLORS } from '../../../config/constants';
import QRCode from 'react-native-qrcode-svg';

import * as RNLocalize from 'react-native-localize';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';

class MembershipQR extends Component {
    constructor(props) {
        super(props);
        setI18nConfig();
        this.state = {
        };
    }

    componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);
    }

    render() {
        const { membership } = this.props.route.params;
        return (
            <View style={styles.container}>
                <ImageBackground
                    source={require('../../../assets/images/golf_course01.png')}
                    style={styles.imageBackground}
                >
                    <View style={styles.contentContainer}>

                        <View style={styles.membershipInfo}>
                            <View style={styles.imageContainer}>
                                <Image
                                    style={styles.logo}
                                    resizeMode={'contain'}
                                    source={{ uri: 'https://fedogolf.org.do/wp-content/uploads/2018/03/fedogolf-escudo.jpg' }}
                                />
                            </View>
                            <TextLabel additionalStyles={styles.membershipTitle}>Federacion dominicana de golf</TextLabel>
                        </View>

                        <View style={styles.qrCard}>
                            <TextLabel additionalStyles={styles.title}>{translate('QrCode')}</TextLabel>
                            <TextLabel additionalStyles={styles.subtitle}>{translate('QrCodeSubtitle')}</TextLabel>
                            <QRCode
                                value={membership.pivot.federation_membership_number}
                                size={scale(220)}
                            />
                        </View>

                    </View>
                </ImageBackground>
            </View>
        );
    }
}

export default MembershipQR;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    imageBackground: {
        width: '100%',
        height: '100%'
    },
    contentContainer: {
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(0,0,0,0.1)',
        alignItems: 'center'
    },
    membershipInfo: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: verticalScale(80)
    },
    imageContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
        backgroundColor: 'white',
        height: 100,
        width: 100
    },
    logo: {
        height: 80,
        width: 80,
        borderRadius: 50
    },
    membershipTitle: {
        fontWeight: 'bold',
        fontSize: moderateScale(22),
        color: COLORS.white,
        marginTop: verticalScale(5)
    },
    qrCard: {
        height: heightPercentageToDP('50%'),
        width: widthPercentageToDP('90%'),
        alignItems: 'center',
        backgroundColor: COLORS.white,
        marginTop: verticalScale(30),
        borderRadius: scale(15),
        padding: scale(15)
    },
    title: {
        fontSize: moderateScale(26),
        fontWeight: 'bold',
        marginBottom: verticalScale(5),
        color: COLORS.softBlack
    },
    subtitle: {
        fontSize: moderateScale(18),
        marginBottom: verticalScale(20),
        color: COLORS.softGrey
    }
})