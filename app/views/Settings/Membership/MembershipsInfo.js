import React, { Component } from 'react';
import { View, Text, Modal, StyleSheet, SafeAreaView, TouchableOpacity, ScrollView, Platform, Alert } from 'react-native';
import { verticalScale, moderateScale, scale, widthPercentageToDP } from '../../../utilities/ScalingScreen';
import Ionicons from 'react-native-vector-icons/Ionicons'
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import { COLORS } from '../../../config/constants';

import * as payment from '../../../store/actions/payment';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Moment from 'moment';
import { extendMoment } from 'moment-range';
import UploadDocs from '../../../components/UI/UploadDocs/UploadDocs';
import Button from '../../../components/UI/Button';
import CardPayment from '../../../components/lists/CardPayment/CardPayment';
import PaymentSelector from '../../../components/UI/Modals/PaymentSelector';
const moment = extendMoment(Moment);

import * as RNLocalize from 'react-native-localize';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';

class MembershipsInfo extends Component {
    constructor(props) {
        super(props);
        setI18nConfig(); 
        this.state = {
            renewPayment: {},
            selectOption: false,
            expired: false,
            showButton: true,
            membershipInfo: {},
            isRecurrent: false,
            loading: true,
            showCardsModal: false,
            route: null
        };
    }

    async componentDidMount() {
        RNLocalize.addEventListener('change', handleLocalizationChange);
        this.validateType();
        //VALIDATE IF MEMBERSHIP IS EXPIRED
        if (this.props.route.params.expired) {
            this.setState({
                showButton: false,
                expired: true
            })
        }
    }

    //We validate the subscrition type
    async validateType() {
        const { allInfo, session, user, expired } = this.props.route.params;

        const request = await this.props.recurrentbyMembership(allInfo.id, this.props.session.accessToken);
        const response = this.props.renewPayment.recurrentMembership.data;
        const validateMembershipType = Object.keys(response).length === 0;

        if (!validateMembershipType) {
            console.log('hey');
            this.setState({
                loading: false,
                membershipInfo: this.props.renewPayment.recurrentMembership.data,
                isRecurrent: true
            })
            console.log(this.state.membershipInfo)
        } else {
            this.setState({
                loading: false,
                membershipInfo: {},
                isRecurrent: false
            })
        }
    }

    async requestPayment() {
        const { allInfo, expired } = this.props.route.params;
        const user = this.props.session.user.data;

        const request = await this.props.renewPaymentRequest(user.id, allInfo.id, null, null, this.props.session.accessToken);

        if (this.props.renewPayment.renewPaymentResponse.success) {
            this.setState({
                renewPaymentInfo: this.props.renewPayment.renewPaymentResponse.data.requestPayment,
                selectOption: true,
            })
            this.props.navigation.navigate('OptionsSelector', {
                renewPaymentInfo: this.props.renewPayment.renewPaymentResponse.data.requestPayment,
                data: allInfo,
                route: 'renew'
            })
        } else {
            console.log('error')
        }
    }


    updateSuscription() {
        Alert.alert(
            'Do you want to change your membership to suscription?',
            '',
            [
                { text: 'Yes', onPress: (e) => this.changeSuscription(true) },
                { text: 'Cancel', onPress: () => console.log('hey') },
            ])
    }

    async changeSuscription(e) {
        this.setState({
            showCardsModal: e,
        })
    }

    reloadPage() {
        this.validateType();
    }

    updatePayment() {
        this.setState({
            showCardsModal: true,
            route: 'updatePayment'
        })
    }

    cancelAlert() {
        Alert.alert(
            'Do you want to cancel your membership suscription?',
            '',
            [
                { text: 'Yes', onPress: (e) => this.cancelSubscription(true) },
                { text: 'Cancel', onPress: () => console.log('hey') },
            ])
    }

    async cancelSubscription() {
        const recurrenceId = this.state.membershipInfo.id;
        const request = await this.props.deleteRecurrentMembership(recurrenceId, this.props.session.accessToken);

        if (this.props.renewPayment.deleteRecurrentMembership.success) {
            this.setState({ settingUp: false })
            console.log('success');
            this.reloadPage();
        } else {
            console.log('error')
        }

    }

    render() {
        const { allInfo, session, user, validating, } = this.props.route.params;
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView style={styles.content}>

                    <Text style={styles.title}>{allInfo.name}</Text>
                    <Text style={styles.subtitle}>{translate('membershipInfo')}: </Text>
                    <View style={styles.infoCotainer}>
                        <Text style={styles.dateInfo}>{translate('dateRequested')}: {moment(allInfo.created_at).locale('en').format('DD MMMM YYYY')} </Text>
                        <Text style={styles.dateInfo}>{translate('dueDate')}: {moment(allInfo.users[0].pivot.validity).locale('en').format('DD MMMM YYYY')}</Text>

                        {validating ? (
                            <Text style={styles.dateInfo}>{translate('membershipStatus')} <Text style={[styles.dateInfo, { color: COLORS.green }]}>{translate('ValidationMembership')}</Text></Text>
                        ) : (
                                <Text style={styles.dateInfo}>{translate('membershipStatus')} <Text style={[styles.dateInfo, this.state.expired ? { color: 'red' } : { color: 'green' }]}>{this.state.expired ? translate('expired') : translate('active')}
                                </Text>
                                </Text>
                            )
                        }
                    </View>

                    {this.state.isRecurrent &&
                        <View>
                            <View style={styles.horizontalLine} />
                            <Text style={styles.subtitle}>{translate('billingInfo')}:</Text>
                            <View style={styles.infoCotainer}>
                                <Text style={styles.dateInfo}><Text style={styles.boldTitle}>Subtotal:</Text> $RD {this.state.membershipInfo.amount} </Text>
                                <Text style={styles.dateInfo}><Text style={styles.boldTitle}>{translate('billingType')}:</Text> Subscription</Text>

                                <View style={styles.horizontalLine} />

                                <Text style={styles.subtitle}>{translate('creditCard')}:</Text>
                                <CardPayment
                                    type={this.state.membershipInfo.card.card_brand}
                                    cardNumber={this.state.membershipInfo.card.card_number}
                                    expiration={this.state.membershipInfo.card.expiration}
                                    onPress={() => console.log('hey')}
                                />
                            </View>
                        </View>
                    }

                    {!this.state.isRecurrent &&
                        <TouchableOpacity
                            style={styles.changebuttonContainer}
                            onPress={() => this.updateSuscription()}
                        >
                            <Text style={styles.paymentButtonText}>{translate('changeMembershipType')}</Text>
                        </TouchableOpacity>
                    }

                    {this.state.isRecurrent &&
                        <View style={styles.buttonWrapper}>
                            <TouchableOpacity
                                style={styles.updateButton}
                                onPress={() => this.updatePayment()}
                            >
                                <Text style={styles.textButton}>{translate('updatePay')}</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={styles.cancelButton}
                                onPress={() => this.cancelAlert()}
                            >
                                <Text style={styles.textButton}>{translate('cancelSuscription')}</Text>
                            </TouchableOpacity>
                        </View>
                    }

                </ScrollView>

                {!this.state.showButton &&
                    <TouchableOpacity
                        style={styles.buttonContainer}
                        onPress={() => this.requestPayment()}
                    >
                        <Text style={styles.paymentButtonText}>{translate('renew')}</Text>
                    </TouchableOpacity>
                }

                <Modal
                    visible={this.state.showCardsModal}
                    animationType='slide'
                >
                    <PaymentSelector
                        close={(e) => this.changeSuscription(e)}
                        showCardsModal={this.state.showCardsModal}
                        data={this.props.route.params.allInfo}
                        reloadPage={() => this.reloadPage()}
                        route={this.state.route}
                        membershipInfo={this.state.membershipInfo}
                    />
                </Modal>

            </SafeAreaView>
        );
    }
}

function mapStateToProps(state) {
    return {
        session: state.session,
        user: state.session.user.data,
        renewPayment: state.payment
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators(payment, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchProps
)(MembershipsInfo);


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.white,
    },
    header: {
        width: '100%',
        height: verticalScale(60),
        paddingHorizontal: moderateScale(20),
        paddingVertical: verticalScale(10)
    },
    content: {
        width: '100%',
        height: '100%',
        padding: scale(20)
    },
    title: {
        fontSize: moderateScale(25),
        fontWeight: Platform.OS === 'ios' ? '600' : 'bold',
        color: COLORS.black,
        marginTop: verticalScale(30)
    },
    subtitle: {
        fontSize: moderateScale(18),
        color: COLORS.airBnbGrey,
        marginBottom: verticalScale(10),
        marginTop: verticalScale(20)
    },
    boldTitle: {
        fontWeight: 'bold',
        fontSize: moderateScale(14),
        color: COLORS.softBlack
    },
    infoCotainer: {
        // backgroundColor: 'red'
    },
    dateInfo: {
        marginTop: verticalScale(10),
        fontSize: moderateScale(14)
    },
    horizontalLine: {
        width: '100%',
        borderWidth: 0.5,
        borderColor: COLORS.airBnbLightGrey,
        marginTop: verticalScale(20)
    },
    creditCard: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    creditCardContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: verticalScale(10)
    },
    cardType: {
        width: 50,
        height: 50
    },
    creditCardNumber: {
        fontWeight: '500',
        fontSize: moderateScale(14),
        marginLeft: moderateScale(10),
    },
    cardContainer: {
        width: 70,
        height: 50,
        justifyContent: 'center',
        // alignItems: 'center'
    },
    changebuttonContainer: {
        marginTop: verticalScale(30),
        justifyContent: 'center',
        alignItems: 'center',
        height: verticalScale(50),
        width: '100%',
        backgroundColor: COLORS.darkblue,
    },
    buttonContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        height: verticalScale(50),
        width: '100%',
        backgroundColor: COLORS.red,
    },
    paymentButtonText: {
        fontSize: moderateScale(16),
        color: 'white',
        fontWeight: 'bold'
    },
    buttonWrapper: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: verticalScale(20)
    },
    updateButton: {
        width: widthPercentageToDP('40%'),
        height: verticalScale(50),
        backgroundColor: COLORS.darkblue,
        justifyContent: 'center',
        alignItems: 'center'
    },
    cancelButton: {
        width: widthPercentageToDP('40%'),
        height: verticalScale(50),
        backgroundColor: COLORS.red,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textButton: {
        fontSize: moderateScale(14),
        color: COLORS.white,
        fontWeight: "bold"
    }
})