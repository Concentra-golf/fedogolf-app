import React, { Component } from 'react';
import { View, Text, StyleSheet, } from 'react-native';

// Modules

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as session from '../../../../store/actions/session';
import SettingsUserCard from '../../../../components/cards/SettingsUserCard/SettingsUserCard';
import SettingsButton from '../../../../components/UI/SettingsButton';

class ConfigurationScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={styles.container}>
                <SettingsUserCard
                    user={this.props.session.user}
                />

                <SettingsButton
                    label="Payments Method"
                    icon="payments"
                    action={() => this.props.navigation.navigate('MyCards', {
                        user: this.props.session.user
                    })}
                />

                {/* <SettingsButton
                    label="Language"
                    icon="notifications"
                    action={() => this.props.navigation.navigate('LanguageSelector')}
                /> */}

                <SettingsButton
                    label="Notifications"
                    icon="notifications"
                    action={() => console.log('hey')}
                />

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})

function mapStateToProps(state) {
    return {
        session: state.session,
        user: state.session.user.data
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators(session, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchProps,
)(ConfigurationScreen);

