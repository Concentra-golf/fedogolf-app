import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { moderateScale, scale, heightPercentageToDP, verticalScale } from '../../../../utilities/ScalingScreen';
import { setI18nConfig } from '../../../../translations/translation';


class LanguageSelector extends Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultLanguage: "es"
    };
    this.onLanguageSelected = this.onLanguageSelected.bind(this);
  }

  onLanguageSelected(e) {
    const getValue = setI18nConfig(e, false);
    console.log(getValue);
  }

  render() {
    return (
      <View style={styles.container}>

        {language.map((item, index) =>
          <TouchableOpacity
            key={index}
            style={styles.textContainer}
            onPress={() => this.onLanguageSelected(item.value)}
          >
            <Text style={styles.languageText}>{item.language}</Text>
          </TouchableOpacity>
        )}

      </View>
    );
  }
}

export default LanguageSelector;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: scale(10)
  },
  languageText: {
    fontSize: moderateScale(17),
    textAlign: 'left'
  },
  textContainer: {
    width: '100%',
    paddingBottom: verticalScale(10),
    borderBottomWidth: 0.5,
    marginVertical: verticalScale(10),
    justifyContent: 'center'
  }
})

const language = [
  { key: '1', language: 'English', value: 'en' },
  { key: '2', language: 'Español', value: 'es' },
]