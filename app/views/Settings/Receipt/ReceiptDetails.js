import React, { Component } from 'react';
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity } from 'react-native';
import { COLORS } from '../../../config/constants';

import { moderateScale, verticalScale, scale } from '../../../utilities/ScalingScreen';
import Moment from 'moment';
import TextLabel from '../../../components/UI/TextLabel';

import * as payment from '../../../store/actions/payment';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import RNFS from 'react-native-fs';
import FileViewer from 'react-native-file-viewer';

class ReceiptDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            url: ''
        };
    }

    downloadPDF() {
        const doc = 'document.pdf'
        const localFile = `${RNFS.DocumentDirectoryPath}/document.pdf`;
        this.setState({ loadingDocument: true })
        const transaction_id = this.props.route.params.data.transaction_id;
        const options = {
            fromUrl: `http://api.golfertek.com/pdf/${transaction_id}`,
            toFile: localFile
        };

        RNFS.downloadFile(options).promise
            .then((e) => {
                this.setState({ loadingDocument: false })
                FileViewer.open(localFile)

            }).catch(error => {
                this.setState({
                    loadingDocument: false
                })
            });
    }

    render() {
        const { data } = this.props.route.params;
        return (
            <SafeAreaView style={styles.container}>

                <View style={styles.priceContainer}>
                    <TextLabel additionalStyles={styles.priceTitle}>Receipt Total</TextLabel>
                    <Text style={styles.price}>{data.currency}${data.amount}</Text>

                    <View style={styles.horizontalLine} />
                    <View style={styles.dateContainer}>
                        <TextLabel additionalStyles={styles.dateTitle}>Date Requested</TextLabel>
                        <Text style={styles.date}>{Moment(data.created_at).format('L')}</Text>
                    </View>
                </View>

                <View style={styles.content}>
                    <TextLabel additionalStyles={styles.itemTitle}>ITEMS</TextLabel>
                    <View style={styles.contentHorizontalLine} />

                    <View style={styles.wrapper}>
                        <View style={styles.federationType}>
                            <Text style={styles.itemName}>{data.membership.name}</Text>
                            <Text style={styles.itemDes}>Fees Type: {data.flow_type}</Text>
                        </View>
                        <Text style={styles.itemPrice} >{data.currency}${data.amount}</Text>
                    </View>

                    <View style={styles.contentHorizontalLine} />

                    <View style={styles.totalPage}>
                        <View style={styles.wrapper}>
                            <Text style={styles.totalTitle}>Subtotal</Text>
                            <Text style={styles.totalAmount}>{data.currency}${data.amount}</Text>
                        </View>
                        <View style={styles.wrapper}>
                            <Text style={styles.totalTitle}>Total</Text>
                            <Text style={styles.totalAmount}>{data.currency}${data.amount}</Text>
                        </View>
                    </View>
                </View>

                <View style={styles.buttonContainer}>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => this.downloadPDF()}
                    >
                        <TextLabel additionalStyles={styles.buttonText}>Download PDF</TextLabel>
                    </TouchableOpacity>
                </View>

            </SafeAreaView>
        );
    }
}

function mapStateToProps(state) {
    return {
        session: state.session,
        user: state.session.user.data,
        payment: state.payment
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators(payment, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchProps
)(ReceiptDetails);


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.white,
        alignItems: 'center'
    },
    priceContainer: {
        width: '100%',
        height: '40%',
        backgroundColor: COLORS.darkblue,
        justifyContent: 'center',
        alignItems: 'center'
    },
    priceTitle: {
        fontSize: moderateScale(16),
        marginTop: verticalScale(-20),
        color: 'rgba(250,250,250,0.9)',
        marginVertical: verticalScale(5)
    },
    price: {
        fontSize: moderateScale(26),
        fontWeight: 'bold',
        color: COLORS.white,
    },
    horizontalLine: {
        width: '90%',
        height: verticalScale(0.5),
        backgroundColor: 'rgba(250,250,250,0.8)',
        marginVertical: verticalScale(20)
    },
    dateContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '90%'
    },
    dateTitle: {
        color: 'rgba(250,250,250,0.9)',
    },
    date: {
        fontSize: moderateScale(14),
        color: COLORS.white,
        fontWeight: 'bold'
    },
    content: {
        width: '95%',
        // height: verticalScale(100),
        backgroundColor: COLORS.white,
        borderRadius: scale(5),
        marginTop: verticalScale(-40),
        padding: scale(15),
        shadowOpacity: 0.25,
        shadowOffset: { width: 1, height: 3 },
        elevation: 10,
    },
    itemTitle: {
        marginBottom: verticalScale(5),
        color: COLORS.softBlack
    },
    contentHorizontalLine: {
        width: '100%',
        height: verticalScale(0.3),
        backgroundColor: "rgba(188, 190, 192, 0.5)",
    },
    wrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: verticalScale(10)
    },
    itemName: {
        fontSize: moderateScale(16),
        fontWeight: 'bold',
        color: COLORS.softBlack,
        marginVertical: verticalScale(5)
    },
    itemDes: {
        fontSize: moderateScale(12),
        fontWeight: '500',
        color: COLORS.softBlack,
    },
    itemPrice: {
        fontSize: moderateScale(16),
        fontWeight: 'bold',
        color: COLORS.softBlack
    },
    totalPage: {
        width: '100%'
    },
    totalTitle: {
        fontSize: moderateScale(16),
        fontWeight: '600',
        color: COLORS.softGrey
    },
    totalAmount: {
        fontSize: moderateScale(14),
        fontWeight: 'bold',
        color: COLORS.softGrey
    },
    buttonContainer: {
        width: '100%',
        height: verticalScale(100),
        position: 'absolute',
        bottom: 0,
        padding: scale(20),
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        width: '100%',
        height: verticalScale(45),
        backgroundColor: COLORS.darkblue,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText: {
        color: COLORS.white
    }
})