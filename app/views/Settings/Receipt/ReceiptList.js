import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar, TouchableOpacity, FlatList } from 'react-native';
import HeaderImageScrollView, { TriggeringView } from 'react-native-image-header-scroll-view';
import Ionicons from 'react-native-vector-icons/Ionicons'

import { verticalScale, moderateScale, scale } from '../../../utilities/ScalingScreen';
import FixedForeground from '../../../components/UI/HeaderButtons/ImageHeader/FixedForeGround';
import RenderForeground from '../../../components/UI/HeaderButtons/ImageHeader/RenderForeground';
import ReceiptCards from '../../../components/cards/ReceiptCards/ReceiptCards';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';


import * as payment from '../../../store/actions/payment';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Loader from '../../../components/UI/Loader/Loader';

class ReceiptList extends Component {
  constructor(props) {
    super(props);
    setI18nConfig(); 
    this.state = {
      receiptList: []
    };
  }

  async componentDidMount() {
    RNLocalize.addEventListener('change', handleLocalizationChange);
    this.loadData();
  }


  async loadData() {
    request = await this.props.getAllReceipt(this.props.session.user.data.id, this.props.session.accessToken);
    if (this.props.payment.receiptList.success) {
      this.setState({
        receiptList: this.props.payment.receiptList.data
      })
    } else {
      this.setState({
        receiptList: []
      })
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" />
        <HeaderImageScrollView
          maxHeight={MAX_HEIGHT}
          minHeight={MIN_HEIGHT}
          maxOverlayOpacity={0.3}
          minOverlayOpacity={0.2}
          fadeOutForeground
          headerImage={require('../../../assets/images/scroll-header-bg.png')}
          renderFixedForeground={() => <FixedForeground title={translate('receiptTitle')} navigation={this.props.navigation} />}
        >
          <TriggeringView style={styles.section}>
            
            {!this.state.loading && this.state.receiptList.length !== 0 &&
              <FlatList
                data={this.state.receiptList}
                keyExtractor={(item, index) => index.toString()}
                contentContainerStyle={{ paddingBottom: verticalScale(30) }}
                renderItem={({ item }) =>
                  <ReceiptCards
                    data={item}
                    navigation={this.props.navigation}
                  />
                }
              />
            }


          </TriggeringView>
        </HeaderImageScrollView>

        {!this.state.loading && this.state.receiptList.length === 0 &&
          <View style={styles.loaderContainer}>
            <Loader
              nothing={true}
              nothingMessage={translate('noReceipt')}
            />
          </View>
        }

      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
    user: state.session.user.data,
    payment: state.payment
  };
}

function mapDispatchProps(dispatch) {
  return bindActionCreators(payment, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchProps
)(ReceiptList);



const MIN_HEIGHT = verticalScale(100);
const MAX_HEIGHT = moderateScale(220);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  headerContainer: {
    height: verticalScale(100),
    justifyContent: "center",
    alignItems: "center",
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: moderateScale(20),
    paddingVertical: verticalScale(10)
  },
  imageTitle: {
    color: 'white',
    fontSize: moderateScale(24),
    marginLeft: moderateScale(20),
    marginTop: verticalScale(20),
    fontWeight: 'bold'
  },
  section: {
    // height: 1000,
    padding: scale(10),
    borderBottomColor: '#cccccc',
    backgroundColor: 'white',
  },
  headerContainer: {
    height: verticalScale(100),
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor: 'red',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: moderateScale(20),
    paddingVertical: verticalScale(10)
  },
  headerBackButton: {
    height: 40,
    width: 40,
    backgroundColor: 'white',
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: verticalScale(15)
  },
  loaderContainer: {
    position: 'absolute',
    bottom: 0,
    top: 0,
    left: 0,
    alignSelf: 'center',
    width: '100%'
  }
})