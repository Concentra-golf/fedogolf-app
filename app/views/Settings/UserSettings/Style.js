import { StyleSheet } from "react-native";

import { COLORS } from "../../../config/constants";

export default StyleSheet.create({
    background: {
        flex: 1
    },
    buttonsContainer: {
        // width: 414
    },
    buttons: {
        borderBottomWidth: 1,
        borderBottomColor: "#D6D6D6",
        borderRadius: 0,
        backgroundColor: "#FFFFFF",
        width: 414, height: 60
    },
    headerInner: {
        alignItems: 'center',
    },
})