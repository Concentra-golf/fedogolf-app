import React, { Component } from 'react';
import { Text, View, ImageBackground, Image, StatusBar, I18nManager, ScrollView } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

// Modules

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import i18n from 'i18n-js';
import memoize from 'lodash.memoize';


import * as session from "../../../store/actions/session";
import OneSignal from "react-native-onesignal";

// Images
import styles from './Style'
import SettingsButton from '../../../components/UI/SettingsButton';
import SettingsUserCard from '../../../components/cards/SettingsUserCard/SettingsUserCard';
import { mainPop } from '../../../utilities/NavigationService';

import * as RNLocalize from 'react-native-localize';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../translations/translation';

class Settings extends Component {
  constructor(props) {
    super(props);
    setI18nConfig();
    this.state = {
      fedogolf: {},
      hasFedogolf: false
    };
  }

  componentDidMount() {
    RNLocalize.addEventListener('change', handleLocalizationChange);
    const fedogolf = this.props.session.user.data.federations.find(x => x.id === 1)
    console.log(fedogolf)
    if (fedogolf !== {} && fedogolf !== undefined) {
      this.setState({
        fedogolf: fedogolf,
        hasFedogolf: true
      })
    } else {
      this.setState({
        fedogolf: {},
        hasFedogolf: false
      })
    }
  }

  componentWillUnmount() {
    RNLocalize.removeEventListener('change', handleLocalizationChange);
  }

  render() {
    const { user, signOut, updateProfile } = this.props
    return (
      <View >
        <StatusBar barStyle='light-content' />
        <SettingsUserCard user={this.props.session.user} />

        <ScrollView style={styles.buttonsContainer}>

          <SettingsButton
            icon={"altUser"}
            label={translate('account')}
            // label={"Account"}
            additionalStyles={styles.btns}
            action={() =>
              this.props.navigation.navigate("AccountOptions", {
                user
              })
            }
          />

          {/* <SettingsButton
            label={"Stats"}
            theme={"settingsButton"}
            action={() => console.log("hey")}
            icon={"stats"}
            additionalStyles={styles.btns}
          /> */}

          <SettingsButton
            // label={"Manage Request"}
            label={translate('request')}
            theme={"settingsButton"}
            action={() => this.props.navigation.navigate('ManageRequest', {
              user
            })}
            icon={'membership'}
          />

          <SettingsButton
            // label="My Benefits"
            label={translate('myBenefits')}
            icon="benefits"
            action={() => this.props.navigation.navigate('BenefitsList', {
              user
            })}
          />

          {this.state.fedogolf && this.state.hasFedogolf &&
            <SettingsButton
              label="Fedogolf QR"
              icon="qrCode"
              action={() => this.props.navigation.navigate('MembershipQR', {
                user,
                membership: this.state.fedogolf
              })}
            />
          }

          <SettingsButton
            label={translate('membership')}
            theme={"settingsButton"}
            action={() => this.props.navigation.navigate('MembershipOptions', {
              user
            })}
            icon={'membership'}
          />

          <SettingsButton
            // label={"Configuration"}
            label={translate('Configuration')}
            theme={"settingsButton"}
            action={() => this.props.navigation.navigate('Configuration')}
            icon={'configuration'}
            additionalStyles={styles.btns}
          />

          <SettingsButton
            // label={"Sign Out"}
            theme={"settingsButton"}
            label={translate('signOut')}
            additionalStyles={styles.btns}
            action={() => {
              AsyncStorage.removeItem('userInfo');
              OneSignal.deleteTag("email");
              signOut();
              mainPop("UserAccess");
            }}
            icon={"signOut"}
          />

        </ScrollView>
      </View >
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
    user: state.session.user.data
  };
}

function mapDispatchProps(dispatch) {
  return bindActionCreators(session, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchProps
)(Settings);
