

import React, { Component } from 'react';
import { Text, View } from 'react-native';
import SettingsUserCard from '../../../../components/cards/SettingsUserCard/SettingsUserCard'
// import PersonalInformationForm from '../../../../components/forms/PersonalInformation/PersonalInformationFormonForm';
import { ScrollView } from 'react-native-gesture-handler';

// Modules

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as session from '../../../../store/actions/session';

class GolferInformation extends Component {

  componentDidMount() {
    console.log('props from the golfer informaiton', this.props)
  }

  profileView = user => {
    return [
      {
        title: 'Handicap',
        value: 'handicap'
      },
      {
        title: 'Ghin Number',
        value: 'ghin_number'
      },
    ].map((item, index) =>
      <View 
      key={index}
      style={{
        marginTop: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#D8D8D8',
        paddingVertical: 10,
        width: '90%',
        flexDirection: "row",
        justifyContent: "space-between"
      }}>
        <Text style={{ fontSize: 14 }}>{item.title}</Text>
        <Text style={{ fontSize: 16, fontWeight: "bold" }}>{user[item.value] || 'None'}</Text>
      </View>)
  }
  render() {
    const { user, session } = this.props.route.params

    console.log('user from golferInformation', user)

    return (
      <ScrollView>
        <View>
          <SettingsUserCard editBtn={() => this.props.navigation.navigate('GIEdit', {
            user, session
          })} 
          user={this.props.session.user.data} 
          />

          {/* <PersonalInformationForm user={user} /> */}
          <View style={{ alignItems: "center" }}>
            {this.props.session && this.props.session.user && this.props.session.user.data && (
              this.profileView(this.props.session.user.data)
            )}

          </View>
        </View>
      </ScrollView>
    );
  }
}



function mapStateToProps(state) {
  return {
    session: state.session,
    user: state.session.user.data
  };
}

function mapDispatchProps(dispatch) {
  return bindActionCreators(session, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchProps,
)(GolferInformation);

