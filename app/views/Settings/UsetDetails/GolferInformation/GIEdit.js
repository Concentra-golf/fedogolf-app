import React, { Component } from 'react';
import { Text, View } from 'react-native';
import SettingsUserCard from '../../../../components/cards/SettingsUserCard/SettingsUserCard'
import GolferInformationForm from '../../../../components/forms/GolferInformation/GolferInformationForm';
import { ScrollView } from 'react-native-gesture-handler';

// Modules

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as session from '../../../../store/actions/session';


class GIEdit extends Component {
  render() {
    const { user, navigation } = this.props.route.params;
    const { updateProfile, session } = this.props

    return (
      <ScrollView>
        <View >
          <SettingsUserCard editing user={user} />

          <GolferInformationForm
            isLoading={session.user.isLoading}
            navigation={navigation}
            user={user}
            session={session}
            updateProfile={updateProfile}
          />
        </View>
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
    user: state.session.user.data
  };
}

function mapDispatchProps(dispatch) {
  return bindActionCreators(session, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchProps,
)(GIEdit);

