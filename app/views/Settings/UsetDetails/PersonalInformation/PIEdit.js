import React, { Component } from 'react';
import { Text, View } from 'react-native';
import SettingsUserCard from '../../../../components/cards/SettingsUserCard/SettingsUserCard'
import PersonalInformationForm from '../../../../components/forms/PersonalInformation/PersonalInformationForm';
import { ScrollView } from 'react-native-gesture-handler';

import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../../translations/translation';
import * as RNLocalize from 'react-native-localize';

// Modules

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as session from '../../../../store/actions/session';


class PIEdit extends Component {

  componentDidMount() {
    RNLocalize.removeEventListener('change', handleLocalizationChange);
  }

  render() {
    const { user, navigation } = this.props.route.params;
    const { updateProfile, session } = this.props
    return (
      <ScrollView>
        <View >
          <SettingsUserCard editing user={user} />
          <PersonalInformationForm
            isLoading={session.user.isLoading}
            navigation={navigation}
            user={user}
            session={session}
            updateProfile={updateProfile}
          // session={session}
          />
        </View>
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
    user: state.session.user.data
  };
}

function mapDispatchProps(dispatch) {
  return bindActionCreators(session, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchProps,
)(PIEdit);

