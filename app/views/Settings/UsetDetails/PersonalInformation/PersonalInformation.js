import React, { Component } from 'react';
import { Text, View } from 'react-native';
import SettingsUserCard from '../../../../components/cards/SettingsUserCard/SettingsUserCard'


// import PersonalInformationForm from '../../../../components/forms/PersonalInformation/PersonalInformationFormonForm';
import { ScrollView } from 'react-native-gesture-handler';
import { translationGetters, translate, setI18nConfig, handleLocalizationChange } from '../../../../translations/translation';
import * as RNLocalize from 'react-native-localize';

// Modules

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as session from '../../../../store/actions/session';

class PersonalInformation extends Component {
  constructor(props) {
    super(props);
    setI18nConfig(); 
    this.state = {
    };
  }

  componentDidMount() {
    RNLocalize.removeEventListener('change', handleLocalizationChange);
  }

  profileView = user => {
    return [
      {
        title: translate('phone'),
        value: 'phone'
      },
      {
        title:  translate('email'),
        value: 'email'
      },
      {
        title:  translate('birthday'),
        value: 'dob'
      },
    ].map(item =>
      <View style={{
        marginTop: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#D8D8D8',
        paddingVertical: 10,
        width: '90%',
        flexDirection: "row",
        justifyContent: "space-between"
      }}>
        <Text style={{ fontSize: 14 }}>{item.title}</Text>
        <Text style={{ fontSize: 16, fontWeight: "bold" }}>{user[item.value] || 'None'}</Text>
      </View>)
  }
  render() {
    const { user } = this.props.route.params
    return (
      <ScrollView>
        <View>
          <SettingsUserCard
            editBtn={() => this.props.navigation.navigate('PIEdit', {
              user
            })}
            user={this.props.session && this.props.session.user && this.props.session.user.data && user} />
          <View style={{ alignItems: "center" }}>
            {this.props.session && this.props.session.user && this.props.session.user.data && (
              this.profileView(this.props.session.user.data)
            )}
          </View>
        </View>
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
    user: state.session.user.data
  };
}

function mapDispatchProps(dispatch) {
  return bindActionCreators(session, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchProps,
)(PersonalInformation);

