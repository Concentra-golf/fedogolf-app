import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TextInput,
    ScrollView,
    Dimensions,
    TouchableOpacity,
    Image,
    FlatList,
    ActivityIndicator
} from 'react-native';
import { CreditCardInput } from "react-native-credit-card-input";

import { Icon } from 'native-base';
import { scale, verticalScale, moderateScale } from '../../../utilities/ScalingScreen';
import { COLORS } from '../../../config/constants';

export default class CreditCardPayment extends Component {

    constructor(props) {
        super(props);
        this.state = {
            payService: false,
            step: 1
        };
        this.processPayment = this.processPayment.bind(this)
    }

    processPayment() {
        this.setState({ loading: true })
        setTimeout(() => {
            this.setState({
                loading: false,
                step: 2
            })
        }, 2000)
    }


    onClick() {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Service' })],
        });
        this.props.navigation.dispatch(resetAction);
        this.props.navigation.navigate('ServiceBuyed')
    }


    getCreditCard(e) {
        console.log(e)
    }


    renderScreen() {
        if (this.state.step === 1) {
            return (
                <ScrollView style={styles.container}>

                    {!this.state.loading &&
                        <View style={styles.priceContainer}>
                            <Text style={styles.totalTitle}>Total price</Text>
                            <Text style={styles.totalText}>RD$ 5,000.00 </Text>
                        </View>
                    }

                    {!this.state.loading &&
                        <View style={styles.cardInfoContainer}>

                            <CreditCardInput
                                onChange={this.getCreditCard}
                                requiresName
                                labelStyle={{
                                    color: COLORS.airBnbGrey,
                                }}
                                inputStyle={{
                                    borderWidth: 1,
                                    borderRadius: 10,
                                    paddingHorizontal: moderateScale(10),
                                    marginTop: moderateScale(10),
                                    height: verticalScale(40)
                                }}
                                inputContainerStyle={{


                                }}
                            />

                        </View>
                    }

                    {!this.state.loading &&
                        <View style={styles.buttonContainer}>
                            <TouchableOpacity
                                onPress={() => this.processPayment()}
                                style={styles.payButton}
                            >
                                <Text style={styles.payText}>Proceed to confirm</Text>
                            </TouchableOpacity>
                        </View>
                    }
                </ScrollView>
            )
        } else if (this.state.step === 2) {
            return (
                <View style={styles.paymentSucces}>

                    <View style={styles.cardReceipt}>

                        <Icon name='checkmark-circle' style={styles.succesIcon} />
                        <Text style={styles.subTitle}>Payment Succesfull.</Text>
                        <Text style={styles.totalText}>$5,000.00 </Text>
                        <Text style={styles.subMsg}>The receipt has been sent to you via email.</Text>

                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => this.props.navigation.goBack()}
                        >
                            <Text style={styles.textButton}>Go to Request List</Text>
                        </TouchableOpacity>

                    </View>
                </View>
            )
        }
    }

    render() {
        return (
            <View style={styles.container}>
                    {this.state.loading &&
                        <View style={styles.loading}>
                            <ActivityIndicator size='large' />
                            <Text style={styles.text}>Procesando Pago</Text>
                        </View>
                    }

                    {this.renderScreen()}

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    loading: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    priceContainer: {
        // marginTop: verticalScale(40),
        marginLeft: moderateScale(20),
        backgroundColor: '#FFF',
        justifyContent: 'center',
        height: verticalScale(120)
    },
    totalTitle: {
        fontSize: moderateScale(17),
        marginBottom: verticalScale(5),
        fontWeight: '500',
        color: COLORS.airBnbGrey
    },
    totalText: {
        fontSize: moderateScale(34),
        fontWeight: 'bold',
        color: COLORS.lightblue
    },
    cardInfoContainer: {
        width: '100%',
        marginTop: verticalScale(-15),
    },
    buttonContainer: {
        backgroundColor: '#FFF',
        width: '100%',
        height:  Platform.OS === 'ios' ? verticalScale(60) : verticalScale(70),
        marginTop: verticalScale(10),
        marginBottom: Platform.OS === 'ios' ? verticalScale(20) : verticalScale(30),
        justifyContent: 'center',
        alignItems: 'center'
    },
    payButton: {
        width: moderateScale(300),
        height: verticalScale(40),
        backgroundColor: COLORS.lightblue,
        borderRadius: scale(50),
        justifyContent: 'center',
        alignItems: 'center'
    },
    payText: {
        fontSize: moderateScale(14),
        color: '#fff',
        fontWeight: 'bold'
    },
    paymentSucces: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    cardReceipt: {
        padding: scale(15),
        width: moderateScale(330),
        backgroundColor: '#FFF',
        borderRadius: scale(10),
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#333333',
        shadowColor: '#333333',
        shadowOpacity: 0.25,
        shadowOffset: { width: 5, height: 5 },
        elevation: 10,
    },
    succesIcon: {
        fontSize: moderateScale(100),
        color: '#3bb000'
    },
    subTitle: {
        fontSize: moderateScale(16),
        margin: scale(10),
        color: COLORS.airBnbGrey
    },
    subMsg: {
        fontSize: moderateScale(14),
        margin: scale(10),
        textAlign: 'center',
        color: COLORS.airBnbGrey
    },
    button: {
        width: moderateScale(250),
        height: verticalScale(40),
        backgroundColor: COLORS.lightblue,
        borderRadius: scale(50),
        justifyContent: 'center',
        alignItems: 'center',
    },
    textButton: {
        fontSize: moderateScale(14),
        fontWeight: '500',
        color: '#FFF'
    },
    text: {
        fontSize: moderateScale(18),
        margin: scale(20)
    }
});
