import React, { Component } from 'react';
import { View, Text, StatusBar, StyleSheet, FlatList, Modal, TouchableOpacity } from 'react-native';
import HeaderImageScrollView, { TriggeringView } from 'react-native-image-header-scroll-view';

import { verticalScale, moderateScale, scale } from '../../../utilities/ScalingScreen';
import FixedForeground from '../../../components/UI/HeaderButtons/ImageHeader/FixedForeGround';
import CreditCardModal from '../../../components/UI/Payment/CreditCardModal';

import * as payment from '../../../store/actions/payment';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import CardPayment from '../../../components/lists/CardPayment/CardPayment';
import Loader from '../../../components/UI/Loader/Loader';
import { COLORS } from '../../../config/constants';
import MaterialCommunityIcons from "react-native-vector-icons/dist/MaterialCommunityIcons";
import CreditCardsDetails from '../../../components/UI/Payment/CreditCardsDetails';

class MyCards extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cardList: [],
            loading: true,
            creditCardModal: false,
            addingCard: false,
            showDetail: false,
            cardData: null
        };
    }

    componentDidMount() {
        this.getMyCard()
    }

    async getMyCard() {
        const userid = this.props.session.user.data.id;
        const request = await this.props.getUserPayments(userid, this.props.session.accessToken);

        if (this.props.payment.userPaymentCards.success) {
            console.log(this.props.payment.userPaymentCards.data.data);
            this.setState({
                cardList: this.props.payment.userPaymentCards.data.data,
                loading: false
            })
        } else {
            this.setState({
                cardList: [],
                loading: false,
                error: true,
            })
        }
    }

    addCreditCardModal(visible) {
        this.setState({ creditCardModal: visible })
    }

    //We receive the new credit card values
    async getCreditCardValues(e) {
        if (e !== {}) {
            this.setState({ addingCard: true })

            //We set the card values to AZUL Structure
            const creditCard = e.number.replace(/ /g, ''); //we eliminate the spaces of the cards

            const expirationMonth = e.expiry.substring(0, 2)
            const expirationYear = e.expiry.substring(3)
            const setExpDate = '20' + expirationYear + expirationMonth; // we create the AZUL format for the dates (202012)

            const newCreditCard = {
                cardNumber: creditCard,
                cvc: e.cvc,
                expiration: setExpDate
            }

            const request = await this.props.saveUserCard(newCreditCard, this.props.session.accessToken);

            if (this.props.payment.saveUserCards.success) {
                this.setState({ addingCard: false })
                this.getMyCard();
                this.addCreditCardModal(false)
            } else {
                console.log('Error adding the card')
            }
        }
    }

    showDetail(e, data) {
        this.setState({
            showDetail: e,
            cardData: data
        })
    }

    async deletePayment(e) {
        const cardId = e.id
        await this.props.deleteUserCard(cardId, this.props.session.accessToken);
        if (this.props.payment.deletedUserPayment.success) {
            this.showDetail(false)
            this.getMyCard();
        } else {
            console.log('Error adding the card')
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar barStyle="light-content" />
                <HeaderImageScrollView
                    maxHeight={MAX_HEIGHT}
                    minHeight={MIN_HEIGHT}
                    maxOverlayOpacity={0.3}
                    minOverlayOpacity={0.2}
                    fadeOutForeground
                    headerImage={require('../../../assets/images/scroll-header-bg.png')}
                    renderFixedForeground={() => <FixedForeground title={'My Payments'} navigation={this.props.navigation} />}
                >

                    {this.state.loading &&
                        <View style={{ marginTop: verticalScale(30) }}>
                            <Loader
                                loadingMessage={'Loading Cards'}
                            />
                        </View>
                    }

                    {!this.state.loading &&
                        <TriggeringView style={styles.section}>
                            <FlatList
                                data={this.state.cardList}
                                keyExtractor={(item, index) => index.toString()}
                                contentContainerStyle={{ paddingBottom: verticalScale(30) }}
                                renderItem={({ item }) =>
                                    <CardPayment
                                        type={item.card_brand}
                                        cardNumber={item.card_number}
                                        expiration={item.expiration}
                                        onPress={() => this.showDetail(true, item)}
                                    />
                                }
                            />
                            <TouchableOpacity
                                style={styles.addNewCreditCard}
                                onPress={() => this.addCreditCardModal(true)}
                            >
                                <MaterialCommunityIcons
                                    name='credit-card'
                                    size={20}
                                    style={{ color: COLORS.red }}
                                />
                                <Text style={styles.addNewCreditCardText}>Add new credit card</Text>
                            </TouchableOpacity>
                        </TriggeringView>
                    }
                </HeaderImageScrollView>

                <Modal
                    visible={this.state.creditCardModal}
                    animationType='slide'
                >
                    <CreditCardModal
                        close={(e) => this.addCreditCardModal(e)}
                        getCreditCardValues={(e) => this.getCreditCardValues(e)}
                        route={'addCard'}
                        addingCard={this.state.addingCard}
                    />
                </Modal>

                <Modal
                    visible={this.state.showDetail}
                    animationType='slide'
                >
                    <CreditCardsDetails
                        showDetail={this.state.showDetail}
                        close={(e) => this.showDetail(e)}
                        cardData={this.state.cardData}
                        deletePayment={(e) => this.deletePayment(e)}
                    />
                </Modal>

            </View>
        );
    }
}


function mapStateToProps(state) {
    return {
        session: state.session,
        user: state.session.user.data,
        payment: state.payment
    };
}

function mapDispatchProps(dispatch) {
    return bindActionCreators(payment, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchProps
)(MyCards);

const MIN_HEIGHT = verticalScale(100);
const MAX_HEIGHT = moderateScale(220);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    section: {
        // height: 1000,
        padding: scale(10),
        borderBottomColor: '#cccccc',
        backgroundColor: 'white',
    },
    addNewCreditCard: {
        flexDirection: 'row',
        width: '100%',
        height: verticalScale(30),
        alignItems: 'center'
    },
    addNewCreditCardText: {
        fontSize: moderateScale(14),
        color: COLORS.red,
        marginLeft: moderateScale(5)
    },
})