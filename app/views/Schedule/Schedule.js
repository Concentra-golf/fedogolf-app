import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';

// Components

import CoursesList from '../../components/lists/CoursesList/CoursesList';
import SectionHeading from '../../components/UI/SectionHeading';
import SearchBar from '../../components/forms/SearchBar/SearchBar';
import ViewHeader from '../../components/UI/ViewHeader/ViewHeader';
import Loader from '../../components/UI/Loader/Loader';

const data = [
  // { id: 1, course_name: "La Romana Golf Club", date: "23/03/2020", price: "$281.00", time: "2:24 PM", image: "https://i.imgur.com/gWiz8LR.jpg", holes: 18 },
  // { id: 2, course_name: "Santo Domingo Golf Club", date: "23/03/2020", price: "$281.00", time: "2:24 PM", image: null, holes: 18 },
  // { id: 3, course_name: "Punta Cana Golf Club", date: "23/03/2020", price: "$281.00", time: "2:24 PM", image: null, holes: 18 },
]

export default class Schedule extends Component {

  // Show single course detail

  showCourseDetail = (item) => {

    this.props.navigation.navigate('CourseDetail', { courseId: item.id });

  }

  render() {
    const { navigation } = this.props;

    return (
      <View style={styles.container}>

        {/* Header & Courses Search */}

        <ViewHeader>

          <SearchBar
            navigation={navigation}
            data={data}
            placeholder={"Search courses near me"}
            isButton={true}
            autoFocus={true}
          />

        </ViewHeader>

        {/* Courses List */}

        <SectionHeading transform={"uppercase"} align={"center"}>Next Courses</SectionHeading>

        {data.length > 0 &&
          <CoursesList
            data={data}
            action={this.showCourseDetail}
            showSchedule={true}
          />
        }
        
        {data.length === 0 &&
          <Loader
            nothing={true}
            nothingMessage={'No games scheduled'}
          />
        }


      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  }
})
