import { StyleSheet } from 'react-native';

import { COLORS } from '../../config/constants';

export default StyleSheet.create({
  wrapper: {
    backgroundColor: COLORS.white,
  },
  signOutButton: {
    marginBottom: 10,
    backgroundColor: COLORS.lightblue,
    color: "#fff"
  },
});
