import * as React from 'react';
import { View, ScrollView, Text, Alert, } from 'react-native';
import ViewHeader from '../../components/UI/ViewHeader/ViewHeader';
import LottieView from 'lottie-react-native';
import AsyncStorage from '@react-native-community/async-storage';
// Modules

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as session from '../../store/actions/session';
import OneSignal from 'react-native-onesignal';

// Components

import UserScoreGraph from '../../components/cards/UserScoreGraph/UserScoreGraph';
import UserScoreAverage from '../../components/cards/UserScoreAverage/UserScoreAverage';
import Card from '../../components/cards/Card/Card';
import StatsEntries from '../../components/lists/StatsEntries/StatsEntries';

// Styles

import styles from './Styles';
import Button from '../../components/UI/Button';
import { mainPop } from '../../utilities/NavigationService';

// Sample Data

const sampleData = [50, 40, 40, 95, 0, 85, 91, 35, 53, 0, 50, 20];

const data = {
  handicap: 0,
  avg: 67,
  rounds: 20
}

const sampleStats = [
  { name: 'Fairways Hit', percentage: 90 },
  { name: 'Fairways Hit', percentage: 90 },
  { name: 'Fairways Hit', percentage: 90 }
]

class Home extends React.Component {
  constructor(props) {
    super(props);
  }


  async componentDidMount() {
    OneSignal.sendTag('email', `${this.props.session.user.data.email}`);
    
    const oldToken = this.props.session.accessToken;
    await this.props.refreshToken(oldToken);
    
    if(this.props.session.user && this.props.session.user.tokenRefreshed) {
      console.log('token refreshed')
    } else {
      OneSignal.deleteTag("email");
      this.props.signOut();
      mainPop('UserAccess')
    }


  }


  render() {
    const { user, signOut } = this.props
    return (
      <ScrollView>
        <View style={styles.wrapper}>

          {/* Header & User Average Scores */}

          <ViewHeader additionalStyles={{ marginBottom: 110 }}>

            <UserScoreAverage data={data} user={user} />

          </ViewHeader>


          {/* User Score Graph */}

          <Card>

            <UserScoreGraph data={sampleData} />

          </Card>


          {/* Stats Card 1 */}

          <Card title={"My Stats"}>

            <StatsEntries data={sampleStats} action={() => console.log('works')} />

          </Card>


          {/* Stats Card 2 */}

          <Card title={"My Stats 2"}>

            <StatsEntries data={sampleStats} action={() => console.log('works')} />

          </Card>

        </View>
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
    user: state.session.user.data
  };
}

function mapDispatchProps(dispatch) {
  return bindActionCreators(session, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchProps,
)(Home);

