// API Root URL

export const API_URL = "http://api.golfertek.com/api";

// Facebook SDK Permissions

export const FACEBOOK_LOGIN_PERMISSIONS = ['public_profile', 'email'];
export const FACEBOOK_LOGIN_PERMISSIONS_REQUEST = '/me?fields=short_name,first_name,last_name,name,email';

//GENDERS

export const GENDERS = [
  { value: 'male', label: 'Male' },
  { value: 'female', label: 'Female' },
];

// Colors

export const COLORS = {
  red: "#eb2929",
  black: "rgb(0, 0, 0)",
  darkgrey: "rgb(44, 42, 42)",
  grey: "rgb(128, 130, 133)",
  lightgrey: "rgb(188, 190, 192)",
  white: "rgb(255, 255, 255)",
  darkgreen: "#233504",
  logoGreen: '#598D1F',
  midgreen: "#83A356",
  green: "#2EB673",
  lightblue: "#0086AD",
  lightGreen: "rgba(87,116,55,0.5)",
  darkblue: '#191E46',
  facebookBlue: "#3B5998",
  danger: "red",
  airBnbGrey: '#484848',
  airBnbLightGrey: '#767676',
  softBlack: '#5B5A62',
  lightBlack: '#333',
  softGrey: '#7A869A'
};

export const privacyPolitics = 'Este GOLFERTEK garantiza que la información personal que usted envía cuenta con la seguridad necesaria. Los datos ingresados por usuario o en el caso de requerir una validación de los pedidos no serán entregados a terceros, salvo que deba ser revelada en cumplimiento a una orden judicial o requerimientos legales. La suscripción a boletines de correos electrónicos publicitarios es voluntaria y podría ser seleccionada al momento de crear su cuenta. Al asociarse a una federación, asociación o liga sus datos pueden estar siendo compartidas con las mismas. Innovix reserva los derechos de cambiar o de modificar estos términos sin previo aviso.'