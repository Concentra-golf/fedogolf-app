import axios from "axios";
import { API_URL } from "../constants";

export default class AboutService {
  static getAllFederations(access_token) {
    const url = `${API_URL}/user/information`;

    return axios.get(url, {
      headers: {
        Authorization: `Bearer ${access_token}`
      }
    });
  }

  static getPrivacyPolicy(access_token) {
    return axios.get(`${API_URL}/bo/about/privacypolicies`, {
      headers: {
        Authorization: `Bearer ${access_token}`
      }
    });
  }

  static getTermsOfUse(access_token) {
    return axios.get(`${API_URL}/bo/about/termsofuse`, {
      headers: {
        Authorization: `Bearer ${access_token}`
      }
    });
  }

  static getGeneralInformation(access_token) {
    return axios.get(`${API_URL}/bo/about/generalinformation`, {
      headers: {
        Authorization: `Bearer ${access_token}`
      }
    });
  }
}
