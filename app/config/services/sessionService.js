import axios from 'axios'
import { API_URL } from '../constants';

export default class SessionService {
    static signup(data) {
        const url = `${API_URL}/register`

        return axios.post(url, data)
    }

    static signupGolferInformation(data) {
        const url = `${API_URL}/register`

        return axios.post(url, data)
    }

    static signIn(data) {
        const url = `${API_URL}/login`

        return axios.post(url, data)
    }

    static facebookSignIn(data) {
        const url = `${API_URL}/facebook_register`

        return axios.post(url, data)
    }

    static resetPassword(data) {
        const url = `${API_URL}/reset_email`

        return axios.post(url, data)
    }

    static sendResetPasswordWithCode(data) {
        const url = `${API_URL}/verify_code`

        return axios.post(url, data)
    }

    static sendResetPasswordWithNewPassword(data) {
        const url = `${API_URL}/reset_password`

        return axios.post(url, data)
    }

    static signUpGolferInformation(id, token, data) {
        const url = `${API_URL}/user/${id}`

        return axios.post(url, data, {
            headers: { Authorization: "Bearer " + token }
        })
    }

    static updateProfile(id, token, data) {

        const url = `${API_URL}/user/${id}`

        return axios.put(url, data, {
            headers: { Authorization: "Bearer " + token }
        })
    }
    static fedogolfSignUp(token, data) {

        const url = `${API_URL}/fedogolf_application/create`

        return axios.post(url, data, {
            headers: { Authorization: "Bearer " + token }
        })
    }
    static fedogolfValidation(data, token) {
        const url = `http://api.golfertek.com/api/join/membership`
        return axios({
            method: "POST",
            url: url,
            data: data,
            headers: {
                Authorization: "Bearer " + token,
            },
        })
    }

    static getAllBenefits(userID, token) {
        const url = `${API_URL}/user/${userID}/benefits`
        return axios.get(url, {
            headers: {
                Authorization: "Bearer " + token,
            },
        })
    }
    static getAllFederations(token) {
        const url = `${API_URL}/federations/client`
        return axios.get(url, {
            headers: {
                Authorization: "Bearer " + token,
            },
        })
    }

    static getBenefitsByFederation(fedID, token) {
        const url = `http://api.golfertek.com/api/federations/${fedID}`
        return axios({
            method: "POST",
            url: url,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    static refreshToken(token) {
        const url = `${API_URL}/resetToken`
        return axios({
            method: "POST",
            url: 'http://api.golfertek.com/api/resetToken',
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    static getAllCourses(token) {
        return axios({
            method: "GET",
            url: 'http://api.golfertek.com/api/golfcourses',
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    static getCoursesByLocation(location, token) {
        return axios({
            method: "POST",
            url: 'http://api.golfertek.com/api/golfclubs/nearest',
            data: location,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    static getCourseById(course_id, token) {
        return axios({
            method: "GET",
            url: `${API_URL}/golfcourses/${course_id}`,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    static getRequests(token) {
        return axios({
            method: "GET",
            url: 'http://api.golfertek.com/api/applications/indexClient',
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    static getRequestsById(application_id, token) {
        return axios({
            method: "GET",
            url: `http://api.golfertek.com/api/applications/${application_id}`,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    static attachDocumentToRequest(golf_application_id, file, token) {
        const data = new FormData();
        data.append('file', file)
        // data.append('amount', amount,)
        return axios({
            method: "POST",
            url: `http://api.golfertek.com/api/applications/upload/${golf_application_id}`,
            data: data,
            headers: {
                Authorization: "Bearer " + token,
                'Accept': 'image/*',
                'Content-Type': 'multipart/form-data'
            },
        });
    }

    static renewPaymentRequest(userId, membershipId, flow_type, request_payment_id, token) {
        const url = `http://api.golfertek.com/api/membership/renewRequest/${userId}/${membershipId}`
        return axios({
            method: "POST",
            url: `http://api.golfertek.com/api/membership/renewRequest/${userId}/${membershipId}`,
            data: { flow_type, request_payment_id },
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    //ENDPOINT TO PAY With AZUL

    static simulatePayment(requestPayment_id, application_id, paymentInfo, token) {
        const data = new FormData();
        const pay = paymentInfo;
        data.append('amount', paymentInfo.amount)
        data.append('type', paymentInfo.type)
        data.append('reason', paymentInfo.reason)
        data.append('cardNumber', paymentInfo.cardNumber)
        data.append('card_token', paymentInfo.card_token)
        data.append('expiration', paymentInfo.expiration),
            data.append('paymentType', paymentInfo.paymentType),
            data.append('cvc', paymentInfo.cvc)
        data.append('file', paymentInfo.file)
        data.append('request_application', application_id)
        data.append('from', 'mobile')
        return axios({
            method: "POST",
            url: `http://api.golfertek.com/api/payment/redeem/${requestPayment_id}`,
            data: data,
            headers: {
                Authorization: "Bearer " + token,
                'Content-Type': 'multipart/form-data'
            },
        });
    }

    //ENDPOINT TO CONFIGURE RECURRENT PAYMENT

    static recurrentPayment(data, token) {
        return axios({
            method: "POST",
            url: `${API_URL}/recurrent/create`,
            data: data,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }


    /*ENDPOINT TO CONFIGURE RECURRENT PAYMENT By Membership =================================================================================================== */

    /**
      * @description ENDPOINT TO CONFIGURE RECURRENT PAYMENT By Membership 
      * @returns membership billing info.
    */

    static recurrentbyMembership(membership_id, token) {
        return axios({
            method: "GET",
            url: `${API_URL}/recurrent/byMembership/${membership_id}`,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    /* ENDPOINT TO CONFIGURE a non recurrent membership recurrent =================================================================================================== */

    /**
      * @description ENDPOINT TO CONFIGURE a non recurrent membership recurrent

    */

    static setRecurrentMembership(data, token) {
        return axios({
            method: "POST",
            url: `${API_URL}/recurrent/create`,
            data: data,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    /* ENDPOINT TO UPDATE a recurrent membership =================================================================================================== */

    static updateRecurrentMembership(id_recurrencia, data, token) {
        return axios({
            method: "POST",
            url: `${API_URL}/recurrent/update/${id_recurrencia}`,
            data: data,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    /* ENDPOINT TO DELETE a recurrent membership =================================================================================================== */

    static deleteRecurrentMembership(id_recurrencia, token) {
        return axios({
            method: "POST",
            url: `${API_URL}/recurrent/delete/${id_recurrencia}`,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    static getMembershipsFees(membership_id, token) {
        return axios({
            method: "GET",
            url: `http://api.golfertek.com/api/memberships/index/${membership_id}`,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    static getMembershipsTypes(federation_id, token) {
        return axios({
            method: "GET",
            url: `http://api.golfertek.com/api/memberships/byEntity/${federation_id}`,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    static getMembershipsTypesById(membership_id, token) {
        return axios({
            method: "GET",
            url: `http://api.golfertek.com/api/memberships/rates/${membership_id}`,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    static getAllReceipt(user_id, token) {
        return axios({
            method: "GET",
            url: `http://api.golfertek.com/api/payment/index/${user_id}`,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    static getPdfReceipt(transaction_id, token) {
        return axios({
            method: "GET",
            url: `http://api.golfertek.com/api/payment/pdf/${transaction_id}`,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    static getCourseRatings(course_id, token) {
        return axios({
            method: "GET",
            url: `${API_URL}/golfcourses/ratings/${course_id}`,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    static getUserPayments(user_id, token) {
        return axios({
            method: "GET",
            url: `${API_URL}/payment/cards/${user_id}`,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    static saveUserCard(data, token) {
        return axios({
            method: "POST",
            url: `${API_URL}/payment/card`,
            data: data,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    static deleteUserCard(card_id, token) {
        return axios({
            method: "POST",
            url: `${API_URL}/payment/cards/delete/${card_id}`,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    /* ENDPOINT TO Get the country =================================================================================================== */

    static getCountry(token) {
        return axios({
            method: "GET",
            url: `${API_URL}/golfclubs/filter/country`,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    static getFilterbyCity(country, token) {
        return axios({
            method: "GET",
            url: `${API_URL}/golfclubs/filter/city/${country}`,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    static getFilter(data, token) {
        return axios({
            method: "POST",
            url: `${API_URL}/golfclubs/filter`,
            data: data,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }


    /* ENDPOINT TO Search player by email =================================================================================================== */

    static searchPlayerByEmail(playerMail, token) {
        return axios({
            method: "GET",
            url: `${API_URL}/game/byEmail?search=${playerMail}`,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    /* ENDPOINT TO get frecuent playets =================================================================================================== */

    static getFrequenlyPlayer(user_id, token) {
        return axios({
            method: "GET",
            url: `${API_URL}/game/frequently/${user_id}`,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    /* ENDPOINT TO CREATE NEW GAME =================================================================================================== */

    static createGolfGame(course_id, data, token) {
        return axios({
            method: "POST",
            url: `${API_URL}/game/${course_id}`,
            data: data,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    /* ENDPOINT TO GET THE OPEN PLAYS =================================================================================================== */

    static getOpenPlays(token) {
        return axios({
            method: "GET",
            url: `${API_URL}/game?open=0`,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    /* ENDPOINT TO GET THE OPEN PLAYS BY ID =================================================================================================== */

    static getOpenPlaysById(id_game, token) {
        return axios({
            method: "GET",
            url: `${API_URL}/game/${id_game}`,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    /* ENDPOINT POST GAME =================================================================================================== */

    static postGame(data, game_id, competitor_id, token) {
        return axios({
            method: "POST",
            url: `${API_URL}/game/post/${game_id}/${competitor_id}`,
            data: data,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    /* ENDPOINT END OPEN GAME =================================================================================================== */

    static endGame(game_id, token) {
        return axios({
            method: "POST",
            url: `${API_URL}/game/end/${game_id}`,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    /* ENDPOINT CONTINUE GAME =================================================================================================== */

    static continueGame(game_id, token) {
        return axios({
            method: "GET",
            url: `${API_URL}/game/${game_id}`,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    /* ENDPOINT DELETE OPEN GAME =================================================================================================== */

    static deleteOpenGame(game_id, token) {
        return axios({
            method: "POST",
            url: `${API_URL}/game/delete/${game_id}`,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }

    /* ENDPOINT RATING GAME =================================================================================================== */

    static ratingGame(data, golf_course_id, token) {
        return axios({
            method: "POST",
            url: `${API_URL}/golfcourses/rating/${golf_course_id}`,
            data: data,
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }


}