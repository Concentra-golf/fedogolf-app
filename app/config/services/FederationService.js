import axios from 'axios'
import { API_URL } from '../constants';

export default class FederationService {
    static getAllFederations(access_token) {
        const url = `${API_URL}/user/information`

        return axios.get(url, {
            headers: {
                "Authorization": `Bearer ${access_token}`
            }
        })
    }


}