import axios from 'axios'
import { API_URL } from '../constants';

export default class SessionService {

    static fedogolfSignUp(token, data) {
        const url = `${API_URL}/fedogolf_application/create`

        return axios.post(url, data, {
            headers: { Authorization: "Bearer " + token }
        })
    }

    static editFedogolfSignUp(token, aplicationId, data) {
        const url = `${API_URL}/fedogolf_application/update/${aplicationId}`

        return axios.post(url, data, {
            headers: { Authorization: "Bearer " + token }
        })
    }
}