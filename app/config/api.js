// Define the API root url

import { API_URL } from './constants';

// Define API headers

export const requestMethod = {
  get: 'GET',
  post: 'POST',
};

export const setApiHeaders = (method, token) => {
  const config = {
    method: method,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: token ? 'Bearer ' + token : null,
    },
  };

  return config;
};

// Define the API routes

const API = {
  // Session routes

  session: {
    // User sign up
    signUp: API_URL + '/register',

    // User sign in
    signIn: API_URL + '/login',

    // User sign out
    signOut: API_URL + '/SignOut',

    // Request Password Reset Code
    forgotPassword: API_URL + '/ForgotPassword',

    // Reset Password
    resetPassword: API_URL + '/ResetPassword',

    // fedogolf get
    fedogolf: API_URL + '/fedogolf_application/create',
    
    
    // fedogolf get
    // fedogolfUser: API_URL + '/fedogolf_users',
  },

  // Courses routes

  courses: {
    // Courses list
    getCourses: API_URL + '/golfcourses',

    // Course Detail
    getCourseDetail: function (id) {
      return API_URL + '/getCourseDetail/' + id;
    },
  },

  // Schedules routes

  schedules: {
    // Schedules list
    getSchedules: API_URL + '/getSchedules',
  },

  // Settings routes

  settings: {
    // Stores list
    getSettings: API_URL + '/getSettings',
  },

  // Stats routes

  stats: {
    // Get User Notifications
    getStats: API_URL + '/getStats',
  },

  // Benefits routes

  benefits: {

    getAllBenefits: function (userId) {
      return API_URL + '/user/' + userId + '/benefits';
    },
  },

  refreshToken: {
    refreshToken: API_URL + '/resetToken',
  }

};

export default API;
